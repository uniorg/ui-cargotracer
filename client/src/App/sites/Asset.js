import React, { Component } from 'react';
import { Breadcrumb, Button, Col, Container, Row } from "react-bootstrap";
import Loader from "react-loader-spinner";
import withQueryParams from "react-router-query-params";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Form from "react-bootstrap/es/Form";
import { Link } from "react-router-dom";
import { createSorter } from "../util/Sort";
import queryString from "query-string";
import ReactPullToRefresh from "react-pull-to-refresh";
import ModuleSearch from "../component/ModuleSearch";
import ModuleFilter from "../component/ModuleFilter";
import ModuleSort from "../component/ModuleSort";
import InfiniteScroll from "react-infinite-scroll-component";
import AssetListItem from "../component/AssetListItem";
import AssetTypeListItem from "../component/AssetTypeListItem";
import i18n from "i18next";
import $ from "jquery";
import Map from '../services/locationLeaFlet';
import moment from 'moment';

class Asset extends Component {

  assetMap = null
  _isMounted = false
  devicesOnMap: []

  constructor( props ) {
    super( props );
    this.state = {
      width: 0,
      height: 0,

      list: [],
      count: 0,
      hasMoreItems: true,
      nextUrlQuery: null,

      filters: this.props.filters,
      sorters: this.props.sorters,
      search: "*",
      groupByFilter: "",
      checkFilter: "",
      searchInputValue: "",
      sortKey: {label: i18n.t( 'sites.Asset.sortByOptions.item1' ), value: "cycleCount"},
      sortDirection: "",
      groupByMode: "assetTypes",
      selectedAssetType: null
    };

  }

  static getGroupByButtons() {
    let groupButtons = [];
    groupButtons.push( {label: i18n.t( 'sites.Asset.groupByFilter.item1' ), groupby: "asset"} )
    groupButtons.push( {label: i18n.t( 'sites.Asset.groupByFilter.item2' ), groupby: "assetTypes"} )

    return groupButtons
  }

  static getSortByOptions() {
    let sortOptions = [];
    sortOptions.push( {label: i18n.t( 'sites.Asset.sortByOptions.item1' ), value: "cycleCount"} )
    sortOptions.push( {label: i18n.t( 'sites.Asset.sortByOptions.item2' ), value: "lastSeenAt"} )
    sortOptions.push( {label: i18n.t( 'sites.Asset.sortByOptions.item3' ), value: "vibrationCount"} )

    return sortOptions
  }

  getSortItem( searchValue ) {
    let sortOption = Asset.getSortByOptions().filter( ( item ) => {
      return item.value === searchValue;

    } )

    if ( sortOption[0] !== undefined )
      return sortOption[0]

    return Asset.getSortByOptions()[0]
  }

  static renderLoading() {
    return <><Row><Col md={"12"} className={"d-flex justify-content-center mt-5"}><Loader type="Oval" color="#CDDC4B" height={70} width={70}/></Col></Row></>;
  }

  static renderLoadingFinish( t ) {
    return <><Row><Col md={"12"} className={"d-flex flex-column mt-3"}>
      <div className="alert alert-primary" role="alert">
        {t( "sites.Asset.noMoreContentInfo" )}
      </div>
    </Col></Row></>;
  }

  static renderError() {
    return <><Row><Col md={"12"} className={"d-flex flex-column mt-3"}>
      <div className={"alert alert-danger"} role={"alert"}>There was a technical problem. Please contact our <a href="/" className={"alert-link"}>Support Team</a>. Or just reload and try it again.</div>
    </Col></Row></>;
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  async componentDidMount() {
    this._isMounted = true;
    await this.setUrlStates();
    await this.fetchAssets()
  }

  componentDidUpdate() {
    let that = this
    if ( !this.props.isMobile ) {
      if ( this.assetMap !== undefined && this.assetMap !== null ) {
        this.assetMap.map.remove();
        $( "#assetMap" ).html( "" );
      }
      this.assetMap = new Map( {startLatitude: '50.8038972', startLongitude: '7.1723139', startZoom: '5', containerId: 'assetMap', mapType: 'openStreet'} );
      this.assetMap.startMap( () => {
        that.setMarker()
      } );
    }
  }

  async setUrlStates() {
    const values = queryString.parse( this.props.location.search )
    if ( values.search !== undefined ) {
      await this.setState( {search: values.search} );
    }
    if ( values.groupByFilter !== undefined ) {
      await this.setState( {groupByFilter: values.groupByFilter} );
    }
    if ( values.checkFilter !== undefined ) {
      await this.setState( {checkFilter: values.checkFilter} );
    }
    if ( values.sortKey !== undefined && values.sortDirection !== undefined ) {
      await this.setState( {sortKey: this.getSortItem( values.sortKey ), sortDirection: values.sortDirection} );
    }
    if ( values.groupBy !== undefined ) {
      await this.setState( {groupByMode: values.groupBy} );
    }
    if ( values.assetTypeId !== undefined ) {
      await this.setState( {assetTypeId: values.assetTypeId} );
    }
    if ( values.assetTypeName !== undefined ) {
      await this.setState( {assetTypeName: values.assetTypeName} );
    }
  }

  setSearchQueryOrderBy( queryString ) {
    const {sortKey, sortDirection} = this.state;
    if ( sortKey !== undefined && sortKey !== "" && sortDirection !== "" ) {
      if (sortKey.value !== undefined) {
        queryString = queryString + "&orderby=" + sortKey.value + " " + sortDirection
      } else {
        queryString = queryString + "&orderby=" + sortKey + " " + sortDirection
      }
    }
    return queryString
  }

  fetchAssets( compleation ) {
    const {search, checkFilter, nextUrlQuery, groupByMode, assetTypeId} = this.state;
    let queryString = ""
    if ( search !== "" ) {
      queryString = "search=" + search
    }
    if ( groupByMode === "asset" ) {
      if ( checkFilter !== "" ) {
        if ( assetTypeId !== undefined ) {
          queryString = queryString + ((search !== "") ? "&" : "") + "filter=" + checkFilter + " and assetTypeId eq " + assetTypeId + "&skip=0&top=10"
        } else {
          queryString = queryString + ((search !== "") ? "&" : "") + "filter=" + checkFilter + "&skip=0&top=10"
        }
      } else {
        queryString = queryString + "filter=assetTypeId eq " + assetTypeId + "&skip=0&top=10"
      }
      queryString = this.setSearchQueryOrderBy( queryString )
    } else {
      queryString = queryString + "&skip=0&top=10"
    }
    if ( nextUrlQuery ) {
      queryString = nextUrlQuery;
    }
    fetch( '/azureSearch/' + groupByMode + '/services.svc?' + queryString )
      .then( res => res.json() )
      .then( ( data ) => {
        this.onLoad( data )
        if ( compleation !== undefined ) {
          compleation();
        }
        this.setMarker()
      } )
  }

  onLoad = ( data ) => {
    const {list} = this.state

    if ( this._isMounted ) {
      if ( data.value !== undefined && data.value.length === 0 ) {
        this.setState( {
                         hasMoreItems: false,
                         nextUrlQuery: null,
                         count: 0
                       } );
      }
      if ( list.length < data.count ) {
        let returnList = list
        let listFinish = this.parseData( data.value !== undefined ? data.value : data )

        returnList = returnList.concat( listFinish )
        this.setDevicesOnMap( data.value !== undefined ? data.value : data )
        this.setState( {
                         list: returnList,
                         count: data.count,
                         nextUrlQuery: data.nextUrlQuery !== undefined ? data.nextUrlQuery : null
                       } );
        let that = this
        setTimeout( () => {
          if ( that.state.list.length === data.count ) {
            that.setState( {
                             hasMoreItems: false,
                             nextUrlQuery: null,
                             count: data.count
                           } );
          }
        }, 500 )
      } else {
        this.setState( {
                         hasMoreItems: false,
                         nextUrlQuery: null
                       } );
      }
    }
  }

  setDevicesOnMap( assets ) {
    /**
     * @search.score: 1
     assetState: "AT_HOME"
     assetStateTimestamp: "2019-03-13T07:09:29Z"
     assetTypeId: 131
     assetTypeName: "Achsgestelle"
     averageAtHomeTimeSeconds: 1301468
     averageAtPoiTimeSeconds: 0
     averageCycleTimeSeconds: 1933449
     averageOnTheWayTimeSeconds: 0
     constructionYear: null
     cycleCount: 6
     deleted: false
     description: ""
     deviceId: "385568"
     id: "1189"
     keyLabel: "304"
     lastMaintenance: null
     lastSeenAt: "2019-07-30T11:29:11Z"
     lastSeenBatteryVoltage: null
     lastValidGeoPositionLatitude: 50.94578385708739
     lastValidGeoPositionLongitude: 7.573459073790862
     lastValidGeoPositionRadius: 51118
     lastValidGeoPositionSentAt: "2019-07-30T11:29:11Z"
     lastValidGeoPositionSpeed: null
     lastValidGeoPositionTemperature: null
     lastValidGeoPositionType: "SIGFOX"
     lastValidGeoPositionVibrationCount: 0
     lastValidGpsPositionLatitude: null
     lastValidGpsPositionLongitude: null
     lastValidGpsPositionSentAt: null
     name: "304"
     version: null
     vibrationCount: 398
     */
    let devicesMarker = assets.map( ( asset ) => {
      let {lastValidGeoPositionLatitude, lastValidGeoPositionLongitude, name, deviceId, assetTypeName, lastValidGeoPositionType, id, lastValidGeoPositionSentAt} = asset
      let color = ((lastValidGeoPositionType === "SIGFOX") ? "#000000" : "#0d6e96")
      return {
        lat: lastValidGeoPositionLatitude,
        lng: lastValidGeoPositionLongitude,
        title: name,
        circleRadius: undefined,
        popup: "<div class='row m-3'>" +
               "<div class='col-12'><a href='/assets/" + id + "'><h5>" + name + "</h5></a> </div>" +
               "<div class='col-12'>Asset Typ: " + assetTypeName + "</div>" +
               "<div class='col-12'>Device Id: " + deviceId + "</div>" +
               "<div class='col-12'>Typ: " + lastValidGeoPositionType + "</div>" +
               "<div class='col-12'>SentAt: " + moment( new Date( lastValidGeoPositionSentAt ) ).format( 'LLL' ) + "</div>" +
               "</div>",
        zIndex: 1000,
        icon: {
          google: "BpwPalletMarker",
          leaflet: "<i class=\"fas fa-map-marker-alt\" style='font-size: 1.3rem;color: " + color + ";position:  relative;top: -12px;left: -2px;'></i>"
        }
      }
    } )

    if ( this.devicesOnMap === undefined )
      this.devicesOnMap = []
    this.devicesOnMap = this.devicesOnMap.concat( devicesMarker )

    this.setMarker()
  }

  setMarker() {
    if ( this.devicesOnMap !== undefined && this.devicesOnMap.length > 0 ) {
      const markerIconsStart = {
        google: "BpwPalletMarker",
        leaflet: "<i class=\"fas fa-map-marker-alt\" style='font-size: 1.5rem;color: #129fd8;position:  relative;top: -12px;left: -2px;'></i>"
      };
      this.assetMap.removeMarker( "AssetMarker" )
      this.assetMap.setMarkers( this.devicesOnMap, "AssetMarker", markerIconsStart, false, true, false );
      this.assetMap.setNowMarkerCenter( "AssetMarker", false );
    }
  }

  handleRefresh( resolve, reject ) {
    this.setState( {
                     list: [],
                     hasMoreItems: true,
                     nextUrlQuery: null
                   } );
    this.fetchAssets( () => {
      resolve()
    } )
  }

  async handleReloadList() {
    this.devicesOnMap = []
    await this.setState( {
                           list: [],
                           hasMoreItems: true,
                           nextUrlQuery: null
                         } );
    await this.fetchAssets()
  }

  parseData( data ) {
    const {sorters} = this.state;

    if ( data && data.length && Array.isArray( sorters ) && sorters.length ) {
      data.sort( createSorter( ...sorters ) );
    }

    return data;
  }

  async handelFilterItems( filterString ) {
    let {groupByMode} = this.state
    await this.setState( {
                           list: [],
                           hasMoreItems: true,
                           nextUrlQuery: null,
                           checkFilter: filterString
                         } )
    const values = queryString.parse( this.props.location.search )
    values.checkFilter = filterString
    values.groupBy = groupByMode
    const {history, location} = this.props;
    history.push( {
                    pathname: location.pathname,
                    search: "?" + queryString.stringify( values ),
                    hash: "",
                    state: {fromDashboard: false}
                  } )

    this.devicesOnMap = []
    this.fetchAssets()
  }

  async handleChangeGroupByMode( mode ) {
    await this.setState( {
                           list: [],
                           hasMoreItems: true,
                           nextUrlQuery: null,
                           groupByMode: mode
                         } )
    const values = queryString.parse( this.props.location.search )
    values.groupBy = mode
    const {history, location} = this.props;
    history.push( {
                    pathname: location.pathname,
                    search: "?" + queryString.stringify( values ),
                    hash: "",
                    state: {fromDashboard: false}
                  } )

    this.devicesOnMap = []
    this.fetchAssets()
  }

  async handleChangeGroupByModeToAssetType() {
    await this.setState( {
                           list: [],
                           hasMoreItems: true,
                           nextUrlQuery: null,
                           groupByMode: "assetTypes"
                         } )
    const values = queryString.parse( this.props.location.search )
    values.groupBy = "assetTypes"
    const {history, location} = this.props;
    history.push( {
                    pathname: location.pathname,
                    search: "?" + queryString.stringify( values ),
                    hash: "",
                    state: {fromDashboard: false}
                  } )

    this.devicesOnMap = []
    this.fetchAssets()
  }

  async handelSortItems( sortKey, sortDirection ) {
    await this.setState( {
                           list: [],
                           hasMoreItems: true,
                           nextUrlQuery: null,
                           sortKey: sortKey,
                           sortDirection: sortDirection
                         } )
    const values = queryString.parse( this.props.location.search )

    if (sortKey !== undefined && sortKey.value !== undefined) {
      values.sortKey = sortKey.value
    } else {
      values.sortKey = sortKey
    }
    values.sortDirection = sortDirection
    const {history, location} = this.props;
    history.push( {
                    pathname: location.pathname,
                    search: "?" + queryString.stringify( values ),
                    hash: "",
                    state: {fromDashboard: false}
                  } )

    this.devicesOnMap = []
    this.fetchAssets()
  }

  handleSortChange( item ) {
    this.setState( {
                     sortKey: item
                   } )
  }

  handleSortDirectionChange( direction ) {

    this.setState( {
                     sortDirection: direction
                   } )
  }

  getCheckByElements() {
    const values = queryString.parse( this.props.location.search )
    let filter
    if ( values.checkFilter !== undefined ) {
      filter = values.checkFilter
    }
    let checkBoxes = []
    checkBoxes.push( {value: 1, label: i18n.t( 'sites.Asset.characteristicsFilter.item1' ), operator: "OR", where: "assetState eq 'ON_THE_WAY'", checked: (filter !== undefined && filter.indexOf( "assetState eq 'ON_THE_WAY'" ) > -1)} )
    checkBoxes.push( {value: 2, label: i18n.t( 'sites.Asset.characteristicsFilter.item2' ), operator: "OR", where: "assetState eq 'AT_POI'", checked: (filter !== undefined && filter.indexOf( "assetState eq 'AT_POI'" ) > -1)} )
    checkBoxes.push( {value: 3, label: i18n.t( 'sites.Asset.characteristicsFilter.item3' ), operator: "OR", where: "assetState eq 'AT_HOME'", checked: (filter !== undefined && filter.indexOf( "assetState eq 'AT_HOME'" ) > -1)} )

    return checkBoxes
  }

  async handleClickSearchItem( e, item, search ) {
    await this.setState( {
                           list: [],
                           hasMoreItems: true,
                           nextUrlQuery: null,
                           search: search
                         } )
    await this.fetchAssets()
    const values = queryString.parse( this.props.location.search )
    if ( item !== undefined ) {
      values.search = item.deliveryNumber
    } else {
      values.search = search
    }
    const {history, location} = this.props;
    this.devicesOnMap = []
    history.push( {
                    pathname: location.pathname,
                    search: "?" + queryString.stringify( values ),
                    hash: "",
                    state: {fromDashboard: false}
                  } )
  }

  async handleOnClickRemoveSearch() {
    await this.setState( {
                           list: [],
                           hasMoreItems: true,
                           nextUrlQuery: null,
                           search: "*"
                         } )
    await this.fetchAssets()
    const values = queryString.parse( this.props.location.search )
    values.search = ""
    this.devicesOnMap = []
    const {history, location} = this.props;
    history.push( {
                    pathname: location.pathname,
                    search: "?" + queryString.stringify( values ),
                    hash: "",
                    state: {fromDashboard: false}
                  } )
  }

  handleOnClickRemoveFilter() {
    const {history, location} = this.props;
    history.push( {
                    pathname: location.pathname
                  } )
    this.devicesOnMap = []
    this.fetchAssets()
  }

  async handleOnClickRemovSort() {
    let sortKey = "cycleCount"
    let sortDirection = "desc"
    await this.setState( {
                           list: [],
                           hasMoreItems: true,
                           nextUrlQuery: null,
                           sortKey: this.getSortItem( sortKey ),
                           sortDirection: sortDirection
                         } )
    const values = queryString.parse( this.props.location.search )


    values.sortKey = sortKey
    values.sortDirection = sortDirection
    const {history, location} = this.props;
    history.push( {
                    pathname: location.pathname,
                    search: "?" + queryString.stringify( values ),
                    hash: "",
                    state: {fromDashboard: false}
                  } )

    this.devicesOnMap = []
    this.fetchAssets()
  }

  async handleShowAssets( assetType ) {
    await this.setState( {
                           list: [],
                           hasMoreItems: true,
                           nextUrlQuery: null,
                           assetTypeId: assetType.id,
                           assetTypeName: assetType.name,
                           groupByMode: "asset"
                         } )
    await this.fetchAssets()
    const values = queryString.parse( this.props.location.search )

    values.assetTypeId = assetType.id
    values.assetTypeName = assetType.name
    values.groupBy = "asset"
    const {history, location} = this.props;
    this.devicesOnMap = []
    history.push( {
                    pathname: location.pathname,
                    search: "?" + queryString.stringify( values ),
                    hash: "",
                    state: {fromDashboard: false}
                  } )
  }

  render() {
    const {list} = this.state;

    return list ?
      this.renderData( list ) :
      Asset.renderLoading();
  }

  renderData( data ) {
    const {hasMoreItems, list, count, sortKey, sortDirection, assetTypeName} = this.state;
    const {t, location} = this.props;
    const values = queryString.parse( location.search )
    return (
      <div className={"site-delivery"}>
        <Container>
          <>
            <Row>
              <ReactPullToRefresh
                id={"asset-list-container"}
                onRefresh={this.handleRefresh.bind( this )}
                className="col-lg-8 mr-0"
                loading={<div className="loading"><FontAwesomeIcon icon={['fal', 'spinner']} className="mr-1" spin size="lg"/></div>}
                style={{"height": this.props.containerHeight}}>
                {this.props.isMobile ?
                  (

                    <h3>Pull down to refresh</h3>
                  ) : ''}
                <Row className="mt-4 mb-md-4 mb-4">
                  <Col md="8">
                    <h1 className={"mb-sm-0 mb-4"}>{t( 'sites.Asset.listTitle' )}</h1>
                  </Col>
                  <Col md="4">
                    {this.props.isClient &&
                     <Link to={`/create/asset`}>
                       <Button variant="secondary" size="lg" block>{t( 'sites.Asset.createDelivery' )}</Button>
                     </Link>
                    }
                  </Col>
                </Row>
                <Row className="mb-4">
                  <Col>
                    <ModuleSearch
                      placeholder={t( 'sites.Asset.ModuleSearch.placeholder' )}
                      buttonText={t( 'sites.Asset.ModuleSearch.searchButton' )}
                      label={t( 'sites.Asset.ModuleSearch.label' )}
                      searchTitle={t( 'sites.Asset.searchTitle' )}
                      searchInputValue={values.search}
                      handleClickSearchItem={this.handleClickSearchItem.bind( this )}
                      handleOnClickRemoveSearch={this.handleOnClickRemoveSearch.bind( this )}
                      {...this.props}
                    />
                  </Col>
                </Row>
                <Row className="mb-4">
                  <Col>
                    <ModuleFilter
                      groupByButtons={Asset.getGroupByButtons()}
                      checkBoxes={this.getCheckByElements()}
                      handelFilterItems={this.handelFilterItems.bind( this )}
                      handleOnClickRemoveFilter={this.handleOnClickRemoveFilter.bind( this )}
                      location={this.props.location.search}
                      lastStatusQuery={"(lastSeenAt ge ###START### and lastSeenAt le ###END###)"}
                      checkTrackerOnlyWhere={"(assetState ne 'NOT_TRACKED')"}
                      checkTrackerAllWhere={"(assetState ne '')"}
                      withOnlyTrackedToggle={true}
                      groupByEnable
                      groupByMode={this.state.groupByMode}
                      handleChangeGroupByMode={this.handleChangeGroupByMode.bind( this )}
                      {...this.props}
                    />
                  </Col>
                </Row>
                {this.state.groupByMode === "asset" &&
                 <Row>
                   <Col>
                     <ModuleSort
                       sortOptions={Asset.getSortByOptions()}
                       handleSortResult={this.handelSortItems.bind( this )}
                       handleChangeSelect={this.handleSortChange.bind( this )}
                       handleOnClickRemoveSort={this.handleOnClickRemovSort.bind( this )}
                       handleSortDirectionChange={this.handleSortDirectionChange.bind( this )}
                       defaultSortItem={sortKey}
                       defaultSortDirection={sortDirection}
                       t={this.props.t}/>
                   </Col>
                 </Row>
                }
                <Row className="mb-3">
                  <Col>
                    <div className="d-flex flex-column flex-md-row flex-lg-row align-self-center">
                      <div className="pl-0 p-sm-2 flex-column">
                        <Form.Label>{t( 'sites.Asset.actualStatment' )}</Form.Label>
                      </div>
                      <div className="d-flex flex-row flex-grow-1">
                        <>
                          <Breadcrumb className="breadcumb__main-site--headline">
                            {this.state.groupByMode === "asset" &&
                             <>
                               <Breadcrumb.Item onClick={this.handleChangeGroupByModeToAssetType.bind( this )}>{assetTypeName}</Breadcrumb.Item>
                               <Breadcrumb.Item active>{t( 'sites.Asset.breadcrumbAssets' )} ({count})</Breadcrumb.Item>
                             </>
                            }
                            {this.state.groupByMode === "assetTypes" &&
                             <Breadcrumb.Item href="https://getbootstrap.com/docs/4.0/components/breadcrumb/" active>{t( 'sites.Asset.breadcrumbAssetTypes' )} ({count})</Breadcrumb.Item>
                            }
                          </Breadcrumb>
                        </>
                        <div className="ml-auto pt-1 justify-content-end">
                          {/*TODO: Implement in v2.0*/}
                          {/*<Button variant="secondary" size="sm" className="btn-light mr-2">
                            <FontAwesomeIcon icon={['fal', 'times']} className="mr-1"/>
                            {t( 'sites.Asset.assetItem.isLateItemClose' )}
                          </Button>*/}
                          <Button variant="primary" size="sm" className="btn-primary btn-round" onClick={this.handleReloadList.bind( this )}>
                            <FontAwesomeIcon icon={['fal', 'sync-alt']} className="mr-1"/>
                            {t( 'sites.Asset.assetItem.isLateItemSync' )}
                          </Button>
                        </div>
                      </div>
                    </div>
                  </Col>
                </Row>

                <InfiniteScroll
                  dataLength={list.length}
                  next={this.fetchAssets.bind( this )}
                  hasMore={hasMoreItems}
                  loader={Asset.renderLoading()}
                  endMessage={Asset.renderLoadingFinish( t )}
                  scrollableTarget="asset-list-container"
                  scrollThreshold={1}
                >
                  <Row>
                    {this.state.groupByMode === "asset" &&
                     this.state.list.map( ( item, index ) => {
                       return (
                         <AssetListItem
                           key={item.id}
                           item={item}
                           index={index}
                           url={this.props.match.url}
                           {...this.props}/>
                       );
                     } )
                    }
                    {this.state.groupByMode === "assetTypes" &&
                     this.state.list.map( ( item, index ) => {
                       return (
                         <AssetTypeListItem
                           key={item.id}
                           item={item}
                           index={index}
                           url={this.props.match.url}
                           handleShowAssets={this.handleShowAssets.bind( this )}
                           {...this.props}/>
                       );
                     } )
                    }
                  </Row>
                </InfiniteScroll>
              </ReactPullToRefresh>
              {!this.props.isMobile ?
                (
                  <Col lg="4 shadow-bpw-lg p-0">
                    <div id="assetMap"/>
                  </Col>
                ) : ''}

            </Row>
          </>
        </Container>

      </div>
    )
  }
}

export default withQueryParams( {
                                  stripUnknownKeys: true,
                                  keys: {
                                    search: {
                                      validate: value => !!value && value.length > 0,
                                    },
                                    assetTypeId: {
                                      validate: value => !!value && value.length > 0,
                                    },
                                    assetTypeName: {
                                      validate: value => !!value && value.length > 0,
                                    },
                                    checkFilter: {
                                      defaulty: "(deliveryState ne 'DONE' and deliveryState ne 'UNKNOWN' and activeDeviceAssignments gt 0)",
                                      default: "(assetState ne '')",
                                      validate: value => !!value && value.length > 0,
                                    },
                                    groupBy: {
                                      default: "assetTypes",
                                      validate: value => !!value && value.length > 0,
                                    },
                                    sortKey: {
                                      default: "cycleCount",
                                      validate: value => !!value && value.length > 0,
                                    },
                                    sortDirection: {
                                      default: "desc",
                                      validate: value => !!value && value.length > 0,
                                    }
                                  }
                                } )( Asset )