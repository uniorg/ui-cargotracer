import React, { Component } from 'react';
import { Alert, Button, Col, Container, Form, FormControl, Row } from 'react-bootstrap'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import AssetInputCheckKeyLabel from '../component/AssetInputCheckKeyLabel'
import withQueryParams from "react-router-query-params";
import Async from 'react-select/async';
import { components } from "react-select";
import TrackerInput from "../component/TrackerInputCheckDelivery";
import DateTimePicker from "../component/DateTimePicker";
import DateYearPicker from "../component/DateYearPicker";
import AddressListItem from "../component/AddressListItem";
import $ from "jquery";
import Map from '../services/locationLeaFlet';
import Loader from "react-loader-spinner";
import { BrowserRouter as Router } from 'react-router-dom';
import CreateUpdateAssetTypeModal from "../component/CreateUpdateAssetTypeModal";

class AssetCreateUpdate extends Component {
  _isMounted = false
  assetMap = null
  isUpdateMode: false
  createAndNew: false

  constructor( props ) {
    super( props );
    this.state = {
      assetItem: null,
      successSend: false,
      failSend: false,
      successItem: undefined,

      showCreateAssetModal: false,

      assetInputIsValid: false,

      deviceInputIsValid: null,

      keyLabel: null,
      name: null,
      description: null,
      assetTypeId: null,
      selectedAssetType: null,
      assetTypeIsLoading: null,
      deviceId: null,
      selectedDevice: null,
      deviceIsLoading: null,
      constructionYear: undefined,
      version: null,
      lastMaintenance: undefined,

      homeAddresses: [],
      poiAddresses: [],

      dummyAddress: {
        keyLabel: null,
        name: null,
        postalCode: null,
        city: null,
        street: null,
        streetNumber: null,
        countryCode: null,
        locationSource: "AZURE",
        locationSourceKey: "",
        geofence: {
          latitude: null,
          longitude: null,
          geofenceType: null,
          radius: 10000
        },
        isUpdate: false
      },

      simpleAsset: {
        keyLabel: "LUKAS1",
        name: "TESTLUKAS",
        description: "Test Asset UI Service Add Device",
        assetTypeId: 38,
        deviceId: "LUKASUITEST",
        homeAddresses: [
          {
            id: 2,
            keyLabel: "BPW",
            name: "BPW Bergische Achsen KG",
            postalCode: "51674",
            city: "Wiehl",
            street: "Ohlerhammer 1",
            streetNumber: null,
            countryCode: "DE",
            locationSource: "UNKNOWN",
            locationSourceKey: "75b68e47-cad1-4ef6-9e8d-2d6cf2a4f1e0",
            geofence: {
              latitude: 50.94652,
              longitude: 7.565246,
              radius: 5000,
              geofenceType: "POINT"
            }
          },
          {
            keyLabel: "lat###52.73344:lon###5.14799",
            name: "BPW Headquarter",
            postalCode: "1693",
            city: "Medemblik",
            street: "Dres",
            streetNumber: "5A",
            countryCode: "NL",
            locationSource: "AZURE",
            locationSourceKey: "RU/GEO/p0/70217",
            geofence: {
              latitude: 52.73344,
              longitude: 5.14799,
              geofenceType: "POINT",
              radius: 10000
            }
          }
        ],
        poiAddresses: []
      },

      keyLabelInputValid: undefined,
      keyLabelInputInvalid: undefined,


      nameInputValid: null,
      nameInputInvalid: null,

      assetTypeIsValid: null,
      assetTypeIsInValid: null,

      homeAddressesIsValid: null,

      errorAsset: null,
      saveMode: false
    };
  }

  static renderLoading() {
    return <><Row><Col md={"12"} className={"d-flex justify-content-center mt-5"}><Loader type="Oval" color="#CDDC4B" height={70} width={70}/></Col></Row></>;
  }

  static renderLoadingFinish( t ) {
    return <><Row><Col md={"12"} className={"d-flex flex-column mt-3"}>
      <div className="alert alert-primary" role="alert">
        {t( "sites.Asset.AssetDetail.noMoreContentInfo" )}
      </div>
    </Col></Row></>;
  }

  static renderError() {
    return <><Row><Col md={"12"} className={"d-flex flex-column mt-3"}>
      <div className={"alert alert-danger"} role={"alert"}>There was a technical problem. Please contact our <a href="/" className={"alert-link"}>Support Team</a>. Or just reload and try it again.</div>
    </Col></Row></>;
  }

  componentWillMount() {
    if ( this.props.match.params.id !== undefined ) {
      this.isUpdateMode = true
    }
  }

  async componentDidMount() {
    this._isMounted = true;
    if ( this.props.match.params.id !== undefined ) {
      this.isUpdateMode = true
      await this.fetchAsset()
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  componentDidUpdate() {
    console.log( "componentDidUpdate" );
    let that = this
    let {homeAddresses, poiAddresses, saveMode} = this.state

    if ( !saveMode ) {
      if ( this.assetMap !== undefined && this.assetMap !== null ) {
        //await this.assetMap.map.remove();
        $( "#assetMapContainer" ).html( "" );
      }
      if ( homeAddresses.length > 0 || poiAddresses.length > 0 ) {
        if ( $( "#assetMap" ).length === 0 )
          $( "#assetMapContainer" ).append( "<div id='assetMap'></div>" )

        this.assetMap = new Map( {startLatitude: '50.8038972', startLongitude: '7.1723139', startZoom: '5', containerId: 'assetMap', mapType: 'openStreet'} );
        this.assetMap.startMap( () => {

          homeAddresses.map( function( obj ) {
            that.setMarkerFromAddressItemWithRadius( obj, obj.geofence.radius, "HOME", false )
            return obj
          } );
          poiAddresses.map( function( obj ) {
            that.setMarkerFromAddressItemWithRadius( obj, obj.geofence.radius, "POI", false )
            return obj
          } );

        } );
        setTimeout( () => {

          that.assetMap.setNowMarkerCenter( undefined, true );
        }, 500 )
      } else {
        $( "#assetMap" ).attr( "style", "" ).attr( "class", "" )
        this.assetMap = null
      }
    }

  }

  fetchAsset( compleation ) {
    fetch( '/asset-ui-service/asset/detail/' + this.props.match.params.id )
      .then( response => {
        if ( response.ok ) {
          return response.json();
        } else {
          throw new Error( "has error" );
        }
      } )
      .then( ( data ) => {
        if ( this._isMounted ) {
          this.onLoad( data )
        }
        if ( compleation !== undefined ) {
          compleation();
        }
      } )
  }

  async onLoad( data ) {
    /**
     * assetCycleId: 4686
     assetState: "AT_HOME"
     assetStateTimestamp: "2018-08-21T04:30:47Z"
     assetTypeId: 41
     assetTypeName: "Achsgestelle"
     averageAtHomeTimeSeconds: 1346880
     averageAtPoiTimeSeconds: 0
     averageCycleTimeSeconds: 464712
     averageOnTheWayTimeSeconds: 0
     constructionYear: "2018"
     cycleCount: 2
     description: null
     deviceId: "389401"
     homeAddresses: [{id: 58, keyLabel: "BPW", name: "BPW Bergische Achsen KG", postalCode: "51674", city: "Wiehl",…},…]
     id: 363
     isTracked: true
     keyLabel: "20"
     lastMaintenance: null
     lastSeenAt: "2018-08-24T13:41:57Z"
     lastSeenBatteryVoltage: null
     lastValidGeoPositionLatitude: 51.407876
     lastValidGeoPositionLongitude: 0.212979
     lastValidGeoPositionRadius: null
     lastValidGeoPositionSentAt: "2018-08-24T13:41:57Z"
     lastValidGeoPositionSpeed: null
     lastValidGeoPositionTemperature: null
     lastValidGeoPositionType: "GPS"
     lastValidGeoPositionVibrationCount: 0
     lastValidGpsPositionLatitude: 51.407876
     lastValidGpsPositionLongitude: 0.212979
     lastValidGpsPositionSentAt: "2018-08-24T13:41:57Z"
     name: "BPW 20"
     odataId: "363"
     poiAddresses: []
     version: "v01"
     vibrationCount: 0
     */
    let homeAddresses = data.homeAddresses.map( ( item ) => {
      item.tempId = Math.random().toString( 36 ).substr( 2, 9 )
      item.isUpdate = true
      item.isValid = true
      return item
    } )

    let poiAddresses = data.poiAddresses.map( ( item ) => {
      item.tempId = Math.random().toString( 36 ).substr( 2, 9 )
      item.isUpdate = true
      item.isValid = true
      return item
    } )


    await this.setState( {
                           assetItem: data,
                           keyLabel: ((data.keyLabel !== null) ? data.keyLabel : undefined),
                           name: ((data.name !== null) ? data.name : undefined),
                           keyLabelInputValid: ((data.name !== null) ? true : null),
                           keyLabelInputInvalid: ((data.name !== null) ? false : null),
                           nameInputValid: ((data.name !== null) ? true : null),
                           nameInputInvalid: ((data.name !== null) ? false : null),
                           assetTypeIsValid: ((data.assetTypeId !== null) ? true : null),
                           assetTypeIsInValid: ((data.assetTypeId !== null) ? false : null),
                           description: ((data.description !== null) ? data.description : undefined),
                           assetTypeId: ((data.assetTypeId !== null) ? data.assetTypeId : undefined),
                           deviceId: ((data.deviceId !== null) ? data.deviceId : undefined),
                           constructionYear: ((data.constructionYear !== null) ? data.constructionYear : undefined),
                           version: ((data.version !== null) ? data.version : undefined),
                           lastMaintenance: ((data.lastMaintenance !== null) ? data.lastMaintenance : undefined),
                           homeAddresses: homeAddresses,
                           poiAddresses: poiAddresses,
                           homeAddressesIsValid: ((homeAddresses.length > 0))
                         } );
    if ( data.assetTypeId !== null ) {
      this.fetchAssetType( data.assetTypeId )
    }
    if ( data.deviceId !== null ) {
      this.fetchDevice( data.deviceId )
    }
  }

  handleAssetInput( keyLabel, isValid ) {
    this.setState( {keyLabel: keyLabel, keyLabelInputIsValid: ((!isValid))} );
  }

  changeAssetValid( isInvalid, isValid ) {
    this.setState( {keyLabelInputInvalid: isInvalid, keyLabelInputValid: isValid} )
  }

  handleChangeAssetType( item ) {
    if ( item !== null && item !== undefined && item !== "" ) {
      this.setState( {selectedAssetType: item, assetTypeIsValid: true, assetTypeIsInValid: false, assetTypeId: item.id} )
    } else {
      this.setState( {selectedAssetType: item, assetTypeIsValid: false, assetTypeIsInValid: true} )
    }
  }

  handleClearTrackerSelect() {
    this.setState( {deviceId: null, selectedDevice: null, deviceInputIsValid: null} )
  }

  getOptionValue = ( option ) => option.id; // maps the result 'id' as the 'value'

  getOptionLabel = ( option ) => option.name + ((option.description !== null) ? " - " + option.description : "");

  async handleGetAssetTypes( input ) {
    if ( !input ) {
      return [];
    }

    return fetch( `/asset-ui-service/asset/searchAssetType/` + input )
      .then( ( response ) => response.json() )
      .then( ( json ) => {
        let returnOptions = []
        if ( json.value !== undefined ) {
          returnOptions = json.value;
        }
        return returnOptions
      } );
  }

  fetchAssetType( id ) {
    let that = this
    this.setState( {assetTypeIsLoading: true} )
    fetch( `/asset-ui-service/asset/searchAssetTypeById/` + id )
      .then( ( response ) => response.json() )
      .then( ( assetType ) => {
        this.setState( {selectedAssetType: assetType, assetTypeIsValid: true, assetTypeIsInValid: false, assetTypeId: assetType.id, assetTypeIsLoading: false} )
      } );
  }

  fetchDevice( id ) {
    let that = this
    this.setState( {deviceIsLoading: true} )
    fetch( `/device-ui-service/devices/searchDeviceById/` + id )
      .then( ( response ) => response.json() )
      .then( ( device ) => {
        that.setState( {selectedDevice: device, deviceInputIsValid: true, deviceIsLoading: false} )
      } );
  }

  handleChangeDescription( e ) {
    this.state.description = e.target.value.trim()
    //this.setState({description: e.target.value.trim()})
  }

  handleChangeVersion( e ) {
    this.state.version = e.target.value.trim()

  }

  handleChangeName( e ) {
    this.state.name = e.target.value.trim()
    if ( this.state.name !== null && this.state.name !== undefined && this.state.name !== "" ) {
      this.setState( {nameInputInvalid: false, nameInputValid: true} )
    } else {
      this.setState( {nameInputInvalid: true, nameInputValid: false} )
    }
  }

  handleTrackerInput( id, isValid, data ) {
    console.log( "handleTrackerInput", id, isValid, data );
    this.setState( {deviceId: id, deviceInputIsValid: isValid, errorAsset: ((data !== null && data !== undefined && data.entries !== undefined && data.entries[0] !== undefined) ? data.entries[0] : null)} );
  }

  handleDeviceExistErrorMessage() {
    let {t} = this.props
    let {errorAsset} = this.state
    let assetId = ""
    let assetName = ""

    if ( errorAsset !== null ) {
      let {id, name} = errorAsset
      assetId = id
      assetName = name
    }

    return (
      <Row>
        <Col>
          {t( 'sites.Asset.assetCreate.deviceExistOnDeviceErrorMessage' ).replace( "@ASSETID", assetId ).replace( "@ASSETNAME", assetName )} <br/>
          <Button onClick={this.handleEditAsset.bind( this )} variant="primary" size="sm">{t( 'sites.Asset.assetCreate.deviceExistOnDeviceErrorEditBtn' )}</Button>
        </Col>
      </Row>
    )
  }

  handleEditAsset() {

    let {errorAsset} = this.state

    if ( errorAsset !== null ) {
      let {id} = errorAsset
      var win = window.open( "/update/asset/" + id, '_blank' );
      win.focus();
    }
  }

  handleChangeLastMaintenance( date ) {
    console.log( date )//25. September 2019 09:38
    this.setState( {lastMaintenance: new Date( date ).toISOString()} );
    this.lastMaintenance = new Date( date );
  }

  handleChangeConstructionYear( date ) {
    this.setState( {constructionYear: date} );
  }

  handleShowCreateAssetModal() {
    this.setState( {showCreateAssetModal: true} )
  }

  handleCloseCreateAssetModal() {
    this.setState( {showCreateAssetModal: false} )
  }

  handleOnSaveAssetType( newAssetType ) {
    this.setState( {selectedAssetType: newAssetType, assetTypeId: newAssetType.id, assetTypeIsValid: true, assetTypeIsInValid: false} )
  }

  handleAddHomeAddress() {
    let {homeAddresses, dummyAddress} = this.state
    let newAddress = Object.assign( {}, dummyAddress )
    newAddress.tempId = Math.random().toString( 36 ).substr( 2, 9 )
    newAddress.isValid = false
    newAddress.isUpdate = false
    homeAddresses.push( newAddress )

    this.setState( {
                     homeAddresses: homeAddresses, homeAddressesIsValid: ((homeAddresses.filter( ( address ) => {
        return address.isValid
      } ).length > 0))
                   } )
  }

  handleRemoveHomeAddress( item ) {
    let {homeAddresses} = this.state
    homeAddresses = homeAddresses.filter( function( obj ) {
      return obj.tempId !== item.tempId;
    } );
    this.setState( {
                     homeAddresses: homeAddresses, homeAddressesIsValid: ((homeAddresses.filter( ( address ) => {
        return address.isValid
      } ).length > 0))
                   } )
  }

  async handleClickOnAddressHome( item, radius, address ) {
    let {homeAddresses} = this.state
    let homeAddressesNew = homeAddresses.map( ( obj ) => {
      address.isValid = true
      if ( obj.tempId === address.tempId )
        return address
      return obj
    } );
    let homes = homeAddressesNew.filter( ( address ) => {
      return address.isValid
    } )
    this.setState( {
                     homeAddresses: homeAddressesNew, homeAddressesIsValid: homes.length > 0
                   } )
  }

  async handleChangeRadiusHome( item, radius, address ) {
    await this.setMarkerFromAddressItemWithRadius( address, radius, "HOME" )
  }

  handeGoToMarkerHome( selectedItem, selectedItemRadius, newAddress ) {
    this.setMarkerFromAddressItemWithRadius( newAddress, selectedItemRadius, "HOME" )
  }

  handleChangeNameHome( value, item, e ) {
    let {homeAddresses} = this.state
    homeAddresses.map( function( obj ) {
      if ( obj.tempId === item.tempId )
        obj.name = value.trim()
      return obj
    } );
  }

  handleAddPoiAddress() {
    let {poiAddresses, dummyAddress} = this.state
    let newAddress = Object.assign( {}, dummyAddress )
    newAddress.tempId = Math.random().toString( 36 ).substr( 2, 9 )
    newAddress.isValid = false
    newAddress.isUpdate = false

    poiAddresses.push( newAddress )
    this.setState( {poiAddresses: poiAddresses} )
  }

  handleRemovePoiAddress( item ) {
    let {poiAddresses} = this.state
    poiAddresses = poiAddresses.filter( function( obj ) {
      return obj.tempId !== item.tempId;
    } );

    this.setState( {poiAddresses: poiAddresses} )
  }

  async handleClickOnAddressPoi( item, radius, address ) {
    let {poiAddresses} = this.state
    let newPoiAddresses = poiAddresses.map( function( obj ) {
      address.isValid = true
      if ( obj.tempId === address.tempId )
        return address
      return obj
    } );

    this.setState( {poiAddresses: newPoiAddresses} )
  }

  async handleChangeRadiusPoi( item, radius, address ) {
    await this.setMarkerFromAddressItemWithRadius( address, radius, "POI" )
  }

  handeGoToMarkerPoi( selectedItem, selectedItemRadius, newAddress ) {
    this.setMarkerFromAddressItemWithRadius( newAddress, selectedItemRadius, "POI" )
  }

  handleChangeNamePoi( value, item, e ) {
    let {poiAddresses} = this.state
    poiAddresses.map( function( obj ) {
      if ( obj.tempId === item.tempId )
        obj.name = value.trim()
      return obj
    } );
  }

  setMarkerFromAddressItemWithRadius( item, radius, addressType, withCenter ) {

    /**
     * city: "Wiehl"
     countryCode: "DE"
     geofence: {id: 79, latitude: 50.94652, longitude: 7.565246, radius: 5000, geofenceType: "POINT"}
     geofenceType: "POINT"
     id: 79
     latitude: 50.94652
     longitude: 7.565246
     radius: 5000
     id: 58
     keyLabel: "BPW"
     locationSource: "UNKNOWN"
     locationSourceKey: "c50b0638-2c49-407b-8b77-c2edacde8a72"
     name: "BPW Bergische Achsen KG"
     postalCode: "51674"
     street: "Ohlerhammer 1"
     streetNumber: null
     */
    const {tempId} = item;

    const marker = this.getMarker( item, radius, addressType );
    const markerIconsStart = {
      google: "BpwPalletMarker",
      leaflet: "<i class=\"fas fa-map-marker-alt\" style='font-size: 1.5rem;color: #129fd8;position:  relative;top: -12px;left: -2px;'></i>"
    };
    this.assetMap.removeMarker( tempId )
    this.assetMap.setMarkers( [marker], tempId, markerIconsStart, false, true, true );
    if ( withCenter === undefined || withCenter )
      this.assetMap.setNowMarkerCenter( tempId, true );
  }

  getMarker( item, radius, addressType ) {
    const {street, streetNumber, city, postalCode, geofence, tempId} = item;
    const {latitude, longitude} = geofence;

    let streetNumberChecked = ((streetNumber !== undefined && streetNumber !== null) ? " " + streetNumber : "")
    let color = ((addressType === "HOME") ? "#002851" : "#CDDC4B")
    const marker = {
      tempId: tempId,
      lat: latitude,
      lng: longitude,
      title: city,
      popup: "<div class='row m-3'>" +
             "<div class='col-12'>" + street + streetNumberChecked + ", </br>" + postalCode + " " + city + "</div>" +
             "</div>",
      circleRadius: radius,
      circleRadiusColor: color,
      zIndex: 1000,
      icon: {
        google: "BpwPalletMarker",
        leaflet: "<i class=\"fas fa-circle\" style='font-size: 1rem;color: " + color + ";position:  relative;top: -1.5px;left: -2px;'></i>"
      }
    };

    return marker
  }

  handleSaveAsset() {
    if ( this.isUpdateMode ) {
      this.updateAsset()
    } else {
      this.createNewAsset()
    }
  }

  handleSaveAssetNew() {
    this.createAndNew = true
    this.handleSaveAsset()
  }

  handleDiscardAsset() {
    const {history} = this.props;
    history.goBack();
  }

  handleDeleteAsset() {
    let asset = this.state.assetItem
    const {history} = this.props;
    fetch( '/asset-ui-service/asset/delete/' + asset.id, {
      method: 'DELETE',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    } )
      .then( ( res ) => {
        if ( res.status !== 200 ) {
          this.setState( {successItem: {}, successSend: false, failSend: true} )
        } else {
          history.push( {
                          pathname: "/assets"
                        } )
        }
      } )
      .catch( ( error ) => {
        console.error( error );
        //ai.appInsights.trackException(error)
        this.setState( {successItem: error, successSend: false, failSend: true, saveMode: false} )
      } )
  }

  handleAbortEditAsset() {
    const {history} = this.props;
    history.push( {
                    pathname: "/assets"
                  } )
  }

  getAssetObject( assetObject ) {
    let {keyLabel, name, description, assetTypeId, selectedAssetType, deviceId, selectedDevice, constructionYear, version, lastMaintenance, homeAddresses, poiAddresses} = this.state
    assetObject.keyLabel = ((keyLabel !== undefined) ? keyLabel : null)
    assetObject.name = ((name !== undefined) ? name : null)
    assetObject.description = ((description !== undefined) ? description : null)
    assetObject.assetTypeId = ((assetTypeId !== undefined) ? assetTypeId : null)
    assetObject.deviceId = ((deviceId !== undefined) ? deviceId : null)
    assetObject.constructionYear = ((constructionYear !== undefined) ? constructionYear : null)
    assetObject.version = ((version !== undefined) ? version : null)
    assetObject.lastMaintenance = ((lastMaintenance !== undefined) ? lastMaintenance : null)

    let home = JSON.parse( JSON.stringify( homeAddresses ) )

    home = home.filter( ( address ) => {
      return address.isValid
    } )
    home = home.map( ( address ) => {
      address.isValid = undefined
      address.tempId = undefined
      address.isUpdate = undefined
      return address
    } )
    assetObject.homeAddresses = home

    let poi = JSON.parse( JSON.stringify( poiAddresses ) )

    poi = poi.filter( ( address ) => {
      return address.isValid
    } )
    poi = poi.map( ( address ) => {
      address.isValid = undefined
      address.tempId = undefined
      address.isUpdate = undefined
      return address
    } )
    assetObject.poiAddresses = poi

    return assetObject
  }

  createNewAsset() {
    const {history} = this.props;
    let that = this

    let {deviceInputIsValid, homeAddresses, keyLabelInputInvalid, keyLabelInputValid, assetTypeIsValid, assetTypeIsInValid, nameInputValid, nameInputInvalid, homeAddressesIsValid, deviceId, selectedDevice} = this.state
    let asset = this.state.simpleAsset
    let updateedAsset = this.getAssetObject( asset )

    let newKeyLabelInputInvalid = false
    let newKeyLabelInputValid = true
    let newNameInputInvalid = false
    let newNameInputValid = true
    let newAssetTypeIsInValid = false
    let newAssetTypeIsValid = true
    let newHomeAddressesIsValid = true

    if ( !(!keyLabelInputInvalid && keyLabelInputValid) || keyLabelInputInvalid === null || keyLabelInputValid === null ) {
      newKeyLabelInputInvalid = true
      newKeyLabelInputValid = false
    }

    if ( !(!nameInputInvalid && nameInputValid) || nameInputInvalid === null || nameInputValid === null ) {
      newNameInputInvalid = true
      newNameInputValid = false
    }

    if ( !(!assetTypeIsInValid && assetTypeIsValid) || assetTypeIsInValid === null || assetTypeIsValid === null ) {
      newAssetTypeIsInValid = true
      newAssetTypeIsValid = false
    }
    let homes = homeAddresses.filter( ( address ) => {
      return address.isValid
    } )
    if ( homes.length === 0 || homeAddressesIsValid === null ) {
      newHomeAddressesIsValid = false

    }

    if ( !(!newKeyLabelInputInvalid && newKeyLabelInputValid) ||
         !(!newNameInputInvalid && newNameInputValid) ||
         !(!newAssetTypeIsInValid && newAssetTypeIsValid) ||
         !(homes.length > 0) || homeAddressesIsValid === null ||
         (deviceId !== null && selectedDevice !== null && !deviceInputIsValid) ) {

      this.setState( {
                       keyLabelInputInvalid: newKeyLabelInputInvalid,
                       keyLabelInputValid: newKeyLabelInputValid,
                       nameInputValid: newNameInputValid,
                       nameInputInvalid: newNameInputInvalid,
                       assetTypeIsValid: newAssetTypeIsValid,
                       assetTypeIsInValid: newAssetTypeIsInValid,
                       homeAddressesIsValid: newHomeAddressesIsValid
                     } )
      return
    } else {
      this.setState( {saveMode: true} )
    }

    fetch( '/asset-ui-service/asset/CreateAsset', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify( updateedAsset )
    } )
      .then( ( res ) => {
        if ( res.status !== 201 ) {
          this.setState( {successItem: {}, successSend: false, failSend: true} )
        } else {
          return res.json()
        }
      } )
      .then( ( data ) => {

        if ( that.createAndNew ) {
          window.location.href = window.location.href
        } else {
          history.push( {
                          pathname: "/assets/" + data.id,
                          state: {lastLocation: history.location}
                        } )
        }

      } )
      .catch( ( error ) => {
        console.error( error );
        //ai.appInsights.trackException(error)
        this.setState( {successItem: error, successSend: false, failSend: true, saveMode: false} )
      } )
  }

  updateAsset() {
    const {history} = this.props;
    let oldAsset = this.state.simpleAsset
    let asset = this.state.assetItem
    oldAsset.id = asset.id
    let {deviceInputIsValid, homeAddresses, keyLabelInputInvalid, keyLabelInputValid, assetTypeIsValid, assetTypeIsInValid, nameInputValid, nameInputInvalid, homeAddressesIsValid, deviceId, selectedDevice} = this.state
    let updateedAsset = this.getAssetObject( oldAsset )

    let newKeyLabelInputInvalid = false
    let newKeyLabelInputValid = true
    let newNameInputInvalid = false
    let newNameInputValid = true
    let newAssetTypeIsInValid = false
    let newAssetTypeIsValid = true
    let newHomeAddressesIsValid = true

    if ( !(!keyLabelInputInvalid && keyLabelInputValid) || keyLabelInputInvalid === null || keyLabelInputValid === null ) {
      newKeyLabelInputInvalid = true
      newKeyLabelInputValid = false
    }

    if ( !(!nameInputInvalid && nameInputValid) || nameInputInvalid === null || nameInputValid === null ) {
      newNameInputInvalid = true
      newNameInputValid = false
    }

    if ( !(!assetTypeIsInValid && assetTypeIsValid) || assetTypeIsInValid === null || assetTypeIsValid === null ) {
      newAssetTypeIsInValid = true
      newAssetTypeIsValid = false
    }
    let homes = homeAddresses.filter( ( address ) => {
      return address.isValid
    } )
    if ( homes.length === 0 || homeAddressesIsValid === null ) {
      newHomeAddressesIsValid = false

    }

    if ( !(!newKeyLabelInputInvalid && newKeyLabelInputValid) ||
         !(!newNameInputInvalid && newNameInputValid) ||
         !(!newAssetTypeIsInValid && newAssetTypeIsValid) ||
         !(homes.length > 0) || homeAddressesIsValid === null ||
         (deviceId !== null && selectedDevice !== null && !deviceInputIsValid) ) {


      this.setState( {
                       keyLabelInputInvalid: newKeyLabelInputInvalid,
                       keyLabelInputValid: newKeyLabelInputValid,
                       nameInputValid: newNameInputValid,
                       nameInputInvalid: newNameInputInvalid,
                       assetTypeIsValid: newAssetTypeIsValid,
                       assetTypeIsInValid: newAssetTypeIsInValid,
                       homeAddressesIsValid: newHomeAddressesIsValid
                     } )
      return
    } else {
      this.setState( {saveMode: true} )
    }

    fetch( '/asset-ui-service/asset/updateAsset', {
      method: 'PATCH',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify( updateedAsset )
    } )
      .then( ( res ) => {
        if ( res.status !== 201 ) {
          this.setState( {successItem: {}, successSend: false, failSend: true} )
        } else {
          return res.json()
        }
      } )
      .then( ( data ) => {
        history.push( {
                        pathname: "/assets/" + data.id
                      } )
      } )
      .catch( ( error ) => {
        console.error( error );
        //ai.appInsights.trackException(error)
        this.setState( {successItem: error, successSend: false, failSend: true, saveMode: false} )
      } )

  }

  render() {
    let {assetItem, saveMode} = this.state
    if ( (this.isUpdateMode && assetItem === null) || (saveMode) )
      return AssetCreateUpdate.renderLoading()
    return this.renderData()
  }

  renderData( item ) {
    const {t} = this.props;
    const ValueContainer = ( {children, ...props} ) => {
      return (
        components.ValueContainer && (
          <components.ValueContainer {...props}>
            {!!children && (
              <FontAwesomeIcon icon={['fal', 'search']} style={{position: 'absolute', left: 6}}/>
            )}
            {children}
          </components.ValueContainer>
        )
      );
    };

    const ClearIndicator = props => {
      return (
        components.ClearIndicator && (
          <components.ClearIndicator {...props}>
            <FontAwesomeIcon icon={['fal', 'times']}/>
          </components.ClearIndicator>
        )
      );
    };
    const DropdownIndicator = props => {
      return (
        components.DropdownIndicator && (
          <components.DropdownIndicator {...props}>
            <FontAwesomeIcon icon={['fal', 'chevron-down']}/>
          </components.DropdownIndicator>
        )
      );
    };

    const LoadingIndicator = props => {
      return (
        components.LoadingIndicator && (
          <components.DropdownIndicator {...props}>
            <FontAwesomeIcon icon={['fal', 'spinner']} spin/>
          </components.DropdownIndicator>
        )
      );
    };
    const {keyLabelInputValid, keyLabelInputInvalid, nameInputValid, nameInputInvalid, successSend, failSend, deviceInputIsValid, selectedDevice} = this.state;
    const {keyLabel, name, description, assetTypeId, deviceId, constructionYear, version, lastMaintenance, deviceIsLoading, assetTypeIsLoading} = this.state;

    return (

      <Router>
        <div className={"site-asset-create"}>
          <Container>
            <Row>
              <Col className={"mb-0 mb-md-4 mt-4"} lg={"12"}>
                <h1>{t( 'sites.Asset.assetCreate.assetForm' )}</h1>
              </Col>
            </Row>
            <Row>
              <Col lg={"12"}>
                <Form>
                  <Row>
                    <Col md={"7"} className={"pr-md-0 mb-md-3"}>
                      <Row>
                        <Col md={"12"} className={"mb-md-3"}>
                          <Form.Row>
                            <AssetInputCheckKeyLabel handleValidate={this.handleAssetInput.bind( this )}
                                                     keyLabelInputValid={((keyLabel !== null) ? ((keyLabelInputValid) ? keyLabelInputValid : true) : keyLabelInputValid)}
                                                     keyLabelInputInvalid={((keyLabel !== null) ? ((keyLabelInputInvalid) ? keyLabelInputInvalid : false) : keyLabelInputInvalid)}
                                                     changeValid={this.changeAssetValid.bind( this )}
                                                     defaultValue={keyLabel}
                                                     readOnly={this.isUpdateMode}
                                                     handleChangeInput={this.handleChangeName.bind( this )}
                                                     placeholder={t( 'sites.Asset.assetCreate.assetIdPlaceholder' )}
                                                     {...this.props}/>
                            <Form.Group as={Col} md={"6"} controlId="formGridDesignation">
                              <Form.Label>{t( 'sites.Asset.assetCreate.assetName' )}</Form.Label>
                              <Form.Control
                                type="text"
                                className="form-control-lg"
                                defaultValue={name}
                                placeholder={t( 'sites.Asset.assetCreate.namePlaceholder' )}
                                onChange={this.handleChangeName.bind( this )}
                                isInvalid={((name !== null) ? ((nameInputInvalid) ? nameInputInvalid : false) : nameInputInvalid)}
                                isValid={((name !== null) ? ((nameInputValid) ? nameInputValid : true) : nameInputValid)}>
                              </Form.Control>
                            </Form.Group>
                          </Form.Row>
                        </Col>
                      </Row>
                      <Row>
                        <Col md={"12"}>
                          <Form.Row>
                            <Form.Group as={Col} md={"6"} controlId="formGridAssetType" className={"delivery-tracker-input-check-delivery"}>
                              {assetTypeIsLoading ? <FontAwesomeIcon icon={['fal', 'spinner']} size="lg" spin/> : null}
                              <Form.Label>{t( 'sites.Asset.assetCreate.assetType' )}</Form.Label>
                              <Async
                                className={"asset-type-select form-control-lg" + ((this.state.assetTypeIsValid) ? " form-control is-valid" : "") + ((this.state.assetTypeIsInValid) ? " form-control is-invalid" : "")}
                                classNamePrefix="select"
                                backspaceRemoves
                                isClearable
                                isSearchable
                                components={{DropdownIndicator, ValueContainer, ClearIndicator, LoadingIndicator}}
                                value={this.state.selectedAssetType}
                                onChange={this.handleChangeAssetType.bind( this )}
                                handleClearInput={this.handleClearTrackerSelect.bind( this )}
                                placeholder={t( 'sites.Asset.assetCreate.selectAssetType' )}
                                valueKey="id"
                                labelKey="name"
                                getOptionValue={this.getOptionValue}
                                getOptionLabel={this.getOptionLabel}
                                loadOptions={this.handleGetAssetTypes}
                              />
                            </Form.Group>
                            <Form.Group as={Col} md={"6"} controlId="formGridTracker" className="d-flex align-self-end">
                              <div className="flex-grow-1">
                                <Button size={"lg"} variant="primary" block onClick={this.handleShowCreateAssetModal.bind( this )}>{t( 'sites.Asset.assetCreate.createNewAssetType' )}</Button>
                              </div>
                            </Form.Group>
                          </Form.Row>
                        </Col>
                      </Row>
                    </Col>
                    <Col md={"5"}>
                      <Row>
                        <Form.Group as={Col} md={"12"} controlId="exampleForm.ControlTextarea1">
                          <Form.Label>{t( 'sites.Asset.assetCreate.description' )}</Form.Label>
                          <Form.Control as="textarea"
                                        placeholder={t( 'sites.Asset.assetCreate.enterDescription' )}
                                        defaultValue={description}
                                        onChange={this.handleChangeDescription.bind( this )}
                                        rows="7"/>
                        </Form.Group>
                      </Row>
                    </Col>
                  </Row>
                  <Row className={"mb-md-3"}>
                    <Col md={"7"} className={"pr-md-0"}>
                      <Row>
                        <Col md={"12"}>
                          <Form.Row>
                            <TrackerInput handleValidate={this.handleTrackerInput.bind( this )} {...this.props} selectedDevice={selectedDevice}
                                          label={t( 'sites.Asset.assetCreate.assignTrackers' )}
                                          checkValueUrl={"/asset-ui-service/asset/assignedAsset/"}
                                          invalidClass={' alert alert-danger'}
                                          invalidColorClass={' select-invalid-danger'}
                                          errorMessageDeviceExist={this.handleDeviceExistErrorMessage()}
                                          handleClearInput={this.handleClearTrackerSelect.bind( this )}
                                          isValid={deviceInputIsValid}
                                          isPropLoading={deviceIsLoading}/>
                            <Form.Group as={Col} md={"6"} controlId="formGridAssetType">
                              <Form.Label>{t( 'sites.Asset.assetCreate.constructionYear' )}</Form.Label>
                              <DateYearPicker onChange={this.handleChangeConstructionYear.bind( this )} elementId={"constructionYearInput"} defaultValue={constructionYear}/>
                            </Form.Group>
                          </Form.Row>
                        </Col>
                      </Row>
                    </Col>
                    <Col md={"5"}>
                      <Row>
                        <Col md={"12"}>
                          <Form.Row>
                            <Form.Group as={Col} md={"3"} controlId="formGridAsset">
                              <Form.Label>{t( 'sites.Asset.assetCreate.version' )}</Form.Label>
                              <FormControl
                                type="text"
                                min="1"
                                aria-required="true"
                                className="form-control-lg"
                                placeholder={t( 'sites.Asset.assetCreate.version' )}
                                aria-label={t( 'sites.Asset.assetCreate.version' )}
                                defaultValue={version}
                                onChange={this.handleChangeVersion.bind( this )}
                              />
                            </Form.Group>
                            <Form.Group
                              as={Col}
                              md={"9"}
                              controlId="formGridDate"
                            >
                              <Form.Label>{t( 'sites.Asset.assetCreate.lastMaintenance' )}</Form.Label>
                              <DateTimePicker onChange={this.handleChangeLastMaintenance.bind( this )} elementId={"lastMaintenanceInput"} defaultValue={lastMaintenance}/>
                            </Form.Group>
                          </Form.Row>
                        </Col>
                      </Row>
                    </Col>
                  </Row>

                  <Row className={"mb-3"}>
                    <Col>
                      <hr/>
                    </Col>
                  </Row>

                  {(this.state.homeAddressesIsValid === false && this.state.homeAddressesIsValid !== null) &&
                   <Row>
                     <Col md={"6"}>
                       <Alert variant={"danger"}>
                         Home base is required
                       </Alert>
                     </Col>
                   </Row>
                  }
                  <Form.Row className={"mb-5"}>
                    <Col md={"6"} className={"pr-md-3"}>
                      <Row>
                        <Col>
                          <h5 className={""}>{t( 'sites.Asset.assetCreate.homeAddresses' )}:</h5>
                        </Col>
                      </Row>
                      {
                        this.state.homeAddresses.map( ( item, index ) => {
                          return (
                            <AddressListItem
                              key={item.tempId}
                              item={item}
                              index={index}
                              label={t( 'sites.Asset.assetCreate.addHomeAddresses' )}
                              placeholder={t( 'sites.Asset.assetCreate.searchAddress' )}
                              handleRemoveAddress={this.handleRemoveHomeAddress.bind( this )}
                              handleClickOnAddress={this.handleClickOnAddressHome.bind( this )}
                              handleChangeRadius={this.handleChangeRadiusHome.bind( this )}
                              handeGoToMarker={this.handeGoToMarkerHome.bind( this )}
                              changeName={this.handleChangeNameHome.bind( this )}
                              {...this.props}/>
                          );
                        } )
                      }

                      <Row>
                        <Col>
                          <Button
                            size="lg"
                            block
                            variant="primary"
                            onClick={this.handleAddHomeAddress.bind( this )}>
                            {t( 'sites.Asset.assetCreate.addAnotherHomeAddress' )}
                          </Button>
                        </Col>
                      </Row>
                    </Col>

                    <Col md={"6"} className={"pl-md-3"}>
                      <Row>
                        <Col>
                          <h5 className={""}>{t( 'sites.Asset.assetCreate.intermediateTargets' )}:</h5>
                        </Col>
                      </Row>
                      {
                        this.state.poiAddresses.map( ( item, index ) => {
                          return (
                            <AddressListItem
                              key={item.tempId}
                              item={item}
                              index={index}
                              label={t( 'sites.Asset.assetCreate.addPoiAddress' )}
                              placeholder={t( 'sites.Asset.assetCreate.searchAddress' )}
                              handleRemoveAddress={this.handleRemovePoiAddress.bind( this )}
                              handleClickOnAddress={this.handleClickOnAddressPoi.bind( this )}
                              handleChangeRadius={this.handleChangeRadiusPoi.bind( this )}
                              handeGoToMarker={this.handeGoToMarkerPoi.bind( this )}
                              changeName={this.handleChangeNamePoi.bind( this )}
                              {...this.props}/>
                          );
                        } )
                      }
                      <Row>
                        <Col>
                          <Button
                            size="lg"
                            block
                            variant="secondary"
                            onClick={this.handleAddPoiAddress.bind( this )}>
                            {t( 'sites.Asset.assetCreate.addAnotherIntermediateTarget' )}
                          </Button>
                        </Col>
                      </Row>
                    </Col>
                  </Form.Row>

                  <div id={"assetMapContainer"} className={"mb-4"}>
                    <div id="assetMap"/>
                  </div>
                  <Row className="mb-5">
                    <Col>
                      <div className="d-flex flex-column flex-md-row flex-lg-row">
                        <div className="p-1 flex-fill">
                          {this.isUpdateMode &&
                           <Button size={"lg"} variant="danger" block onClick={this.handleDeleteAsset.bind( this )}>{t( 'sites.Asset.assetCreate.deleteAsset' )}</Button>
                          }
                          {!this.isUpdateMode &&
                           <Button size={"lg"} variant="light" block onClick={this.handleDiscardAsset.bind( this )}>{t( 'sites.Asset.assetCreate.discardAsset' )}</Button>
                          }
                        </div>
                        <div className="p-1 flex-fill">

                          {this.isUpdateMode &&
                           <Button size={"lg"} variant="primary" block onClick={this.handleAbortEditAsset.bind( this )}>{t( 'sites.Asset.assetCreate.abortEditAsset' )}</Button>
                          }
                          {!this.isUpdateMode &&
                           <Button size={"lg"} variant="primary" block onClick={this.handleSaveAssetNew.bind( this )}>{t( 'sites.Asset.assetCreate.saveAndCreate' )}</Button>
                          }
                        </div>
                        <div className="p-1 flex-fill">
                          <Button size={"lg"} variant="secondary" block onClick={this.handleSaveAsset.bind( this )}>{((this.isUpdateMode) ? t( 'sites.Asset.assetCreate.updateAsset' ) : t( 'sites.Asset.assetCreate.createAsset' ))}</Button>
                        </div>
                      </div>
                    </Col>
                  </Row>
                </Form>
              </Col>

              <CreateUpdateAssetTypeModal {...this.props} showModal={this.state.showCreateAssetModal} closeModal={this.handleCloseCreateAssetModal.bind( this )} onSave={this.handleOnSaveAssetType.bind( this )}/>
            </Row>
          </Container>
        </div>
      </Router>
    )
  }


}

export default withQueryParams( {
                                  stripUnknownKeys: false
                                } )( AssetCreateUpdate )