import React, { Component } from 'react';
import { Button, Card, Col, Container, InputGroup, Row } from "react-bootstrap";
import Form from "react-bootstrap/es/Form";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Loader from "react-loader-spinner";
import { Link } from "react-router-dom";
import withQueryParams from "react-router-query-params";

class User extends Component {

  _isMounted = false;

  constructor() {
    super();

    this.state = {
      items: [],
      filterItems: [],
      searchQuery: ""
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  componentDidMount() {
    this._isMounted = true;
    this.fetchUsers()
  }

  reloadList() {
    this.setState( {items: []} )
    this.fetchUsers()
  }

  checkFilterItem( item, key, value ) {
    if (item[key] === null)
      return false
    let itemValue = item[key].toLowerCase()
    return item[key] !== null && itemValue.indexOf( value.toLowerCase() ) > -1;
  }

  handleChangeSearchInput( e ) {
    let {items} = this.state
    let {target} = e
    let {value} = target
    let that = this
    let filterItems = items.filter( ( item ) => {
      return that.checkFilterItem( item, "displayName", value ) ||
             that.checkFilterItem( item, "mail", value ) ||
             that.checkFilterItem( item, "givenName", value ) ||
             that.checkFilterItem( item, "surname", value )
    } )
    this.setState( {searchQuery: value, filterItems: filterItems} )
  }

  handleGoToDetail( e, item ) {
    const {history} = this.props;

    let pushLocation = {
      pathname: "/update/user/" + item.objectId,
      state: {lastLocation: history.location}
    }
    history.push( pushLocation )
  }

  clearSearch() {
    this.setState( {searchQuery: "", filterItems: []} )
  }

  onPressEnter( e ) {
    if ( e.target.type === 'text' && e.which === 13 /* Enter */ ) {
      this.setState( {
                       searchQuery: e.target.value
                     } );
      this.handleChangeSearchInput( e, undefined, e.target.value )

      e.preventDefault();
    }
  }

  fetchUsers() {
    fetch( '/user-azure-ui-service/Users' )
      .then( res => res.json() )
      .then( ( data ) => {
        if ( data.value !== undefined ) {
          return data.value
        } else {
          return []
        }
      } )
      .then( ( items ) => {
        if ( this._isMounted )
          this.setState( {items} )
      } )
  }


  render() {
    const {t} = this.props;
    const {items, filterItems, searchQuery} = this.state

    const listItems = ((filterItems.length >= 0 && searchQuery !== "") ? filterItems : items)
    const emptyList = ((filterItems.length >= 0 && searchQuery !== "") ? User.renderLoadingFinish( t ) : User.renderLoading())
    return <div className={"site-delivery-detail"}>
      <Container>
        <>
          <Row>
            <Col lg="12 mr-0" id={""}>
              <Row className="mt-md-5 mt-4 mb-md-5 mb-5">
                <Col md="8" className="d-flex">
                  <h1 className={""}>{t( 'sites.UserManagement.userManagement' )}</h1>
                  <button variant="link" size="sm" className="ml-auto mt-0 border-0 justify-content-end clickable-a" onClick={this.reloadList.bind( this )}>
                    <FontAwesomeIcon icon={['far', 'sync-alt']} size="lg" className="mr-1 text-primary"/>
                  </button>
                </Col>
                <Col md="4">
                  <Link to={`/create/user`}>
                    <Button variant="secondary" size="lg" block>{t( 'sites.UserManagement.createUser' )}</Button>
                  </Link>
                </Col>
              </Row>
              <Form>
                <Form.Group as={Row}>
                  <Col>
                    <Form.Label>{t( 'sites.UserManagement.searchForUsers' )}</Form.Label>
                    <InputGroup>
                      <InputGroup.Prepend>
                        <InputGroup.Text className={"cursor-pointer input-group-append--transparent rounded-left border-right-0"} id="basic-addon3">
                          <FontAwesomeIcon icon={['fal', 'search']}/>
                        </InputGroup.Text>
                      </InputGroup.Prepend>
                      <Form.Control
                        size="lg"
                        placeholder={t( 'sites.UserManagement.enterUsernameOrMail' )}
                        id="id"
                        aria-describedby="basic-addon1"
                        className={"border-right-0 border-left-0"}
                        onChange={this.handleChangeSearchInput.bind( this )}
                        onKeyDown={this.onPressEnter.bind( this )}
                        value={this.state.searchQuery}
                      />
                      <InputGroup.Append>
                        <InputGroup.Text className={"cursor-pointer input-group-append--transparent rounded-right border-left-0"} id="basic-addon3" onClick={this.clearSearch.bind( this )}>
                          <FontAwesomeIcon icon={['fal', 'times']}/>
                        </InputGroup.Text>
                      </InputGroup.Append>
                      <Form.Control.Feedback type="invalid">
                        Fehlermeldung kommt hier ...
                      </Form.Control.Feedback>
                    </InputGroup>
                  </Col>
                </Form.Group>
              </Form>

              {listItems.length > 0 &&
               <Row className={"mb-4"}>
                 {
                   listItems.map( ( item, index ) => {
                     let {givenName, surname, bpwMandantClient, bpwMandantSubClient} = item
                     return (
                       <Col md={"4"} key={index}>
                         <Card>
                           <Card.Body>
                             <div className="d-flex mb-2">
                               <div className="pt-2 bd-highlight">
                                 <FontAwesomeIcon icon={['fal', 'user']} size="lg" className="text-primary"/>
                               </div>
                               <div className="ml-auto p-2">
                                 <FontAwesomeIcon icon={['fal', 'door-open']} size="lg" className="text-primary"/>
                               </div>
                             </div>
                             <Row className="">
                               <Col md={"12"}>
                                 <h5>
                                   <span className="text-muted">{t( 'sites.UserManagement.lastName' )}:</span> {givenName}
                                 </h5>
                               </Col>
                               <Col md={"12"}>
                                 <h5>
                                   <span className="text-muted">{t( 'sites.UserManagement.firstGivenName' )}:</span> {surname}
                                 </h5>
                               </Col>
                               {(bpwMandantClient !== undefined && bpwMandantClient !== null && this.props.isAdmin) &&
                                <Col md={"12"}>
                                  <h5>
                                    <span className="text-muted">Mandant:</span> {bpwMandantClient.name}
                                  </h5>
                                </Col>
                               }
                               {(bpwMandantSubClient !== undefined && bpwMandantSubClient !== null && !this.props.isAdmin) &&
                                <Col md={"12"}>
                                  <h5>
                                    <span className="text-muted">{t( 'sites.UserManagement.subMandant' )}:</span> {bpwMandantSubClient.name}
                                  </h5>
                                </Col>
                               }
                               <Col md={"12"}>
                                 <Button className="mt-2" variant="primary" block onClick={( e ) => {
                                   this.handleGoToDetail( e, item )
                                 }}>{t( 'sites.UserManagement.editProfile' )}</Button>
                               </Col>
                             </Row>

                           </Card.Body>
                         </Card>
                       </Col>
                     )
                   } )
                 }
               </Row>
              }
              {listItems.length <= 0 &&
               emptyList
              }
            </Col>
          </Row>

        </>
      </Container>
    </div>
  }

  static renderLoading() {
    return <><Row><Col md={"12"} className={"d-flex justify-content-center mt-5"}><Loader type="Oval" color="#CDDC4B" height={70} width={70}/></Col></Row></>;
  }

  static renderLoadingFinish( t ) {
    return <><Row><Col md={"12"} className={"d-flex flex-column mt-3"}>
      <div className="alert alert-primary" role="alert">
        {t( "sites.Asset.noMoreContentInfo" )}
      </div>
    </Col></Row></>;
  }

  static renderError() {
    return <><Row><Col md={"12"} className={"d-flex flex-column mt-3"}>
      <div className={"alert alert-danger"} role={"alert"}>There was a technical problem. Please contact our <a href="/" className={"alert-link"}>Support Team</a>. Or just reload and try it again.</div>
    </Col></Row></>;
  }

}

export default withQueryParams( {
                                  stripUnknownKeys: true
                                } )( User )
