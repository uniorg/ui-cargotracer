import React, { Component } from 'react';
import Map from '../services/locationLeaFlet';
import $ from 'jquery'


class Dashboard extends Component {

  dashboardMap = null

  constructor( props ) {
    super( props );

  }

  render() {
    return (
      <div id="dashboardMap" style={{"height": this.props.containerHeight, "width": "100%"}}/>
    )
  }

  updateOrSetAfterSetState() {
    if ( this.dashboardMap !== undefined && this.dashboardMap !== null ) {
      this.dashboardMap.map.remove();
      $( "#dashboardMap" ).html( "" );
    }
    this.dashboardMap = new Map( {startLatitude: '50.8038972', startLongitude: '7.1723139', startZoom: '5', containerId: 'dashboardMap', mapType: 'openStreet'} );
    this.dashboardMap.startMap();
  }


  componentDidMount() {
    this.updateOrSetAfterSetState()
  }

  componentDidUpdate() {
    this.updateOrSetAfterSetState()
  }

}

export default Dashboard