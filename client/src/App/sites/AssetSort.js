import React, { Component } from 'react';
import { Accordion, Button, ButtonGroup, Card, Col, Container, OverlayTrigger, Row, ToggleButton, Tooltip } from "react-bootstrap";
import Form from "react-bootstrap/es/Form";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import CreateUpdateAssetTypeModal from "../component/CreateUpdateAssetTypeModal";
import { Route } from 'react-router-dom'


class AssetSort extends Component {

  constructor( props ) {
    super( props );
    this.state = {
      width: 0,
      height: 0,
    };
  }

  render() {
    const {t} = this.props;
    return <div className={"site-delivery-detail"}>
      <Container>
        <>
          <Row>
            <Col lg="8 mr-0" id={"delivery-list-container"}>
              <Row className="mt-md-5 mt-4 mb-md-5 mb-5">
                <Col md="8" className="d-flex">
                  <h1 className={""}>{t( 'sites.Asset.actualStatment' )}</h1>
                  <a href={"/"} variant="link" size="sm" className="ml-auto mt-3 border-0 justify-content-end clickable-a">
                    <FontAwesomeIcon icon={['far', 'sync-alt']} size="lg" className="mr-1 text-primary"/>
                  </a>
                </Col>
                <Col md="4">
                  <Button variant="secondary" size="lg" block>{t( 'sites.Asset.createAsset' )}</Button>
                </Col>
              </Row>
              <Row>
                <Col>
                  <Accordion defaultActiveKey="0" id={"accordion"} className="mt-4 mb-5">
                    <Card className={"card--filter card__border-radius"}>
                      <Accordion.Toggle as={Card.Header} variant="link"
                                        className="card-header--transparent border-bottom-0 pb-2"
                                        eventKey="0"
                                        data-toggle="collapse"
                                        data-target="#filterCollapse"
                                        aria-expanded="true"
                                        aria-controls="0">
                        <div className="d-flex">
                          <div className="pt-2 pr-2">
                            <FontAwesomeIcon icon={['fal', 'sort-amount-down']} size="lg"/>
                          </div>
                          <div className="pt-1 pl-1 mb-0">
                            <h3>
                              {t( 'sites.Asset.sortResults' )}
                            </h3>
                          </div>
                          <div className="ml-auto pt-2">
                            <FontAwesomeIcon icon={['fal', 'chevron-down']} size="lg"/>
                          </div>
                        </div>
                      </Accordion.Toggle>
                      <Accordion.Collapse eventKey="0"
                                          id="filterCollapse"
                                          className="collapse show"
                                          aria-labelledby="1"
                                          data-parent="#accordion">
                        <Card.Body className="pt-2">
                          <Row>
                            <Col md={"12"}>
                              <Form.Row>
                                <Form.Group as={Col} md={"6"} controlId="formGridAssetType">
                                  <Form.Label>{t( 'sites.Asset.sortBy' )}</Form.Label>
                                  <Form.Control
                                    type="selection"
                                    as="select"
                                    required="required"
                                    className="form-control"
                                    placeholder={t( 'sites.Asset.filterFeatures' )}>
                                    <option value="">{t( 'sites.Asset.filterFeatures' )}</option>
                                    <option value={t( 'sites.Asset.rounds' )}>{t( 'sites.Asset.rounds' )}</option>
                                    <option value={t( 'sites.Asset.shocks' )}>{t( 'sites.Asset.shocks' )}</option>
                                    <option value={t( 'sites.Asset.lastTransmissionTime' )}>{t( 'sites.Asset.lastTransmissionTime' )}</option>
                                    <option value={t( 'sites.Asset.battery' )}>{t( 'sites.Asset.battery' )}</option>
                                  </Form.Control>
                                </Form.Group>
                                <Form.Group as={Col} md={"6"} controlId="formGridAddress1" className="d-flex align-self-end flex-fill">
                                  <ButtonGroup toggle className="flex-grow-1" size={"md"} block>
                                    <ToggleButton type="radio" name="radio" defaultChecked value="1" variant="primary" className="">
                                      {t( 'sites.Asset.ascending' )}
                                    </ToggleButton>
                                    <ToggleButton type="radio" name="radio" value="2" variant="outline-primary" className="">
                                      {t( 'sites.Asset.descending' )}
                                    </ToggleButton>
                                  </ButtonGroup>
                                </Form.Group>
                              </Form.Row>
                            </Col>
                          </Row>
                          <Row className="mb-1">
                            <Col>
                              <div className="d-flex flex-column flex-md-row flex-lg-row">
                                <div className="p-1 flex-fill">
                                  <Button size={"md"} variant="light" type="submit" block>{t( 'sites.Asset.resetSorting' )}</Button>
                                </div>
                                <div className="p-1 flex-fill">
                                  <Button size={"md"} variant="primary" type="submit" block>{t( 'sites.Asset.sortResults' )}</Button>
                                </div>
                              </div>
                            </Col>
                          </Row>
                        </Card.Body>
                      </Accordion.Collapse>
                    </Card>
                  </Accordion>
                </Col>
              </Row>
              <Row className={"popovers-maps--tooltip"}>
                <Col md={"4"}>
                  <Row className="mb-3">
                    <Col md={"12"}>
                      <Button size={"sm"} variant="primary" type="submit" block>146</Button>
                    </Col>
                  </Row>
                  <Row className="mb-3">
                    <Col md={"12"}>
                      <div className="d-flex flex-column">
                        <div className="text-muted">
                          <h6>{t( 'sites.Asset.type' )}:</h6>
                        </div>
                      </div>
                      <div className="d-flex">
                        <div className="p-1 flex-shrink-1">
                          <OverlayTrigger overlay={<Tooltip id="tooltip-gps">{t( 'sites.Asset.tooltip' )}</Tooltip>}>
                            <span className="d-inline-block"
                                  tabIndex="0"
                                  data-toggle="tooltip"
                                  title={t( 'sites.Asset.tooltip' )}>
                              <FontAwesomeIcon icon={['fal', 'map-marker-alt']} size="lg" className="text-primary"/>
                            </span>
                          </OverlayTrigger>
                        </div>
                        <div className="p-1 ">
                          <p>GPS:</p>
                        </div>
                        <div className="flex-grow-1">
                          <Button size={"sm"} variant="light" type="submit" className="flex-fill" block>
                            {t( 'sites.Asset.copyLink' )}
                          </Button>
                        </div>
                      </div>
                    </Col>
                  </Row>

                  <Row className="mb-3">
                    <Col md={"12"}>
                      <div className="d-flex flex-column">
                        <div className="text-muted">
                          <h6>{t( 'sites.Asset.time' )}:</h6>
                        </div>
                      </div>
                      <div className="d-flex">
                        <div className="p-1 flex-shrink-1">
                          <OverlayTrigger overlay={<Tooltip id="tooltip-clock">{t( 'sites.Asset.tooltip' )}</Tooltip>}>
                            <span className="d-inline-block"
                                  tabIndex="1"
                                  data-toggle="tooltip"
                                  title={t( 'sites.Asset.tooltip' )}>
                              <FontAwesomeIcon icon={['fal', 'clock']} size="lg" className="text-primary"/>
                            </span>
                          </OverlayTrigger>
                        </div>
                        <div className="p-1 flex-grow-1">
                          <p>02.07.2019 19:54</p>
                        </div>
                      </div>
                    </Col>
                  </Row>

                  <Row className="mb-3">
                    <Col md={"12"}>
                      <div className="d-flex flex-column">
                        <div className="text-muted">
                          <h6>{t( 'sites.Asset.additionalInformation' )}:</h6>
                        </div>
                      </div>
                      <div className="d-flex">
                        <div className="p-1 flex-shrink-1">
                          <OverlayTrigger overlay={<Tooltip id="tooltip-battery">{t( 'sites.Asset.tooltip' )}</Tooltip>}>
                            <span className="d-inline-block"
                                  tabIndex="2"
                                  data-toggle="tooltip"
                                  title={t( 'sites.Asset.tooltip' )}>
                              <FontAwesomeIcon icon={['fal', 'battery-quarter']} size="lg" className="text-primary"/>
                            </span>
                          </OverlayTrigger>
                        </div>
                        <div className="p-1 flex-grow-1">
                          <p>2900 mV</p>
                        </div>
                      </div>
                      <div className="d-flex">
                        <div className="p-1 flex-shrink-1 justify-content-center">
                          <OverlayTrigger overlay={<Tooltip id="tooltip-tempeture">{t( 'sites.Asset.tooltip' )}</Tooltip>} className="">
                            <span className="d-inline-block"
                                  tabIndex="3"
                                  data-toggle="tooltip"
                                  title={t( 'sites.Asset.tooltip' )}>
                              <FontAwesomeIcon icon={['fal', 'temperature-high']} size="lg" className="text-primary"/>
                            </span>
                          </OverlayTrigger>
                        </div>
                        <div className="p-1 flex-grow-1">
                          <p>20.8°</p>
                        </div>
                      </div>
                      <div className="d-flex">
                        <div className="p-1 flex-shrink-1 justify-content-center">
                          <OverlayTrigger overlay={<Tooltip id="tooltip-bolt">{t( 'sites.Asset.tooltip' )}</Tooltip>} className="">
                            <span className="d-inline-block"
                                  tabIndex="4"
                                  data-toggle="tooltip"
                                  title={t( 'sites.Asset.tooltip' )}>
                              <FontAwesomeIcon icon={['fal', 'bolt']} size="lg" className="text-primary"/>
                            </span>
                          </OverlayTrigger>
                        </div>
                        <div className="p-1 flex-grow-1">
                          <p>5</p>
                        </div>
                      </div>
                      <div className="d-flex">
                        <div className="p-1 flex-shrink-1 justify-content-center">
                          <OverlayTrigger overlay={<Tooltip id="tooltip-info">{t( 'sites.Asset.tooltip' )}</Tooltip>} className="">
                            <span className="d-inline-block"
                                  tabIndex="5"
                                  data-toggle="tooltip"
                                  title={t( 'sites.Asset.tooltip' )}>
                              <FontAwesomeIcon icon={['fal', 'info-circle']} size="lg" className="text-primary"/>
                            </span>
                          </OverlayTrigger>
                        </div>
                        <div className="p-1 flex-grow-1">
                          <p>3659994</p>
                        </div>
                      </div>
                    </Col>
                  </Row>


                </Col>
              </Row>
            </Col>
            <Col lg="4 shadow-bpw-lg p-0">
              <div id="deliveryMap"/>
            </Col>
          </Row>

          <Route path={`${this.props.match.path}/modal/:id`} component={CreateUpdateAssetTypeModal}/>

        </>
      </Container>
    </div>
  }
}

export default AssetSort