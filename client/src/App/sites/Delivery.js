import React, { Component } from 'react';
import queryString from 'query-string'
import { createSorter } from '../util/Sort';
import i18n from "i18next";
import { Breadcrumb, Button, Col, Container, Form, Row } from 'react-bootstrap'
import withQueryParams from 'react-router-query-params';
import Loader from 'react-loader-spinner'


import ModuleSearch from '../component/ModuleSearch'
import ModuleFilter from '../component/ModuleFilter'
import DeliveryListItem from '../component/DeliveryListItem'

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import $ from "jquery";
import Map from '../services/locationLeaFlet';
import InfiniteScroll from "react-infinite-scroll-component";
import { Link } from "react-router-dom";
import ReactPullToRefresh from 'react-pull-to-refresh'


class Delivery extends Component {
  static defaultProps = {
    filters: [{
      property: "deliveryNumber",
      value: "86585"
    }],

    sorters: [{
      property: "orders",
      direction: 'DESC'
    }, {
      property: "arrivalTimeTo",
      direction: 'DESC'
    }]
  }

  deliveryMap = null
  _isMounted = false;

  constructor( props ) {
    super( props );
    this.state = {
      width: 0,
      height: 0,

      list: [],
      count: 0,
      hasMoreItems: true,
      nextUrlQuery: null,

      filters: this.props.filters,
      sorters: this.props.sorters,
      search: "*",
      groupByFilter: "",
      checkFilter: "",
      searchInputValue: ""
    };

  }

  static getGroupByButtons() {
    let groupButtons = [];
    groupButtons.push( {label: i18n.t( 'sites.Delivery.groupByFilter.item1' ), groupby: "orderNumber"} )
    groupButtons.push( {label: i18n.t( 'sites.Delivery.groupByFilter.item2' ), groupby: "transportNumber"} )
    groupButtons.push( {label: i18n.t( 'sites.Delivery.groupByFilter.item3' ), groupby: "deliveryNumber"} )

    return groupButtons
  }

  static renderLoading() {
    return <><Row><Col md={"12"} className={"d-flex justify-content-center mt-5"}><Loader type="Oval" color="#CDDC4B" height={70} width={70}/></Col></Row></>;
  }

  static renderLoadingFinish( t ) {
    return <><Row><Col md={"12"} className={"d-flex flex-column mt-3"}>
      <div className="alert alert-primary" role="alert">
        {t( "sites.Delivery.noMoreContentInfo" )}
      </div>
    </Col></Row></>;
  }

  static renderError() {
    return <><Row><Col md={"12"} className={"d-flex flex-column mt-3"}>
      <div className={"alert alert-danger"} role={"alert"}>There was a technical problem. Please contact our <a href="/" className={"alert-link"}>Support Team</a>. Or just reload and try it again.</div>
    </Col></Row></>;
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  componentDidUpdate() {
    if ( !this.props.isMobile ) {
      if ( this.deliveryMap !== undefined && this.deliveryMap !== null ) {
        this.deliveryMap.map.remove();
        $( "#deliveryMap" ).html( "" );
      }
      this.deliveryMap = new Map( {startLatitude: '50.8038972', startLongitude: '7.1723139', startZoom: '5', containerId: 'deliveryMap', mapType: 'openStreet'} );
      this.deliveryMap.startMap();
    }
  }

  async componentDidMount() {
    this._isMounted = true;
    await this.setUrlStates();
    await this.getList()
  }

  async setUrlStates() {
    const values = queryString.parse( this.props.location.search )
    if ( values.search !== undefined ) {
      await this.setState( {search: values.search} );
    }
    if ( values.groupByFilter !== undefined ) {
      await this.setState( {groupByFilter: values.groupByFilter} );
    }
    if ( values.checkFilter !== undefined ) {
      await this.setState( {checkFilter: values.checkFilter} );
    }
  }

  onLoad = ( data ) => {
    const {list} = this.state

    if ( this._isMounted ) {
      if ( data.value !== undefined && data.value.length === 0 ) {
        this.setState( {
                         hasMoreItems: false,
                         nextUrlQuery: null,
                         count: 0
                       } );
      }
      if ( list.length < data.count ) {
        let returnList = list
        let listFinish = this.parseData( data.value !== undefined ? data.value : data )
        listFinish.map( ( item, index ) => {
          returnList.push( item )
          return item
        } )
        this.setState( {
                         list: returnList,
                         count: data.count,
                         nextUrlQuery: data.nextUrlQuery !== undefined ? data.nextUrlQuery : null
                       } );
      } else {
        this.setState( {
                         hasMoreItems: false,
                         nextUrlQuery: null
                       } );
      }
    }
  }

  parseData( data ) {
    const {sorters} = this.state;

    if ( data && data.length && Array.isArray( sorters ) && sorters.length ) {
      data.sort( createSorter( ...sorters ) );
    }

    return data;
  }

  // Retrieves the list of items from the Express app
  async getList( compleation ) {
    const {search, checkFilter, nextUrlQuery} = this.state;
    let queryString = ""
    if ( search !== "" ) {
      queryString = "search=" + search
    }
    if ( checkFilter !== "" ) {
      queryString = queryString + ((search !== "") ? "&" : "") + "filter=" + checkFilter + "&skip=0&top=10"
    } else {
      queryString = queryString + "&skip=0&top=10"
    }
    if ( nextUrlQuery ) {
      queryString = nextUrlQuery;
    }
    fetch( '/azureSearch/delivery/services.svc?' + queryString )
      .then( res => res.json() )
      .then( ( data ) => {
        this.onLoad( data )
        if ( compleation !== undefined ) {
          compleation();
        }
      } )
  }

  handleRefresh( resolve, reject ) {
    this.setState( {
                     list: [],
                     hasMoreItems: true,
                     nextUrlQuery: null
                   } );
    this.getList( () => {
      resolve()
    } )
  }

  async handleReloadList() {
    await this.setState( {
                           list: [],
                           hasMoreItems: true,
                           nextUrlQuery: null
                         } );
    await this.getList()
  }

  render() {
    const {list} = this.state;

    return list ?
      this.renderData( list ) :
      Delivery.renderLoading();
  }

  async handleClickSearchItem( e, item, search ) {
    await this.setState( {
                           list: [],
                           hasMoreItems: true,
                           nextUrlQuery: null,
                           search: search
                         } )
    await this.getList()
    const values = queryString.parse( this.props.location.search )
    if ( item !== undefined ) {
      values.search = item.deliveryNumber
    } else {
      values.search = search
    }
    const {history, location} = this.props;
    history.push( {
                    pathname: location.pathname,
                    search: "?" + queryString.stringify( values ),
                    hash: "",
                    state: {fromDashboard: false}
                  } )
  }

  async handleClickSearchItem2( e, item ) {
    const {history} = this.props;
    history.push( {
                    pathname: this.props.match.url + "/" + item.id,
                    search: "",
                    hash: ""
                  } )
  }

  async handelFilterItems( filterString ) {
    await this.setState( {
                           list: [],
                           hasMoreItems: true,
                           nextUrlQuery: null,
                           checkFilter: filterString
                         } )
    const values = queryString.parse( this.props.location.search )
    values.checkFilter = filterString
    const {history, location} = this.props;
    history.push( {
                    pathname: location.pathname,
                    search: "?" + queryString.stringify( values ),
                    hash: "",
                    state: {fromDashboard: false}
                  } )
    this.getList()
  }

  handleOnClickRemoveSearch() {
    const values = queryString.parse( this.props.location.search )
    values.search = undefined
    const {history, location} = this.props;
    history.push( {
                    pathname: location.pathname,
                    search: "?" + queryString.stringify( values ),
                    hash: "",
                    state: {fromDashboard: false}
                  } )
  }

  async handleOnClickRemoveFilter() {

    window.location.href = window.location.origin + window.location.pathname
  }

  getCheckByElements() {
    const values = queryString.parse( this.props.location.search )
    let filter
    if ( values.checkFilter !== undefined ) {
      filter = values.checkFilter
    }
    let checkBoxes = []
    checkBoxes.push( {value: 1, label: i18n.t( 'sites.Delivery.characteristicsFilter.item1' ), operator: "OR", where: "deliveryState eq 'SCANNED'", checked: (filter !== undefined && filter.indexOf( "deliveryState eq 'SCANNED'" ) > -1)} )
    checkBoxes.push( {value: 2, label: i18n.t( 'sites.Delivery.characteristicsFilter.item2' ), operator: "OR", where: "deliveryState eq 'ON_THE_WAY'", checked: (filter !== undefined && filter.indexOf( "deliveryState eq 'ON_THE_WAY'" ) > -1)} )
    checkBoxes.push( {value: 3, label: i18n.t( 'sites.Delivery.characteristicsFilter.item3' ), operator: "OR", where: "deliveryState eq 'ARRIVED'", checked: (filter !== undefined && filter.indexOf( "deliveryState eq 'ARRIVED'" ) > -1)} )
    checkBoxes.push( {value: 4, label: i18n.t( 'sites.Delivery.characteristicsFilter.item4' ), operator: "AND", where: "isLate eq true", checked: (filter !== undefined && filter.indexOf( "isLate eq true" ) > -1)} )
    checkBoxes.push( {value: 5, label: i18n.t( 'sites.Delivery.characteristicsFilter.item5' ), operator: "AND", where: "vibrationCount gt 0", checked: (filter !== undefined && filter.indexOf( "vibrationCount gt 0" ) > -1)} )
    return checkBoxes
  }

  renderData( data ) {
    const {hasMoreItems, list, count} = this.state;
    const {t, location} = this.props;
    const values = queryString.parse( location.search )
    return (
      <div className={"site-delivery"}>
        <Container>
          <>
            <Row>
              <ReactPullToRefresh
                id={"delivery-list-container"}
                onRefresh={this.handleRefresh.bind( this )}
                className="col-lg-8 mr-0"
                loading={<div className="loading"><FontAwesomeIcon icon={['fal', 'spinner']} className="mr-1" spin size="lg"/></div>}
                style={{"height": this.props.containerHeight}}>
                {this.props.isMobile ?
                  (

                    <h3>Pull down to refresh</h3>
                  ) : ''}
                <Row className="mt-4 mb-md-4 mb-4">
                  <Col md="8">
                    <h1 className={"mb-sm-0 mb-4"}>{t( 'sites.Delivery.listTitle' )}</h1>
                  </Col>
                  <Col md="4">
                    {this.props.isClient &&
                     <Link to={`/create/delivery`}>
                       <Button variant="secondary" size="lg" block>{t( 'sites.Delivery.createDelivery' )}</Button>
                     </Link>
                    }
                  </Col>
                </Row>
                <Row className="mb-4">
                  <Col>
                    <ModuleSearch
                      placeholder={t( 'sites.Delivery.ModuleSearch.placeholder' )}
                      buttonText={t( 'sites.Delivery.ModuleSearch.searchButton' )}
                      label={t( 'sites.Delivery.ModuleSearch.label' )}
                      searchTitle={t( 'sites.Delivery.searchTitle' )}
                      searchInputValue={values.search}
                      handleClickSearchItem={this.handleClickSearchItem.bind( this )}
                      handleOnClickRemoveSearch={this.handleOnClickRemoveSearch.bind( this )}
                      {...this.props}
                    />
                  </Col>
                </Row>
                <Row className="mb-4">
                  <Col>
                    <ModuleFilter
                      groupByButtons={Delivery.getGroupByButtons()}
                      checkBoxes={this.getCheckByElements()}
                      handelFilterItems={this.handelFilterItems.bind( this )}
                      handleOnClickRemoveFilter={this.handleOnClickRemoveFilter.bind( this )}
                      location={this.props.location.search}
                      {...this.props}
                    />
                  </Col>
                </Row>
                <Row className="mb-3">
                  <Col>
                    <div className="d-flex flex-column flex-md-row flex-lg-row align-self-center">
                      <div className="pl-0 p-sm-2 flex-column">
                        <Form.Label>{t( 'sites.Delivery.actualStatment' )}</Form.Label>
                      </div>
                      <div className="d-flex flex-row flex-grow-1">
                        <>
                          <Breadcrumb className="breadcumb__main-site--headline">
                            {/*<Breadcrumb.Item href="#">TyssenKrupp</Breadcrumb.Item>*/}
                            <Breadcrumb.Item href="https://getbootstrap.com/docs/4.0/components/breadcrumb/" active>{t( 'sites.Delivery.breadcrumbDelivery' )} ({count})</Breadcrumb.Item>
                          </Breadcrumb>
                        </>
                        <div className="ml-auto pt-1 justify-content-end">
                          {/*TODO: Implement in v2.0*/}
                          {/*<Button variant="secondary" size="sm" className="btn-light mr-2">
                            <FontAwesomeIcon icon={['fal', 'times']} className="mr-1"/>
                            {t( 'sites.Delivery.deliveryItem.isLateItemClose' )}
                          </Button>*/}
                          <Button variant="primary" size="sm" className="btn-primary btn-round" onClick={this.handleReloadList.bind( this )}>
                            <FontAwesomeIcon icon={['fal', 'sync-alt']} className="mr-1"/>
                            {t( 'sites.Delivery.deliveryItem.isLateItemSync' )}
                          </Button>
                        </div>
                      </div>
                    </div>
                  </Col>
                </Row>

                <InfiniteScroll
                  dataLength={list.length}
                  next={this.getList.bind( this )}
                  hasMore={hasMoreItems}
                  loader={Delivery.renderLoading()}
                  endMessage={Delivery.renderLoadingFinish( t )}
                  scrollableTarget="delivery-list-container"
                  scrollThreshold={1}
                >
                  <Row>
                    {
                      this.state.list.map( ( item, index ) => {
                        return (
                          <DeliveryListItem key={item.id} item={item} index={index} url={this.props.match.url} {...this.props}/>
                        );
                      } )
                    }
                  </Row>
                </InfiniteScroll>
              </ReactPullToRefresh>
              {!this.props.isMobile ?
                (
                  <Col lg="4 shadow-bpw-lg p-0">
                    <div id="deliveryMap"/>
                  </Col>
                ) : ''}

            </Row>
          </>
        </Container>

      </div>
    )
  }
}

export default withQueryParams( {
                                  stripUnknownKeys: true,
                                  keys: {
                                    search: {
                                      validate: value => !!value && value.length > 1,
                                    },
                                    checkFilter: {
                                      defaulty: "(deliveryState ne 'DONE' and deliveryState ne 'UNKNOWN' and activeDeviceAssignments gt 0)",
                                      defaultx: "(deliveryState ne 'UNKNOWN')",
                                      validate: value => !!value && value.length > 0,
                                    }
                                  }
                                } )( Delivery )