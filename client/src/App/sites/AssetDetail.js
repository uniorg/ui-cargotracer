import React, { Component } from 'react';
import { Button, ButtonGroup, Col, Container, Row, Tab, Tabs } from 'react-bootstrap';
import Loader from 'react-loader-spinner';
import withQueryParams from 'react-router-query-params';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import queryString from 'query-string';
import i18n from 'i18next';
import $ from 'jquery';
import Map from '../services/locationLeaFlet';
import AssetPopOver from '../component/AssetPopOver';
import TabSelectedCycle from '../component/TabSelectedCycle'
import TabAssetInformation from '../component/TabAssetInformation'
import TabCycles from '../component/TabCycles'
import { Helper } from '../util/Helper'
import { Link } from "react-router-dom";
import moment from 'moment';

class AssetDetail extends Component {

  assetMap = null
  _isMounted = false;
  assetMarkerKey = "AssetMarker"
  assetCycleGeoMessagesMarkerKey = "assetCycleGeoMessages"
  assetCycleGeoMessages = []

  constructor( props ) {
    super( props );
    this.state = {
      width: 0,
      height: 0,
      loadCycle: false,
      assetNotFound: false,
      item: null,
      selectedCycleId: null,
      defaultActiveKey: "assetInformation",
      activeKey: "assetInformation",
      cycle: null,
      cycles: [],
      assetCycleGeoMessages: [],

      filters: this.props.filters,
      sorters: this.props.sorters,
      search: "*",
      groupByFilter: "",
      checkFilter: "",
      searchInputValue: ""
    };

  }

  static renderLoading() {
    return <><Row><Col md={"12"} className={"d-flex justify-content-center mt-5"}><Loader type="Oval" color="#CDDC4B" height={70} width={70}/></Col></Row></>;
  }

  static renderAssetNotFound( t ) {
    return <><Container><Row><Col md={"12"} className={"d-flex flex-column mt-3"}>
      <div className={"alert alert-danger"} role={"alert"}>{t( "sites.Asset.assetNotExist" )}</div>
    </Col></Row></Container></>;
  }

  static renderLoadingFinish( t ) {
    return <><Row><Col md={"12"} className={"d-flex flex-column mt-3"}>
      <div className="alert alert-primary" role="alert">
        {t( "sites.Asset.noMoreContentInfo" )}
      </div>
    </Col></Row></>;
  }

  static renderError() {
    return <><Row><Col md={"12"} className={"d-flex flex-column mt-3"}>
      <div className={"alert alert-danger"} role={"alert"}>There was a technical problem. Please contact our <a href="/" className={"alert-link"}>Support Team</a>. Or just reload and try it again.</div>
    </Col></Row></>;
  }

  async componentDidMount() {
    this._isMounted = true;
    await this.fetchAsset()
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  componentDidUpdate() {
    let that = this
    if ( !this.props.isMobile && this._isMounted && $( '#assetMap' ).length > 0 ) {
      if ( this.assetMap !== undefined && this.assetMap !== null ) {
        this.assetMap.map.remove();
        $( "#assetMap" ).html( "" );
      }
      this.assetMap = new Map( {startLatitude: '50.8038972', startLongitude: '7.1723139', startZoom: '5', containerId: 'assetMap', mapType: 'openStreet'} );
      this.assetMap.startMap( () => {
        if ( this.props.match.params.cycle !== undefined && this.state.loadCycle ) {
          this.fetchCycle( this.props.match.params.cycle )
          this.fetchAssetCycleGeoMessages( ( assetCycleGeoMessages ) => {
            this.setAssetCycleGeoMessages( assetCycleGeoMessages )
          } )
        } else if ( this.props.match.params.cycle !== undefined && !this.state.loadCycle && this.state.defaultActiveKey === "selectedCycle" ) {
          console.log( this.assetCycleGeoMessages )
          this.setAssetCycleGeoMessages( ((this.assetCycleGeoMessages.length !== 0) ? this.assetCycleGeoMessages : undefined) )
        }
        if ( this.state.defaultActiveKey === "assetInformation" || this.state.defaultActiveKey === "cycles" ) {
          let {lastValidGeoPositionLatitude, lastValidGeoPositionLongitude, name, deviceId, assetTypeName, lastValidGeoPositionType, id, lastValidGeoPositionSentAt} = this.state.item
          let color = ((lastValidGeoPositionType === "SIGFOX") ? "#000000" : "#0d6e96")
          let assetLastLocationMarker = {
            lat: lastValidGeoPositionLatitude,
            lng: lastValidGeoPositionLongitude,
            title: name,
            circleRadius: undefined,
            popup: "<div class='row m-3'>" +
                   "<div class='col-12'><a href='/assets/" + id + "'><h5>" + name + "</h5></a> </div>" +
                   "<div class='col-12'>Asset Typ: " + assetTypeName + "</div>" +
                   "<div class='col-12'>Device Id: " + deviceId + "</div>" +
                   "<div class='col-12'>Typ: " + lastValidGeoPositionType + "</div>" +
                   "<div class='col-12'>Typ: " + moment( new Date( lastValidGeoPositionSentAt ) ).format( "LLL" ) + "</div>" +
                   "</div>",
            zIndex: 1000,
            icon: {
              google: "BpwPalletMarker",
              leaflet: "<i class=\"fas fa-map-marker-alt\" style='font-size: 1.3rem;color: " + color + ";position:  relative;top: -12px;left: -2px;'></i>"
            }
          }
          const markerIconsStart = {
            google: "BpwPalletMarker",
            leaflet: "<i class=\"fas fa-map-marker-alt\" style='font-size: 1.5rem;color: #129fd8;position:  relative;top: -12px;left: -2px;'></i>"
          };
          this.assetMap.removeMarker( this.assetMarkerKey )
          this.assetMap.removeMarker( this.assetCycleGeoMessagesMarkerKey )
          this.assetMap.setMarkers( [assetLastLocationMarker], this.assetMarkerKey, markerIconsStart, false, true, false );
          setTimeout( () => {
            that.assetMap.setNowMarkerCenter( undefined, true );
          }, 500 )
        }
        this.setHomeAndPoiMarker()
      } );
    }

  }

  setAssetCycleGeoMessages( lastAssetCycleGeoMessages ) {
    let assetCycleGeoMessages = this.state.assetCycleGeoMessages.map( ( assetCycleGeoMessages ) => {
      let {latitude, longitude, name, deviceId, vibrationCount, geoPositionType, id, sentAt} = assetCycleGeoMessages
      let color = ((geoPositionType === "SIGFOX") ? "#000000" : "#ff0009")
      return {
        lat: latitude,
        lng: longitude,
        title: name,
        circleRadius: undefined,
        popup: "<div class='row m-3'>" +
               "<div class='col-12'>Vibration: " + vibrationCount + "</div>" +
               "<div class='col-12'>Device Id: " + deviceId + "</div>" +
               "<div class='col-12'>Typ: " + geoPositionType + "</div>" +
               "<div class='col-12'>Typ: " + moment( new Date( sentAt ) ).format( 'LLL' ) + "</div>" +
               "</div>",
        zIndex: 1000,
        timestamp: new Date( sentAt ),
        source: geoPositionType,
        icon: {
          google: "BpwPalletMarker",
          leaflet: "<i class=\"fas fa-map-pin\" style='font-size: 1.3rem;color: " + color + ";position:  relative;top: -12px;left: -2px;'></i>"
        }
      }
    } )

    if ( lastAssetCycleGeoMessages !== undefined )
      assetCycleGeoMessages = lastAssetCycleGeoMessages.map( ( assetCycleGeoMessages ) => {
        let {latitude, longitude, name, deviceId, vibrationCount, geoPositionType, id, sentAt} = assetCycleGeoMessages
        let color = ((geoPositionType === "SIGFOX") ? "#000000" : "#ff0009")
        return {
          lat: latitude,
          lng: longitude,
          title: name,
          circleRadius: undefined,
          popup: "<div class='row m-3'>" +
                 "<div class='col-12'>Vibration: " + vibrationCount + "</div>" +
                 "<div class='col-12'>Device Id: " + deviceId + "</div>" +
                 "<div class='col-12'>Typ: " + geoPositionType + "</div>" +
                 "<div class='col-12'>Typ: " + moment( new Date( sentAt ) ).format( 'LLL' ) + "</div>" +
                 "</div>",
          zIndex: 1000,
          timestamp: new Date( sentAt ),
          source: geoPositionType,
          icon: {
            google: "BpwPalletMarker",
            leaflet: "<i class=\"fas fa-map-pin\" style='font-size: 1.3rem;color: " + color + ";position:  relative;top: -12px;left: -2px;'></i>"
          }
        }
      } )
    if ( this.assetCycleGeoMessages === undefined )
      this.assetCycleGeoMessages = []
    this.assetCycleGeoMessages = this.assetCycleGeoMessages.concat( assetCycleGeoMessages )

    this.setMarker()
  }

  setHomeAndPoiMarker() {
    let that = this
    let {item} = this.state
    let {homeAddresses, poiAddresses} = item

    homeAddresses.map( function( obj ) {
      that.setMarkerFromAddressItemWithRadius( obj, obj.geofence.radius, "HOME", false )
      return obj
    } );
    poiAddresses.map( function( obj ) {
      that.setMarkerFromAddressItemWithRadius( obj, obj.geofence.radius, "POI", false )
      return obj
    } );
  }

  setMarkerFromAddressItemWithRadius( item, radius, addressType, withCenter ) {
    const {id} = item;

    const marker = this.getMarker( item, radius, addressType );
    const markerIconsStart = {
      google: "BpwPalletMarker",
      leaflet: "<i class=\"fas fa-map-marker-alt\" style='font-size: 1.5rem;color: #129fd8;position:  relative;top: -12px;left: -2px;'></i>"
    };
    this.assetMap.removeMarker( id )
    this.assetMap.setMarkers( [marker], id, markerIconsStart, false, true, false );
    if ( withCenter === undefined || withCenter )
      this.assetMap.setNowMarkerCenter( id, true );
  }

  getMarker( item, radius, addressType ) {
    const {street, streetNumber, city, postalCode, geofence, tempId} = item;
    const {latitude, longitude} = geofence;

    let streetNumberChecked = ((streetNumber !== undefined && streetNumber !== null) ? " " + streetNumber : "")
    let color = ((addressType === "HOME") ? "#002851" : "#CDDC4B")
    const marker = {
      tempId: tempId,
      lat: latitude,
      lng: longitude,
      title: city,
      popup: "<div class='row m-3'>" +
             "<div class='col-12'>" + street + streetNumberChecked + ", </br>" + postalCode + " " + city + "</div>" +
             "</div>",
      circleRadius: radius,
      circleRadiusColor: color,
      zIndex: 1000,
      icon: {
        google: "BpwPalletMarker",
        leaflet: "<i class=\"fas fa-circle\" style='font-size: 1rem;color: " + color + ";position:  relative;top: -1.5px;left: -2px;'></i>"
      }
    };

    return marker
  }

  setMarker() {
    let that = this
    if ( this.assetCycleGeoMessages !== undefined && this.assetCycleGeoMessages.length > 0 ) {
      const markerIconsStart = {
        google: "BpwPalletMarker",
        leaflet: "<i class=\"fas fa-map-marker-alt\" style='font-size: 1.5rem;color: #129fd8;position:  relative;top: -12px;left: -2px;'></i>"
      };
      this.assetMap.removeMarker( this.assetMarkerKey )
      this.assetMap.removeMarker( this.assetCycleGeoMessagesMarkerKey )
      this.assetMap.setMarkers( this.assetCycleGeoMessages, this.assetCycleGeoMessagesMarkerKey, markerIconsStart, false, true, true );
      setTimeout( () => {

        that.assetMap.setNowMarkerCenter( undefined, true );
      }, 500 )
    }
  }

  fetchCycle( cycleId, callback ) {
    fetch( '/asset-ui-service/asset/cycle/' + cycleId )
      .then( res => res.json() )
      .then( ( cycle ) => {
        if ( this._isMounted ) {
          this.setState( {
                           cycle: cycle,
                           loadCycle: false,
                           selectedCycleId: cycleId
                         } )
          if ( callback !== undefined )
            callback()
        }
      } )
  }

  fetchAssetCycleGeoMessages( callback ) {
    let that = this
    let {selectedCycleId, item} = this.state
    let {id} = item

    fetch( '/asset-ui-service/asset/assetGeoMessages/' + id + '/' + selectedCycleId + '/null/null' )
      .then( res => res.json() )
      .then( ( data ) => {
        if ( data.value !== undefined ) {
          return data.value
        } else {
          return []
        }
      } )
      .then( ( assetCycleGeoMessages ) => {
        if ( this._isMounted ) {
          this.assetCycleGeoMessages = assetCycleGeoMessages
          this.setState( {
                           assetCycleGeoMessages: assetCycleGeoMessages
                         } )
          if ( callback !== undefined )
            callback( assetCycleGeoMessages )
          //this.setAssetCycleGeoMessages()
        }
      } )
  }

  setAddressCenter( address ) {
    let {id} = address
    this.assetMap.setNowMarkerCenter( id, true );

  }

  fetchCycles( assetId, sortKey, sortDirection, skip, top ) {

    if ( sortKey == undefined )
      sortKey = 'cycleNumber'
    if ( sortDirection == undefined )
      sortDirection = 'desc'
    if ( skip == undefined )
      skip = '0'
    if ( top == undefined )
      top = '500'
    fetch( '/asset-ui-service/asset/assetCycleList/' + assetId + '/' + sortKey + '/' + sortDirection + '/' + skip + '/' + top )
      .then( res => res.json() )
      .then( ( data ) => {
        return data.value !== undefined ? data.value : []
      } )
      .then( ( cycles ) => {

        if ( this._isMounted ) {
          this.setState( {
                           cycles: cycles,
                           loadCycle: false
                         } )
        }
      } )
  }

  fetchAsset( compleation ) {
    fetch( '/asset-ui-service/asset/detail/' + this.props.match.params.id )
      .then( response => {
        if ( response.status === 404 ) {
          throw response.status;
        }
        return response
      } )
      .then( response => {
        if ( response.ok ) {
          return response.json();
        } else {
          throw new Error( "has error" );
        }
      } )
      .then( ( data ) => {
        if ( this._isMounted ) {
          this.onLoad( data )
        }
        if ( compleation !== undefined ) {
          compleation();
        }
      } )
      .catch( ( error ) => {
        if ( this._isMounted && error === 404 ) {
          this.setState( {
                           item: null,
                           assetNotFound: true
                         } );
        }
        if ( compleation !== undefined ) {
          compleation();
        }
      } )
  }

  onLoad = ( data ) => {
    this.setState( {
                     item: data,
                     selectedCycleId: data.assetCycleId,
                     loadCycle: true,
                     assetNotFound: false,
                     defaultActiveKey: ((this.props.match.params.cycle !== undefined) ? "selectedCycle" : "assetInformation")
                   } );
  }

  handleRefresh( resolve, reject ) {
    this.setState( {
                     list: [],
                     hasMoreItems: true,
                     nextUrlQuery: null
                   } );
    this.fetchAssets( () => {
      resolve()
    } )
  }

  async handleReloadList() {
    await this.setState( {
                           list: [],
                           hasMoreItems: true,
                           nextUrlQuery: null
                         } );
    await this.fetchAssets()
  }

  async handelFilterItems( filterString ) {
    await this.setState( {
                           list: [],
                           hasMoreItems: true,
                           nextUrlQuery: null,
                           checkFilter: filterString
                         } )
    const values = queryString.parse( this.props.location.search )
    values.checkFilter = filterString
    const {history, location} = this.props;
    history.push( {
                    pathname: location.pathname,
                    search: "?" + queryString.stringify( values ),
                    hash: "",
                    state: {fromDashboard: false}
                  } )
  }

  getCheckByElements() {
    const values = queryString.parse( this.props.location.search )
    let filter
    if ( values.checkFilter !== undefined ) {
      filter = values.checkFilter
    }
    let checkBoxes = []
    checkBoxes.push( {value: 1, label: i18n.t( 'sites.Asset.AssetDetail.characteristicsFilter.item1' ), operator: "OR", where: "assetState eq 'ON_THE_WAY'", checked: (filter !== undefined && filter.indexOf( "assetState eq 'ON_THE_WAY'" ) > -1)} )
    checkBoxes.push( {value: 2, label: i18n.t( 'sites.Asset.AssetDetail.characteristicsFilter.item2' ), operator: "OR", where: "assetState eq 'AT_POI'", checked: (filter !== undefined && filter.indexOf( "assetState eq 'AT_POI'" ) > -1)} )
    checkBoxes.push( {value: 3, label: i18n.t( 'sites.Asset.AssetDetail.characteristicsFilter.item3' ), operator: "OR", where: "assetState eq 'AT_HOME'", checked: (filter !== undefined && filter.indexOf( "assetState eq 'AT_HOME'" ) > -1)} )

    return checkBoxes
  }

  async handleClickSearchItem( e, item, search ) {
    await this.setState( {
                           list: [],
                           hasMoreItems: true,
                           nextUrlQuery: null,
                           search: search
                         } )
    await this.fetchAssets()
    const values = queryString.parse( this.props.location.search )
    if ( item !== undefined ) {
      values.search = item.deliveryNumber
    } else {
      values.search = search
    }
    const {history, location} = this.props;
    history.push( {
                    pathname: location.pathname,
                    search: "?" + queryString.stringify( values ),
                    hash: "",
                    state: {fromDashboard: false}
                  } )
  }

  handleOnClickRemoveSearch() {
    const values = queryString.parse( this.props.location.search )
    values.search = undefined
    const {history, location} = this.props;
    history.push( {
                    pathname: location.pathname,
                    search: "?" + queryString.stringify( values ),
                    hash: "",
                    state: {fromDashboard: false}
                  } )
  }

  handleOnClickRemoveFilter() {
    const {history, location} = this.props;
    history.push( {
                    pathname: location.pathname
                  } )
  }

  selectCycle( id ) {
    this.setState( {loadCycle: true, selectedCycleId: id, defaultActiveKey: "selectedCycle", cycle: null} )
    let path = '/assets/' + this.props.match.params.id + "/" + id
    window.history.pushState( path, "Title", path );
    this.fetchCycle( id, () => {
      this.fetchAssetCycleGeoMessages( ( assetCycleGeoMessages ) => {
        this.setAssetCycleGeoMessages( assetCycleGeoMessages )
      } )
    } )
  }

  selectTab( key ) {
    let path
    const {item} = this.state;
    switch ( key ) {
      case 'selectedCycle':
        if ( item.assetCycleId !== null ) {
          if ( this.props.match.params.cycle !== undefined ) {
            this.setState( {loadCycle: true, defaultActiveKey: key, cycle: null} )
            path = '/assets/' + this.props.match.params.id + "/" + this.props.match.params.cycle
            this.fetchCycle( this.props.match.params.cycle, () => {
              this.fetchAssetCycleGeoMessages( ( assetCycleGeoMessages ) => {
                this.setAssetCycleGeoMessages( assetCycleGeoMessages )
              } )
            } )
          } else {
            this.setState( {loadCycle: true, defaultActiveKey: key, selectedCycleId: item.assetCycleId, cycle: null} )
            path = '/assets/' + this.props.match.params.id + "/" + item.assetCycleId
            this.fetchCycle( item.assetCycleId, () => {
              this.fetchAssetCycleGeoMessages( ( assetCycleGeoMessages ) => {
                this.setAssetCycleGeoMessages( assetCycleGeoMessages )
              } )
            } )
          }
        }
        break;
      case 'cycles':
        if ( item.assetCycleId !== null ) {
          path = '/assets/' + this.props.match.params.id
          this.setState( {defaultActiveKey: key, cycles: null} )
          //this.fetchCycles( item.id )
        }
        break;
      case 'assetInformation':
        path = '/assets/' + this.props.match.params.id
        this.setState( {defaultActiveKey: key} )
        break;
    }
    window.history.pushState( path, "Title", path );
  }

  handleReload() {
    const {item, defaultActiveKey, selectedCycleId} = this.state;
    switch ( defaultActiveKey ) {
      case 'selectedCycle':
        this.setState( {cycle: null} )
        this.fetchCycle( selectedCycleId, () => {
          this.fetchAssetCycleGeoMessages( ( assetCycleGeoMessages ) => {
            this.setAssetCycleGeoMessages( assetCycleGeoMessages )
          } )
        } )
        break;
      case 'cycles':
        this.setState( {cycles: null} )
        //this.fetchCycles( item.id )
        break;
      case 'assetInformation':
        this.setState( {item: null} )
        this.fetchAsset( () => {
          this.setAssetCycleGeoMessages()
        } )
        this.setAssetCycleGeoMessages()
        break;
    }
  }

  goBack() {
    const {history, location} = this.props;
    if ( location.state.lastLocation !== null || location.state.lastLocation !== undefined ) {
      history.push( location.state.lastLocation )
    } else {
      history.push( {
                      pathname: "/assets"
                    } )
    }
  }

  render() {
    const {t} = this.props
    const {item, assetNotFound} = this.state;

    if ( assetNotFound )
      return AssetDetail.renderAssetNotFound( t );

    return item ?
      this.renderData( item ) :
      AssetDetail.renderLoading();
  }

  renderData( item ) {
    const {t} = this.props;
    const {cycle, cycles, selectedCycleId} = this.state;
    const {name, keyLabel, assetState, isTracked, deviceId} = item;
    return (
      <div className={"site-asset-detail"}>
        <Container>
          <>
            <Row>
              <Col lg="8 mr-0" id={"asset-list-container"}>
                <Row className="mt-md-5 mt-4 mb-md-5 mb-5">
                  <Col md="8" className="d-flex">
                    <h1 className={""}>{t( 'sites.Asset.AssetDetail.actualStatment' )}</h1>
                  </Col>
                  <Col md="4">
                    <Link to={`/create/asset/`}>
                      <Button variant="secondary" size="lg" block>{t( 'sites.Asset.AssetDetail.createAsset' )}</Button>
                    </Link>
                  </Col>
                </Row>
                <Row className={"mb-4 mt-0 mt-md-4"}>
                  <Col md="12" className="mb-3">
                    <div className="d-flex">
                      <div className="pt-2">
                        <FontAwesomeIcon icon={['fal', 'bullseye']} size="sm" className={"text-primary"}/>
                      </div>
                      <div className="p-2">
                        <h5 className={"text-primary"}>{name}</h5>
                      </div>
                      <div className="ml-auto p-2">
                        <ButtonGroup className="mr-2" aria-label="First group">
                          <Button variant="light" size="sm" onClick={this.goBack.bind( this )} style={{fontSize: '19px', marginTop: "0px"}}><FontAwesomeIcon style={{fontSize: '21px'}} icon={['fal', 'times']}/></Button>
                          <Button variant="light" size="sm" onClick={this.handleReload.bind( this )} style={{fontSize: '19px', marginTop: "-1px"}}><FontAwesomeIcon style={{fontSize: '17px'}} icon={['fal', 'sync-alt']}/></Button>
                          <Link to={`/update/asset/` + item.id}>
                            <Button variant="light" size="sm" style={{fontSize: '19px'}}><FontAwesomeIcon icon={['fal', 'edit']}/></Button>
                          </Link>
                          <Button variant="light" size="sm" style={{fontSize: '19px'}}><AssetPopOver isTracked={isTracked} deviceId={deviceId} {...this.props}/></Button>
                        </ButtonGroup>

                      </div>
                    </div>
                    <h6>{t( 'sites.Asset.AssetDetail.assetId' )}: <span className="text-primary"> {keyLabel}</span></h6>
                    <h6>{t( 'sites.Asset.AssetDetail.currentStatus' )}: <span className="text-primary"> {Helper.getStateAsString( t, assetState )}</span></h6>
                  </Col>
                </Row>

                <Tabs activeKey={this.state.defaultActiveKey} id="uncontrolled-tab-example" className="nav-fill flex-md-row flex-column" onSelect={this.selectTab.bind( this )}>
                  <Tab eventKey="assetInformation" title={t( 'sites.Asset.AssetDetail.assetInformation' )}>
                    <TabAssetInformation asset={item} {...this.props} {...this.state} setAddressCenter={this.setAddressCenter.bind( this )} map={this.assetMap}/>
                  </Tab>
                  <Tab eventKey="selectedCycle" title={t( 'sites.Asset.AssetDetail.chosenRound' ) + ((cycle !== null) ? ' (' + cycle.cycleNumber + ')' : "")}>
                    <TabSelectedCycle cycle={cycle} {...this.props} {...this.state}/>
                  </Tab>
                  <Tab eventKey="cycles" title={t( 'sites.Asset.AssetDetail.circulationOverview' )}>
                    <TabCycles fetchCycles={this.fetchCycles.bind(this)} cycles={cycles} item={item} selectCycle={this.selectCycle.bind( this )} {...this.props} {...this.state}/>
                  </Tab>
                </Tabs>

              </Col>
              <Col lg="4 shadow-bpw-lg p-0">
                <div id="assetMap"/>
              </Col>
            </Row>

            {/*<Route path={`${this.props.match.path}/modal/:id`} component={AssetStaticModal}/>*/}

          </>
        </Container>
      </div>
    )
  }
}

export default withQueryParams( {
                                  stripUnknownKeys: false
                                } )( AssetDetail )