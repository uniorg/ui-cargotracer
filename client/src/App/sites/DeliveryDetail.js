import React, { Component } from 'react';
import withQueryParams from "react-router-query-params";
import { Link } from 'react-router-dom'
import { Accordion, Breadcrumb, Button, Card, Col, Container, OverlayTrigger, ProgressBar, Row, Tooltip } from 'react-bootstrap'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Loader from 'react-loader-spinner'
import Form from "react-bootstrap/es/Form";
import EtaPopOver from '../component/EtaPopOver'
import Moment from "react-moment";
import moment from 'moment';
import 'moment/locale/en-gb';
import 'moment/locale/de';
import $ from "jquery";
import Map from '../services/locationLeaFlet';

moment().locale( navigator.language )


class DeliveryDetail extends Component {
  _isMounted = false;
  deliveryMap = null
  lastGeoMessagesId = null

  constructor( props ) {
    super( props );
    this.state = {
      width: 0,
      height: 0,
      item: null,
      geoData: null,
      error: null,
      isLoading: false
    };
  }

  static renderLoading() {
    return <><Row><Col md={"12"} className={"d-flex justify-content-center mt-5"}><Loader type="Oval" color="#CDDC4B" height={70} width={70}/></Col></Row></>;
  }

  static renderLoadingFinish() {
    return <><Row><Col md={"12"} className={"d-flex flex-column mt-3"}>
      <div className="alert alert-primary" role="alert">
        The required content has been loaded successfully!
      </div>
    </Col></Row></>;
  }

  static renderError() {
    return <><Container><Row><Col md={"12"} className={"d-flex flex-column mt-3"}>
      <div className={"alert alert-danger"} role={"alert"}>There was a technical problem. Please contact our <a href="mailTo:cargotracer@bpw.de" className={"alert-link"}>Support Team</a>. Or just reload and try it again.</div>
    </Col></Row></Container></>;
  }

  setListContainerHeight( containerId ) {
    let that = this
    that.setContainerHeight( containerId )

    $( window ).resize( function() {
      that.setContainerHeight( containerId )
    } );
  }

  setContainerHeight( containerId ) {
    const height = $( ".application-main-container" ).height() - (($( ".dev-breadcrumb" ).length) > 0 ? $( ".dev-breadcrumb" ).outerHeight() : 0) - 20;
    $( "#" + containerId ).height( height );
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  async componentDidMount() {
    await this.fetchDeliveryItemById()
    await this.setListContainerHeight( "delivery-detail-container" )
  }

  componentDidUpdate() {
    const {isLoading} = this.state
    this.setListContainerHeight( "delivery-detail-container" )

    if ( !this.props.isMobile && this._isMounted && !isLoading ) {
      if ( this.deliveryMap !== undefined && this.deliveryMap !== null ) {
        this.deliveryMap.map.remove();
        $( "#deliveryMap" ).html( "" );
      }
      this.deliveryMap = new Map( {startLatitude: '50.8038972', startLongitude: '7.1723139', startZoom: '5', containerId: 'deliveryMap', mapType: 'openStreet'} );
      this.deliveryMap.startMap();
    }
  }

  fetchDeliveryItemById() {
    const {isLoading} = this.state
    if ( this.props.match.params.id === undefined ) {
      return
    }
    this.setState( {item: null, isLoading: true} );
    if ( !isLoading ) {
      fetch( '/delivery-ui-service/delivery/detail/' + this.props.match.params.id )
        .then( response => {
          if ( response.ok ) {
            this._isMounted = true;
            return response.json();
          } else {
            throw new Error( "has error" );
          }
        } )
        .then( data => this.setState( {item: data, isLoading: false} ) )
        .then( response => {
          let {item} = this.state
          let {lastValidGeoPositionLatitude, lastValidGeoPositionLongitude, lastValidGeoPositionRadius, shippingFromAddress, shippingToAddress} = item
          if ( lastValidGeoPositionLatitude !== null && lastValidGeoPositionLongitude !== null ) {
            this.setMarker( item.id, lastValidGeoPositionLatitude, lastValidGeoPositionLongitude, lastValidGeoPositionRadius, "letzte Position", "<div class='row m-3'>" +
                                                                                                                                                 "<div class='col-12'><h5>letzte Position</h5></div>" +
                                                                                                                                                 "</div>", "fas fa-location", "#ca2027" )
          }
          let startMarker, endMarker;
          if ( shippingFromAddress !== null && shippingFromAddress.geofence !== null ) {
            let {id, latitude, longitude, radius} = shippingFromAddress.geofence
            startMarker = this.setMarker( "shippingFromAddress" + id, latitude, longitude, radius, "Start Adresse", "<div class='row m-3'>" +
                                                                                                                    "<div class='col-12'><h5>Start Adresse</h5></div>" +
                                                                                                                    "<div class='col-12'>" + shippingFromAddress.street + ((shippingFromAddress.streetNumber !== null) ? " " + shippingFromAddress.streetNumber : "") + ", </br>" + shippingFromAddress.postalCode + " " + shippingFromAddress.city + "</div>" +
                                                                                                                    "</div>", "fas fa-bullseye", "#111111" )
          }
          if ( shippingToAddress !== null && shippingToAddress.geofence !== null ) {
            let {id, latitude, longitude, radius} = shippingToAddress.geofence
            endMarker = this.setMarker( "shippingToAddress" + id, latitude, longitude, radius, "Ziel Adresse", "<div class='row m-3'>" +
                                                                                                               "<div class='col-12'><h5>Ziel Adresse</h5></div>" +
                                                                                                               "<div class='col-12'>" + shippingToAddress.street + ((shippingToAddress.streetNumber !== null) ? " " + shippingFromAddress.streetNumber : "") + ", </br>" + shippingToAddress.postalCode + " " + shippingToAddress.city + "</div>" +
                                                                                                               "</div>", "fas fa-flag-checkered", "#111111" )

          }
          this.fetchDeliveryGeoMessages( startMarker, endMarker )
        } )
        .catch( this.fetchFail.bind( this ) );
    }
  }

  getGeoMessagePopOver() {
    let {t} = this.props
    return (
      <Row className={"popovers-maps--tooltip"}>
        <Col md={"4"}>
          <Row className="mb-3">
            <Col md={"12"}>
              <Button size={"sm"} variant="primary" type="submit" block>146</Button>
            </Col>
          </Row>
          <Row className="mb-3">
            <Col md={"12"}>
              <div className="d-flex flex-column">
                <div className="text-muted">
                  <h6>{t( 'sites.Asset.type' )}:</h6>
                </div>
              </div>
              <div className="d-flex">
                <div className="p-1 flex-shrink-1">
                  <OverlayTrigger overlay={<Tooltip id="tooltip-gps">{t( 'sites.Asset.tooltip' )}</Tooltip>}>
                            <span className="d-inline-block"
                                  tabIndex="0"
                                  data-toggle="tooltip"
                                  title={t( 'sites.Asset.tooltip' )}>
                              <FontAwesomeIcon icon={['fal', 'map-marker-alt']} size="lg" className="text-primary"/>
                            </span>
                  </OverlayTrigger>
                </div>
                <div className="p-1 ">
                  <p>GPS:</p>
                </div>
                <div className="flex-grow-1">
                  <Button size={"sm"} variant="light" type="submit" className="flex-fill" block>
                    {t( 'sites.Asset.copyLink' )}
                  </Button>
                </div>
              </div>
            </Col>
          </Row>

          <Row className="mb-3">
            <Col md={"12"}>
              <div className="d-flex flex-column">
                <div className="text-muted">
                  <h6>{t( 'sites.Asset.time' )}:</h6>
                </div>
              </div>
              <div className="d-flex">
                <div className="p-1 flex-shrink-1">
                  <OverlayTrigger overlay={<Tooltip id="tooltip-clock">{t( 'sites.Asset.tooltip' )}</Tooltip>}>
                            <span className="d-inline-block"
                                  tabIndex="1"
                                  data-toggle="tooltip"
                                  title={t( 'sites.Asset.tooltip' )}>
                              <FontAwesomeIcon icon={['fal', 'clock']} size="lg" className="text-primary"/>
                            </span>
                  </OverlayTrigger>
                </div>
                <div className="p-1 flex-grow-1">
                  <p>02.07.2019 19:54</p>
                </div>
              </div>
            </Col>
          </Row>

          <Row className="mb-3">
            <Col md={"12"}>
              <div className="d-flex flex-column">
                <div className="text-muted">
                  <h6>{t( 'sites.Asset.additionalInformation' )}:</h6>
                </div>
              </div>
              <div className="d-flex">
                <div className="p-1 flex-shrink-1">
                  <OverlayTrigger overlay={<Tooltip id="tooltip-battery">{t( 'sites.Asset.tooltip' )}</Tooltip>}>
                            <span className="d-inline-block"
                                  tabIndex="2"
                                  data-toggle="tooltip"
                                  title={t( 'sites.Asset.tooltip' )}>
                              <FontAwesomeIcon icon={['fal', 'battery-quarter']} size="lg" className="text-primary"/>
                            </span>
                  </OverlayTrigger>
                </div>
                <div className="p-1 flex-grow-1">
                  <p>2900 mV</p>
                </div>
              </div>
              <div className="d-flex">
                <div className="p-1 flex-shrink-1 justify-content-center">
                  <OverlayTrigger overlay={<Tooltip id="tooltip-tempeture">{t( 'sites.Asset.tooltip' )}</Tooltip>} className="">
                            <span className="d-inline-block"
                                  tabIndex="3"
                                  data-toggle="tooltip"
                                  title={t( 'sites.Asset.tooltip' )}>
                              <FontAwesomeIcon icon={['fal', 'temperature-high']} size="lg" className="text-primary"/>
                            </span>
                  </OverlayTrigger>
                </div>
                <div className="p-1 flex-grow-1">
                  <p>20.8°</p>
                </div>
              </div>
              <div className="d-flex">
                <div className="p-1 flex-shrink-1 justify-content-center">
                  <OverlayTrigger overlay={<Tooltip id="tooltip-bolt">{t( 'sites.Asset.tooltip' )}</Tooltip>} className="">
                            <span className="d-inline-block"
                                  tabIndex="4"
                                  data-toggle="tooltip"
                                  title={t( 'sites.Asset.tooltip' )}>
                              <FontAwesomeIcon icon={['fal', 'bolt']} size="lg" className="text-primary"/>
                            </span>
                  </OverlayTrigger>
                </div>
                <div className="p-1 flex-grow-1">
                  <p>5</p>
                </div>
              </div>
              <div className="d-flex">
                <div className="p-1 flex-shrink-1 justify-content-center">
                  <OverlayTrigger overlay={<Tooltip id="tooltip-info">{t( 'sites.Asset.tooltip' )}</Tooltip>} className="">
                            <span className="d-inline-block"
                                  tabIndex="5"
                                  data-toggle="tooltip"
                                  title={t( 'sites.Asset.tooltip' )}>
                              <FontAwesomeIcon icon={['fal', 'info-circle']} size="lg" className="text-primary"/>
                            </span>
                  </OverlayTrigger>
                </div>
                <div className="p-1 flex-grow-1">
                  <p>3659994</p>
                </div>
              </div>
            </Col>
          </Row>


        </Col>
      </Row>
    )
  }

  setGeoMessages( id, geoData, startMarker, endMarker ) {
    //const {geoData} = this.state
    let markerId = "setGeoMessages" + id
    // let {geoData} = this.state;
    const icon = {
      google: "BpwPalletMarker",
      leaflet: "<i class=\"fas fa-map-pin\" style='font-size: 1.3rem;color: #ca2027;position:  relative;top: -12px;left: -2px;'></i>"
    };
    let newGeoData = geoData.map( ( location ) => {
      let {latitude, longitude, geoPositionType, radius, sentAt, deviceId} = location
      //const placeholder = document.createElement('div');
      //ReactDOM.render(this.getGeoMessagePopOver(), placeholder);
      /**
       * deliveryId: 291
       deviceId: "3854A3"
       geoPositionType: "GPS"
       handlingUnitCode: "ef020578-88a4-4773-be9c-4f6fc2444482"
       handlingUnitId: 785
       latitude: 50.938918
       longitude: 7.258142
       numberOfSatellites: 8
       radius: null
       sentAt: "2019-08-09T09:17:49Z"
       speed: null
       temperature: null
       vibrationCount: 0
       vibrationThreshold: null
       * @type {{popup: string, lng: *, icon: {google: string, leaflet: string}, title: *, lat: *, zIndex: number, timestamp: *}}
       */
      const marker = {
        lat: latitude,
        lng: longitude,
        title: geoPositionType,
        zIndex: 999,
        popup: "<div class='row m-3'>" +
               "<div class='col-12'><h6>Routen Position</h6></div>" +
               "<div class='col-12'>" + location.geoPositionType + "</div>" +
               "<div class='col-12'>" + moment( sentAt ).format( "LLL" ) + "</div>" +
               "<div class='col-12'>Device Id:" + deviceId + "</div>" +
               "</div>",
        timestamp: sentAt,
        source: location.geoPositionType,
        icon: {
          google: "BpwPalletMarker",
          leaflet: "<i class=\"fas fa-map-pin\" style='font-size: 1.3rem;color: " + ((geoPositionType !== "GPS") ? "#000000" : "#ca2027") + ";position:  relative;top: -12px;left: -2px;'></i>"
        }
      };
      return marker
    } );
    startMarker.icon = icon
    endMarker.icon = icon
    if ( geoData.length > 0 ) {
      newGeoData.push( startMarker )
      newGeoData.push( endMarker )
      const markerIconsStart = {
        google: "BpwPalletMarker",
        leaflet: "<i class=\"fas fa-map-pin\" style='font-size: 1.5rem;color: #129fd8;position:  relative;top: -12px;left: -2px;'></i>"
      };
      this.deliveryMap.removeMarker( markerId )
      this.deliveryMap.setMarkers( newGeoData, markerId, markerIconsStart, false, true, true );
      this.deliveryMap.setNowMarkerCenter( undefined, true );
    }
  }

  setMarker( id, latitude, longitude, radius, title, popup, icon, color ) {
    if ( icon === undefined )
      icon = "fas fa-map-marker-alt"
    const marker = {
      lat: latitude,
      lng: longitude,
      title: title,
      circleRadius: ((radius !== null) ? radius : undefined),
      popup: popup,
      zIndex: 1000,
      icon: {
        google: "BpwPalletMarker",
        leaflet: "<i class=\"" + icon + "\" style='font-size: 1.3rem;color: " + color + ";position:  relative;top: -12px;left: -2px;'></i>"
      }
    };
    const markerIconsStart = {
      google: "BpwPalletMarker",
      leaflet: "<i class=\"fas fa-map-marker-alt\" style='font-size: 1.5rem;color: #129fd8;position:  relative;top: -12px;left: -2px;'></i>"
    };
    this.deliveryMap.removeMarker( id )
    this.deliveryMap.setMarkers( [marker], id, markerIconsStart, false, true, false );
    this.deliveryMap.setNowMarkerCenter( undefined, true );
    return marker
  }

  fetchDeliveryGeoMessages( startMarker, endMarker ) {
    //this.setState( {item: null, isLoading: true} );
    let {item} = this.state
    let {id} = item;

    /*if (this.lastGeoMessagesId === 'setGeoMessages' + id) {
      this.deliveryMap.removeMarker( this.lastGeoMessagesId )
      this.lastGeoMessagesId = null
      if (item.lastValidGeoPositionLatitude !== null && item.lastValidGeoPositionLongitude !== null) {
        this.setMarker( item.id, item.lastValidGeoPositionLatitude, item.lastValidGeoPositionLongitude, item.lastValidGeoPositionRadius )
      }
    } else {*/
    //this.setState( {geoData: null, isLoading: true} );
    //this.lastGeoMessagesId = 'setGeoMessages' + id
    //this.deliveryMap.removeMarker( item.id )
    fetch( '/delivery-ui-service/delivery/geoMessages/' + id + '/null/null/null/0/100' )
      .then( response => {
        if ( response.ok ) {
          this._isMounted = true;
          return response.json();
        } else {
          throw new Error( "has error" );
        }
      } )
      .then( data => {
        if ( data.hasOwnProperty( "value" ) ) {
          return data.value;
        } else {
          throw [];
        }
      } )
      //.then( data => this.setState( {geoData: data, isLoading: false} ) )
      //.then( data => setTimeout(() => { this.setGeoMessages( id, data )}, 1500) )
      .then( data => this.setGeoMessages( id, data, startMarker, endMarker ) )
      //.then( data => this.setState( {geoData: data, isLoading: false} ) )
      .catch( this.fetchFail.bind( this ) );
    //}
  }

  fetchFail( error ) {
    this.setState( {error: error, isLoading: false} )
  }

  progressBarValue( deliveryState ) {
    switch ( deliveryState ) {
      case "SAVED":
        return 0
      case "SCANNED":
        return 10
      case "ON_THE_WAY":
        return 33
      case "ARRIVED":
        return 66
      case "DONE":
        return 100
      case "UNKNOWN":
        return 100
      default:
        return 0
    }
  }

  goBack() {
    const {history} = this.props;
    (history)
      ?
      this.props.history.goBack()
      :
      this.props.history.push( "/delivery" )

  }

  render() {
    const {item, error, isLoading} = this.state;

    if ( error !== null ) {
      return DeliveryDetail.renderError();
    }
    if ( isLoading )
      return DeliveryDetail.renderLoading();

    return item ?
      this.renderData( item ) :
      DeliveryDetail.renderLoading();

  }

  renderData( item ) {
    const {t} = this.props;
    const {geoData} = this.state;
    let {plannedArrivalTime, deliveryNumber, isLate, orderNumber, transportNumber, etaArrival, deliveryStateTimestamp, deliveryState, items, shippingFromContact, shippingToAddress, stateHistory} = item;

    let {onTheWayAt, scanAt, arrivedAt} = stateHistory;

    let startDate = new Date( scanAt )
    let endDate = new Date( onTheWayAt )

    let startDateMoment = new moment( startDate )
    let endDateMoment = new moment( endDate )

    let duration = moment.duration( endDateMoment.diff( startDateMoment ) )
    let days = Number( duration.get( 'days' ) ).toFixed( 0 );
    let hours = Number( duration.get( 'hours' ) ).toFixed( 0 );

    if ( geoData !== null ) {
      //this.setGeoMessages()
    }

    return (
      <div className={"site-delivery-detail"}>
        <Container>
          <>
            <Row>
              <Col lg="8 mr-0" id={"delivery-detail-container"}>
                <Row className="mt-md-5 mt-4 mb-md-5 mb-5">
                  <Col md="8">
                    <h1 className={"mb-sm-0 mb-4"}>{t( 'sites.Delivery.listTitle' )}</h1>
                  </Col>
                  <Col md="4">
                    <Link to={`/create/delivery`}>
                      <Button variant="secondary" size="lg" block>{t( 'sites.Delivery.createDelivery' )}</Button>
                    </Link>
                  </Col>
                </Row>
                <Row className="mb-3">
                  <Col>
                    <div className="d-flex flex-column flex-md-row flex-lg-row align-self-center">
                      <div className="pl-0 p-sm-2 pl-md-0 flex-column">
                        <Form.Label>{t( 'sites.Delivery.actualStatment' )}</Form.Label>
                      </div>
                      <div className="d-flex flex-row flex-grow-1">
                        <>
                          <Breadcrumb className="breadcumb__main-site--headline">
                            <li className={"breadcrumb-item"}><Link to="/delivery">{t( 'sites.Delivery.deliveryDetail.breadcrumbDelivery' )}</Link></li>
                            <Breadcrumb.Item href="https://getbootstrap.com/docs/4.0/components/breadcrumb/" active>{deliveryNumber}</Breadcrumb.Item>
                          </Breadcrumb>
                        </>
                        <div className="ml-auto pt-1 justify-content-end">
                          <Button variant="secondary" size="sm" className="btn-light mr-2" onClick={this.goBack.bind( this )}>
                            <FontAwesomeIcon icon={['fal', 'times']} className="mr-1"/>
                            {t( 'sites.Delivery.deliveryItem.isLateItemClose' )}
                          </Button>
                          <Button variant="primary" size="sm" className="btn-primary btn-round" onClick={this.fetchDeliveryItemById.bind( this )}>
                            <FontAwesomeIcon icon={['fal', 'sync-alt']} className="mr-1"/>
                            {t( 'sites.Delivery.deliveryItem.isLateItemSync' )}
                          </Button>
                        </div>
                      </div>
                    </div>
                  </Col>
                </Row>

                <Row className={"mb-4"}>
                  <Col md="6" className="mb-3">
                    <div className="d-flex">
                      <div className="pt-2 bd-highlight">
                        <FontAwesomeIcon icon={['fal', 'truck-moving']} size="lg" className={"text-primary"}/>
                      </div>
                      <div className="p-2">
                        <h5 className={"text-primary"}>{deliveryNumber}</h5>
                      </div>
                      <div className="ml-auto p-2">
                        <EtaPopOver item={item} isLate={isLate} {...this.props}/>
                      </div>
                    </div>
                    <h6>{t( 'sites.Delivery.deliveryDetail.assignment' )}: <span className="text-primary"> {orderNumber}</span></h6>
                    <h6>{t( 'sites.Delivery.deliveryDetail.tour' )}: <span className="text-primary"> {transportNumber}</span></h6>
                  </Col>
                  <Col md="6" className="mb-3 border-left-0 border-md-left border-primary">
                    <h5 className={""}>{t( 'sites.Delivery.deliveryDetail.arrivals' )}:</h5>
                    <h6>{t( 'sites.Delivery.deliveryDetail.expectedArrivalTime' )}:&nbsp;
                      <span className="text-primary">
                      {etaArrival &&
                       <Moment date={new Date( etaArrival )} format="LLL"/>
                      }
                      </span>
                    </h6>
                    <h6>{t( 'sites.Delivery.deliveryDetail.estimatedTimeOfArrival' )}:&nbsp;
                      <span className="text-primary">
                        {plannedArrivalTime &&
                         <Moment date={new Date( plannedArrivalTime )} format="LLL"/>
                        }
                      </span>
                    </h6>
                    <h6 className={"mb-0"}>{t( 'sites.Delivery.deliveryDetail.arrivalTime' )}:&nbsp;
                      <span className="text-primary">
                      {arrivedAt &&
                       <Moment date={new Date( arrivedAt )} format="LLL"/>
                      }
                      </span>
                    </h6>
                  </Col>
                </Row>

                <Row>
                  <Col>
                    <div className="d-flex justify-content-between font-weight-lighter text-muted">
                      <p className="mb-0">{t( 'sites.Delivery.deliveryDetail.scanned' )}</p>
                      <p className="mb-0">{t( 'sites.Delivery.deliveryDetail.onTheWay' )}</p>
                      <p className="mb-0">{t( 'sites.Delivery.deliveryDetail.arrived' )}</p>
                      <p className="mb-0">{t( 'sites.Delivery.deliveryDetail.done' )}</p>
                    </div>
                  </Col>
                </Row>

                <Row className={"mb-5"}>
                  <Col>
                    <>
                      {
                        deliveryState === "UNKNOWN" &&
                        <ProgressBar variant="danger" now={this.progressBarValue( deliveryState )}/>
                      }
                      {
                        deliveryState !== "UNKNOWN" &&
                        <ProgressBar variant="secondary" now={this.progressBarValue( deliveryState )}/>
                      }
                    </>
                  </Col>
                </Row>

                <Accordion id={"accordionDeliveryItemAccordion"}>
                  {
                    items.map( ( deliveryItem, index ) => {
                      let {id, itemNumber, materialSender, descriptionShort, materialRecipient, quantity, quantityUnit, handlingUnits, devicesCount} = deliveryItem
                      return (
                        <div key={"deliveryItem" + id}>
                          <Row className={"mb-1"}>
                            <Col>
                              <div className="d-flex flex-column flex-md-row flex-lg-row">
                                <div className="flex-grow-1 flex-column">
                                  <div className="pl-md-3 border-left-0 border-md-left border-primary align-content-start">
                                    <h5 className={""}>{itemNumber}</h5>
                                    {materialSender &&
                                     <h6>{t( 'sites.Delivery.deliveryDetail.material' )}:
                                       <span className="text-primary"> {/*{materialSender}*/}</span>
                                     </h6>
                                    }
                                    {descriptionShort &&
                                     <h6>
                                       <span className="text-muted"> {descriptionShort}</span>
                                     </h6>
                                    }
                                    {materialRecipient &&
                                     <h6>{t( 'sites.Delivery.deliveryDetail.yourArticle' )}:
                                       <span className="text-primary"> {materialRecipient}</span>
                                     </h6>
                                    }
                                  </div>
                                </div>
                                <div className="d-flex flex-row">
                                  <div className="p-2 ml-auto justify-content-end flex-grow-1 flex-md-grow-0 align-items-end align-self-end text-right">
                                    <span className="text-primary"> {quantity} {quantityUnit} {t( 'sites.Delivery.deliveryDetail.piece' )}</span>
                                  </div>
                                </div>
                              </div>
                            </Col>
                          </Row>
                          <Row className={"mb-5"}>
                            <Col>
                              <Card className={"card--filter card__border-radius card__border-dark"}>
                                <Accordion.Toggle as={Card.Header} variant="link"
                                                  className="card-header--transparent border-bottom-0 pb-2"
                                                  eventKey={"#deliveryItem" + id}
                                                  data-toggle="collapse"
                                                  data-target={"#deliveryItem" + id}
                                                  aria-expanded="true"
                                                  aria-controls="0">
                                  <div className="d-flex">
                                    <div className="pt-2 pr-2">
                                      <FontAwesomeIcon icon={['fal', 'cube']} size="lg"/>
                                    </div>
                                    <div className="pt-1 pl-1 mb-0">
                                      <h3>
                                        {handlingUnits.length} {t( 'sites.Delivery.deliveryDetail.packages' )}
                                      </h3>
                                    </div>
                                    <div className="ml-auto pt-2">
                                      <span className="badge badge-pill badge-primary mr-2">{devicesCount} {t( 'sites.Delivery.deliveryDetail.deviceCountTitle' )}</span>
                                      <FontAwesomeIcon icon={['fal', 'chevron-down']} size="lg"/>
                                    </div>
                                  </div>
                                </Accordion.Toggle>
                                <Accordion.Collapse eventKey={"#deliveryItem" + deliveryItem.id}
                                                    id={"deliveryItem" + deliveryItem.id}
                                                    aria-labelledby="1"
                                                    data-parent={"#accordionDeliveryItemAccordion"}>
                                  <Card.Body className="pt-0 pb-0">
                                    {
                                      handlingUnits.map( ( deliveryItemHandlingUnit ) => {
                                        let {id, handlingUnitCode, vibrationCount, batchCode, materialOrigin} = deliveryItemHandlingUnit
                                        return (
                                          <div key={"deliveryItemHandlingUnit" + id}>
                                            <Row className="mt-3 pb-2 border-bottom">
                                              <Col md={"12"}>
                                                <div className="d-flex">
                                                  <div className="">
                                                    <h5>{t( 'sites.Delivery.deliveryDetail.package' )}:
                                                      <span className="text-primary"> {handlingUnitCode}</span>
                                                    </h5>
                                                  </div>
                                                  <div className="ml-auto">
                                                    {vibrationCount === 0 &&
                                                     <>
                                                       --
                                                     </>
                                                    }

                                                    {(vibrationCount > 0 && vibrationCount < 2) &&
                                                     <>
                                                       <FontAwesomeIcon icon={['fal', 'exclamation-triangle']} size="sm" className="text-danger"/>
                                                       <span className="text-danger pl-2 align-self-end">{vibrationCount}</span>
                                                     </>
                                                    }

                                                    {vibrationCount > 2 &&
                                                     <>
                                                       <FontAwesomeIcon icon={['fal', 'exclamation-triangle']} className="text-danger" size="sm"/>
                                                       <span className="text-danger pl-2 align-self-end">{vibrationCount}</span>
                                                     </>
                                                    }
                                                  </div>
                                                </div>
                                              </Col>
                                              <Col md={"6"}>
                                                <p className={"text-small"}>{t( 'sites.Delivery.deliveryDetail.charge' )}:
                                                  <span className="text-primary"> {batchCode}</span>
                                                </p>
                                              </Col>
                                              <Col md={"6"}>
                                                <p className={"text-small"}>{t( 'sites.Delivery.deliveryDetail.werkscoil' )}:
                                                  <span className="text-primary"> {materialOrigin}</span>
                                                </p>
                                              </Col>
                                            </Row>
                                          </div>
                                        );
                                      } )
                                    }
                                  </Card.Body>
                                </Accordion.Collapse>
                              </Card>
                            </Col>
                          </Row>
                        </div>

                      );
                    } )
                  }
                </Accordion>

                <Row className="mb-2">
                  <Col>
                    <h3>
                      {t( 'sites.Delivery.deliveryDetail.contactPerson' )}
                    </h3>
                  </Col>
                </Row>
                <Row className="dl__info--contact pb-3 border-bottom mb-2 mb-md-4">
                  <Col md={"4"}>
                    <dl>
                      <dt>
                        {t( 'sites.Delivery.deliveryDetail.clerk' )}
                      </dt>
                      <dd>
                        {shippingFromContact !== undefined && shippingFromContact !== null && shippingFromContact.name !== undefined && shippingFromContact.name !== null ?
                          shippingFromContact.name : ""
                        }
                      </dd>
                    </dl>
                  </Col>
                  <Col md={"4"}>
                    <dl>
                      <dt>
                        {t( 'sites.Delivery.deliveryDetail.contact' )}
                      </dt>
                      <dd>
                        {shippingFromContact !== undefined && shippingFromContact !== null && shippingFromContact.phoneNumber !== undefined && shippingFromContact.phoneNumber !== null ?
                          shippingFromContact.phoneNumber : ""
                        }
                      </dd>
                      <dd>
                        {shippingFromContact !== undefined && shippingFromContact !== null && shippingFromContact.faxNumber !== undefined && shippingFromContact.faxNumber !== null ?
                          shippingFromContact.faxNumber : ""
                        }
                      </dd>
                      <dd>
                        {shippingFromContact !== undefined && shippingFromContact !== null && shippingFromContact.emailAddress !== undefined && shippingFromContact.emailAddress !== null ?
                          shippingFromContact.emailAddress : ""
                        }
                      </dd>
                    </dl>
                  </Col>
                  <Col md={"4"}>
                    <dl>
                      <dt>
                        {t( 'sites.Delivery.deliveryDetail.destinationAddress' )}
                      </dt>
                      <dd>
                        {shippingToAddress !== undefined && shippingToAddress !== null && shippingToAddress.name !== undefined && shippingToAddress.name !== null ?
                          shippingToAddress.name : ""
                        }
                      </dd>

                      <dd>
                        {shippingToAddress !== undefined && shippingToAddress !== null && shippingToAddress.street !== undefined && shippingToAddress.street !== null ?
                          shippingToAddress.street : ""
                        }
                        {shippingToAddress !== undefined && shippingToAddress !== null && shippingToAddress.streetNumber !== undefined && shippingToAddress.streetNumber !== null ?
                          " " + shippingToAddress.streetNumber : ""
                        }
                      </dd>
                      <dd>
                        {shippingToAddress !== undefined && shippingToAddress !== null && shippingToAddress.postalCode !== undefined && shippingToAddress.postalCode !== null ?
                          shippingToAddress.postalCode + " " : ""
                        }
                        {shippingToAddress !== undefined && shippingToAddress !== null && shippingToAddress.city !== undefined && shippingToAddress.city !== null ?
                          shippingToAddress.city : ""
                        }
                      </dd>
                    </dl>
                  </Col>
                </Row>

                <Row className="mb-2">
                  <Col>
                    <h3>
                      {t( 'sites.Delivery.deliveryDetail.statistics' )}
                    </h3>
                  </Col>
                </Row>

                <Row className="dl__info--contact pb-3 mb-4">
                  <Col md={"6"}>
                    <dl>
                      <dt>
                        {t( 'sites.Delivery.deliveryDetail.inTheFactory' )}
                      </dt>
                      <dd>
                        {t( 'sites.Delivery.deliveryDetail.timeAtTheFactory' )}:
                        {
                          onTheWayAt &&
                          <span className="text-primary"> {days} {t( 'sites.Delivery.deliveryDetail.days' )} {hours} {t( 'sites.Delivery.deliveryDetail.hours' )}</span>
                        }
                      </dd>
                      <dd>
                        {t( 'sites.Delivery.deliveryDetail.scanTime' )}: <span className="text-primary">
                        {stateHistory.scanAt &&
                         <Moment date={new Date( stateHistory.scanAt )} format="LLL"/>
                        }</span>
                      </dd>
                      <dd>
                        {t( 'sites.Delivery.deliveryDetail.strongVibration' )}: <span className="text-primary">
                        {stateHistory &&
                         stateHistory.scanVibrationCount
                        }
                      </span>
                      </dd>
                    </dl>
                  </Col>
                  <Col md={"6"}>
                    <dl>
                      <dt>
                        {t( 'sites.Delivery.deliveryDetail.onRoad' )}
                      </dt>
                      <dd>
                        {t( 'sites.Delivery.deliveryDetail.transitTime' )}:
                        <span className="text-primary">
                        {stateHistory !== null && stateHistory.transitTimeSeconds !== null &&
                         <>0 {t( 'sites.Delivery.deliveryDetail.days' )} 0 {t( 'sites.Delivery.deliveryDetail.hours' )}</>
                        }
                      </span>
                      </dd>
                      <dd>
                        {t( 'sites.Delivery.deliveryDetail.onTheWaySince' )}:
                        {stateHistory !== null && stateHistory.onTheWayAt !== null &&
                         <><span className="text-primary">
                          {stateHistory &&
                           <Moment date={new Date( stateHistory.onTheWayAt )} format="LLL"/>
                          }

                           {t( 'sites.Delivery.deliveryDetail.clock' )}</span></>
                        }
                      </dd>
                      <dd>
                        {t( 'sites.Delivery.deliveryDetail.strongVibration' )}: <span className="text-primary">
                        {stateHistory &&
                         stateHistory.onTheWayVibrationCount
                        }
                        </span>
                      </dd>
                    </dl>
                  </Col>
                </Row>

              </Col>
              {!this.props.isMobile ?
                (
                  <Col lg="4 shadow-bpw-lg p-0">
                    <div id="deliveryMap"/>
                  </Col>
                ) : ''}
            </Row>

          </>
        </Container>
      </div>
    )
  }

}

export default withQueryParams( {
                                  stripUnknownKeys: true
                                } )( DeliveryDetail )