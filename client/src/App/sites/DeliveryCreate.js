import React, { Component } from 'react';
import { Alert, Button, Col, Container, Form, ResponsiveEmbed, Row } from 'react-bootstrap'

import Map from '../services/locationLeaFlet';
import DeliveryInput from '../component/DeliveryInputCheckExist'
import TrackerInput from '../component/TrackerInputCheckDelivery'
import AddressSearchInput from '../component/AddressSearchInput'
import DateTimePicker from '../component/DateTimePicker'
import $ from "jquery";
import moment from "moment";
import withQueryParams from "react-router-query-params";
import Loader from "react-loader-spinner";


class DeliveryCreate extends Component {
  _isMounted = false;
  deliveryCreateMap = null
  plannedArrivalTimeTo = null

  constructor( props ) {
    super( props );
    this.state = {
      successSend: false,
      failSend: false,
      successItem: undefined,
      saveMode: false,
      deliveryInputIsValid: false,
      trackerInputIsValid: null,
      trackerInputHasDelivery: null,
      fromAddressInputIsValid: false,
      toAddressInputIsValid: false,

      saveBtnDisabled: true,
      width: 0,
      height: 0,
      deliveryNumber: null,
      deviceId: null,
      plannedArrivalTimeTo: null,
      simpleDelivery: {
        deliveryNumber: null,
        deviceId: null,
        plannedArrivalTimeTo: null
      },
      shippingFromAddress: null,
      shippingFromAddressRadius: null,
      shippingFromAddressItem: null,
      shippingToAddress: null,
      shippingToAddressRadius: null,
      shippingToAddressItem: null,

      deliveryInputValid: null,
      deliveryInputInvalid: null
    };
  }

  static renderLoading() {
    return <><Row><Col md={"12"} className={"d-flex justify-content-center mt-5"}><Loader type="Oval" color="#CDDC4B" height={70} width={70}/></Col></Row></>;
  }

  static renderError() {
    return <>Error...</>;
  }

  checkAllIsValid() {
    let {deliveryInputIsValid, trackerInputIsValid, fromAddressInputIsValid, toAddressInputIsValid} = this.state;

    if ( deliveryInputIsValid && trackerInputIsValid && fromAddressInputIsValid && toAddressInputIsValid ) {
      console.log( "checkAllIsValid", false )
      //this.setState( {saveBtnDisabled: false} );
      return false
    } else {
      console.log( "checkAllIsValid", true )
      //this.setState( {saveBtnDisabled: true} );
      return true
    }
  }

  updateOrSetAfterSetState() {
    const {shippingFromAddress, shippingFromAddressRadius, shippingFromAddressItem} = this.state;
    const {shippingToAddress, shippingToAddressRadius, shippingToAddressItem, saveMode} = this.state;
    if ( !saveMode && !this.props.isMobile ) {
      if ( this.deliveryCreateMap !== undefined && this.deliveryCreateMap !== null ) {
        this.deliveryCreateMap.map.remove();
        $( "#deliveryCreateMap" ).html( "" );
      }
      this.deliveryCreateMap = new Map( {startLatitude: '50.8038972', startLongitude: '7.1723139', startZoom: '5', containerId: 'deliveryCreateMap', mapType: 'openStreet'} );
      let that = this;
      this.deliveryCreateMap.startMap( () => {
        if ( shippingFromAddress !== null && shippingFromAddressRadius !== null && shippingFromAddressItem !== null ) {
          that.setMarkerFromAddressItemWithRadius( shippingFromAddressItem, shippingFromAddressRadius )
        }
        if ( shippingToAddress !== null && shippingToAddressRadius !== null && shippingToAddressItem !== null ) {
          that.setMarkerFromAddressItemWithRadius( shippingToAddressItem, shippingToAddressRadius )
        }
      } );
    }
  }

  componentDidMount() {
    this.updateOrSetAfterSetState()
  }

  componentDidUpdate() {
    this.updateOrSetAfterSetState()
  }

  handleValidate( event ) {
    const form = event.currentTarget;
    if ( form.checkValidity() === false ) {
      event.preventDefault();
      event.stopPropagation();
    }
    this.setState( {validated: true, saveBtnDisabled: false} );
  }

  render() {
    let {saveMode} = this.state
    if ( saveMode )
      return DeliveryCreate.renderLoading()
    return this.renderData()
  }

  async handleClickOnAddressFrom( item, radius, address ) {
    await this.setState( {shippingFromAddress: address, fromAddressInputIsValid: true, shippingFromAddressRadius: radius, shippingFromAddressItem: item} );
    await this.setMarkerFromAddressItemWithRadius( item, radius )
  }

  async handleChangeRadiusFrom( item, radius, address ) {
    await this.setState( {shippingFromAddress: address, shippingFromAddressRadius: radius, shippingFromAddressItem: item} );
    await this.setMarkerFromAddressItemWithRadius( item, radius )
  }

  async handleClickOnAddressTo( item, radius, address ) {
    await this.setState( {shippingToAddress: address, toAddressInputIsValid: true, shippingToAddressRadius: radius, shippingToAddressItem: item} );
    await this.setMarkerFromAddressItemWithRadius( item, radius )
  }

  async handleChangeRadiusTo( item, radius, address ) {
    await this.setState( {shippingToAddress: address, shippingToAddressRadius: radius, shippingToAddressItem: item} );
    await this.setMarkerFromAddressItemWithRadius( item, radius )
  }

  handleCreateDelivery( event ) {
    const {history} = this.props;
    let {deliveryNumber, deviceId, shippingFromAddress, shippingToAddress} = this.state;
    if ( deliveryNumber === null || deviceId === null
         || shippingFromAddress === null || shippingToAddress === null ) {
      return
    }
    let newDelivery = {
      "simpleDelivery": {
        deliveryNumber: deliveryNumber,
        deviceId: deviceId,
        plannedArrivalTimeTo: this.plannedArrivalTimeTo,
        shippingFromAddress: shippingFromAddress,
        shippingToAddress: shippingToAddress
      }
    }
    this.setState( {saveMode: true} )
    fetch( '/delivery-ui-service/delivery/CreateDelivery', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify( newDelivery )
    } )
      .then( ( res ) => {
        if ( res.status !== 201 ) {
          this.setState( {successItem: {}, successSend: false, failSend: true} )
        } else {
          return res.json()
        }
      } )
      .then( ( data ) => {
        this.setState( {successItem: data, successSend: true, failSend: false} )
        history.push( {
                        pathname: "/delivery/" + data.id
                      } )
      } )
      .catch( ( error ) => {
        console.error( error );
        //ai.appInsights.trackException(error)
        this.setState( {successItem: error, successSend: false, failSend: true} )
      } )
  }

  setMarkerFromAddressItemWithRadius( item, radius ) {
    let {position, id, address} = item;
    let {lat, lon} = position
    const {streetName, streetNumber, postalCode, municipality} = address;


    const marker = {
      lat: lat,
      lng: lon,
      title: municipality,
      popup: "<div class='row m-3'>" +
             "<div class='col-12'>" + streetName + " " + streetNumber + ", </br>" + postalCode + " " + municipality + "</div>" +
             "</div>",
      pointObject: {
        name: "Test Marker",
        deviceid: "1234"
      },
      circleRadius: radius,
      zIndex: 1000,
      icon: {
        google: "BpwPalletMarker",
        leaflet: "<i class=\"fas fa-map-marker-alt\" style='font-size: 1.3rem;color: #000000;position:  relative;top: -12px;left: -2px;'></i>"
      }
    };
    const markerIconsStart = {
      google: "BpwPalletMarker",
      leaflet: "<i class=\"fas fa-map-marker-alt\" style='font-size: 1.5rem;color: #129fd8;position:  relative;top: -12px;left: -2px;'></i>"
    };
    this.deliveryCreateMap.removeMarker( id )
    this.deliveryCreateMap.setMarkers( [marker], id, markerIconsStart, false, true, true );
    this.deliveryCreateMap.setNowMarkerCenter( id, true );
  }

  changeDateTimePicker( date ) {
    this.plannedArrivalTimeTo = moment( date, 'LLL' );
  }

  handleDeliveryInput( id, isValid ) {
    this.setState( {deliveryNumber: id, deliveryInputIsValid: ((!isValid))} );
  }

  handleTrackerInput( id, isValid ) {
    console.log(isValid)
    this.setState( {deviceId: id, trackerInputIsValid: true, trackerInputHasDelivery: isValid} );
  }

  handleClearTrackerSelect() {
    this.setState( {deviceId: null, trackerInputIsValid: null, trackerInputHasDelivery: null} )
  }

  handeGoToMarker( selectedItem, selectedItemRadius, newAddress ) {
    this.setMarkerFromAddressItemWithRadius( selectedItem, selectedItemRadius )
  }

  changeDeliveryValid( isInvalid, isValid ) {
    this.setState( {deliveryInputInvalid: isInvalid, deliveryInputValid: isValid} )
  }

  renderData( item ) {
    const {t} = this.props;
    const {trackerInputHasDelivery, deliveryInputValid, deliveryInputInvalid, successSend, failSend, successItem} = this.state;
    return (
      <div className={"site-delivery-create"}>
        <Container>
          <Row>
            <Col className={"mb-0 mb-md-4 mt-4"} lg={"12"}>
              <h1>{t( 'sites.Delivery.deliveryCreate.deliveryForm' )}</h1>
            </Col>
          </Row>
          <Row>
            <Col lg={"12"}>
              <Form autoComplete="off">
                <Form.Row>
                  <DeliveryInput
                    handleValidate={this.handleDeliveryInput.bind( this )}
                    deliveryInputValid={deliveryInputValid}
                    deliveryInputInvalid={deliveryInputInvalid}
                    changeValid={this.changeDeliveryValid.bind( this )}
                    {...this.props}/>

                  <TrackerInput handleValidate={this.handleTrackerInput.bind( this )} {...this.props}
                                invalidClass={' alert alert-warning'}
                                invalidColorClass={' select-invalid-warning'}
                                isValid={trackerInputHasDelivery}
                                handleClearInput={this.handleClearTrackerSelect.bind( this )}
                                reloadButton={false}/>
                </Form.Row>
                <Form.Row>
                  <AddressSearchInput inputId={"from-address-input"}
                                      title={t( 'sites.Delivery.deliveryCreate.startAddress' )}
                                      placeholder={t( 'sites.Delivery.deliveryCreate.addStartAddress' )}
                                      icon={['fal', 'map-marker-alt']}
                                      handleClickOnAddress={this.handleClickOnAddressFrom.bind( this )}
                                      handleChangeRadius={this.handleChangeRadiusFrom.bind( this )}
                                      handeGoToMarker={this.handeGoToMarker.bind( this )}
                                      {...this.props}/>

                  <AddressSearchInput inputId={"to-address-input"}
                                      title={t( 'sites.Delivery.deliveryCreate.destinationAddress' )}
                                      placeholder={t( 'sites.Delivery.deliveryCreate.addDestinationAddress' )}
                                      icon={['fal', 'location-arrow']}
                                      handleClickOnAddress={this.handleClickOnAddressTo.bind( this )}
                                      handleChangeRadius={this.handleChangeRadiusTo.bind( this )}
                                      handeGoToMarker={this.handeGoToMarker.bind( this )}
                                      {...this.props}/>

                </Form.Row>
                <Form.Group
                  controlId="formGridDate"
                  className={"mb-5"}>
                  <Form.Label>{t( 'sites.Delivery.deliveryCreate.estimatedTimeOfArrival' )}</Form.Label>
                  <DateTimePicker onChange={this.changeDateTimePicker.bind( this )}/>
                </Form.Group>
                <div className={"mb-4"}>
                  <ResponsiveEmbed
                    aspect="a16by9"
                    className={"mb-5 map-responsive img-thumbnail"}
                    style={{width: 'auto', height: 460}}>

                    <Col className="shadow-bpw-lg p-0">
                      <div id="deliveryCreateMap"/>
                    </Col>
                  </ResponsiveEmbed>
                </div>

                {successSend &&
                 <Row>
                   <Col>
                     <Alert key={0} variant={"success"}>
                       {t( 'sites.Delivery.deliveryCreate.successSendMessage' )}
                       <Alert.Link href={"/delivery/" + successItem.id}>{"/delivery/" + successItem.id}</Alert.Link> {t( 'sites.Delivery.deliveryCreate.successSendMessageLink' )}
                     </Alert>
                   </Col>
                 </Row>
                }
                {failSend &&
                 <Row>
                   <Col>
                     <Alert key={0} variant={"danger"}>
                       {t( 'sites.Delivery.deliveryCreate.failSendMessage' )}
                     </Alert>
                   </Col>
                 </Row>
                }
                <Row>
                  <Col>
                    <div className="d-flex flex-column flex-md-row flex-lg-row">
                      <div className="p-1 flex-fill">
                        <Button size={"lg"} variant="light" type="submit" block>{t( 'sites.Delivery.deliveryCreate.discardDelivery' )}</Button>
                      </div>
                      <div className="p-1 flex-fill">
                        <Button size={"lg"} variant="secondary" block onClick={this.handleCreateDelivery.bind( this )} disabled={this.checkAllIsValid()}>{t( 'sites.Delivery.deliveryCreate.createDelivery' )}</Button>
                      </div>
                    </div>
                  </Col>
                </Row>
                <Row className={"mb-5"}>
                  <Col>
                    <p className={"text-right"}>{t( "sites.Delivery.deliveryCreate.requiredInfoText" )}</p>
                  </Col>
                </Row>
              </Form>
            </Col>
          </Row>
        </Container>
      </div>
    )
  }

}

export default withQueryParams( {
                                  stripUnknownKeys: true
                                } )( DeliveryCreate )