import React, { Component } from 'react';
import { Accordion, Button, Card, Col, Container, FormControl, InputGroup, Row } from "react-bootstrap";
import Form from "react-bootstrap/es/Form";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import SearchPopver from "../component/SearchPopOver";
import UserManagementPopOver from "../component/UserManagementPopOver";

class UserManagement extends Component {

  render() {
    const {t} = this.props;
    return <div className={"site-delivery-detail"}>
      <Container>
        <>
          <Row>
            <Col lg="12 mr-0" id={""}>
              <Row className="mt-md-5 mt-4 mb-md-5 mb-5">
                <Col md="8" className="d-flex">
                  <h1 className={""}>{t( 'sites.UserManagement.userManagement' )}</h1>
                  <a href={"/"} variant="link" size="sm" className="ml-auto mt-3 border-0 justify-content-end clickable-a">
                    <FontAwesomeIcon icon={['far', 'sync-alt']} size="lg" className="mr-1 text-primary"/>
                  </a>
                </Col>
                <Col md="4">
                  <Button variant="secondary" size="lg" block>{t( 'sites.UserManagement.createUser' )}</Button>
                </Col>
              </Row>
              <Form>
                <Form.Group as={Row}>
                  <Col md="8">
                    <Form.Label>{t( 'sites.UserManagement.searchForUsers' )}</Form.Label>
                    <InputGroup>
                      <InputGroup.Prepend>
                        <InputGroup.Text className={"cursor-pointer input-group-append--transparent rounded-left border-right-0"} id="basic-addon3">
                          <FontAwesomeIcon icon={['fal', 'search']}/>
                        </InputGroup.Text>
                      </InputGroup.Prepend>
                      <Form.Control
                        size="lg"
                        placeholder={t( 'sites.UserManagement.enterUsernameOrMail' )}
                        id="id"
                        aria-describedby="basic-addon1"
                        value=""
                        className={"border-right-0 border-left-0"}
                      />
                      <InputGroup.Append>
                        <InputGroup.Text className={"cursor-pointer input-group-append--transparent rounded-right border-left-0"} id="basic-addon3">
                          <FontAwesomeIcon icon={['fal', 'times']}/>
                        </InputGroup.Text>
                      </InputGroup.Append>
                      <Form.Control.Feedback type="invalid">
                        Fehlermeldung kommt hier ...
                      </Form.Control.Feedback>
                    </InputGroup>
                    <Row>
                      <Col>
                        <Form.Text className="text-muted">
                          <SearchPopver {...this.props}/>
                        </Form.Text>
                      </Col>
                    </Row>
                  </Col>
                  <Col md="4" className="align-self-center mt-2 col-md-4">
                    <Button variant="primary" size="lg" block>{t( 'sites.UserManagement.search' )}</Button>
                  </Col>
                </Form.Group>
              </Form>
              <Row>
                <Col>
                  <Accordion defaultActiveKey="0" id={"accordion"} className="mt-4 mb-5">
                    <Card className={"card--filter card__border-radius"}>
                      <Accordion.Toggle as={Card.Header} variant="link"
                                        className="card-header--transparent border-bottom-0 pb-2"
                                        eventKey="0"
                                        data-toggle="collapse"
                                        data-target="#filterCollapse"
                                        aria-expanded="false"
                                        aria-controls="0">
                        <div className="d-flex">
                          <div className="pt-2 pr-2">
                            <FontAwesomeIcon icon={['fal', 'filter']} size="lg"/>
                          </div>
                          <div className="pt-1 pl-1 mb-0">
                            <h3>
                              {t( 'sites.UserManagement.filter' )}
                            </h3>
                          </div>
                          <div className="ml-auto pt-2">
                            <FontAwesomeIcon icon={['fal', 'chevron-down']} size="lg"/>
                          </div>
                        </div>
                      </Accordion.Toggle>
                      <Accordion.Collapse eventKey="1"
                                          id="filterCollapse"
                                          className=""
                                          aria-labelledby="1"
                                          data-parent="#accordion">
                        <Card.Body className="pt-2">
                          <Row className={"mb-5"}>
                            <Form>
                              <Form.Row>
                                <Col md={"6"}>
                                  <fieldset>
                                    <Form.Group as={Row} className="col-md-12" controlId="formGridUserManagement">
                                      <Form.Label as="legend" column md={12}>
                                        <h4>{t( 'sites.UserManagement.userManagementRoles' )}</h4>
                                      </Form.Label>
                                      <Col md={6} className={"mb-3"}>
                                        <div className="d-flex flex-row">
                                          <div className="flex-shrink-1 align-self-baseline">
                                            <UserManagementPopOver/>
                                          </div>
                                          <Form.Check
                                            inline
                                            type="checkbox"
                                            label={t( 'sites.UserManagement.mandantenAdmin' )}
                                            name="formHorizontalCheckbox"
                                            id="formHorizontalCheckbox1"
                                            className={"bpw-checkbox bpw-checkbox-primary align-self-end"}
                                          />
                                        </div>
                                      </Col>
                                      <Col md={6} className={"mb-3"}>
                                        <div className="d-flex flex-row">
                                          <div className="flex-shrink-1 align-self-baseline">
                                            <UserManagementPopOver/>
                                          </div>
                                          <Form.Check
                                            inline
                                            type="checkbox"
                                            label={t( 'sites.UserManagement.subMandantenAdmin' )}
                                            name="formHorizontalCheckbox"
                                            id="formHorizontalCheckbox2"
                                            className={"bpw-checkbox bpw-checkbox-primary align-self-end"}
                                          />
                                        </div>
                                      </Col>
                                    </Form.Group>
                                  </fieldset>
                                </Col>
                                <Col md={"6"} className={"border-left-0 border-md-left border-primary"}>
                                  <fieldset>
                                    <Form.Group as={Row} className="col-md-12" controlId="formGridUserAllgemein">
                                      <Form.Label as="legend" column md={12}>
                                        <h4>{t( 'sites.UserManagement.general' )}</h4>
                                      </Form.Label>
                                      <Col md={6} className={"mb-3"}>
                                        <div className="d-flex flex-row">
                                          <div className="flex-shrink-1 align-self-baseline">
                                            <UserManagementPopOver/>
                                          </div>
                                          <Form.Check
                                            inline
                                            type="checkbox"
                                            label={t( 'sites.UserManagement.readEverything' )}
                                            name="formHorizontalCheckbox"
                                            id="formHorizontalCheckbox3"
                                            className={"bpw-checkbox bpw-checkbox-primary align-self-end"}
                                          />
                                        </div>
                                      </Col>
                                      <Col md={6} className={"mb-3"}>
                                        <div className="d-flex flex-row">
                                          <div className="flex-shrink-1 align-self-baseline">
                                            <UserManagementPopOver/>
                                          </div>
                                          <Form.Check
                                            inline
                                            type="checkbox"
                                            label={t( 'sites.UserManagement.admin' )}
                                            name="formHorizontalCheckbox"
                                            id="formHorizontalCheckbox4"
                                            className={"bpw-checkbox bpw-checkbox-primary align-self-end"}
                                          />
                                        </div>
                                      </Col>
                                    </Form.Group>
                                  </fieldset>
                                </Col>
                              </Form.Row>
                              <Col className={"mt-4 mb-4"} md={"12"}>
                                <h4>{t( 'sites.UserManagement.applicationRole' )}</h4>
                              </Col>
                              <Form.Row>
                                <Col md={"4"}>
                                  <fieldset>
                                    <Form.Group as={Row} className="col-md-12" controlId="formGridUserManagement">
                                      <Form.Label as="legend" column md={12} className={"mb-2"}>
                                        <h5>{t( 'sites.UserManagement.deliveries' )}:</h5>
                                      </Form.Label>
                                      <Col md={12} className={"mb-3"}>
                                        <div className="d-flex flex-row">
                                          <div className="flex-shrink-1 align-self-baseline">
                                            <UserManagementPopOver/>
                                          </div>
                                          <Form.Check
                                            inline
                                            type="checkbox"
                                            label={t( 'sites.UserManagement.showData' )}
                                            name="formHorizontalCheckbox"
                                            id="formHorizontalCheckbox5"
                                            className={"bpw-checkbox bpw-checkbox-primary align-self-end"}
                                          />
                                        </div>
                                      </Col>
                                      <Col md={12} className={"mb-3"}>
                                        <div className="d-flex flex-row">
                                          <div className="flex-shrink-1 align-self-baseline">
                                            <UserManagementPopOver/>
                                          </div>
                                          <Form.Check
                                            inline
                                            type="checkbox"
                                            label={t( 'sites.UserManagement.creatingData' )}
                                            name="formHorizontalCheckbox"
                                            id="formHorizontalCheckbox6"
                                            className={"bpw-checkbox bpw-checkbox-primary align-self-end"}
                                          />
                                        </div>
                                      </Col>
                                      <Col md={12} className={"mb-3"}>
                                        <div className="d-flex flex-row">
                                          <div className="flex-shrink-1 align-self-baseline">
                                            <UserManagementPopOver/>
                                          </div>
                                          <Form.Check
                                            inline
                                            type="checkbox"
                                            label={t( 'sites.UserManagement.writeData' )}
                                            name="formHorizontalCheckbox"
                                            id="formHorizontalCheckbox7"
                                            className={"bpw-checkbox bpw-checkbox-primary align-self-end"}
                                          />
                                        </div>
                                      </Col>
                                      <Col md={12} className={"mb-3"}>
                                        <div className="d-flex flex-row">
                                          <div className="flex-shrink-1 align-self-baseline">
                                            <UserManagementPopOver/>
                                          </div>
                                          <Form.Check
                                            inline
                                            type="checkbox"
                                            label={t( 'sites.UserManagement.deleteData' )}
                                            name="formHorizontalCheckbox"
                                            id="formHorizontalCheckbox8"
                                            className={"bpw-checkbox bpw-checkbox-primary align-self-end"}
                                          />
                                        </div>
                                      </Col>
                                      <Col md={12} className={"mb-3"}>
                                        <div className="d-flex flex-row">
                                          <div className="flex-shrink-1 align-self-baseline">
                                            <UserManagementPopOver/>
                                          </div>
                                          <Form.Check
                                            inline
                                            type="checkbox"
                                            label={t( 'sites.UserManagement.forwardData' )}
                                            name="formHorizontalCheckbox"
                                            id="formHorizontalCheckbox9"
                                            className={"bpw-checkbox bpw-checkbox-primary align-self-end"}
                                          />
                                        </div>
                                      </Col>
                                    </Form.Group>
                                  </fieldset>
                                </Col>
                                <Col md={"4"} className={"border-left-0 border-md-left border-primary"}>
                                  <fieldset>
                                    <Form.Group as={Row} className="col-md-12" controlId="formGridUserAllgemein">
                                      <Form.Label as="legend" column md={12} className={"mb-2"}>
                                        <h5>{t( 'sites.UserManagement.assets' )}:</h5>
                                      </Form.Label>
                                      <Col md={12} className={"mb-3"}>
                                        <div className="d-flex flex-row">
                                          <div className="flex-shrink-1 align-self-baseline">
                                            <UserManagementPopOver/>
                                          </div>
                                          <Form.Check
                                            inline
                                            type="checkbox"
                                            label={t( 'sites.UserManagement.showData' )}
                                            name="formHorizontalCheckbox"
                                            id="formHorizontalCheckbox10"
                                            className={"bpw-checkbox bpw-checkbox-primary align-self-end"}
                                          />
                                        </div>
                                      </Col>
                                      <Col md={12} className={"mb-3"}>
                                        <div className="d-flex flex-row">
                                          <div className="flex-shrink-1 align-self-baseline">
                                            <UserManagementPopOver/>
                                          </div>
                                          <Form.Check
                                            inline
                                            type="checkbox"
                                            label={t( 'sites.UserManagement.creatingData' )}
                                            name="formHorizontalCheckbox"
                                            id="formHorizontalCheckbox11"
                                            className={"bpw-checkbox bpw-checkbox-primary align-self-end"}
                                          />
                                        </div>
                                      </Col>
                                      <Col md={12} className={"mb-3"}>
                                        <div className="d-flex flex-row">
                                          <div className="flex-shrink-1 align-self-baseline">
                                            <UserManagementPopOver/>
                                          </div>
                                          <Form.Check
                                            inline
                                            type="checkbox"
                                            label={t( 'sites.UserManagement.editData' )}
                                            name="formHorizontalCheckbox"
                                            id="formHorizontalCheckbox12"
                                            className={"bpw-checkbox bpw-checkbox-primary align-self-end"}
                                          />
                                        </div>
                                      </Col>
                                      <Col md={12} className={"mb-3"}>
                                        <div className="d-flex flex-row">
                                          <div className="flex-shrink-1 align-self-baseline">
                                            <UserManagementPopOver/>
                                          </div>
                                          <Form.Check
                                            inline
                                            type="checkbox"
                                            label={t( 'sites.UserManagement.deleteData' )}
                                            name="formHorizontalCheckbox"
                                            id="formHorizontalCheckbox13"
                                            className={"bpw-checkbox bpw-checkbox-primary align-self-end"}
                                          />
                                        </div>
                                      </Col>
                                      <Col md={12} className={"mb-3"}>
                                        <div className="d-flex flex-row">
                                          <div className="flex-shrink-1 align-self-baseline">
                                            <UserManagementPopOver/>
                                          </div>
                                          <Form.Check
                                            inline
                                            type="checkbox"
                                            label={t( 'sites.UserManagement.forwardData' )}
                                            name="formHorizontalCheckbox"
                                            id="formHorizontalCheckbox14"
                                            className={"bpw-checkbox bpw-checkbox-primary align-self-end"}
                                          />
                                        </div>
                                      </Col>
                                    </Form.Group>
                                  </fieldset>
                                </Col>
                                <Col md={"4"} className={"border-left-0 border-md-left border-primary"}>
                                  <fieldset>
                                    <Form.Group as={Row} className="col-md-12" controlId="formGridUserAllgemein">
                                      <Form.Label as="legend" column md={12} className={"mb-2"}>
                                        <h5>{t( 'sites.UserManagement.tracker' )}:</h5>
                                      </Form.Label>
                                      <Col md={12} className={"mb-3"}>
                                        <div className="d-flex flex-row">
                                          <div className="flex-shrink-1 align-self-baseline">
                                            <UserManagementPopOver/>
                                          </div>
                                          <Form.Check
                                            inline
                                            type="checkbox"
                                            label={t( 'sites.UserManagement.showData' )}
                                            name="formHorizontalCheckbox"
                                            id="formHorizontalCheckbox15"
                                            className={"bpw-checkbox bpw-checkbox-primary align-self-end"}
                                          />
                                        </div>
                                      </Col>
                                      <Col md={12} className={"mb-3"}>
                                        <div className="d-flex flex-row">
                                          <div className="flex-shrink-1 align-self-baseline">
                                            <UserManagementPopOver/>
                                          </div>
                                          <Form.Check
                                            inline
                                            type="checkbox"
                                            label={t( 'sites.UserManagement.editData' )}
                                            name="formHorizontalCheckbox"
                                            id="formHorizontalCheckbox16"
                                            className={"bpw-checkbox bpw-checkbox-primary align-self-end"}
                                          />
                                        </div>
                                      </Col>
                                    </Form.Group>
                                  </fieldset>
                                </Col>
                              </Form.Row>
                            </Form>
                          </Row>
                          <Row className="mb-1">
                            <Col>
                              <div className="d-flex flex-column flex-md-row flex-lg-row">
                                <div className="p-1 flex-fill">
                                  <Button size={"md"} variant="light" type="submit" block>{t( 'sites.UserManagement.btnSetFilterBack' )}</Button>
                                </div>
                                <div className="p-1 flex-fill">
                                  <Button size={"md"} variant="primary" type="submit" block>{t( 'sites.UserManagement.btnViewResult' )}</Button>
                                </div>
                              </div>
                            </Col>
                          </Row>
                        </Card.Body>
                      </Accordion.Collapse>
                    </Card>
                  </Accordion>
                </Col>
              </Row>

              <Row className={"mb-4"}>
                <Col md={"4"}>
                  <Card>
                    <Card.Body>
                      <div className="d-flex mb-2">
                        <div className="pt-2 bd-highlight">
                          <FontAwesomeIcon icon={['fal', 'user']} size="lg" className="text-primary"/>
                        </div>
                        <div className="ml-auto p-2">
                          <FontAwesomeIcon icon={['fal', 'door-open']} size="lg" className="text-primary"/>
                        </div>
                      </div>
                      <Row className="">
                        <Col md={"12"}>
                          <h5>
                            <span className="text-muted">{t( 'sites.UserManagement.lastName' )}:</span> Ackner
                          </h5>
                        </Col>
                        <Col md={"12"}>
                          <h5>
                            <span className="text-muted">{t( 'sites.UserManagement.firstGivenName' )}:</span> Günther
                          </h5>
                        </Col>
                        <Col md={"12"}>
                          <h5>
                            <span className="text-muted">{t( 'sites.UserManagement.subMandant' )}:</span> BPW
                          </h5>
                        </Col>
                        <Col md={"12"}>
                          <h6 className="mt-2">
                            <span className="text-muted">{t( 'sites.UserManagement.lastLogin' )}:</span> <span className="pull-right">22.06.2019 - 17:54</span>
                          </h6>
                        </Col>
                        <Col md={"12"}>
                          <Button className="mt-2" variant="primary" block>{t( 'sites.UserManagement.editProfile' )}</Button>
                        </Col>
                      </Row>

                    </Card.Body>
                  </Card>
                </Col>
                <Col md={"4"}>
                  <Card>
                    <Card.Body>
                      <div className="d-flex mb-2">
                        <div className="pt-2 bd-highlight">
                          <FontAwesomeIcon icon={['fal', 'user']} size="lg" className="text-primary"/>
                        </div>
                        <div className="ml-auto p-2">
                          <FontAwesomeIcon icon={['fal', 'door-closed']} size="lg" className="text-primary"/>
                        </div>
                      </div>
                      <Row className="">
                        <Col md={"12"}>
                          <h5>
                            <span className="text-muted">{t( 'sites.UserManagement.lastName' )}:</span> Max
                          </h5>
                        </Col>
                        <Col md={"12"}>
                          <h5>
                            <span className="text-muted">{t( 'sites.UserManagement.firstGivenName' )}:</span> Mustermann
                          </h5>
                        </Col>
                        <Col md={"12"}>
                          <h5>
                            <span className="text-muted">{t( 'sites.UserManagement.subMandant' )}:</span> Thyssenkrupp
                          </h5>
                        </Col>
                        <Col md={"12"}>
                          <h6 className="mt-2">
                            <span className="text-muted">{t( 'sites.UserManagement.lastLogin' )}:</span> <span className="pull-right">22.06.2019 - 17:54</span>
                          </h6>
                        </Col>
                        <Col md={"12"}>
                          <Button className="mt-2" variant="primary" block>{t( 'sites.UserManagement.editProfile' )}</Button>
                        </Col>
                      </Row>

                    </Card.Body>
                  </Card>
                </Col>
                <Col md={"4"}>
                  <Card>
                    <Card.Body>
                      <div className="d-flex mb-2">
                        <div className="pt-2 bd-highlight">
                          <FontAwesomeIcon icon={['fal', 'user']} size="lg" className="text-primary"/>
                        </div>
                        <div className="ml-auto p-2">
                          <FontAwesomeIcon icon={['fal', 'door-open']} size="lg" className="text-primary"/>
                        </div>
                      </div>
                      <Row className="">
                        <Col md={"12"}>
                          <h5>
                            <span className="text-muted">{t( 'sites.UserManagement.lastName' )}:</span> Jana
                          </h5>
                        </Col>
                        <Col md={"12"}>
                          <h5>
                            <span className="text-muted">{t( 'sites.UserManagement.firstGivenName' )}:</span> Rossmann
                          </h5>
                        </Col>
                        <Col md={"12"}>
                          <h5>
                            <span className="text-muted">{t( 'sites.UserManagement.subMandant' )}:</span> BPW
                          </h5>
                        </Col>
                        <Col md={"12"}>
                          <h6 className="mt-2">
                            <span className="text-muted">{t( 'sites.UserManagement.lastLogin' )}:</span> <span className="pull-right">22.06.2019 - 17:54</span>
                          </h6>
                        </Col>
                        <Col md={"12"}>
                          <Button className="mt-2" variant="primary" block>{t( 'sites.UserManagement.editProfile' )}</Button>
                        </Col>
                      </Row>

                    </Card.Body>
                  </Card>
                </Col>
              </Row>
              <Row>
                <Col className={"mb-0 mb-md-4 mt-4"} lg={"12"}>
                  <h1>{t( 'sites.UserManagement.createUser' )}</h1>
                </Col>
              </Row>
              <Form>
                <Row>
                  <Col md={"12"} className={"mb-md-3"}>
                    <Form.Row>
                      <Form.Group as={Col} md={"6"} controlId="formGridFirstName">
                        <Form.Label>{t( 'sites.UserManagement.firstGivenName' )}</Form.Label>
                        <FormControl
                          type="text"
                          min="1"
                          required="required"
                          aria-required="true"
                          className="form-control-lg"
                          placeholder={t( 'sites.UserManagement.enterFirstGivenName' )}
                          aria-label={t( 'sites.UserManagement.firstGivenName' )}
                        />
                        <Form.Control.Feedback type="invalid">
                          {t( 'sites.UserManagement.errorMessage' )}
                        </Form.Control.Feedback>
                      </Form.Group>
                      <Form.Group as={Col} md={"6"} controlId="formGridLastName">
                        <Form.Label>{t( 'sites.UserManagement.lastName' )}</Form.Label>
                        <Form.Control
                          type="text"
                          required="required"
                          className="form-control-lg"
                          placeholder={t( 'sites.UserManagement.enterLastName' )}
                          aria-label={t( 'sites.UserManagement.lastName' )}
                        />
                      </Form.Group>
                    </Form.Row>
                  </Col>
                </Row>
                <Row>
                  <Col md={"12"} className={"mb-md-3"}>
                    <Form.Row>
                      <Form.Group as={Col} md={"6"} controlId="formGridEmail">
                        <Form.Label>{t( 'sites.UserManagement.emailAddress' )}</Form.Label>
                        <FormControl
                          type="email"
                          min="1"
                          required="required"
                          aria-required="true"
                          className="form-control-lg"
                          placeholder={t( 'sites.UserManagement.enterTheEmailSddress' )}
                          aria-label={t( 'sites.UserManagement.emailAddress' )}
                        />
                        <Form.Control.Feedback type="invalid">
                          {t( 'sites.UserManagement.errorMessage' )}
                        </Form.Control.Feedback>
                      </Form.Group>
                      <Form.Group as={Col} md={"6"} controlId="formGridMandant">
                        <Form.Label>{t( 'sites.UserManagement.mandant' )}</Form.Label>
                        <Form.Control
                          type="selection"
                          as="select"
                          required="required"
                          className="form-control-lg"
                          placeholder={t( 'sites.UserManagement.selectMandant' )}>
                          <option value="default">{t( 'sites.UserManagement.selectMandant' )}</option>
                          <option value={"Mandant A"}>Mandant A</option>
                          <option value={"Mandant B"}>Mandant B</option>
                        </Form.Control>
                      </Form.Group>
                    </Form.Row>
                  </Col>
                </Row>
                <Row className={"mb-md-4"}>
                  <Col md={"12"} className={"mb-md-3"}>
                    <Form.Row>
                      <Col className="">
                        <div className="d-flex mt-4">
                          <FontAwesomeIcon icon={['fal', 'toggle-on']} size="lg" className="" transform=""/><span className="ml-2">{t( 'sites.UserManagement.accountActiv' )}</span>
                        </div>
                      </Col>
                      <Form.Group as={Col} md={"6"} controlId="formGridMandant">
                        <Form.Label>{t( 'sites.UserManagement.subMandant' )}</Form.Label>
                        <Form.Control
                          type="selection"
                          as="select"
                          required="required"
                          className="form-control-lg"
                          placeholder={t( 'sites.UserManagement.selectSubMandant' )}>
                          <option value="default">{t( 'sites.UserManagement.selectSubMandant' )}</option>
                          <option value={"Submandant A"}>Submandant A</option>
                          <option value={"Submandant B"}>Submandant B</option>
                        </Form.Control>
                      </Form.Group>
                    </Form.Row>
                  </Col>
                </Row>
                <Row className={"mb-md-5"}>
                  <Form>
                    <Form.Row className={"mb-md-5"}>
                      <Col md={"6"}>
                        <fieldset>
                          <Form.Group as={Row} className="col-md-12" controlId="formGridUserManagement">
                            <Form.Label as="legend" column md={12}>
                              <h4>{t( 'sites.UserManagement.userManagementRoles' )}</h4>
                            </Form.Label>
                            <Col md={6} className={"mb-3"}>
                              <div className="d-flex flex-row">
                                <div className="flex-shrink-1 align-self-baseline">
                                  <UserManagementPopOver/>
                                </div>
                                <Form.Check
                                  inline
                                  type="checkbox"
                                  label={t( 'sites.UserManagement.mandantenAdmin' )}
                                  name="formHorizontalCheckbox"
                                  id="formHorizontalCheckbox1"
                                  className={"bpw-checkbox bpw-checkbox-primary align-self-end"}
                                />
                              </div>
                            </Col>
                            <Col md={6} className={"mb-3"}>
                              <div className="d-flex flex-row">
                                <div className="flex-shrink-1 align-self-baseline">
                                  <UserManagementPopOver/>
                                </div>
                                <Form.Check
                                  inline
                                  type="checkbox"
                                  label={t( 'sites.UserManagement.subMandantenAdmin' )}
                                  name="formHorizontalCheckbox"
                                  id="formHorizontalCheckbox2"
                                  className={"bpw-checkbox bpw-checkbox-primary align-self-end"}
                                />
                              </div>
                            </Col>
                          </Form.Group>
                        </fieldset>
                      </Col>
                      <Col md={"6"} className={"border-left-0 border-md-left border-primary"}>
                        <fieldset>
                          <Form.Group as={Row} className="col-md-12" controlId="formGridUserAllgemein">
                            <Form.Label as="legend" column md={12}>
                              <h4>{t( 'sites.UserManagement.general' )}</h4>
                            </Form.Label>
                            <Col md={6} className={"mb-3"}>
                              <div className="d-flex flex-row">
                                <div className="flex-shrink-1 align-self-baseline">
                                  <UserManagementPopOver/>
                                </div>
                                <Form.Check
                                  inline
                                  type="checkbox"
                                  label={t( 'sites.UserManagement.readEverything' )}
                                  name="formHorizontalCheckbox"
                                  id="formHorizontalCheckbox3"
                                  className={"bpw-checkbox bpw-checkbox-primary align-self-end"}
                                />
                              </div>
                            </Col>
                            <Col md={6} className={"mb-3"}>
                              <div className="d-flex flex-row">
                                <div className="flex-shrink-1 align-self-baseline">
                                  <UserManagementPopOver/>
                                </div>
                                <Form.Check
                                  inline
                                  type="checkbox"
                                  label={t( 'sites.UserManagement.admin' )}
                                  name="formHorizontalCheckbox"
                                  id="formHorizontalCheckbox4"
                                  className={"bpw-checkbox bpw-checkbox-primary align-self-end"}
                                />
                              </div>
                            </Col>
                          </Form.Group>
                        </fieldset>
                      </Col>
                    </Form.Row>
                    <Col className={"mt-4 mb-4"} md={"12"}>
                      <h4>{t( 'sites.UserManagement.applicationRole' )}</h4>
                    </Col>
                    <Form.Row>
                      <Col md={"4"}>
                        <fieldset>
                          <Form.Group as={Row} className="col-md-12" controlId="formGridUserManagement">
                            <Form.Label as="legend" column md={12} className={"mb-2"}>
                              <h5>{t( 'sites.UserManagement.deliveries' )}:</h5>
                            </Form.Label>
                            <Col md={12} className={"mb-3"}>
                              <div className="d-flex flex-row">
                                <div className="flex-shrink-1 align-self-baseline">
                                  <UserManagementPopOver/>
                                </div>
                                <Form.Check
                                  inline
                                  type="checkbox"
                                  label={t( 'sites.UserManagement.showData' )}
                                  name="formHorizontalCheckbox"
                                  id="formHorizontalCheckbox5"
                                  className={"bpw-checkbox bpw-checkbox-primary align-self-end"}
                                />
                              </div>
                            </Col>
                            <Col md={12} className={"mb-3"}>
                              <div className="d-flex flex-row">
                                <div className="flex-shrink-1 align-self-baseline">
                                  <UserManagementPopOver/>
                                </div>
                                <Form.Check
                                  inline
                                  type="checkbox"
                                  label={t( 'sites.UserManagement.creatingData' )}
                                  name="formHorizontalCheckbox"
                                  id="formHorizontalCheckbox6"
                                  className={"bpw-checkbox bpw-checkbox-primary align-self-end"}
                                />
                              </div>
                            </Col>
                            <Col md={12} className={"mb-3"}>
                              <div className="d-flex flex-row">
                                <div className="flex-shrink-1 align-self-baseline">
                                  <UserManagementPopOver/>
                                </div>
                                <Form.Check
                                  inline
                                  type="checkbox"
                                  label={t( 'sites.UserManagement.writeData' )}
                                  name="formHorizontalCheckbox"
                                  id="formHorizontalCheckbox7"
                                  className={"bpw-checkbox bpw-checkbox-primary align-self-end"}
                                />
                              </div>
                            </Col>
                            <Col md={12} className={"mb-3"}>
                              <div className="d-flex flex-row">
                                <div className="flex-shrink-1 align-self-baseline">
                                  <UserManagementPopOver/>
                                </div>
                                <Form.Check
                                  inline
                                  type="checkbox"
                                  label={t( 'sites.UserManagement.deleteData' )}
                                  name="formHorizontalCheckbox"
                                  id="formHorizontalCheckbox8"
                                  className={"bpw-checkbox bpw-checkbox-primary align-self-end"}
                                />
                              </div>
                            </Col>
                            <Col md={12} className={"mb-3"}>
                              <div className="d-flex flex-row">
                                <div className="flex-shrink-1 align-self-baseline">
                                  <UserManagementPopOver/>
                                </div>
                                <Form.Check
                                  inline
                                  type="checkbox"
                                  label={t( 'sites.UserManagement.forwardData' )}
                                  name="formHorizontalCheckbox"
                                  id="formHorizontalCheckbox9"
                                  className={"bpw-checkbox bpw-checkbox-primary align-self-end"}
                                />
                              </div>
                            </Col>
                          </Form.Group>
                        </fieldset>
                      </Col>
                      <Col md={"4"} className={"border-left-0 border-md-left border-primary"}>
                        <fieldset>
                          <Form.Group as={Row} className="col-md-12" controlId="formGridUserAllgemein">
                            <Form.Label as="legend" column md={12} className={"mb-2"}>
                              <h5>{t( 'sites.UserManagement.assets' )}:</h5>
                            </Form.Label>
                            <Col md={12} className={"mb-3"}>
                              <div className="d-flex flex-row">
                                <div className="flex-shrink-1 align-self-baseline">
                                  <UserManagementPopOver/>
                                </div>
                                <Form.Check
                                  inline
                                  type="checkbox"
                                  label={t( 'sites.UserManagement.showData' )}
                                  name="formHorizontalCheckbox"
                                  id="formHorizontalCheckbox10"
                                  className={"bpw-checkbox bpw-checkbox-primary align-self-end"}
                                />
                              </div>
                            </Col>
                            <Col md={12} className={"mb-3"}>
                              <div className="d-flex flex-row">
                                <div className="flex-shrink-1 align-self-baseline">
                                  <UserManagementPopOver/>
                                </div>
                                <Form.Check
                                  inline
                                  type="checkbox"
                                  label={t( 'sites.UserManagement.creatingData' )}
                                  name="formHorizontalCheckbox"
                                  id="formHorizontalCheckbox11"
                                  className={"bpw-checkbox bpw-checkbox-primary align-self-end"}
                                />
                              </div>
                            </Col>
                            <Col md={12} className={"mb-3"}>
                              <div className="d-flex flex-row">
                                <div className="flex-shrink-1 align-self-baseline">
                                  <UserManagementPopOver/>
                                </div>
                                <Form.Check
                                  inline
                                  type="checkbox"
                                  label={t( 'sites.UserManagement.editData' )}
                                  name="formHorizontalCheckbox"
                                  id="formHorizontalCheckbox12"
                                  className={"bpw-checkbox bpw-checkbox-primary align-self-end"}
                                />
                              </div>
                            </Col>
                            <Col md={12} className={"mb-3"}>
                              <div className="d-flex flex-row">
                                <div className="flex-shrink-1 align-self-baseline">
                                  <UserManagementPopOver/>
                                </div>
                                <Form.Check
                                  inline
                                  type="checkbox"
                                  label={t( 'sites.UserManagement.deleteData' )}
                                  name="formHorizontalCheckbox"
                                  id="formHorizontalCheckbox13"
                                  className={"bpw-checkbox bpw-checkbox-primary align-self-end"}
                                />
                              </div>
                            </Col>
                            <Col md={12} className={"mb-3"}>
                              <div className="d-flex flex-row">
                                <div className="flex-shrink-1 align-self-baseline">
                                  <UserManagementPopOver/>
                                </div>
                                <Form.Check
                                  inline
                                  type="checkbox"
                                  label={t( 'sites.UserManagement.forwardData' )}
                                  name="formHorizontalCheckbox"
                                  id="formHorizontalCheckbox14"
                                  className={"bpw-checkbox bpw-checkbox-primary align-self-end"}
                                />
                              </div>
                            </Col>
                          </Form.Group>
                        </fieldset>
                      </Col>
                      <Col md={"4"} className={"border-left-0 border-md-left border-primary"}>
                        <fieldset>
                          <Form.Group as={Row} className="col-md-12" controlId="formGridUserAllgemein">
                            <Form.Label as="legend" column md={12} className={"mb-2"}>
                              <h5>{t( 'sites.UserManagement.tracker' )}:</h5>
                            </Form.Label>
                            <Col md={12} className={"mb-3"}>
                              <div className="d-flex flex-row">
                                <div className="flex-shrink-1 align-self-baseline">
                                  <UserManagementPopOver/>
                                </div>
                                <Form.Check
                                  inline
                                  type="checkbox"
                                  label={t( 'sites.UserManagement.showData' )}
                                  name="formHorizontalCheckbox"
                                  id="formHorizontalCheckbox15"
                                  className={"bpw-checkbox bpw-checkbox-primary align-self-end"}
                                />
                              </div>
                            </Col>
                            <Col md={12} className={"mb-3"}>
                              <div className="d-flex flex-row">
                                <div className="flex-shrink-1 align-self-baseline">
                                  <UserManagementPopOver/>
                                </div>
                                <Form.Check
                                  inline
                                  type="checkbox"
                                  label={t( 'sites.UserManagement.editData' )}
                                  name="formHorizontalCheckbox"
                                  id="formHorizontalCheckbox16"
                                  className={"bpw-checkbox bpw-checkbox-primary align-self-end"}
                                />
                              </div>
                            </Col>
                          </Form.Group>
                        </fieldset>
                      </Col>
                    </Form.Row>
                  </Form>
                </Row>
                <Row className="mb-md-5">
                  <Col>
                    <div className="d-flex flex-column flex-md-row flex-lg-row">
                      <div className="p-1 flex-fill">
                        <Button size={"md"} variant="light" type="submit" block>{t( 'sites.UserManagement.btnDiscardUser' )}</Button>
                      </div>
                      <div className="p-1 flex-fill">
                        <Button size={"md"} variant="secondary" type="submit" block>{t( 'sites.UserManagement.btnCreateUser' )}</Button>
                      </div>
                    </div>
                  </Col>
                </Row>
              </Form>
            </Col>
          </Row>

        </>
      </Container>
    </div>
  }

}

export default UserManagement