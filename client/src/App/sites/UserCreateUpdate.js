import React, { Component } from 'react';
import { Alert, Button, ButtonToolbar, Col, Container, Row, ToggleButton, ToggleButtonGroup } from "react-bootstrap";
import Form from "react-bootstrap/es/Form";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Loader from "react-loader-spinner";
import Async from 'react-select/async';
import Select from 'react-select';
import withQueryParams from "react-router-query-params";

class UserCreateUpdate extends Component {

  _isMounted = false;
  isUpdateMode: false

  constructor() {
    super();

    this.state = {
      user: null,
      selectedRole: null,
      userRoleValid: null,
      clients: [],
      selectedClient: null,
      clientIsValid: null,
      clientIsInValid: null,
      clientId: 1,
      subclients: [],
      selectedSublient: null,
      subclientIsValid: null,
      subclientIsInValid: null,
      subclientId: 13,
      subclientsLoaded: false,
      userMail: null,
      userMailInputInvalid: null,
      userMailInputValid: null,
      userGivenName: null,
      userGivenNameInputInvalid: null,
      userGivenNameInputValid: null,
      userLastName: null,
      userLastNameInputInvalid: null,
      userLastNameInputValid: null,
      userPassword: "null",
      userPasswordInputInvalid: null,
      userPasswordInputValid: null,
      userActive: true,
      saveLoading: false,
      saveSuccess: false,
      formFail: false,
      formFailText: "",
      saveHasError: false,
      saveError: null,
      userLoaded: true
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  componentWillMount() {
    if ( this.props.match.params.id !== undefined ) {
      this.isUpdateMode = true
    }
  }

  async componentDidMount() {
    this._isMounted = true;
    if ( this.props.match.params.id !== undefined ) {
      this.isUpdateMode = true
      await this.fetchUser()
    } else {
      await this.setState( {userLoaded: true} )
      await this.fetchSubclients()
      let {subclients} = this.state
      await this.setState( {
                             userLoaded: false,
                             selectedRole: "defaultUser",
                             userRoleValid: true,
                             subclients: subclients,
                             selectedSublient: ((subclients.length > 0) ? subclients[0] : undefined),
                             subclientIsValid: true,
                             subclientIsInValid: false
                           } )
    }
  }

  getUSerRole( bpwAdminBpw, bpwAdminSender, bpwAdminRecipient ) {
    if ( bpwAdminBpw !== null && bpwAdminBpw !== 0 ) {
      return "globalAdmin"
    }
    if ( bpwAdminSender !== null && bpwAdminSender !== 0 ) {
      return "clientAdmin"
    }
    if ( bpwAdminRecipient !== null && bpwAdminRecipient !== 0 ) {
      return "sublientAdmin"
    }
    return "defaultUser"
  }

  fetchUser() {
    let {id} = this.props.match.params
    if ( id !== undefined ) {
      this.setState( {userLoaded: true} )
      return fetch( '/user-azure-ui-service/Users/' + id )
        .then( res => res.json() )
        .then( ( user ) => {
          /**
           * givenName: "ZZZ",
           surname: "ZZZ",
           objectId: "059e02ba-fd7c-4f1b-ad95-f8430809f1e1",
           accountEnabled: true,
           displayName: "ZZZ ZZZ",
           bpwMandant: 1,
           bpwMandantSub: 1,
           bpwAdminBpw: null,
           bpwAdminSender: null,
           bpwAdminRecipient: null,
           bpwCompany: null,
           mail: "ZZZ@bpw.de",
           bpwMandantClient: {
odataId: "1",
id: 1,
name: "BPW"
},
           bpwMandantSubClient: null
           */
          let {bpwAdminBpw, bpwAdminSender, bpwAdminRecipient, bpwMandantClient, bpwMandant, bpwMandantSub, mail, givenName, surname, accountEnabled, subclients, clients, bpwMandantSubClient} = user
          console.log( user )
          this.setState( {
                           user,
                           selectedRole: this.getUSerRole( bpwAdminBpw, bpwAdminSender, bpwAdminRecipient ),
                           userRoleValid: true,
                           subclients: subclients,
                           clients: clients,
                           userLoaded: false,
                           selectedClient: bpwMandantClient,
                           clientIsValid: ((bpwMandantClient !== null)),
                           clientIsInValid: ((bpwMandantClient === null)),
                           clientId: bpwMandant,
                           selectedSublient: bpwMandantSubClient,
                           subclientIsValid: ((bpwMandantSubClient !== null)),
                           subclientIsInValid: ((bpwMandantSubClient === null)),
                           subclientId: bpwMandantSub,
                           userMail: mail,
                           userMailInputInvalid: false,
                           userMailInputValid: true,
                           userGivenName: givenName,
                           userGivenNameInputInvalid: false,
                           userGivenNameInputValid: true,
                           userLastName: surname,
                           userLastNameInputInvalid: false,
                           userLastNameInputValid: true,
                           userActive: accountEnabled
                         } )
          if ( subclients.length === 0 )
            this.fetchSubclientsByClient( bpwMandant )
        } )
    }
  }

  async fetchSubclients() {
    return fetch( '/user-azure-ui-service/Subclients' )
      .then( res => res.json() )
      .then( ( subclients ) => {
        if ( this._isMounted )
          this.setState( {subclients: subclients, subclientsLoaded: false} )
        return subclients
      } )
  }

  async fetchClients() {
    return fetch( '/user-azure-ui-service/Clients' )
      .then( res => res.json() )
      .then( ( clients ) => clients )
  }

  fetchSubclientsByClient( clientId ) {
    fetch( '/user-azure-ui-service/Subclients/' + clientId )
      .then( res => res.json() )
      .then( ( subclients ) => {
        if ( this._isMounted )
          this.setState( {subclients: subclients, subclientsLoaded: false} )
      } )
  }

  onPressEnter( e ) {
    if ( e.target.type === 'text' && e.which === 13 /* Enter */ ) {
      this.setState( {
                       searchQuery: e.target.value
                     } );
      this.handleChangeSearchInput( e, undefined, e.target.value )

      e.preventDefault();
    }
  }

  getClientOptionValue = ( option ) => option.id; // maps the result 'id' as the 'value'

  getClientOptionLabel = ( option ) => option.name;

  handleChangeClient( item ) {
    if ( item !== null && item !== undefined && item !== "" ) {
      this.setState( {selectedClient: item, clientIsValid: true, clientIsInValid: false, clientId: item.id, subclientsLoaded: true, selectedSublient: null, subclientId: null, subclientIsValid: null, subclientIsInValid: null} )
      this.fetchSubclientsByClient( item.id )
    } else {
      this.setState( {selectedClient: item, clientIsValid: false, clientIsInValid: true} )
    }
  }

  handleClearClient() {
    this.setState( {clientId: null, selectedClient: null, clientIsValid: null, clientIsInValid: null} )
  }

  getSubclientOptionValue = ( option ) => option.id; // maps the result 'id' as the 'value'

  getSubclientOptionLabel = ( option ) => option.name;

  handleChangeSubclient( item ) {
    if ( item !== null && item !== undefined && item !== "" ) {
      this.setState( {selectedSublient: item, subclientIsValid: true, subclientIsInValid: false, subclientId: item.id} )
    } else {
      this.setState( {selectedSublient: item, subclientIsValid: false, subclientIsInValid: true} )
    }
  }

  handleClearSubclient() {
    this.setState( {clientId: null, selectedSublient: null, clientIsValid: null, clientIsInValid: null} )
  }

  handleChangeUserMail( e ) {
    this.state.userMail = e.target.value.trim()
    if ( this.state.userMail !== null && this.state.userMail !== undefined && this.state.userMail !== "" ) {
      this.setState( {userMailInputInvalid: false, userMailInputValid: true} )
    } else {
      this.setState( {userMailInputInvalid: true, userMailInputValid: false} )
    }
  }

  handleChangeUserGivenName( e ) {
    this.state.userGivenName = e.target.value.trim()
    if ( this.state.userGivenName !== null && this.state.userGivenName !== undefined && this.state.userGivenName !== "" ) {
      this.setState( {userGivenNameInputInvalid: false, userGivenNameInputValid: true} )
    } else {
      this.setState( {userGivenNameInputInvalid: true, userGivenNameInputValid: false} )
    }
  }

  handleChangeUserLastName( e ) {
    this.state.userLastName = e.target.value.trim()
    if ( this.state.userLastName !== null && this.state.userLastName !== undefined && this.state.userLastName !== "" ) {
      this.setState( {userLastNameInputInvalid: false, userLastNameInputValid: true} )
    } else {
      this.setState( {userLastNameInputInvalid: true, userLastNameInputValid: false} )
    }
  }

  handleChangePassword( e ) {
    this.state.userPassword = e.target.value.trim()
    if ( this.state.userPassword !== null && this.state.userPassword !== undefined && this.state.userPassword !== "" ) {
      this.checkPassword( this.state.userPassword )
    } else {
      this.checkPassword( this.state.userPassword )
      this.setState( {userPasswordInputInvalid: true, userPasswordInputValid: false} )
    }
  }

  handleActiveUser() {
    let {userActive} = this.state
    this.setState( {userActive: (!userActive)} )
  }

  checkPassword( value ) {
    const letter = document.getElementById( "letter" );
    const capital = document.getElementById( "capital" );
    const number = document.getElementById( "number" );
    const special = document.getElementById( "special" );
    const length = document.getElementById( "length" );
    const lowerCaseLetters = /[a-z]/g;

    let letterLalide = false;
    if ( value.match( lowerCaseLetters ) ) {
      letter.classList.remove( "invalid" );
      letter.classList.add( "valid" );
      letterLalide = true;
    } else {
      letterLalide = false;
      letter.classList.remove( "valid" );
      letter.classList.add( "invalid" );
    }

// Validate capital letters
    let capitalLalide = false;
    const upperCaseLetters = /[A-Z]/g;
    if ( value.match( upperCaseLetters ) ) {
      capital.classList.remove( "invalid" );
      capital.classList.add( "valid" );
      capitalLalide = true;
    } else {
      capitalLalide = false;
      capital.classList.remove( "valid" );
      capital.classList.add( "invalid" );
    }

// Validate numbers
    let numberLalide = false;
    const numbers = /[0-9]/g;
    if ( value.match( numbers ) ) {
      numberLalide = true;
      number.classList.remove( "invalid" );
      number.classList.add( "valid" );
    } else {
      numberLalide = false;
      number.classList.remove( "valid" );
      number.classList.add( "invalid" );
    }

    let specialsValide = false;
    const specials = /[#!?*%&$"]/g;
    if ( value.match( specials ) ) {
      specialsValide = true;
      special.classList.remove( "invalid" );
      special.classList.add( "valid" );
    } else {
      specialsValide = false;
      special.classList.remove( "valid" );
      special.classList.add( "invalid" );
    }

// Validate length
    let lengthLalide = false;
    if ( value.length >= 4 && value.length <= 16 ) {
      lengthLalide = true;
      length.classList.remove( "invalid" );
      length.classList.add( "valid" );
    } else {
      lengthLalide = false;
      length.classList.remove( "valid" );
      length.classList.add( "invalid" );
    }
    if ( letterLalide && capitalLalide && numberLalide && lengthLalide && specialsValide ) {
      this.setState( {userPasswordInputInvalid: false, userPasswordInputValid: true} )
    } else {
      this.setState( {userPasswordInputInvalid: true, userPasswordInputValid: false} )
    }
  }

  backToList() {
    const {history} = this.props;

    let pushLocation = {
      pathname: "/users",
      state: {lastLocation: history.location}
    }
    history.push( pushLocation )
  }

  deleteUser() {
    fetch( '/user-azure-ui-service/Users' + ((this.isUpdateMode) ? "/" + this.state.user.objectId : ""), {
      method: "DELETE",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    } )
      .then( ( res ) => {
        if ( res.status !== 200 ) {
          this.setState( {saveLoading: false, saveHasError: true, saveSuccess: false} )
          return res.text()
        } else {
          this.setState( {saveLoading: false, saveHasError: false, saveSuccess: true} )
          const {history} = this.props;

          let pushLocation = {
            pathname: "/users",
            state: {lastLocation: history.location}
          }
          history.push( pushLocation )
        }
      } )
      .then( ( data ) => {
        this.setState( {saveError: data} )
        return true
      } )
      .catch( ( error ) => {
        this.setState( {saveLoading: false, saveHasError: true, saveError: error} )
        //ai.appInsights.trackException(error)

      } )
  }

  changeRole( e ) {
    this.setState( {selectedRole: e, userRoleValid: true} )
  }

  saveNewUser() {
    let {adminBpw, adminSender, adminRecipient} = this.props
    let userSubclients = this.props.subClients;
    let {clientId, subclientId, userMail, userGivenName, userLastName, selectedRole, userPassword, userActive} = this.state
    let {clientIsValid, userRoleValid, subclientIsValid, userMailInputValid, userGivenNameInputValid, userLastNameInputValid, userPasswordInputValid} = this.state

    this.setState( {formFail: false} )
    if ( !clientIsValid && adminBpw ) {
      this.setState( {formFail: true, formFailText: "clientIsValid", clientIsInValid: true, clientIsValid: false} )
      return
    }
    if ( !subclientIsValid ) {
      this.setState( {formFail: true, formFailText: "subclientIsValid", subclientIsInValid: true, subclientIsValid: false} )
      return
    }
    if ( !userRoleValid ) {
      this.setState( {formFail: true, formFailText: "userRoleValid", userRoleValid: false} )
      return
    }
    if ( !userMailInputValid ) {
      this.setState( {formFail: true, formFailText: "userMailInputValid", userMailInputInvalid: true, userMailInputValid: false} )
      return
    }
    if ( !userGivenNameInputValid ) {
      this.setState( {formFail: true, formFailText: "userGivenNameInputValid", userGivenNameInputInvalid: true, userGivenNameInputValid: false} )
      return
    }
    if ( !userLastNameInputValid ) {
      this.setState( {formFail: true, formFailText: "userLastNameInputValid", userLastNameInputInvalid: true, userLastNameInputValid: false} )
      return
    }
    if ( !this.isUpdateMode && !userPasswordInputValid ) {
      this.setState( {formFail: true, formFailText: "userPasswordInputValid", userPasswordInputInvalid: true, userPasswordInputValid: false} )
      return
    }

    let newItem = {
      "accountEnabled": userActive,
      "signInNames": [{"type": "emailAddress", "value": userMail}],
      "creationType": "LocalAccount",
      "displayName": userGivenName + " " + userLastName,
      "givenName": userGivenName,
      "surname": userLastName,
      "extension_<<clientId>>_BpwMandantSub": ((adminRecipient) ? userSubclients[0].id : subclientId),
      "extension_<<clientId>>_BpwCompany": "",
      "passwordProfile": {"password": userPassword, "forceChangePasswordNextLogin": true},
      "passwordPolicies": "DisablePasswordExpiration"
    }

    if ( this.isUpdateMode ) {
      newItem = {
        "accountEnabled": userActive,
        "signInNames": [{"type": "emailAddress", "value": userMail}],
        "displayName": userGivenName + " " + userLastName,
        "givenName": userGivenName,
        "surname": userLastName,
        "extension_<<clientId>>_BpwMandantSub": ((adminRecipient) ? userSubclients[0].id : subclientId),
        "extension_<<clientId>>_BpwCompany": ""
      }
    }

    if ( adminBpw ) {
      newItem["extension_<<clientId>>_BpwMandant"] = clientId
    }
    if ( adminRecipient )
      selectedRole = "defaultUser"
    switch ( selectedRole ) {
      case "globalAdmin":
        if ( adminBpw )
          newItem["extension_<<clientId>>_BpwAdminBpw"] = 1
        if ( adminBpw )
          newItem["extension_<<clientId>>_BpwAdminSender"] = 0
        if ( adminBpw || adminSender )
          newItem["extension_<<clientId>>_BpwAdminRecipient"] = 0
        break;
      case "clientAdmin":
        if ( adminBpw )
          newItem["extension_<<clientId>>_BpwAdminBpw"] = 0
        if ( adminBpw )
          newItem["extension_<<clientId>>_BpwAdminSender"] = 1
        if ( adminBpw || adminSender )
          newItem["extension_<<clientId>>_BpwAdminRecipient"] = 0
        break;
      case "sublientAdmin":
        if ( adminBpw )
          newItem["extension_<<clientId>>_BpwAdminBpw"] = 0
        if ( adminBpw )
          newItem["extension_<<clientId>>_BpwAdminSender"] = 0
        if ( adminBpw || adminSender )
          newItem["extension_<<clientId>>_BpwAdminRecipient"] = 1
        break;
      case "defaultUser":
        if ( adminBpw )
          newItem["extension_<<clientId>>_BpwAdminBpw"] = 0
        if ( adminBpw )
          newItem["extension_<<clientId>>_BpwAdminSender"] = 0
        if ( adminBpw || adminSender )
          newItem["extension_<<clientId>>_BpwAdminRecipient"] = 0
        break;
    }


    this.setState( {saveLoading: true, saveHasError: false, saveSuccess: false} )
    fetch( '/user-azure-ui-service/Users' + ((this.isUpdateMode) ? "/" + this.state.user.objectId : ""), {
      method: ((this.isUpdateMode) ? "PATCH" : "POST"),
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify( newItem )
    } )
      .then( ( res ) => {
        if ( res.status !== 200 ) {
          this.setState( {saveLoading: false, saveHasError: true, saveSuccess: false} )
          return res.text()
        } else {
          this.setState( {saveLoading: false, saveHasError: false, saveSuccess: true} )
          this.backToList()
          if ( !this.isUpdateMode )
            return res.json()
          if ( this.isUpdateMode )
            return res.text()
        }
      } )
      .then( ( data ) => {
        this.setState( {saveError: data} )
        return true
      } )
      .catch( ( error ) => {
        this.setState( {saveLoading: false, saveHasError: true, saveError: error} )
        //ai.appInsights.trackException(error)

      } )

  }

  render() {
    const {t, adminBpw, isClient, adminSender, adminRecipient, subClients} = this.props;
    let userSubclients = this.props.subClients;
    const {subclients, selectedClient, selectedSublient, subclientsLoaded, userMail, userMailInputInvalid, userMailInputValid} = this.state
    const {userGivenName, userGivenNameInputInvalid, userGivenNameInputValid, userLastName, userLastNameInputInvalid, userLastNameInputValid} = this.state
    const {userPassword, userPasswordInputInvalid, userPasswordInputValid, userActive, saveLoading, saveHasError, saveError, saveSuccess, formFail, formFailText} = this.state
    const {userRoleValid} = this.state
    if ( userSubclients === null )
      userSubclients = []

    if ( this.state.userLoaded )
      return <Row>
        <Col>
          {UserCreateUpdate.renderLoading()}
        </Col>
      </Row>


    return <>
      <Container>
        <>
          <Row>
            <Col className={"mb-0 mb-md-4 mt-4"} lg={"12"}>
              <h1>{t( 'sites.UserManagement.' + ((this.isUpdateMode) ? 'updateUser' : 'createUser') )}</h1>
            </Col>
          </Row>
          <Form>
            <Row>
              <Col md={"12"} className={"mb-md-3"}>
                <Form.Row>
                  <Form.Group as={Col} md={"6"} controlId="formGridFirstName">
                    <Form.Label>{t( 'sites.UserManagement.firstGivenName' )}</Form.Label>
                    <Form.Control
                      type="text"
                      min="1"
                      className="form-control-lg"
                      defaultValue={userGivenName}
                      placeholder={t( 'sites.UserManagement.enterFirstGivenName' )}
                      aria-label={t( 'sites.UserManagement.firstGivenName' )}
                      onChange={this.handleChangeUserGivenName.bind( this )}
                      isInvalid={((userGivenName !== null) ? ((userGivenNameInputInvalid) ? userGivenNameInputInvalid : false) : userGivenNameInputInvalid)}
                      isValid={((userGivenName !== null) ? ((userGivenNameInputValid) ? userGivenNameInputValid : true) : userGivenNameInputValid)}>
                    </Form.Control>
                    <Form.Control.Feedback type="invalid">
                      {t( 'sites.UserManagement.errorMessage' )}
                    </Form.Control.Feedback>
                  </Form.Group>
                  <Form.Group as={Col} md={"6"} controlId="formGridLastName">
                    <Form.Label>{t( 'sites.UserManagement.lastName' )}</Form.Label>
                    <Form.Control
                      type="text"
                      min="1"
                      className="form-control-lg"
                      defaultValue={userLastName}
                      placeholder={t( 'sites.UserManagement.enterLastName' )}
                      aria-label={t( 'sites.UserManagement.lastName' )}
                      onChange={this.handleChangeUserLastName.bind( this )}
                      isInvalid={((userLastName !== null) ? ((userLastNameInputInvalid) ? userLastNameInputInvalid : false) : userLastNameInputInvalid)}
                      isValid={((userLastName !== null) ? ((userLastNameInputValid) ? userLastNameInputValid : true) : userLastNameInputValid)}>
                    </Form.Control>
                  </Form.Group>
                </Form.Row>
              </Col>
            </Row>
            <Row>
              <Col md={"12"} className={"mb-md-3"}>
                <Form.Row>
                  <Form.Group as={Col} md={"6"} controlId="formGridEmail">
                    <Form.Label>{t( 'sites.UserManagement.emailAddress' )}</Form.Label>
                    <Form.Control
                      type="email"
                      min="1"
                      className="form-control-lg"
                      defaultValue={userMail}
                      placeholder={t( 'sites.UserManagement.enterTheEmailSddress' )}
                      aria-label={t( 'sites.UserManagement.emailAddress' )}
                      onChange={this.handleChangeUserMail.bind( this )}
                      isInvalid={((userMail !== null) ? ((userMailInputInvalid) ? userMailInputInvalid : false) : userMailInputInvalid)}
                      isValid={((userMail !== null) ? ((userMailInputValid) ? userMailInputValid : true) : userMailInputValid)}>
                    </Form.Control>
                    <Form.Control.Feedback type="invalid">
                      {t( 'sites.UserManagement.errorMessage' )}
                    </Form.Control.Feedback>
                  </Form.Group>
                  {adminBpw &&
                   <Form.Group as={Col} md={"6"} controlId="formGridMandant">
                     <Form.Label>{t( 'sites.UserManagement.mandant' )}</Form.Label>
                     <Async
                       className={"asset-type-select form-control-lg" + ((this.state.clientIsValid) ? " form-control is-valid" : "") + ((this.state.clientIsInValid) ? " form-control is-invalid" : "")}
                       classNamePrefix="select"
                       backspaceRemoves
                       isClearable
                       value={selectedClient}
                       onChange={this.handleChangeClient.bind( this )}
                       handleClearInput={this.handleClearClient.bind( this )}
                       placeholder={t( 'sites.UserManagement.selectMandant' )}
                       valueKey="id"
                       labelKey="name"
                       getOptionValue={this.getClientOptionValue}
                       getOptionLabel={this.getClientOptionLabel}
                       loadOptions={this.fetchClients}
                       defaultOptions={true}
                     />
                   </Form.Group>
                  }
                  {adminSender &&
                   <Form.Group as={Col} md={"6"}>
                     <Form.Label>{t( 'sites.UserManagement.subMandant' )}</Form.Label>
                     <Async
                       className={"asset-type-select form-control-lg" + ((this.state.subclientIsValid) ? " form-control is-valid" : "") + ((this.state.subclientIsInValid) ? " form-control is-invalid" : "")}
                       classNamePrefix="select"
                       backspaceRemoves
                       isClearable
                       value={selectedSublient}
                       onChange={this.handleChangeSubclient.bind( this )}
                       handleClearInput={this.handleClearSubclient.bind( this )}
                       placeholder={t( 'sites.UserManagement.selectSubMandant' )}
                       valueKey="id"
                       labelKey="name"
                       getOptionValue={this.getSubclientOptionValue}
                       getOptionLabel={this.getSubclientOptionLabel}
                       loadOptions={this.fetchSubclients.bind( this )}
                       defaultOptions={true}
                     />
                   </Form.Group>
                  }
                  {adminRecipient &&

                   <Form.Group as={Col} md={"6"}>
                     <Form.Label>{t( 'sites.UserManagement.subMandant' )}</Form.Label>
                     <Select
                       className={"asset-type-select form-control-lg form-control is-valid p-0"}
                       classNamePrefix="select"
                       backspaceRemoves
                       value={((userSubclients.length > 0) ? userSubclients[0] : undefined)}
                       defaultValue={((userSubclients.length > 0) ? userSubclients[0] : undefined)}
                       onChange={this.handleChangeSubclient.bind( this )}
                       handleClearInput={this.handleClearSubclient.bind( this )}
                       placeholder={t( 'sites.UserManagement.selectSubMandant' )}
                       valueKey="id"
                       labelKey="name"
                       getOptionValue={this.getSubclientOptionValue}
                       getOptionLabel={this.getSubclientOptionLabel}
                       options={userSubclients}
                       isDisabled={true}
                     />
                   </Form.Group>
                  }
                </Form.Row>
              </Col>
            </Row>
            <Row className={"mb-md-4"}>
              <Col md={{span: 6, offset: 6}} className={"mb-md-3 pl-1"}>
                <Form.Row>
                  {adminBpw &&
                   <Form.Group as={Col} controlId="formGridMandant">
                     <Form.Label>{t( 'sites.UserManagement.subMandant' )}</Form.Label>
                     <Select
                       className={"asset-type-select form-control-lg" + ((this.state.subclientIsValid) ? " form-control is-valid" : "") + ((this.state.subclientIsInValid) ? " form-control is-invalid" : "")}
                       classNamePrefix="select"
                       backspaceRemoves
                       isClearable
                       isLoading={subclientsLoaded}
                       value={selectedSublient}
                       onChange={this.handleChangeSubclient.bind( this )}
                       handleClearInput={this.handleClearSubclient.bind( this )}
                       placeholder={t( 'sites.UserManagement.selectSubMandant' )}
                       valueKey="id"
                       labelKey="name"
                       getOptionValue={this.getSubclientOptionValue}
                       getOptionLabel={this.getSubclientOptionLabel}
                       options={subclients}
                     />

                   </Form.Group>
                  }
                </Form.Row>
              </Col>
            </Row>
            <Row className={"mb-md-4"}>
              <Col md={"12"} className={"mb-md-3"}>
                <Form.Row>
                  <Col className="">
                    <Row>
                      <Col>
                        {isClient &&
                         <Col md={"6"}>
                           <fieldset>
                             <Form.Group as={Row} className="col-md-12" controlId="formGridUserManagement">
                               <Form.Label>{t( 'sites.UserManagement.userManagementRoles' )}</Form.Label>
                               {(userRoleValid === false) &&

                                <Alert variant={"danger"}>
                                  Bitte wählen sie eine User rolle
                                </Alert>
                               }
                               <ButtonToolbar>
                                 {adminBpw &&
                                  <ToggleButtonGroup type="radio" name="radio" className="mt-3" onChange={this.changeRole.bind( this )} value={this.state.selectedRole}>
                                    <ToggleButton type="radio" name="radio" value="globalAdmin" variant="secondary">
                                      {t( 'sites.UserManagement.role.globalAdmin' )}
                                    </ToggleButton>
                                    <ToggleButton type="radio" name="radio" value="clientAdmin" variant="secondary">
                                      {t( 'sites.UserManagement.role.clientAdmin' )}
                                    </ToggleButton>

                                    <ToggleButton type="radio" name="radio" value="sublientAdmin" variant="secondary">
                                      {t( 'sites.UserManagement.role.sublientAdmin' )}
                                    </ToggleButton>
                                    <ToggleButton type="radio" name="radio" value="defaultUser" variant="secondary">
                                      {t( 'sites.UserManagement.role.defaultUser' )}
                                    </ToggleButton>
                                  </ToggleButtonGroup>
                                 }
                                 {adminSender &&
                                  <ToggleButtonGroup type="radio" name="radio" className="mt-3" onChange={this.changeRole.bind( this )} value={this.state.selectedRole}>
                                    <ToggleButton type="radio" name="radio" value="sublientAdmin" variant="secondary">
                                      {t( 'sites.UserManagement.role.sublientAdmin' )}
                                    </ToggleButton>
                                    <ToggleButton type="radio" name="radio" value="defaultUser" variant="secondary">
                                      {t( 'sites.UserManagement.role.defaultUser' )}
                                    </ToggleButton>
                                  </ToggleButtonGroup>
                                 }
                                 {adminRecipient &&
                                  <ToggleButtonGroup type="radio" name="radio" className="mt-3" onChange={this.changeRole.bind( this )} value={this.state.selectedRole}>
                                    <ToggleButton type="radio" name="radio" value="defaultUser" variant="secondary">
                                      {t( 'sites.UserManagement.role.defaultUser' )}
                                    </ToggleButton>
                                  </ToggleButtonGroup>
                                 }

                               </ButtonToolbar>
                             </Form.Group>
                           </fieldset>
                         </Col>
                        }
                      </Col>
                    </Row>

                    <Row>
                      <Col>
                        <div className="d-flex mt-4" onClick={this.handleActiveUser.bind( this )}>
                          <FontAwesomeIcon icon={['fal', 'toggle-' + ((userActive) ? "on" : "off")]} size="lg" className="" transform=""/><span
                          className="ml-2">{t( 'sites.UserManagement.' + ((userActive) ? "accountActiv" : "accountInActiv") )}</span>
                        </div>
                      </Col>
                    </Row>
                  </Col>

                  {!this.isUpdateMode &&
                   <Form.Group as={Col} md={"6"} controlId="formGridMandant">
                     <Form.Control
                       type="password"
                       min="1"
                       className="form-control-lg"
                       defaultValue={userPassword}
                       placeholder={t( 'sites.UserManagement.password.passwordPlaceholder' )}
                       aria-label={t( 'sites.UserManagement.password.passwordLabel' )}
                       onChange={this.handleChangePassword.bind( this )}
                       isInvalid={((userPassword !== null) ? ((userPasswordInputInvalid) ? userPasswordInputInvalid : false) : userPasswordInputInvalid)}
                       isValid={((userPassword !== null) ? ((userPasswordInputValid) ? userPasswordInputValid : true) : userPasswordInputValid)}>
                     </Form.Control>
                     <div id="message">
                       <p id="letter" className="invalid"><b>{t( 'sites.UserManagement.password.lowercase' )}</b></p>
                       <p id="capital" className="invalid"><b>{t( 'sites.UserManagement.password.uppercase' )}</b></p>
                       <p id="number" className="invalid"><b>{t( 'sites.UserManagement.password.number' )}</b></p>
                       <p id="special" className="invalid"><b>{t( 'sites.UserManagement.password.special' )}</b></p>
                       <p id="length" className="invalid"><b>{t( 'sites.UserManagement.password.characters' )}</b></p>
                     </div>
                   </Form.Group>
                  }
                </Form.Row>
              </Col>
            </Row>
            {saveLoading &&
             <Row>
               <Col>
                 {UserCreateUpdate.renderLoading()}
               </Col>
             </Row>
            }

            {saveSuccess &&
             <Row>
               <Col>
                 <Alert variant={"success"}>
                   Wurde erfolgreich gespeichert
                 </Alert>
               </Col>
             </Row>
            }
            {formFail &&
             <Row>
               <Col>
                 <Alert variant={"danger"}>
                   {formFailText}
                 </Alert>
               </Col>
             </Row>
            }
            {saveHasError &&
             <Row>
               <Col>
                 <Alert variant={"danger"}>
                   {saveError}
                 </Alert>
               </Col>
             </Row>
            }
            <Row className="mb-md-5">
              <Col>
                <div className="d-flex flex-column flex-md-row flex-lg-row">

                  <div className="p-1 flex-fill">
                    <Button size={"md"} variant="light" block onClick={this.backToList.bind( this )}>{t( 'sites.UserManagement.btnBackToList' )}</Button>
                  </div>
                  {/*{this.isUpdateMode &&
                   <div className="p-1 flex-fill">
                     <Button size={"md"} variant="danger" block onClick={this.deleteUser.bind( this )}>{t( 'sites.UserManagement.btnDeletedUser' )}</Button>
                   </div>
                  }*/}
                  <div className="p-1 flex-fill">
                    <Button size={"md"} variant="secondary" block onClick={this.saveNewUser.bind( this )}>{t( 'sites.UserManagement.' + ((this.isUpdateMode) ? 'btnUpdateUser' : 'btnCreateUser') )}</Button>
                  </div>
                </div>
              </Col>
            </Row>
          </Form>
        </>
      </Container>
    </>
  }

  static renderLoading() {
    return <><Row><Col md={"12"} className={"d-flex justify-content-center mt-5"}><Loader type="Oval" color="#CDDC4B" height={70} width={70}/></Col></Row></>;
  }

  static renderLoadingFinish( t ) {
    return <><Row><Col md={"12"} className={"d-flex flex-column mt-3"}>
      <div className="alert alert-primary" role="alert">
        {t( "sites.Asset.noMoreContentInfo" )}
      </div>
    </Col></Row></>;
  }

  static renderError() {
    return <><Row><Col md={"12"} className={"d-flex flex-column mt-3"}>
      <div className={"alert alert-danger"} role={"alert"}>There was a technical problem. Please contact our <a href="/" className={"alert-link"}>Support Team</a>. Or just reload and try it again.</div>
    </Col></Row></>;
  }

}

export default withQueryParams( {
                                  stripUnknownKeys: false
                                } )( UserCreateUpdate )
