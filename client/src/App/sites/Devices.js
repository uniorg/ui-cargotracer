import React, { Component } from 'react';
import { Button, Col, Container, Row } from "react-bootstrap";
import ReactPullToRefresh from "react-pull-to-refresh";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import ModuleSearch from "../component/ModuleSearch";
import ModuleFilter from "../component/ModuleFilter";
import ModuleSort from "../component/ModuleSort";
import Form from "react-bootstrap/es/Form";
import InfiniteScroll from "react-infinite-scroll-component";
import DevicesListItem from "../component/DevicesListItem";
import queryString from "query-string";
import withQueryParams from "react-router-query-params";
import i18n from "i18next";
import { createSorter } from "../util/Sort";
import Loader from "react-loader-spinner";
import $ from "jquery";
import Map from '../services/locationLeaFlet';
import DeviceMarker from "../component/DeviceMarker";

import { render } from 'react-dom';



class Devices extends Component {
  deviceMap = null
  _isMounted = false

  constructor( props ) {
    super( props );

    this.state = {
      agList: []
    }

    this.state = {
      width: 0,
      height: 0,

      list: [],
      count: 0,
      hasMoreItems: true,
      nextUrlQuery: null,

      checkFilter: "",
      search: "*",

    };
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  async componentDidMount() {
    this._isMounted = true;
    await this.setUrlStates();
    await this.fetchDevices()
  }


  componentDidUpdate() {
    let that = this
    if ( !this.props.isMobile ) {
      if ( this.deviceMap !== undefined && this.deviceMap !== null ) {
        this.deviceMap.map.remove();
        $( "#deviceMap" ).html( "" );
      }
      this.deviceMap = new Map( {startLatitude: '50.8038972', startLongitude: '7.1723139', startZoom: '5', containerId: 'deviceMap', mapType: 'openStreet'} );
      this.deviceMap.startMap( () => {
        that.setMarker()
      } );
    }
  }

  setMarker() {
    if ( this.devicesOnMap !== undefined && this.devicesOnMap.length > 0 ) {
      const markerIconsStart = {
        google: "BpwPalletMarker",
        leaflet: "<i class=\"fas fa-map-marker-alt\" style='font-size: 1.5rem;color: #129fd8;position:  relative;top: -12px;left: -2px;'></i>"
      };
      this.deviceMap.removeMarker( "DeviceMarker" )
      this.deviceMap.setMarkers( this.devicesOnMap, "DeviceMarker", markerIconsStart, true, true, false );
      this.deviceMap.setNowMarkerCenter( "DeviceMarker", false );
    }
  }

  setDevicesOnMap( assets ) {
    /**
     * @odata.id: "Devices('3605E1')"
     @odata.type: "#com.innolab.tracking.device.ui.service.v2.Device"
     deviceId: "3605E1"
     lastSeenAt: "2019-11-05T12:45:54Z"
     lastSeenAt@odata.type: "#DateTimeOffset"
     lastSeenBatteryVoltage: null
     lastSeenBatteryVoltage@odata.type: "#Int32"
     lastSeenTemperature: null
     lastValidGeoPositionLatitude: 51.477923
     lastValidGeoPositionLongitude: 5.443456
     lastValidGeoPositionNumberOfSatellites: 5
     lastValidGeoPositionNumberOfSatellites@odata.type: "#Int32"
     lastValidGeoPositionRadius: null
     lastValidGeoPositionRadius@odata.type: "#Int32"
     lastValidGeoPositionSentAt: "2019-11-05T12:45:54Z"
     lastValidGeoPositionSentAt@odata.type: "#DateTimeOffset"
     lastValidGeoPositionSpeed: null
     lastValidGeoPositionSpeed@odata.type: "#Int32"
     lastValidGeoPositionType: "GPS"
     lastValidGeoPositionType@odata.type: "#com.innolab.tracking.device.ui.service.v2.GeoPositionType"
     lastValidGeoPositionVibrationCount: null
     lastValidGeoPositionVibrationCount@odata.type: "#Int32"
     lastValidGeoPositionVibrationThreshold: null
     lastValidGeoPositionVibrationThreshold@odata.type: "#Int32"
     name: "NEW_TRACKER_3605E1"
     odataId: "3605E1"
     stillAssigned: true
     */
    let devicesMarker = assets.map( ( asset ) => {
      let {lastValidGeoPositionLatitude, lastValidGeoPositionLongitude, name, deviceId, assetTypeName, lastValidGeoPositionType, id, lastValidGeoPositionSentAt} = asset
      let color = ((lastValidGeoPositionType === "SIGFOX") ? "#000000" : "#0d6e96")
      let container = document.createElement('div')
      let html = render(<DeviceMarker item={asset}/>, container)
      return {
        lat: lastValidGeoPositionLatitude,
        lng: lastValidGeoPositionLongitude,
        title: name,
        circleRadius: undefined,
        popupRender: <DeviceMarker item={asset}/>,
        zIndex: 1000,
        icon: {
          google: "BpwPalletMarker",
          leaflet: "<i class=\"fas fa-map-marker-alt\" style='font-size: 1.3rem;color: " + color + ";position:  relative;top: -12px;left: -2px;'></i>"
        }
      }
    } )

    if ( this.devicesOnMap === undefined )
      this.devicesOnMap = []
    this.devicesOnMap = this.devicesOnMap.concat( devicesMarker )

    this.setMarker()
  }

  goToDetail( item ) {
    const url = this.props.match.url
    const {history} = this.props;

    let pushLocation = {
      pathname: url + "/" + item.deviceId,
      state: {lastLocation: history.location}
    }
    history.push( pushLocation )
  }

  static getSortByOptions() {
    let sortOptions = [];
    sortOptions.push( {label: i18n.t( 'sites.Device.sortByOptions.item1' ), value: "lastSeenAt"} )
    sortOptions.push( {label: i18n.t( 'sites.Device.sortByOptions.item2' ), value: "lastValidGeoPositionSentAt"} )
    sortOptions.push( {label: i18n.t( 'sites.Device.sortByOptions.item3' ), value: "Battery"} )

    return sortOptions
  }


  async setUrlStates() {
    const values = queryString.parse( this.props.location.search )
    if ( values.search !== undefined ) {
      await this.setState( {search: values.search} );
    }
    if ( values.checkFilter !== undefined ) {
      await this.setState( {checkFilter: values.checkFilter} );
    }
    if ( values.sortKey !== undefined && values.sortDirection !== undefined ) {
      await this.setState( {sortKey: this.getSortItem( values.sortKey ), sortDirection: values.sortDirection} );
    }
  }

  getSortItem( searchValue ) {
    let sortOption = Devices.getSortByOptions().filter( ( item ) => {
      return item.value === searchValue;

    } )

    if ( sortOption[0] !== undefined )
      return sortOption[0]

    return Devices.getSortByOptions()[0]
  }

  // MARK: Liste
  async handleReloadList() {
    this.devicesOnMap = []
    await this.setState( {
                           list: [],
                           hasMoreItems: true,
                           nextUrlQuery: null
                         } );
    await this.fetchDevices()
  }

  fetchDevices( compleation ) {
    const {search, checkFilter, nextUrlQuery, groupByMode, assetTypeId} = this.state;
    let queryString = ""
    if ( search !== "" ) {
      queryString = "search=" + search
    }
    if ( checkFilter !== "" ) {
      if ( assetTypeId !== undefined ) {
        queryString = queryString + ((search !== "") ? "&" : "") + "filter=" + checkFilter + "&skip=0&top=100"
      } else {
        queryString = queryString + ((search !== "") ? "&" : "") + "filter=" + checkFilter + "&skip=0&top=100"
      }
    } else {
      queryString = queryString + "&skip=0&top=100"
    }
    queryString = this.setSearchQueryOrderBy( queryString )
    if ( nextUrlQuery ) {
      queryString = nextUrlQuery;
    }
    fetch( '/device-ui-service/devices/list/?' + queryString )
      .then( res => res.json() )
      .then( ( data ) => {
        this.onLoad( data )
        if ( compleation !== undefined ) {
          compleation();
        }
        //this.setMarker()
      } )
  }

  setSearchQueryOrderBy( queryString ) {
    const {sortKey, sortDirection} = this.state;
    if ( sortKey !== undefined && sortKey !== "" && sortDirection !== "" ) {
      if ( sortKey.value !== undefined ) {
        queryString = queryString + "&orderby=" + sortKey.value + " " + sortDirection
      } else {
        queryString = queryString + "&orderby=" + sortKey + " " + sortDirection
      }
    }
    return queryString
  }

  onLoad = ( data ) => {
    const {list} = this.state

    if ( this._isMounted ) {
      if ( data.value !== undefined && data.value.length === 0 ) {
        this.setState( {
                         hasMoreItems: false,
                         nextUrlQuery: null,
                         count: 0
                       } );
      }
      if ( list.length < data.count ) {
        let returnList = list
        let listFinish = this.parseData( data.value !== undefined ? data.value : data )

        returnList = returnList.concat( listFinish )
        this.setDevicesOnMap( data.value !== undefined ? data.value : data )
        this.setState( {
                         list: returnList,
                         count: data.count,
                         nextUrlQuery: data.nextUrlQuery !== undefined ? data.nextUrlQuery : null
                       } );
        let that = this
        setTimeout( () => {
          if ( that.state.list.length === data.count ) {
            that.setState( {
                             hasMoreItems: false,
                             nextUrlQuery: null,
                             count: data.count
                           } );
          }
        }, 500 )
      } else {
        this.setState( {
                         hasMoreItems: false,
                         nextUrlQuery: null
                       } );
      }
    }
  }

  parseData( data ) {
    const {sorters} = this.state;

    if ( data && data.length && Array.isArray( sorters ) && sorters.length ) {
      data.sort( createSorter( ...sorters ) );
    }

    return data;
  }

  async handelSortItems( sortKey, sortDirection ) {
    await this.setState( {
                           list: [],
                           hasMoreItems: true,
                           nextUrlQuery: null,
                           sortKey: sortKey,
                           sortDirection: sortDirection
                         } )
    const values = queryString.parse( this.props.location.search )

    if ( sortKey !== undefined && sortKey.value !== undefined ) {
      values.sortKey = sortKey.value
    } else {
      values.sortKey = sortKey
    }
    values.sortDirection = sortDirection
    const {history, location} = this.props;
    history.push( {
                    pathname: location.pathname,
                    search: "?" + queryString.stringify( values ),
                    hash: "",
                    state: {fromDashboard: false}
                  } )

    this.devicesOnMap = []
    this.fetchDevices()
  }

  async handleOnClickRemovSort() {
    let sortKey = "cycleCount"
    let sortDirection = "desc"
    await this.setState( {
                           list: [],
                           hasMoreItems: true,
                           nextUrlQuery: null,
                           sortKey: this.getSortItem( sortKey ),
                           sortDirection: sortDirection
                         } )
    const values = queryString.parse( this.props.location.search )


    values.sortKey = sortKey
    values.sortDirection = sortDirection
    const {history, location} = this.props;
    history.push( {
                    pathname: location.pathname,
                    search: "?" + queryString.stringify( values ),
                    hash: "",
                    state: {fromDashboard: false}
                  } )

    this.devicesOnMap = []
    this.fetchDevices()
  }

  handleSortChange( item ) {
    this.setState( {
                     sortKey: item
                   } )
  }

  handleSortDirectionChange( direction ) {

    this.setState( {
                     sortDirection: direction
                   } )
  }

// MARK: Search

  async handleClickSearchItem( e, item, search ) {
    await this.setState( {
                           list: [],
                           hasMoreItems: true,
                           nextUrlQuery: null,
                           search: search
                         } )
    await this.fetchDevices()
    const values = queryString.parse( this.props.location.search )
    if ( item !== undefined ) {
      values.search = item.deliveryNumber
    } else {
      values.search = search
    }
    const {history, location} = this.props;
    this.devicesOnMap = []
    history.push( {
                    pathname: location.pathname,
                    search: "?" + queryString.stringify( values ),
                    hash: "",
                    state: {fromDashboard: false}
                  } )
  }

  async handleOnClickRemoveSearch() {
    await this.setState( {
                           list: [],
                           hasMoreItems: true,
                           nextUrlQuery: null,
                           search: "*"
                         } )
    await this.fetchDevices()
    const values = queryString.parse( this.props.location.search )
    values.search = ""
    this.devicesOnMap = []
    const {history, location} = this.props;
    history.push( {
                    pathname: location.pathname,
                    search: "?" + queryString.stringify( values ),
                    hash: "",
                    state: {fromDashboard: false}
                  } )
  }

  // MARK: Filter
  async handleOnClickRemoveFilter() {
    await this.setState( {
                           list: [],
                           hasMoreItems: true,
                           nextUrlQuery: null,
                           checkFilter: ""
                         } )
    const values = queryString.parse( this.props.location.search )


    values.checkFilter = ""
    const {history, location} = this.props;
    history.push( {
                    pathname: location.pathname,
                    search: "?" + queryString.stringify( values ),
                    hash: "",
                    state: {fromDashboard: false}
                  } )

    this.devicesOnMap = []
    this.fetchDevices()
  }

  async handelFilterItems( filterString ) {
    await this.setState( {
                           list: [],
                           hasMoreItems: true,
                           nextUrlQuery: null,
                           checkFilter: filterString
                         } )
    const values = queryString.parse( this.props.location.search )
    values.checkFilter = filterString
    const {history, location} = this.props;
    history.push( {
                    pathname: location.pathname,
                    search: "?" + queryString.stringify( values ),
                    hash: "",
                    state: {fromDashboard: false}
                  } )

    this.devicesOnMap = []
    this.fetchDevices()
  }

  render() {
    const {hasMoreItems, list, count, sortKey, sortDirection, assetTypeName} = this.state;
    const {t, location} = this.props;
    const values = queryString.parse( location.search )
    const render = (
      <div className={"site-devices"}>
        <Container>
          <>
            <Row>
              <ReactPullToRefresh
                id={"devices-list-container"}
                onRefresh={() => {
                }}
                className="col-lg-8 mr-0"
                loading={<div className="loading"><FontAwesomeIcon icon={['fal', 'spinner']} className="mr-1" spin size="lg"/></div>}
                style={{"height": this.props.containerHeight}}>
                {this.props.isMobile ?
                  (

                    <h3>Pull down to refresh</h3>
                  ) : ''}
                <Row className="mt-4 mb-md-4 mb-4">
                  <Col md="12">
                    <h1 className={"mb-sm-0 mb-4"}>{t( 'sites.Device.listTitle' )}</h1>
                  </Col>
                </Row>
                <Row className="mb-4">
                  <Col>
                    <ModuleSearch
                      placeholder={t( 'sites.Device.ModuleSearch.placeholder' )}
                      buttonText={t( 'sites.Device.ModuleSearch.searchButton' )}
                      label={t( 'sites.Device.ModuleSearch.label' )}
                      searchTitle={t( 'sites.Device.ModuleSearch.searchTitle' )}
                      searchInputValue={values.search}
                      handleClickSearchItem={this.handleClickSearchItem.bind( this )}
                      handleOnClickRemoveSearch={this.handleOnClickRemoveSearch.bind( this )}
                      {...this.props}
                    />
                  </Col>
                </Row>
                <Row className="mb-4">
                  <Col>
                    <ModuleFilter
                      groupByButtons={[]}
                      checkBoxes={[]}
                      handelFilterItems={this.handelFilterItems.bind( this )}
                      handleOnClickRemoveFilter={this.handleOnClickRemoveFilter.bind( this )}
                      location={this.props.location.search}
                      lastStatusQuery={"(lastSeenAt ge ###START### and lastSeenAt le ###END###)"}
                      checkTrackerOnlyWhere={"(stillAssigned eq true)"}
                      checkTrackerAllWhere={"(name ne '')"}
                      withOnlyTrackedToggle={true}
                      groupByEnable={false}
                      groupByMode={"asset"}
                      handleChangeGroupByMode={() => {
                      }}
                      filterPanelTitle={t( 'sites.Device.ModuleFilter.filterPanelTitle' )}
                      onlyTrackedDeliveries={t( 'sites.Device.ModuleFilter.onlyTrackedDeliveries' )}
                      defineDateTimeTitle={t( 'sites.Device.ModuleFilter.defineDateTimeTitle' )}
                      btnSetFilterBack={t( 'sites.Device.ModuleFilter.btnSetFilterBack' )}
                      btnViewResult={t( 'sites.Device.ModuleFilter.btnViewResult' )}
                      {...this.props}
                    />
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <ModuleSort
                      sortOptions={Devices.getSortByOptions()}
                      handleSortResult={this.handelSortItems.bind( this )}
                      handleChangeSelect={this.handleSortChange.bind( this )}
                      handleOnClickRemoveSort={this.handleOnClickRemovSort.bind( this )}
                      handleSortDirectionChange={this.handleSortDirectionChange.bind( this )}
                      defaultSortItem={sortKey}
                      defaultSortDirection={sortDirection}
                      t={this.props.t}/>
                  </Col>
                </Row>
                <Row className="mb-3">
                  <Col>
                    <div className="d-flex flex-column flex-md-row flex-lg-row align-self-center">
                      <div className="pl-0 p-sm-2 flex-column">
                        <Form.Label>{t( 'sites.Asset.actualStatment' )}</Form.Label>
                      </div>
                      <div className="d-flex flex-row flex-grow-1">
                        <div className="ml-auto pt-1 justify-content-end">
                          {/*TODO: Implement in v2.0*/}
                          {/*<Button variant="secondary" size="sm" className="btn-light mr-2">
                            <FontAwesomeIcon icon={['fal', 'times']} className="mr-1"/>
                            {t( 'sites.Asset.assetItem.isLateItemClose' )}
                          </Button>*/}
                          <Button variant="primary" size="sm" className="btn-primary btn-round" onClick={this.handleReloadList.bind( this )}>
                            <FontAwesomeIcon icon={['fal', 'sync-alt']} className="mr-1"/>
                            {t( 'sites.Asset.assetItem.isLateItemSync' )}
                          </Button>
                        </div>
                      </div>
                    </div>
                  </Col>
                </Row>

                <InfiniteScroll
                  dataLength={list.length}
                  next={this.fetchDevices.bind( this )}
                  hasMore={hasMoreItems}
                  loader={Devices.renderLoading()}
                  endMessage={Devices.renderLoadingFinish( t )}
                  scrollableTarget="devices-list-container"
                  scrollThreshold={1}
                >
                  <Row>
                    {this.state.list.map( ( item, index ) => {
                      return (
                        <DevicesListItem
                          key={index}
                          item={item}
                          index={index}
                          url={this.props.match.url}
                          {...this.props}/>
                      );
                    } )
                    }
                  </Row>
                </InfiniteScroll>
              </ReactPullToRefresh>
              {!this.props.isMobile ?
                (
                  <Col lg="4 shadow-bpw-lg p-0">
                    <div id="deviceMap"/>
                  </Col>
                ) : ''}

            </Row>
          </>
        </Container>

      </div>
    );
    return render
  }

  static renderLoading() {
    return <><Row><Col md={"12"} className={"d-flex justify-content-center mt-5"}><Loader type="Oval" color="#CDDC4B" height={70} width={70}/></Col></Row></>;
  }

  static renderLoadingFinish( t ) {
    return <><Row><Col md={"12"} className={"d-flex flex-column mt-3"}>
      <div className="alert alert-primary" role="alert">
        {t( "sites.Asset.noMoreContentInfo" )}
      </div>
    </Col></Row></>;
  }

  static renderError() {
    return <><Row><Col md={"12"} className={"d-flex flex-column mt-3"}>
      <div className={"alert alert-danger"} role={"alert"}>There was a technical problem. Please contact our <a href="/" className={"alert-link"}>Support Team</a>. Or just reload and try it again.</div>
    </Col></Row></>;
  }
}

export default withQueryParams( {
                                  stripUnknownKeys: true,
                                  keys: {
                                    search: {
                                      validate: value => !!value && value.length > 0,
                                    },
                                    checkFilter: {
                                      defaulty: "(deliveryState ne 'DONE' and deliveryState ne 'UNKNOWN' and activeDeviceAssignments gt 0)",
                                      default: "(stillAssigned eq true)",
                                      validate: value => !!value && value.length > 0,
                                    },
                                    sortKey: {
                                      default: "lastSeenAt",
                                      validate: value => !!value && value.length > 0,
                                    },
                                    sortDirection: {
                                      default: "desc",
                                      validate: value => !!value && value.length > 0,
                                    }
                                  }
                                } )( Devices )
