import React, { Component } from 'react';
import { Accordion, Button, ButtonGroup, Card, Col, Container, Row, OverlayTrigger, Tooltip } from "react-bootstrap";
import Form from "react-bootstrap/es/Form";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import DateRangePicker from '../component/DateRangePicker';
import Loader from "react-loader-spinner";
import withQueryParams from "react-router-query-params";
import moment from 'moment';
import $ from "jquery";
import Map from '../services/locationLeaFlet';
import queryString from "query-string";


class DevicesDetail extends Component {
  _isMounted = false
  _pageIsload = false
  _mapIsload = false
  deviceMap = null
  devicesOnMap = []
  geoDevicesOnMap = []

  constructor( props ) {
    super( props );
    this.state = {
      width: 0,
      height: 0,
      item: null,
      itemName: null,
      successSend: false,
      failSend: false,
      successItem: undefined,
      deviceNotFound: false,
      lastStatusStart: undefined,
      lastStatusEnd: undefined,
      deviceLat: null,
      deviceLng: null,
      editMode: false,
      geoDevicesCount: null,

      nameInputValid: null,
      nameInputInvalid: null,

      loadGeo: false
    };
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  componentDidMount() {
    this._isMounted = true;
    this.fetchDevice()
  }

  componentDidUpdate() {
    console.log(this._pageIsload, "componentDidUpdate" )
    let that = this
    const {deviceLat, deviceLng, item} = this.state;
    const {startDate, endDate} = this.props.match.params;
    if ( !this.props.isMobile && item !== null  && !this._mapIsload ) {
      this._mapIsload = true
      if ( this.deviceMap !== undefined && this.deviceMap !== null ) {
        this.deviceMap.map.remove();
        $( "#deviceMap" ).html( "" );
      }
      this.deviceMap = new Map( {startLatitude: ((deviceLat !== null) ? deviceLat : '50.8038972'), startLongitude: ((deviceLng !== null) ? deviceLng : '7.1723139'), startZoom: '5', containerId: 'deviceMap', mapType: 'openStreet'} );
      this.deviceMap.startMap( () => {
        that.setMarker()
      } );
    }

    if ( (startDate !== undefined || endDate !== undefined) && !this._pageIsload ) {
      this._pageIsload = true
      this.fetchDeviceGeoMessages( new Date( this.props.match.params.startDate ), new Date( this.props.match.params.endDate ), () => {
      })
    }
  }


  setGeoMarker() {
    if ( this.geoDevicesOnMap !== undefined && this.geoDevicesOnMap.length > 0 ) {
      const markerIconsStart = {
        google: "BpwPalletMarker",
        leaflet: "<i class=\"fas fa-map-marker-alt\" style='font-size: 1.5rem;color: #129fd8;position:  relative;top: -12px;left: -2px;'></i>"
      };
      this.deviceMap.removeMarker( "DeviceGeoMarker" )
      this.deviceMap.setMarkers( this.geoDevicesOnMap, "DeviceGeoMarker", markerIconsStart, false, true, true );
      this.deviceMap.setNowMarkerCenter( "DeviceGeoMarker", false );
    }
  }

  setMarker() {
    if ( this.devicesOnMap !== undefined && this.devicesOnMap.length > 0 ) {
      const markerIconsStart = {
        google: "BpwPalletMarker",
        leaflet: "<i class=\"fas fa-map-marker-alt\" style='font-size: 1.5rem;color: #129fd8;position:  relative;top: -12px;left: -2px;'></i>"
      };
      this.deviceMap.removeMarker( "DeviceMarker" )
      this.deviceMap.setMarkers( this.devicesOnMap, "DeviceMarker", markerIconsStart, false, true, false );
      this.deviceMap.setNowMarkerCenter( "DeviceMarker", false );
    }
  }

  fetchDevice( compleation ) {
    fetch( '/device-ui-service/devices/searchDeviceById/' + this.props.match.params.id )
      .then( response => {
        if ( response.status === 404 ) {
          throw response.status;
        }
        return response
      } )
      .then( response => {
        if ( response.ok ) {
          return response.json();
        } else {
          throw new Error( "has error" );
        }
      } )
      .then( ( data ) => {
        if ( this._isMounted ) {
          this.onLoad( data )
        }
        if ( compleation !== undefined ) {
          compleation();
        }
      } )
      .catch( ( error ) => {
        if ( this._isMounted && error === 404 ) {
          this.setState( {
                           item: null,
                           deviceNotFound: true
                         } );
        }
        if ( compleation !== undefined ) {
          compleation();
        }
      } )
  }

  onLoad = ( data ) => {
    let {lastValidGeoPositionLatitude, lastValidGeoPositionLongitude, name, deviceId, lastValidGeoPositionType, lastValidGeoPositionSentAt} = data
    this.setState( {
                     item: data,
                     itemName: name,
                     lastStatusStart: moment().startOf('day'),
                     lastStatusEnd: moment().endOf('day'),
                     deviceLat: lastValidGeoPositionLatitude,
                     deviceLng: lastValidGeoPositionLongitude
                   } );
    let color = ((lastValidGeoPositionType === "SIGFOX") ? "#000000" : "#0d6e96")
    let devicesMarker = {
      lat: lastValidGeoPositionLatitude,
      lng: lastValidGeoPositionLongitude,
      title: name,
      circleRadius: undefined,
      popup: "<div class='row m-3'>" +
             "<div class='col-12'><h5>" + name + "</h5></div>" +
             "<div class='col-12'>Device Id: " + deviceId + "</div>" +
             "<div class='col-12'>Typ: " + lastValidGeoPositionType + "</div>" +
             "<div class='col-12'>SentAt: " + moment( new Date( lastValidGeoPositionSentAt ) ).format( 'LLL' ) + "</div>" +
             "</div>",
      zIndex: 1000,
      icon: {
        google: "BpwPalletMarker",
        leaflet: "<i class=\"fas fa-map-marker-alt\" style='font-size: 1.3rem;color: " + color + ";position:  relative;top: -12px;left: -2px;'></i>"
      }
    }

    this.devicesOnMap = []
    this.devicesOnMap = this.devicesOnMap.concat( devicesMarker )

    this.setMarker()
  }


  fetchDeviceGeoMessages( lastStatusStart, lastStatusEnd, callback ) {
    this.setState({loadGeo: true})
    fetch( '/device-ui-service/devices/deviceGeoMessages/' + this.props.match.params.id + '/' + lastStatusStart.toISOString() + '/' + lastStatusEnd.toISOString() )
      .then( response => {
        if ( response.status === 404 ) {
          throw response.status;
        }
        return response
      } )
      .then( response => {
        if ( response.ok ) {
          return response.json();
        } else {
          throw new Error( "has error" );
        }
      } )
      .then( ( data ) => {
        if ( this._isMounted ) {
          if ( data.value !== undefined && callback !== undefined ) {
            callback( data.value );
          }
          if ( data.value !== undefined ) {
            this.setState({loadGeo: false, geoDevicesCount: data.value.length})
            this.onLoadGeoMessages(data.value)
          }
        }
      } )
      .catch( ( error ) => {
        if ( this._isMounted && error === 404 ) {

        }
        if ( callback !== undefined ) {
          callback();
        }
      } )
  }

  /**
   * deviceId: "3ABE70"
   geoPositionType: "GPS"
   latitude: 47.931111
   longitude: 3.512197
   numberOfSatellites: null
   radius: null
   sentAt: "2019-11-13T07:40:57Z"
   speed: 75
   temperature: 3
   vibrationCount: null
   vibrationThreshold: 2000
   * @param data
   */
  onLoadGeoMessages(data) {

    data = data.map((geoMessage)=> {
      let {geoPositionType, sentAt, latitude, longitude} = geoMessage

      let color = ((geoPositionType === "SIGFOX") ? "#000000" : "#ff0009")
      let devicesMarker = {
        lat: latitude,
        lng: longitude,
        title: geoPositionType + moment( new Date( sentAt ) ).format( 'LLL' ),
        circleRadius: undefined,
        popup: "<div class='row m-3'>" +
               "<div class='col-12'>Typ: " + geoPositionType + "</div>" +
               "<div class='col-12'>lat: " + latitude + "</div>" +
               "<div class='col-12'>lng: " + longitude + "</div>" +
               "<div class='col-12'>SentAt: " + moment( new Date( sentAt ) ).format( 'LLL' ) + "</div>" +
               "</div>",
        zIndex: 1000,
        timestamp: new Date( sentAt ),
        source: geoPositionType,
        icon: {
          google: "BpwPalletMarker",
          leaflet: "<i class=\"fas fa-map-pin\" style='font-size: 1.3rem;color: " + color + ";position:  relative;top: -12px;left: -2px;'></i>"
        }
      }
      return devicesMarker
    })

    this.geoDevicesOnMap = []
    this.geoDevicesOnMap = this.geoDevicesOnMap.concat( data )

    this.setGeoMarker()
  }

  handleChangeDateRangeStatus( start, end ) {
    this.setState( {lastStatusStart: new Date( start ), lastStatusEnd: new Date( end )} )
  }

  handleGetGeoMessages() {
    let {lastStatusStart, lastStatusEnd} = this.state
    this.fetchDeviceGeoMessages( lastStatusStart, lastStatusEnd)

    let path = "/devices/" + this.props.match.params.id + "/" + lastStatusStart.toISOString() + "/" + lastStatusEnd.toISOString()
    window.history.pushState( path, "Title", path );
  }

  handleRemoveGeoMessages() {
    this.geoDevicesOnMap = []
    this.deviceMap.removeMarker( "DeviceGeoMarker" )

    this.setState({geoDevicesCount: null})

    let path = "/devices/" + this.props.match.params.id
    window.history.pushState( path, "Title", path );
  }

  handleChangeEditMode() {
    let {editMode} = this.state
    this.setState( {editMode: ((!editMode))} )
  }

  handleChangeName( e ) {
    this.state.itemName = e.target.value.trim()
    if ( this.state.itemName !== null && this.state.itemName !== undefined && this.state.itemName !== "" ) {
      this.setState( {nameInputInvalid: false, nameInputValid: true} )
    } else {
      this.setState( {nameInputInvalid: true, nameInputValid: false} )
    }
  }

  handleAbortUpdateName() {
    let {editMode} = this.state
    this.setState( {itemName: this.state.item.name, editMode: ((!editMode))} )
  }

  handelSaveName() {
    this.state.item.name = this.state.itemName

    fetch( '/device-ui-service/devices/updateDevice', {
      method: 'PATCH',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify( this.state.item )
    } )
      .then( ( res ) => {
        if ( res.status !== 201 ) {
          this.setState( {successItem: {}, successSend: false, failSend: true} )
        } else {
          return res.json()
        }
      } )
      .then( ( data ) => {
        this.setState( {successItem: data, successSend: true, failSend: false, editMode: false} )
      } )
      .catch( ( error ) => {
        console.error( error );
        //ai.appInsights.trackException(error)
        this.setState( {successItem: error, successSend: false, failSend: true, saveMode: false} )
      } )
  }

  handleReloadPage() {
    this._pageIsload = false
    this._mapIsload = false
    this.setState( {
                     item: null,
                     itemName: null,
                     lastStatusStart: moment().startOf('day'),
                     lastStatusEnd: moment().endOf('day'),
                     deviceLat: null,
                     deviceLng: null
                   } );
    this.fetchDevice()
  }

  handleGoBack() {
    const {history, location} = this.props;
    if ( (location.state !== undefined) && (location.state.lastLocation !== null || location.state.lastLocation !== undefined) ) {
      history.push( location.state.lastLocation )
    } else {
      history.push( {
                      pathname: "/devices"
                    } )
    }
  }

  getDateRange() {
    let {t} = this.props
    let ranges = []
    ranges[t("sites.Device.DateRangePicker.lastHour")] = [moment().subtract( 1, 'hours' ), moment()]
    ranges[t("sites.Device.DateRangePicker.last3Hour")] = [moment().subtract( 3, 'hours' ), moment()]
    ranges[t("sites.Device.DateRangePicker.last8Hour")] = [moment().subtract( 8, 'hours' ), moment()]
    ranges[t("sites.Device.DateRangePicker.last24Hour")] = [moment().subtract( 24, 'hours' ), moment()]
    ranges[t("sites.Device.DateRangePicker.last3Days")] = [moment().startOf( 'day' ).subtract( 3, 'days' ), moment().endOf( 'day' ).subtract( 1, 'days' )]
    ranges[t("sites.Device.DateRangePicker.last7Days")] = [moment().startOf( 'day' ).subtract( 7, 'days' ), moment().endOf( 'day' ).subtract( 1, 'days' )]

    return ranges
  }

  render() {
    const {t} = this.props
    const {item, deviceNotFound} = this.state;

    if ( deviceNotFound )
      return DevicesDetail.renderDeviceNotFound( t );

    return item ?
      this.renderData( item ) :
      DevicesDetail.renderLoading();

  }

  renderData() {
    const {t} = this.props;
    let {lastStatusStart, lastStatusEnd, item, itemName, nameInputInvalid, nameInputValid, geoDevicesCount} = this.state
    let {
      deviceId, lastSeenAt,
      lastSeenBatteryVoltage, lastSeenTemperature,
      lastValidGeoPositionLatitude, lastValidGeoPositionLongitude,
      lastValidGeoPositionSentAt,
      lastValidGeoPositionType, lastValidGeoPositionVibrationCount,
      name, stillAssigned
    } = item
    return <div className={"site-device-detail"}>
      <Container>
        <>
          <Row>
            <Col lg="8 mr-0" id={"device-container"}>
              <Row>
                <Col>
                  <h1 className={"mt-3"}>{t( 'sites.Device.detailTitle' )}</h1>
                </Col>
              </Row>

              <Row className={"mt-0 mt-md-4"}>
                <Col md="12" className="">
                  <div className="d-flex">
                    <div className="pt-2">
                      <FontAwesomeIcon icon={['fal', 'location']} size="sm" className={"text-primary"}/>
                    </div>
                    <div className="p-2 col-8">
                      {this.state.editMode &&
                       <Form>
                         <Form.Row>
                           <Form.Group as={Col} controlId="formGridDesignation">
                             <Form.Control
                               type="text"
                               className="form-control-lg"
                               defaultValue={itemName}
                               placeholder={t( 'sites.Asset.assetCreate.namePlaceholder' )}
                               onChange={this.handleChangeName.bind( this )}
                               isInvalid={((itemName !== null) ? ((nameInputInvalid) ? nameInputInvalid : false) : nameInputInvalid)}
                               isValid={((itemName !== null) ? ((nameInputValid) ? nameInputValid : true) : nameInputValid)}>
                             </Form.Control>
                             <Button variant="light" size="sm" style={{fontSize: '19px', marginTop: "0px"}} onClick={this.handelSaveName.bind( this )}><FontAwesomeIcon style={{fontSize: '21px'}} icon={['fal', 'save']}/></Button>
                             <Button variant="light" size="sm" style={{fontSize: '19px', marginTop: "0px"}} onClick={this.handleAbortUpdateName.bind( this )}><FontAwesomeIcon style={{fontSize: '21px'}} icon={['fal', 'times']}/></Button>
                           </Form.Group>
                         </Form.Row>
                       </Form>
                      }

                      {!this.state.editMode &&
                       <h5 className={"text-primary"}>{name}</h5>
                      }
                    </div>
                    <div className="ml-auto">
                      <ButtonGroup className="mr-2" aria-label="First group">
                        <OverlayTrigger
                          key={'closeButtonTooltip'}
                          placement={'closeButtonTooltip'}
                          overlay={
                            <Tooltip id={`tooltip-closeButtonTooltip`}>
                              {t('DefaultButtons.close')}
                            </Tooltip>
                          }
                        >
                          <Button variant="light" size="sm" style={{fontSize: '19px', marginTop: "0px"}} onClick={this.handleGoBack.bind(this)}><FontAwesomeIcon style={{fontSize: '21px'}} icon={['fal', 'times']}/></Button>
                        </OverlayTrigger>
                        <OverlayTrigger
                          key={'refreshButtonTooltip'}
                          placement={'refreshButtonTooltip'}
                          overlay={
                            <Tooltip id={`tooltip-refreshButtonTooltip`}>
                              {t('DefaultButtons.refresh')}
                            </Tooltip>
                          }
                        >
                          <Button variant="light" size="sm" style={{fontSize: '19px', marginTop: "-1px"}} onClick={this.handleReloadPage.bind( this )}><FontAwesomeIcon style={{fontSize: '17px'}} icon={['fal', 'sync-alt']}/></Button>
                        </OverlayTrigger>
                        <OverlayTrigger
                          key={'renameButtonTooltip'}
                          placement={'renameButtonTooltip'}
                          overlay={
                            <Tooltip id={`tooltip-renameButtonTooltip`}>
                              {t('DefaultButtons.rename')}
                            </Tooltip>
                          }
                        >
                          <Button variant="light" size="sm" style={{fontSize: '19px'}} onClick={this.handleChangeEditMode.bind( this )} disabled={this.state.editMode}><FontAwesomeIcon icon={['fal', 'edit']}/></Button>
                        </OverlayTrigger>

                        {stillAssigned ?
                          (
                            <div className="ml-auto p-2">
                              <FontAwesomeIcon icon={['fa', 'lightbulb-exclamation']} size="lg" color={"green"}/>
                            </div>
                          ) : (
                            <div className="ml-auto p-2">
                              <FontAwesomeIcon icon={['fa', 'lightbulb-exclamation']} size="lg" color={"red"}/>
                            </div>
                          )}
                      </ButtonGroup>

                    </div>
                  </div>
                </Col>
              </Row>
              <Row>
                <Col>
                  <Accordion defaultActiveKey="0" id={"accordion"} className="mt-4 mb-5">
                    <Card className={"card--filter card__border-radius"}>
                      <Accordion.Toggle as={Card.Header} variant="link"
                                        className="card-header--transparent border-bottom-0 pb-2"
                                        eventKey="0"
                                        data-toggle="collapse"
                                        data-target="#filterCollapse"
                                        aria-expanded="true"
                                        aria-controls="0">
                        <div className="d-flex">
                          <div className="pt-2 pr-2">
                            <FontAwesomeIcon icon={['fal', 'sort-amount-down']} size="lg"/>
                          </div>
                          <div className="pt-1 pl-1 mb-0">
                            <h3>
                              {t( 'sites.Device.setSendTimeTitle' )}
                            </h3>
                          </div>
                          <div className="ml-auto pt-2">
                            <FontAwesomeIcon icon={['fal', 'chevron-down']} size="lg"/>
                          </div>
                        </div>
                      </Accordion.Toggle>
                      <Accordion.Collapse eventKey="0"
                                          id="filterCollapse"
                                          className="collapse show"
                                          aria-labelledby="1"
                                          data-parent="#accordion">
                        <Card.Body className="pt-2">
                          {geoDevicesCount !== null &&
                           <Row>
                             <Col>
                               <h6>
                                 {geoDevicesCount} {t('sites.Device.geoCount')}
                               </h6>
                               {(geoDevicesCount !== null && geoDevicesCount === 1000) &&
                                <h6>
                                  {t('sites.Device.geoCountMore1000')}
                                </h6>
                               }
                             </Col>
                           </Row>
                          }
                          <Row className="mb-4">
                            <Col>
                              <Form.Label>{t( 'component.ModuleFilter.defineDateTimeTitle' )}</Form.Label>
                              <DateRangePicker onChange={(( start, end, label ) => this.handleChangeDateRangeStatus( start, end, label, {label: "lastStatus", operator: "AND", where: this.props.lastStatusQuery} ))}
                                               startDate={lastStatusStart}
                                               endDate={lastStatusEnd}
                                               elementId={"data-time-range-select"}
                                               newRanges={this.getDateRange()}

                              />

                            </Col>
                          </Row>
                          <Row className="mb-1">
                            <Col>
                              <div className="d-flex flex-column flex-md-row flex-lg-row">
                                <div className="p-1 flex-fill">
                                  <Button variant="light" block onClick={this.handleRemoveGeoMessages.bind(this)}>{t( 'component.ModuleFilter.btnSetFilterBack' )}</Button>
                                </div>
                                <div className="p-1 flex-fill">
                                  <Button variant="primary" block onClick={this.handleGetGeoMessages.bind(this)}>{t( 'component.ModuleFilter.btnViewResult' )}</Button>
                                </div>
                              </div>
                            </Col>
                          </Row>
                          {this.state.loadGeo &&
                            DevicesDetail.renderLoading()
                          }
                        </Card.Body>
                      </Accordion.Collapse>
                    </Card>
                  </Accordion>
                </Col>
              </Row>
              <Row className={"mb-4"}>
                <Col md="4" className="mb-2">
                  <div className="d-flex">
                    <div className="pt-2 pr-1">
                      <FontAwesomeIcon icon={['fal', 'info-circle']} size="2x" className={"text-primary"}/>
                    </div>
                    <div className="p-2">
                      <h6 className={"text-primary"}>{t( 'sites.Device.deviceItem.deviceId' )}</h6>
                      <p>
                        {deviceId &&
                         deviceId
                        }
                        {!deviceId &&
                          "N/A"
                        }
                      </p>
                    </div>
                  </div>
                </Col>
                <Col md="4" className="mb-2">
                  <div className="d-flex">
                    <div className="pt-2 pr-1">
                      <FontAwesomeIcon icon={['far', 'cloud-download-alt']} size="2x" className={"text-primary"}/>
                    </div>
                    <div className="p-2">
                      <h6 className={"text-primary"}>{t( 'sites.Device.deviceItem.lastSendPosition' )}</h6>
                      <p>
                        {lastSeenAt &&
                         moment( lastSeenAt ).format( "L hh:mm" )
                        }<br/>
                        {lastValidGeoPositionSentAt &&
                         moment( lastValidGeoPositionSentAt ).format( "L hh:mm" )
                        }
                      </p>
                    </div>
                  </div>
                </Col>
                <Col md="4" className="mb-2">
                  <div className="d-flex">
                    <div className="pt-2 pr-1">
                      <FontAwesomeIcon icon={['fal', 'battery-half']} size="2x" className={"text-primary"}/>
                    </div>
                    <div className="p-2">
                      <h6 className={"text-primary"}>{t( 'sites.Device.deviceItem.batteryVoltage' )}</h6>
                      <p>
                        {lastSeenBatteryVoltage &&
                         lastSeenBatteryVoltage + " mv"
                        }

                        {!lastSeenBatteryVoltage &&
                         "N/A"
                        }
                      </p>
                    </div>
                  </div>
                </Col>
                <Col md="4" className="mb-2">
                  <div className="d-flex">
                    <div className="pt-2 pr-1">
                      <FontAwesomeIcon icon={['fal', 'flag']} size="2x" className={"text-primary"}/>
                    </div>
                    <div className="p-2">
                      <h6 className={"text-primary"}>{t( 'sites.Device.deviceItem.geoPosition' )}</h6>
                      <p>
                        {lastValidGeoPositionLatitude &&
                         "Lat: " + lastValidGeoPositionLatitude
                        }
                        {!lastValidGeoPositionLatitude &&
                         "N/A"
                        }
                        <br/>
                        {lastValidGeoPositionLongitude &&
                         "Lng: " + lastValidGeoPositionLongitude
                        }

                        {!lastValidGeoPositionLongitude &&
                         "N/A"
                        }

                      </p>
                    </div>
                  </div>
                </Col>
                <Col md="4" className="mb-2">
                  <div className="d-flex">
                    <div className="pt-2 pr-1">
                      <FontAwesomeIcon icon={['far', 'bolt']} size="2x" className={"text-primary"}/>
                    </div>
                    <div className="p-2">
                      <h6 className={"text-primary"}>{t( 'sites.Device.deviceItem.vibrationCount' )}</h6>
                      <p>
                        {lastValidGeoPositionVibrationCount &&
                         lastValidGeoPositionVibrationCount
                        }
                        {!lastValidGeoPositionVibrationCount &&
                         "N/A"
                        }
                      </p>
                    </div>
                  </div>
                </Col>
                <Col md="4" className="mb-2">
                  <div className="d-flex">
                    <div className="pt-2 pr-1">
                      <FontAwesomeIcon icon={['fal', 'thermometer-quarter']} size="2x" className={"text-primary"}/>
                    </div>
                    <div className="p-2">
                      <h6 className={"text-primary"}>{t( 'sites.Device.deviceItem.temperature' )}</h6>
                      <p>
                        {lastSeenTemperature &&
                         lastSeenTemperature + "°C"
                        }
                        {!lastSeenTemperature &&
                         "N/A"
                        }
                      </p>
                    </div>
                  </div>
                </Col>
                <Col md="4" className="mb-2">
                  <div className="d-flex">
                    <div className="pt-2 pr-1">
                      <FontAwesomeIcon icon={['fal', 'rss']} size="2x" className={"text-primary"}/>
                    </div>
                    <div className="p-2">
                      <h6 className={"text-primary"}>{t( 'sites.Device.deviceItem.geoProvider' )}</h6>
                      <p>
                        {lastValidGeoPositionType &&
                         lastValidGeoPositionType
                        }
                        {!lastValidGeoPositionType &&
                         "N/A"
                        }
                      </p>
                    </div>
                  </div>
                </Col>
              </Row>
            </Col>
            {!this.props.isMobile ?
              (
                <Col lg="4 shadow-bpw-lg p-0">
                  <div id="deviceMap"/>
                </Col>
              ) : ''}
          </Row>

        </>
      </Container>
    </div>
  }

  static renderLoading() {
    return <><Row><Col md={"12"} className={"d-flex justify-content-center mt-5"}><Loader type="Oval" color="#CDDC4B" height={70} width={70}/></Col></Row></>;
  }

  static renderDeviceNotFound( t ) {
    return <><Container><Row><Col md={"12"} className={"d-flex flex-column mt-3"}>
      <div className={"alert alert-danger"} role={"alert"}>{t( "sites.Device.deviceNotExist" )}</div>
    </Col></Row></Container></>;
  }
}

export default withQueryParams( {
                                  stripUnknownKeys: false
                                } )( DevicesDetail )