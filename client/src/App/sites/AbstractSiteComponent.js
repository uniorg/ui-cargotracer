import React, { Component } from 'react';
import Device from 'react-device'
import $ from 'jquery'
import withSizes from 'react-sizes'
import { Breadcrumb } from 'react-bootstrap'

class AbstractSiteComponent extends Component {
  withLog = false

  constructor( props ) {
    super( props );
    this.state = {width: 0, height: 0};
  }

  static mapSizesToProps() {
    return sizes => ({
      isMobile: withSizes.isMobile( sizes ),
      isTablet: withSizes.isTablet( sizes ),
      isDesktop: withSizes.isDesktop( sizes ),
      isGtMobile: withSizes.isGtMobile( sizes ),
      isGtTablet: withSizes.isGtTablet( sizes ),
      isStTablet: withSizes.isStTablet( sizes ),
      isStDesktop: withSizes.isStDesktop( sizes ),
      isTabletAndGreater: withSizes.isTabletAndGreater( sizes ),
      isTabletAndSmaller: !withSizes.isTabletAndSmaller( sizes ),
    });
  }

  render( render ) {
    const onChange = ( deviceInfo ) => {
      if ( this.withLog ) {
        // eslint-disable-next-line no-console
        console.log( 'Screen', deviceInfo )
        // eslint-disable-next-line no-console
        console.log( 'Screen height', deviceInfo.screen.height )
        // eslint-disable-next-line no-console
        console.log( 'Screen width', deviceInfo.screen.width )
        // eslint-disable-next-line no-console
        console.log( 'Screen orientation', deviceInfo.screen.orientation )
        // eslint-disable-next-line no-console
        console.log( 'Browser name', deviceInfo.browser.name )
      }
    }
    if ( !process.env.NODE_ENV || process.env.NODE_ENV === 'development' ) {
      return (
        <React.Fragment>
          <Device onChange={onChange}/>
          <Breadcrumb className={"dev-breadcrumb"}>
            <Breadcrumb.Item active>w: {this.state.width}</Breadcrumb.Item>
            <Breadcrumb.Item active>h: {this.state.height}</Breadcrumb.Item>
            <Breadcrumb.Item active>{this.props.isMobile ? 'Is Mobile' : 'Is Not Mobile'}</Breadcrumb.Item>
            <Breadcrumb.Item active>{this.props.isTablet ? 'Is Tablet' : 'Is Not Tablet'}</Breadcrumb.Item>
            <Breadcrumb.Item active>{this.props.isDesktop ? 'Is Desktop' : 'Is Not Desktop'}</Breadcrumb.Item>
          </Breadcrumb>
          {render}
        </React.Fragment>
      );
    } else {
      return (
        <>
          {render}
        </>
      )
    }

  }

  updateDimensions() {
    this.setState( {width: $( window ).width(), height: $( window ).height()} );
    this.forceUpdate()
  }

  componentWillMount() {
    this.updateDimensions();
  }

  componentDidMount() {
    //window.addEventListener( "resize", this.updateDimensions.bind( this ) );
    this.setState( {width: $( window ).width(), height: $( window ).height()} )
  }

  componentWillUnmount() {
    //window.removeEventListener( "resize", this.updateDimensions.bind( this ) );
  }

}


export default AbstractSiteComponent