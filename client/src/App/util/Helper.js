import React from 'react';

class HelperClass {

  constructor() {

  }

  getStateAsString( t, assetState ) {
    //AT_POI, ABORTED_ADDRESSCHANGE, ABORTED_DEVICECHANGED
    switch ( assetState ) {
      case 'FINISHED':
        return t( 'Helper.States.FINISHED' )
      case "AT_HOME":
        return t( 'Helper.States.AT_HOME' )
      case "ABORTED_ADDRESSCHANGE":
        return t( 'Helper.States.ABORTED_ADDRESSCHANGE' )
      case "ABORTED_DEVICECHANGED":
        return t( 'Helper.States.ABORTED_DEVICECHANGED' )
      case "ABORTED_DEVICEREMOVED":
        return t( 'Helper.States.ABORTED_DEVICEREMOVED' )
      case "ABORTED_DELETED":
        return t( 'Helper.States.ABORTED_DELETED' )
      case "AT_POI":
        return t( 'Helper.States.AT_POI' )
      case "ON_THE_WAY":
        return t( 'Helper.States.ON_THE_WAY' )
      case "NOT_TRACKED":
        return t( 'Helper.States.NOT_TRACKED' )
      case "TRACKED":
        return t( 'Helper.States.TRACKED' )
      default:
        return assetState
    }
  }
}

export let Helper = new HelperClass();
