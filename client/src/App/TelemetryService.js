import { ApplicationInsights } from '@microsoft/applicationinsights-web';
import { ReactPlugin } from '@microsoft/applicationinsights-react-js';
import { withAITracking } from "@microsoft/applicationinsights-react-js";
import React from 'react';

class TelemetryService {

  constructor() {
    this.reactPlugin = new ReactPlugin();
  }

  initialize( reactPluginConfig, callback ) {
    this.fetchEnvironment( () => {
      this.fetchMe( (data) => {
        const INSTRUMENTATION_KEY = window.env["APPSETTING_AppInsightsInstrumentationKey"] || '2c07150a-e0f3-4a34-a36c-9c4648ac3fb2'
        if ( data !== null && window.appInsights === undefined ) {
          this.appInsights = new ApplicationInsights( {
                                                          config: {
                                                            instrumentationKey: INSTRUMENTATION_KEY,
                                                            accountId: data.oid,
                                                            maxAjaxCallsPerView: -1,
                                                            maxBatchInterval: 0,
                                                            disableFetchTracking: false,
                                                            extensions: [this.reactPlugin ],
                                                            extensionConfig: {
                                                              [this.reactPlugin.identifier]: reactPluginConfig
                                                            }
                                                          }
                                                        } );
          this.appInsights.loadAppInsights();
        } else {
          if ( callback !== undefined )
            callback()
        }
      } )
    } )
  }

  fetchMe( callback ) {
    if ( window.me === undefined ) {
      fetch( '/user-ui-service/me' )
        .then( res => res.json() )
        .then( ( data ) => {
          if ( callback !== undefined )
            window.me = data
            callback(data)
        } ).catch( ( error ) => {
        console.error( error );
      } )
    } else {
      if ( callback !== undefined )
        callback(null)
    }
  }

  fetchEnvironment( callback ) {
    if ( window.env === undefined ) {
      fetch( '/environment', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify( {
                                "keys": ["APPSETTING_environment", "APPSETTING_AppInsightsInstrumentationKey"]
                              } )
      } )
        .then( ( res ) => {
          if ( res.status !== 200 ) {
            alert( "can not read environment" );
          } else {
            return res.json()
          }
        } )
        .then( ( data ) => {
          if ( Array.isArray( data ) ) {
            window.env = {}
            for ( let i = 0; i < data.length; i++ ) {
              let key = data[i]
              window.env[key.key] = key.value
            }
            if ( callback !== undefined )
              callback()
          } else {
            alert( "can not read environment" );
          }
        } )
        .catch( ( error ) => {
          console.error( error );
        } )
    } else {
      if ( callback !== undefined )
        callback()
    }
  }
}

export let ai = new TelemetryService();
