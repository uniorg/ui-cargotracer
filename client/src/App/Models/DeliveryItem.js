// To parse this data:
//
//   const Convert = require("./file");
//
//   const deliveryItem = Convert.toDeliveryItem(json);
//
// These functions will throw an error if the JSON doesn't
// match the expected interface, even if the JSON is valid.

// Converts JSON strings to/from your types
// and asserts the results of JSON.parse at runtime

function deliveryItem(deliveryItem) {
  return cast(deliveryItem, r("DeliveryItem"));
}

function toDeliveryItem(json) {
  return cast(JSON.parse(json), r("DeliveryItem"));
}

function deliveryItemToJson(value) {
  return JSON.stringify(uncast(value, r("DeliveryItem")), null, 2);
}

function invalidValue(typ, val) {
  throw Error(`Invalid value ${JSON.stringify(val)} for type ${JSON.stringify(typ)}`);
}

function jsonToJSProps(typ) {
  if (typ.jsonToJS === undefined) {
    var map = {};
    typ.props.forEach((p) => map[p.json] = { key: p.js, typ: p.typ });
    typ.jsonToJS = map;
  }
  return typ.jsonToJS;
}

function jsToJSONProps(typ) {
  if (typ.jsToJSON === undefined) {
    var map = {};
    typ.props.forEach((p) => map[p.js] = { key: p.json, typ: p.typ });
    typ.jsToJSON = map;
  }
  return typ.jsToJSON;
}

function transform(val, typ, getProps) {
  function transformPrimitive(typ, val) {
    if (typeof typ === typeof val) return val;
    return invalidValue(typ, val);
  }

  function transformUnion(typs, val) {
    // val must validate against one typ in typs
    var l = typs.length;
    for (var i = 0; i < l; i++) {
      var typ = typs[i];
      try {
        return transform(val, typ, getProps);
      } catch (_) {}
    }
    return invalidValue(typs, val);
  }

  function transformEnum(cases, val) {
    if (cases.indexOf(val) !== -1) return val;
    return invalidValue(cases, val);
  }

  function transformArray(typ, val) {
    // val must be an array with no invalid elements
    if (!Array.isArray(val)) return invalidValue("array", val);
    return val.map(el => transform(el, typ, getProps));
  }

  function transformDate(typ, val) {
    if (val === null) {
      return null;
    }
    const d = new Date(val);
    if (isNaN(d.valueOf())) {
      return invalidValue("Date", val);
    }
    return d;
  }

  function transformObject(props, additional, val) {
    if (val === null || typeof val !== "object" || Array.isArray(val)) {
      return invalidValue("object", val);
    }
    var result = {};
    Object.getOwnPropertyNames(props).forEach(key => {
      const prop = props[key];
      const v = Object.prototype.hasOwnProperty.call(val, key) ? val[key] : undefined;
      result[prop.key] = transform(v, prop.typ, getProps);
    });
    Object.getOwnPropertyNames(val).forEach(key => {
      if (!Object.prototype.hasOwnProperty.call(props, key)) {
        result[key] = transform(val[key], additional, getProps);
      }
    });
    return result;
  }

  if (typ === "any") return val;
  if (typ === null) {
    if (val === null) return val;
    return invalidValue(typ, val);
  }
  if (typ === false) return invalidValue(typ, val);
  while (typeof typ === "object" && typ.ref !== undefined) {
    typ = typeMap[typ.ref];
  }
  if (Array.isArray(typ)) return transformEnum(typ, val);
  if (typeof typ === "object") {
    return typ.hasOwnProperty("unionMembers") ? transformUnion(typ.unionMembers, val)
      : typ.hasOwnProperty("arrayItems")    ? transformArray(typ.arrayItems, val)
        : typ.hasOwnProperty("props")         ? transformObject(getProps(typ), typ.additional, val)
          : invalidValue(typ, val);
  }
  // Numbers can be parsed by Date but shouldn't be.
  if (typ === Date && typeof val !== "number") return transformDate(typ, val);
  return transformPrimitive(typ, val);
}

function cast(val, typ) {
  return transform(val, typ, jsonToJSProps);
}

function uncast(val, typ) {
  return transform(val, typ, jsToJSONProps);
}

function a(typ) {
  return { arrayItems: typ };
}

function u(...typs) {
  return { unionMembers: typs };
}

function o(props, additional) {
  return { props, additional };
}

// eslint-disable-next-line no-unused-vars
function m(additional) {
  return { props: [], additional };
}

function r(name) {
  return { ref: name };
}

const typeMap = {
  "DeliveryItem": o([
                      { json: "@odata.context", js: "@odata.context", typ: u(undefined, "") },
                      { json: "odataId", js: "odataId", typ: u(undefined, "") },
                      { json: "id", js: "id", typ: u(undefined, 0) },
                      { json: "deliveryNumber", js: "deliveryNumber", typ: u(undefined, "") },
                      { json: "transportId", js: "transportId", typ: u(undefined, 0) },
                      { json: "transportNumber", js: "transportNumber", typ: u(undefined, "") },
                      { json: "orderId", js: "orderId", typ: u(undefined, 0) },
                      { json: "orderNumber", js: "orderNumber", typ: u(undefined, "") },
                      { json: "shippingFromContact", js: "shippingFromContact", typ: u(undefined, r("ShippingFromContact")) },
                      { json: "shippingFromAddress", js: "shippingFromAddress", typ: u(undefined, r("ShippingAddress")) },
                      { json: "shippingToAddress", js: "shippingToAddress", typ: u(undefined, r("ShippingAddress")) },
                      { json: "plannedArrivalTime", js: "plannedArrivalTime", typ: u(undefined, Date) },
                      { json: "shippingConditions", js: "shippingConditions", typ: u(undefined, null) },
                      { json: "shipmentMode", js: "shipmentMode", typ: u(undefined, null) },
                      { json: "weightTotal", js: "weightTotal", typ: u(undefined, 0) },
                      { json: "weightTotalUnit", js: "weightTotalUnit", typ: u(undefined, r("Unit")) },
                      { json: "carrier", js: "carrier", typ: u(undefined, null) },
                      { json: "handingOverDate", js: "handingOverDate", typ: u(undefined, null) },
                      { json: "soldToPartyCode", js: "soldToPartyCode", typ: u(undefined, null) },
                      { json: "purchaseOrderRecipient", js: "purchaseOrderRecipient", typ: u(undefined, "") },
                      { json: "deliveryState", js: "deliveryState", typ: u(undefined, "") },
                      { json: "deliveryStateTimestamp", js: "deliveryStateTimestamp", typ: u(undefined, Date) },
                      { json: "deliveryStateDeviceId", js: "deliveryStateDeviceId", typ: u(undefined, "") },
                      { json: "lastValidGeoPositionLongitude", js: "lastValidGeoPositionLongitude", typ: u(undefined, null) },
                      { json: "lastValidGeoPositionLatitude", js: "lastValidGeoPositionLatitude", typ: u(undefined, null) },
                      { json: "lastValidGeoPositionRadius", js: "lastValidGeoPositionRadius", typ: u(undefined, null) },
                      { json: "lastValidGeoPositionType", js: "lastValidGeoPositionType", typ: u(undefined, null) },
                      { json: "lastValidGeoPositionSpeed", js: "lastValidGeoPositionSpeed", typ: u(undefined, 0) },
                      { json: "lastValidGeoPositionTemperature", js: "lastValidGeoPositionTemperature", typ: u(undefined, 0) },
                      { json: "etaArrival", js: "etaArrival", typ: u(undefined, Date) },
                      { json: "etaArrivalGeoPositionType", js: "etaArrivalGeoPositionType", typ: u(undefined, null) },
                      { json: "isLate", js: "isLate", typ: u(undefined, true) },
                      { json: "vibrationCount", js: "vibrationCount", typ: u(undefined, 0) },
                      { json: "itemCount", js: "itemCount", typ: u(undefined, 0) },
                      { json: "stateHistory", js: "stateHistory", typ: u(undefined, r("StateHistory")) },
                      { json: "items", js: "items", typ: u(undefined, a(r("Item"))) },
                    ], false),
  "Item": o([
              { json: "id", js: "id", typ: u(undefined, 0) },
              { json: "deliveryId", js: "deliveryId", typ: u(undefined, 0) },
              { json: "itemNumber", js: "itemNumber", typ: u(undefined, "") },
              { json: "materialSender", js: "materialSender", typ: u(undefined, "") },
              { json: "materialRecipient", js: "materialRecipient", typ: u(undefined, "") },
              { json: "descriptionShort", js: "descriptionShort", typ: u(undefined, "") },
              { json: "descriptionLong", js: "descriptionLong", typ: u(undefined, "") },
              { json: "dimensions", js: "dimensions", typ: u(undefined, "") },
              { json: "quantity", js: "quantity", typ: u(undefined, 0) },
              { json: "quantityUnit", js: "quantityUnit", typ: u(undefined, r("Unit")) },
              { json: "weightGross", js: "weightGross", typ: u(undefined, 0) },
              { json: "weightNet", js: "weightNet", typ: u(undefined, 0) },
              { json: "weightUnit", js: "weightUnit", typ: u(undefined, r("WeightUnit")) },
              { json: "packagingUnit", js: "packagingUnit", typ: u(undefined, null) },
              { json: "devicesCount", js: "devicesCount", typ: u(undefined, 0) },
              { json: "handlingUnits", js: "handlingUnits", typ: u(undefined, a(r("HandlingUnit"))) },
            ], false),
  "HandlingUnit": o([
                      { json: "id", js: "id", typ: u(undefined, 0) },
                      { json: "deliveryId", js: "deliveryId", typ: u(undefined, 0) },
                      { json: "handlingUnitCode", js: "handlingUnitCode", typ: u(undefined, "") },
                      { json: "descriptionShort", js: "descriptionShort", typ: u(undefined, r("DescriptionShort")) },
                      { json: "dimensions", js: "dimensions", typ: u(undefined, null) },
                      { json: "batchCode", js: "batchCode", typ: u(undefined, "") },
                      { json: "materialOrigin", js: "materialOrigin", typ: u(undefined, "") },
                      { json: "vibrationCount", js: "vibrationCount", typ: u(undefined, 0) },
                      { json: "stateDeviceId", js: "stateDeviceId", typ: u(undefined, null) },
                    ], false),
  "ShippingAddress": o([
                         { json: "id", js: "id", typ: u(undefined, 0) },
                         { json: "keyLabel", js: "keyLabel", typ: u(undefined, "") },
                         { json: "name", js: "name", typ: u(undefined, "") },
                         { json: "postalCode", js: "postalCode", typ: u(undefined, "") },
                         { json: "postalCodeBox", js: "postalCodeBox", typ: u(undefined, null) },
                         { json: "city", js: "city", typ: u(undefined, "") },
                         { json: "street", js: "street", typ: u(undefined, "") },
                         { json: "streetNumber", js: "streetNumber", typ: u(undefined, "") },
                         { json: "countryCode", js: "countryCode", typ: u(undefined, "") },
                         { json: "locationSource", js: "locationSource", typ: u(undefined, "") },
                         { json: "locationSourceKey", js: "locationSourceKey", typ: u(undefined, "") },
                         { json: "geofence", js: "geofence", typ: u(undefined, r("Geofence")) },
                       ], false),
  "Geofence": o([
                  { json: "id", js: "id", typ: u(undefined, 0) },
                  { json: "latitude", js: "latitude", typ: u(undefined, 3.14) },
                  { json: "longitude", js: "longitude", typ: u(undefined, 3.14) },
                  { json: "radius", js: "radius", typ: u(undefined, 0) },
                  { json: "type", js: "type", typ: u(undefined, "") },
                ], false),
  "ShippingFromContact": o([
                             { json: "id", js: "id", typ: u(undefined, 0) },
                             { json: "keyLabel", js: "keyLabel", typ: u(undefined, "") },
                             { json: "name", js: "name", typ: u(undefined, "") },
                             { json: "emailAddress", js: "emailAddress", typ: u(undefined, "") },
                             { json: "phoneNumber", js: "phoneNumber", typ: u(undefined, "") },
                             { json: "faxNumber", js: "faxNumber", typ: u(undefined, "") },
                           ], false),
  "StateHistory": o([
                      { json: "id", js: "id", typ: u(undefined, 0) },
                      { json: "savedAt", js: "savedAt", typ: u(undefined, Date) },
                      { json: "scanAt", js: "scanAt", typ: u(undefined, Date) },
                      { json: "scanVibrationCount", js: "scanVibrationCount", typ: u(undefined, 0) },
                      { json: "onTheWayAt", js: "onTheWayAt", typ: u(undefined, null) },
                      { json: "onTheWayVibrationCount", js: "onTheWayVibrationCount", typ: u(undefined, 0) },
                      { json: "arrivedAt", js: "arrivedAt", typ: u(undefined, Date) },
                      { json: "arrivedVibrationCount", js: "arrivedVibrationCount", typ: u(undefined, 0) },
                      { json: "doneAt", js: "doneAt", typ: u(undefined, Date) },
                      { json: "unknownAt", js: "unknownAt", typ: u(undefined, Date) },
                      { json: "transitTimeSeconds", js: "transitTimeSeconds", typ: u(undefined, null) },
                    ], false),
  "DescriptionShort": [
    "cupiditate quis excepturi esse error",
    "et laudantium necessitatibus repudiandae id",
    "facere illo voluptatum qui magnam",
    "id eveniet dolorum voluptatem ipsam",
    "numquam at eligendi ut et",
    "repellendus similique sint qui ut",
    "vel ipsam ut ab ut",
  ],
  "Unit": [
    "KG",
    "ST",
  ],
  "WeightUnit": [
    "KG",
    "TO",
  ],
};


export { deliveryItemToJson, toDeliveryItem, deliveryItem }
