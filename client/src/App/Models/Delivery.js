import PropTypes from 'prop-types';


export default class Delivery {
  static id = PropTypes.number.isRequired

  set id(id) {
    let idInt = parseInt(id)
    if (typeof idInt === "Nummber") {
      this.id = idInt
    } else {
      this.id = null
    }
  }

  get id() {
    return this.id
  }
}