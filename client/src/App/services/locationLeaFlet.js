import React, { Component } from 'react'

import L from 'leaflet'
import $ from 'jquery'
import { Map, TileLayer, withLeaflet } from 'react-leaflet';
import MeasureControlDefault from 'react-leaflet-measure';
import 'leaflet-easybutton'
import 'leaflet-range'
import { basemapLayer, tiledMapLayer } from 'esri-leaflet'
import 'leaflet.gridlayer.googlemutant'
import 'leaflet-polylinedecorator'
import 'leaflet-gesture-handling'
import 'leaflet.markercluster'
import 'leaflet.markercluster/dist/MarkerCluster.Default.css'
import moment from 'moment'

import { renderToString } from 'react-dom/server'

import i18next from '../services/i18n';
//import google from 'react-leaflet-google';


// Wrap our new variable and assign it to the one we used before. The rest of the codes stays the same.
const MeasureControl = withLeaflet( MeasureControlDefault );

type State = {
  lat: number,
  lng: number,
  zoom: number,
}

class LocationLeaFlet extends Component<{}, State> {
  map = null;
  scriptIsLoad = false;
  googleScriptIsLoad = false;
  subscriptionKey = "AIzaSyBLcL4wSHW_QvLSuzGsm-9uxmB1cr7XAxs";
  marker = {};
  markerTemp = [];
  markerCluster = {};
  markerPoly = {};
  maxZoom = 20;
  minZoom = 4;
  googleIsLoad = false;
  googleSubscriptionKey = "AIzaSyBLcL4wSHW_QvLSuzGsm-9uxmB1cr7XAxs";
  attribution = "&copy; <a href=\"https://www.openstreetmap.org/copyright\">OpenStreetMap</a> contributors";
  changeMapLogger = i18next.t( 'bpwLibs.locationLeaFlet.changeMapLoggerOpenStreet' );

  startLatitude = null;
  startLongitude = null;
  startZoom = null;
  containerId = null;
  lineIsSet = false;
  lineRouteIsSet = false;
  googleControlLayer = null;

  clusterOptions = {
    maxClusterRadius: 50
  };

  showZoomBtns = true;
  showCurrentPosition = true;
  showZoomToAllMarkerBtn = true;
  showLineRouteBtn = true;
  showAreaCalculationBtn = true;
  showFullScreenBtn = true;
  showMapSelectorBtn = true;
  showClusterBtn = true;


  state = {
    lat: 51.505,
    lng: -0.09,
    zoom: 13,
  }

  /**
   *
   * @param startLatitude Start Latitude für das Init der Karte
   * @param startLongitude Start Longitude für das Init der Karte
   * @param startZoom Start Zoom für das Init der Karte
   * @param containerId Container ID im HTML
   * @param mapType Start Map Type (openStreet, azureStreet), definiert in der Funktion getTileUrl
   */
  constructor( {startLatitude = '50.8038972', startLongitude = '7.1723139', startZoom = '5', containerId = 'map', mapType = 'openStreet'} = {} ) {

    super();
    this.startLatitude = startLatitude;
    this.startLongitude = startLongitude;
    this.startZoom = startZoom;
    this.containerId = containerId;
    this.mapType = mapType;
    this.state = {
      center: [2.935403, 101.448205],
      zoom: 10,
      minZoom: 4,
      maxZoom: 17,
    }
  }

  static initMapStart( that ) {
    that.setMapContainerHeight( that.containerId );
    that.initMap( that.containerId, that.startLatitude, that.startLongitude, that.startZoom,
                  function() {

                  },
                  that.mapType,
                  {
                    showZoomBtns: true,
                    showZoomToAllMarkerBtn: true,
                    showLineRouteBtn: true,
                    showAreaCalculationBtn: true,
                    showFullScreenBtn: true,
                    showMapSelectorBtn: true,
                    showClusterBtn: true,
                    showCurrentPosition: true
                  } )
  }

  /**
   *
   * @param callback Callback Funktion die ausgefüht wird wenn die Karte fertig geladen ist
   */
  startMap( callback ) {
    let that = this;
    this.setMapContainerHeight( this.containerId );
    this.initMap( this.containerId, this.startLatitude, this.startLongitude, this.startZoom,
                  function() {
                    if ( callback !== undefined ) {
                      that.setMapContainerHeight( that.containerId );
                      callback();
                    }
                  },
                  this.mapType,
                  {
                    showZoomBtns: true,
                    showZoomToAllMarkerBtn: true,
                    showLineRouteBtn: true,
                    showAreaCalculationBtn: true,
                    showFullScreenBtn: true,
                    showMapSelectorBtn: true,
                    showClusterBtn: true,
                    showCurrentPosition: true
                  } )
  }

  componentDidMount() {
    this.componentDidUpdate()
  }

  componentDidUpdate() {


  }

  renderx() {
    const measureOptions = {
      position: 'topright',
      primaryLengthUnit: 'meters',
      secondaryLengthUnit: 'kilometers',
      primaryAreaUnit: 'sqmeters',
      secondaryAreaUnit: 'acres',
      activeColor: '#db4a29',
      completedColor: '#9b2d14'
    };
    return (
      <Map {...this.state}>
        <TileLayer
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url='http://{s}.tile.osm.org/{z}/{x}/{y}.png'
        />
        <MeasureControl {...measureOptions} />
      </Map>
    );
  }

  /**
   * Berrechung und setzten der Container höhe der Karte
   * @param containerId Container ID im HTML
   */
  setMapContainerHeight( containerId ) {
    const height = $( ".application-main-container" ).height() - 20;

    $( "#" + containerId ).height( height );


    $( window ).resize( function() {
      const height = $( ".application-main-container" ).height() - 20;

      $( "#" + containerId ).height( height );
    } );
  }


  render() {
    return (
      <div id={this.props.containerId} className="w-100"></div>
    );
  }

  /**
   *
   * @param {object} options
   */
  setOptions( options ) {
    //app.console.log( "setOptions", options );
    const that = this;
    if ( options === undefined ) {
      return false;
    }

    for ( var key in options ) {
      //app.console.log( "setOptions key", key, that[key], options[key] );
      if ( that[key] !== undefined || that.hasOwnProperty( key ) ) {
        that[key] = options[key];
      }
    }

    return true;
  }

  /**
   * Initialisierung der Karte mit Options
   * @param containerId
   * @param startLatitude
   * @param startLongitude
   * @param startZoom
   * @param callback
   * @param mapType
   * @param options
   */
  initMap( containerId, startLatitude, startLongitude, startZoom, callback, mapType, options ) {
    const that = this;
    this.setOptions( options );

    //const user = app.user.getUser();
    const user = {
      bpwAdminBpw: true,
    };
    that.startLatitude = startLatitude;
    that.startLongitude = startLongitude;
    that.startZoom = startZoom;
    that.containerId = containerId;

    that.map = new L.map( containerId, {
      minZoom: 1,
      maxZoom: 18,
      gestureHandling: false,
      zoomControl: that.showZoomBtns,
      gestxureHandlingOptions: {
        text: {
          touch: i18next.t( "bpwLibs.locationLeaFlet.gestureHandlingTouch" ),
          scroll: i18next.t( "bpwLibs.locationLeaFlet.gestureHandlingScroll" ),
          scrollMac: i18next.t( "bpwLibs.locationLeaFlet.gestureHandlingScrollMac" )
        },
        duration: 1000
      }
    } ).setView( [startLatitude, startLongitude], startZoom );

    that.map.invalidateSize();
    const measureControl = L.control.measure( {
                                                primaryLengthUnit: "kilometers",
                                                secondaryLengthUnit: "miles",
                                                primaryAreaUnit: "sqmeters",
                                                secondaryAreaUnit: "sqmiles"
                                              } );
    if ( that.showAreaCalculationBtn && user !== undefined && user.bpwAdminBpw ) {
      measureControl.addTo( that.map );
    }

    if ( that.showFullScreenBtn && user !== undefined && user.bpwAdminBpw ) {
      L.Control.Fullscreen = L.Control.extend( {
                                                 options: {
                                                   position: 'topright',
                                                   title: {
                                                     'false': 'View Fullscreen',
                                                     'true': 'Exit Fullscreen'
                                                   },
                                                   icon: {
                                                     'false': 'fa-expand',
                                                     'true': 'fa-expand-arrows'
                                                   }
                                                 },

                                                 onAdd: function( map ) {
                                                   var container = L.DomUtil.create( 'div', 'leaflet-bar easy-button-container leaflet-control' );

                                                   this.button = L.DomUtil.create( 'button', 'easy-button-button leaflet-bar-part leaflet-interactive select-active leaflet-fullscreen-button', container );
                                                   this.link = L.DomUtil.create( 'i', 'fal fa-expand-arrows text-bpw-blue', this.button );

                                                   this._map = map;
                                                   this._map.on( 'fullscreenchange', this._toggleTitle, this );
                                                   this._toggleTitle();

                                                   L.DomEvent.on( this.button, 'click', this._click, this );

                                                   return container;
                                                 },

                                                 _click: function( e ) {
                                                   L.DomEvent.stopPropagation( e );
                                                   L.DomEvent.preventDefault( e );
                                                   this._map.toggleFullscreen( this.options );
                                                 },

                                                 _toggleTitle: function() {
                                                   this.button.title = this.options.title[this._map.isFullscreen()];
                                                   let linkElement = $( this.link );
                                                   if ( this._map.isFullscreen() ) {
                                                     linkElement.removeClass( this.options.icon[true] );
                                                     linkElement.addClass( this.options.icon[false] );
                                                   } else {
                                                     linkElement.removeClass( this.options.icon[false] );
                                                     linkElement.addClass( this.options.icon[true] );
                                                   }
                                                 }
                                               } );

      L.Map.include( {
                       isFullscreen: function() {
                         return this._isFullscreen || false;
                       },

                       toggleFullscreen: function( options ) {
                         var container = this.getContainer();
                         if ( this.isFullscreen() ) {
                           if ( options && options.pseudoFullscreen ) {
                             this._disablePseudoFullscreen( container );
                           } else if ( document.exitFullscreen ) {
                             document.exitFullscreen();
                           } else if ( document.mozCancelFullScreen ) {
                             document.mozCancelFullScreen();
                           } else if ( document.webkitCancelFullScreen ) {
                             document.webkitCancelFullScreen();
                           } else if ( document.msExitFullscreen ) {
                             document.msExitFullscreen();
                           } else {
                             this._disablePseudoFullscreen( container );
                           }

                         } else {
                           if ( options && options.pseudoFullscreen ) {
                             this._enablePseudoFullscreen( container );
                           } else if ( container.requestFullscreen ) {
                             container.requestFullscreen();
                           } else if ( container.mozRequestFullScreen ) {
                             container.mozRequestFullScreen();
                           } else if ( container.webkitRequestFullscreen ) {
                             container.webkitRequestFullscreen( Element.ALLOW_KEYBOARD_INPUT );
                           } else if ( container.msRequestFullscreen ) {
                             container.msRequestFullscreen();
                           } else {
                             this._enablePseudoFullscreen( container );
                           }

                         }

                       },

                       _enablePseudoFullscreen: function( container ) {
                         L.DomUtil.addClass( container, 'leaflet-pseudo-fullscreen' );
                         this._setFullscreen( true );
                         this.fire( 'fullscreenchange' );
                       },

                       _disablePseudoFullscreen: function( container ) {
                         L.DomUtil.removeClass( container, 'leaflet-pseudo-fullscreen' );
                         this._setFullscreen( false );
                         this.fire( 'fullscreenchange' );
                       },

                       _setFullscreen: function( fullscreen ) {
                         this._isFullscreen = fullscreen;
                         var container = this.getContainer();
                         if ( fullscreen ) {
                           L.DomUtil.addClass( container, 'leaflet-fullscreen-on' );
                         } else {
                           L.DomUtil.removeClass( container, 'leaflet-fullscreen-on' );
                         }
                         this.invalidateSize();
                       },

                       _onFullscreenChange: function( e ) {
                         var fullscreenElement =
                           document.fullscreenElement ||
                           document.mozFullScreenElement ||
                           document.webkitFullscreenElement ||
                           document.msFullscreenElement;

                         if ( fullscreenElement === this.getContainer() && !this._isFullscreen ) {
                           this._setFullscreen( true );
                           this.fire( 'fullscreenchange' );
                         } else if ( fullscreenElement !== this.getContainer() && this._isFullscreen ) {
                           this._setFullscreen( false );
                           this.fire( 'fullscreenchange' );
                         }
                       }
                     } );

      L.Map.mergeOptions( {
                            fullscreenControl: false
                          } );

      L.Map.addInitHook( function() {
        if ( this.options.fullscreenControl ) {
          this.fullscreenControl = new L.Control.Fullscreen( this.options.fullscreenControl );
          this.addControl( this.fullscreenControl );
        }

        var fullscreenchange;

        if ( 'onfullscreenchange' in document ) {
          fullscreenchange = 'fullscreenchange';
        } else if ( 'onmozfullscreenchange' in document ) {
          fullscreenchange = 'mozfullscreenchange';
        } else if ( 'onwebkitfullscreenchange' in document ) {
          fullscreenchange = 'webkitfullscreenchange';
        } else if ( 'onmsfullscreenchange' in document ) {
          fullscreenchange = 'MSFullscreenChange';
        }

        if ( fullscreenchange ) {
          var onFullscreenChange = L.bind( this._onFullscreenChange, this );

          this.whenReady( function() {
            L.DomEvent.on( document, fullscreenchange, onFullscreenChange );
          } );

          this.on( 'unload', function() {
            L.DomEvent.off( document, fullscreenchange, onFullscreenChange );
          } );
        }
      } );

      L.control.fullscreen = function( options ) {
        return new L.Control.Fullscreen( options );
      };
      that.map.addControl( new L.control.fullscreen() );
    }


    const current = L.easyButton( {
                                    id: "current",
                                    states: [
                                      {
                                        stateName: "select", // name the state
                                        icon: i18next.t( "bpwLibs.locationLeaFlet.btnOwnPositionIcon" ), // and define its properties
                                        title: i18next.t( "bpwLibs.locationLeaFlet.btnOwnPosition" ),
                                        onClick: function( btn, map ) { // and its callback
                                          that.map.locate( {setView: true, maxZoom: 16} );
                                        }
                                      }
                                    ]
                                  } );

    that.map.on( "locationfound", that.onLocationFound );
    that.map.on( "locationerror", that.onLocationError );

    const allMarker = L.easyButton( {
                                      id: "allMarker",
                                      states: [
                                        {
                                          stateName: "select", // name the state
                                          icon: i18next.t( "bpwLibs.locationLeaFlet.btnZoomAllMarkerIcon" ), // and define its properties
                                          title: i18next.t( "bpwLibs.locationLeaFlet.btnZoomAllMarker" ),
                                          onClick: function( btn, map ) { // and its callback
                                            that.setMarkerCenter();
                                          }
                                        }
                                      ]
                                    } );
    const markerGroup = [];
    //if ( device.default.mobile() && that.showCurrentPosition ) {
    markerGroup.push( current );
    //}
    if ( that.showZoomToAllMarkerBtn ) {
      markerGroup.push( allMarker );
    }

    L.easyBar( markerGroup ).addTo( that.map );

    const polyMarker = L.easyButton( {
                                       id: "polyMarker",
                                       states: [
                                         {
                                           stateName: "select", // name the state
                                           icon: i18next.t( "bpwLibs.locationLeaFlet.btnPolyMarkerIcon" ), // and define its properties
                                           title: i18next.t( "bpwLibs.locationLeaFlet.btnPolyMarker" ),
                                           onClick: function( btn, map ) {
                                             // app.console.log( "polyMarker" );
                                             if ( that.lineIsSet ) {
                                               that.removeMarker( "polyLine" );
                                               that.lineIsSet = false;
                                             } else {
                                               //app.console.log( "polyLine markerTemp", that.markerTemp );
                                               for ( var key in that.markerTemp ) {
                                                 if ( key !== undefined && that.markerTemp.hasOwnProperty( key ) && that.markerTemp[key].lineMode ) {
                                                   console.log( that.markerTemp )
                                                   console.log( key )
                                                   console.log( that.markerTemp[key] )
                                                   const locations = that.sortAndMapLocations( that.markerTemp[key].locations );
                                                   //app.console.log( "polyLine", locations );
                                                   that.removeMarker( "polyLine" );
                                                   console.log( locations )
                                                   const newLocations = locations.map( function( point ) {
                                                     return [point.lat, point.lng];
                                                   } );
                                                   that.setPolyline( newLocations, "polyLine" );
                                                   that.lineIsSet = true;
                                                 }
                                               }
                                             }
                                           }
                                         }
                                       ]
                                     } );

    const routeMarker = L.easyButton( {
                                        id: "routeMarker",
                                        states: [
                                          {
                                            stateName: "select", // name the state
                                            icon: i18next.t( "bpwLibs.locationLeaFlet.btnRoutMarkerIcon" ), // and define its properties
                                            title: i18next.t( "bpwLibs.locationLeaFlet.btnRoutMarker" ),
                                            onClick: function( btn, map ) {
                                              if ( that.lineRouteIsSet ) {
                                                that.removeMarker( "polyLineRoute" );
                                                that.lineRouteIsSet = false;
                                              } else {
                                                //app.console.log( "polyLineRoute markerTemp", that.markerTemp );
                                                //app.loader.loaderIn( 1, false, $( "#" + that.containerId ) );
                                                for ( var key in that.markerTemp ) {
                                                  if ( key !== undefined && that.markerTemp.hasOwnProperty( key ) && that.markerTemp[key].lineMode ) {
                                                    const locations = that.sortAndMapLocations( that.markerTemp[key].locations );

                                                    fetch( '/azure/direction', {
                                                      method: 'POST',
                                                      headers: {
                                                        'Accept': 'application/json',
                                                        'Content-Type': 'application/json'
                                                      },
                                                      body: JSON.stringify( {locations: locations} )
                                                    } )
                                                      .then( ( res ) => {
                                                        if ( res.status === 201 || res.status === 200 ) {
                                                          return res.json()
                                                        } else {
                                                          return {}
                                                        }
                                                      } )
                                                      .then( ( response ) => {
                                                        that.removeMarker( "polyLineRoute" );
                                                        const locations = response.mainRout.points[0].map( function( point ) {
                                                          return [point.latitude, point.longitude];
                                                        } );
                                                        that.setPolyline( locations, "polyLineRoute", {color: "#8c8b8b", weight: 5, opacity: 0.8} );
                                                        that.lineRouteIsSet = true;
                                                      } )
                                                      .catch( ( error ) => {
                                                        console.error( error );
                                                      } )
                                                  }
                                                }
                                              }// and its callback
                                            }
                                          }
                                        ]
                                      } );
    if ( that.showLineRouteBtn ) {
      L.easyBar( [polyMarker, routeMarker] ).addTo( that.map );
    }

    const slider = L.control.range( {
                                      position: "topright",
                                      min: 0,
                                      max: 100,
                                      value: that.clusterOptions.maxClusterRadius,
                                      step: 1,
                                      orient: "vertical",
                                      iconClass: "fal fa-cloud text-bpw-blue",
                                      icon: true
                                    } );

    slider.on( "input change", function( e ) {
      //app.console.log( e.value );
      that.clusterOptions.maxClusterRadius = e.value;
      for ( var markerTempKey in that.markerTemp ) {
        const item = that.markerTemp[markerTempKey];
        if ( item.hasOwnProperty( "locations" ) && item.hasOwnProperty( "key" ) ) {
          if ( that.markerCluster.hasOwnProperty( item.key ) ) {
            that.map.removeLayer( that.markerCluster[item.key] );
            that.markerCluster[item.key] = new L.MarkerClusterGroup( that.clusterOptions );
          }
          if ( that.marker.hasOwnProperty( item.key ) ) {
            for ( var markerKey in that.marker[item.key] ) {
              const marker = that.marker[item.key][markerKey];
              that.map.removeLayer( marker );
            }
            that.marker[item.key] = [];
          }
          that.setMarkers( item.locations, item.key, item.icon, item.cluster, item.onMap );
        }
      }
    } );

    that.setGoogleTileLayer( "roadmap", function() {
      if ( that.showClusterBtn && user !== undefined && user.bpwAdminBpw ) {
        that.map.addControl( slider );
      }
    } );
    that.map.addControl( slider );

    that.checkMarkerLineMode();

    $( "#" + that.containerId )
      .find( ".easy-button-button" )
      .attr( "data-toggle", "tooltip" );

    $( "#" + that.containerId )
      .find( ".leaflet-control-zoom-in, .leaflet-control-zoom-out, .leaflet-fullscreen-button" )
      .attr( "data-toggle", "tooltip" );


    $( "#" + that.containerId )
      .find( ".leaflet-control-zoom-in" )
      .addClass( "fal fa-search-plus text-bpw-blue" )
      .removeClass( "leaflet-control-zoom-in" )
      .css( {"font-size": "17px"} )
      .html( "" );

    $( "#" + that.containerId )
      .find( ".leaflet-control-zoom-out" )
      .addClass( "fal fa-search-minus text-bpw-blue" )
      .removeClass( "leaflet-control-zoom-out" )
      .css( {"font-size": "17px"} )
      .html( "" );


    $( "#" + that.containerId )
      .find( "[data-toggle=\"tooltip\"]" ).tooltip();
    that.setupTileLayerSelect();
    callback( that );
  }

  /**
   * Definieren der Karten Tile Layer Provider
   */
  setupTileLayerSelect() {
    const that = this;

    const openStreet = L.tileLayer( "https://tile.openstreetmap.org/{z}/{x}/{y}.png", {
      attribution: "Tiles courtesy of <a href=\"http://hot.openstreetmap.org/\" target=˜\"_blank\">Humanitarian OpenStreetMap Team</a>"
    } );
    /*const openStreet = L.tileLayer( "/azure/openstreetmap?zoom={z}&x={x}&y={y}", {
      attribution: "Tiles courtesy of <a href=\"http://hot.openstreetmap.org/\" target=˜\"_blank\">Humanitarian OpenStreetMap Team</a>"
    } );*/


    const openStreetHot = L.tileLayer( "https://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png", {
      attribution: "Tiles courtesy of <a href=\"http://hot.openstreetmap.org/\" target=\"_blank\">Humanitarian OpenStreetMap Team</a>"
    } );

    const OpenSeaMap = L.tileLayer( "https://tiles.openseamap.org/seamark/{z}/{x}/{y}.png", {
      attribution: "Map data: &copy; <a href=\"http://www.openseamap.org\">OpenSeaMap</a> contributors"
    } );

    const OpenPtMap = L.tileLayer( "http://openptmap.org/tiles/{z}/{x}/{y}.png", {
      maxZoom: 17,
      attribution: "Map data: &copy; <a href=\"http://www.openptmap.org\">OpenPtMap</a> contributors"
    } );

    const OpenTopoMap = L.tileLayer( "https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png", {
      maxZoom: 17,
      attribution: "Map data: {attribution.OpenStreetMap}, <a href=\"http://viewfinderpanoramas.org\">SRTM</a> | Map style: &copy; <a href=\"https://opentopomap.org\">OpenTopoMap</a> (<a href=\"https://creativecommons.org/licenses/by-sa/3.0/\">CC-BY-SA</a>)"
    } );

    const OpenRailwayMap = L.tileLayer( "https://{s}.tiles.openrailwaymap.org/standard/{z}/{x}/{y}.png", {
      maxZoom: 19,
      attribution: "Map data: {attribution.OpenStreetMap} | Map style: &copy; <a href=\"https://www.OpenRailwayMap.org\">OpenRailwayMap</a> (<a href=\"https://creativecommons.org/licenses/by-sa/3.0/\">CC-BY-SA</a>)"
    } );

    const OpenFireMap = L.tileLayer( "http://openfiremap.org/hytiles/{z}/{x}/{y}.png", {
      maxZoom: 19,
      attribution: "Map data: {attribution.OpenStreetMap} | Map style: &copy; <a href=\"http://www.openfiremap.org\">OpenFireMap</a> (<a href=\"https://creativecommons.org/licenses/by-sa/3.0/\">CC-BY-SA</a>)"
    } );

    const SafeCast = L.tileLayer( "https://s3.amazonaws.com/te512.safecast.org/{z}/{x}/{y}.png", {
      maxZoom: 16,
      attribution: "Map data: {attribution.OpenStreetMap} | Map style: &copy; <a href=\"https://blog.safecast.org/about/\">SafeCast</a> (<a href=\"https://creativecommons.org/licenses/by-sa/3.0/\">CC-BY-SA</a>)"
    } );

    const OpenMapSurfer = L.tileLayer( "https://korona.geog.uni-heidelberg.de/tiles/{variant}/x={x}&y={y}&z={z}", {
      maxZoom: 20,
      variant: "roads",
      attribution: "Imagery from <a href=\"http://giscience.uni-hd.de/\">GIScience Research Group @ University of Heidelberg</a> &mdash; Map data {attribution.OpenStreetMap}"
    } );

    const OpenMapSurferAdminBounds = L.tileLayer( "https://korona.geog.uni-heidelberg.de/tiles/{variant}/x={x}&y={y}&z={z}", {
      maxZoom: 19,
      variant: "adminb",
      attribution: "Imagery from <a href=\"http://giscience.uni-hd.de/\">GIScience Research Group @ University of Heidelberg</a> &mdash; Map data {attribution.OpenStreetMap}"
    } );

    const OpenMapSurferGrayscale = L.tileLayer( "https://korona.geog.uni-heidelberg.de/tiles/{variant}/x={x}&y={y}&z={z}", {
      maxZoom: 19,
      variant: "roadsg",
      attribution: "Imagery from <a href=\"http://giscience.uni-hd.de/\">GIScience Research Group @ University of Heidelberg</a> &mdash; Map data {attribution.OpenStreetMap}"
    } );

    const bingStreet = L.tileLayer( that.getTileUrl( "bingStreet" ), {
      attribution: this.attribution
    } );

    const bingSat = L.tileLayer( that.getTileUrl( "bingSat" ), {
      attribution: this.attribution
    } );

    const bingHybrid = L.tileLayer( that.getTileUrl( "bingHybrid" ), {
      attribution: this.attribution
    } );

    const azure = L.tileLayer( that.getTileUrl( "azure" ), {
      attribution: this.attribution
    } );
    azure.addTo( that.map );

    const EsriWorldStreet = basemapLayer( "Streets", {
      detectRetina: true
    } );

    const EsriTopografisch = basemapLayer( "Topographic", {
      detectRetina: true
    } );

    const EsriOceans = basemapLayer( "Oceans", {
      detectRetina: true
    } );

    const EsriOceansLabels = basemapLayer( "OceansLabels", {
      detectRetina: true
    } );

    const EsriNationalGeographic = basemapLayer( "NationalGeographic", {
      detectRetina: true
    } );
    const EsriPhysical = basemapLayer( "Physical", {
      detectRetina: true
    } );

    const EsriGray = basemapLayer( "Gray", {
      detectRetina: true
    } );

    const EsriGrayLabels = basemapLayer( "GrayLabels", {
      detectRetina: true
    } );
    const EsriDarkGray = basemapLayer( "DarkGray", {
      detectRetina: true
    } );
    const EsriDarkGrayLabels = basemapLayer( "DarkGrayLabels", {
      detectRetina: true
    } );

    const EsriWorldSatellite = basemapLayer( "Imagery", {
      detectRetina: true
    } );
    const EsriSatelliteLabels = basemapLayer( "ImageryLabels", {
      detectRetina: true
    } );
    const EsriImageryTransportation = basemapLayer( "ImageryTransportation", {
      detectRetina: true
    } );
    const EsriWorldSatelliteClarity = basemapLayer( "ImageryClarity", {
      detectRetina: true
    } );
    const EsriWorldSatelliteFirefly = basemapLayer( "ImageryFirefly", {
      detectRetina: true
    } );
    const EsriShadedRelief = basemapLayer( "ShadedRelief", {
      detectRetina: true
    } );

    const EsriShadedReliefLabels = basemapLayer( "ShadedReliefLabels", {
      detectRetina: true
    } );
    const EsriTerrain = basemapLayer( "Terrain", {
      detectRetina: true
    } );
    const EsriTerrainLabels = basemapLayer( "TerrainLabels", {
      detectRetina: true
    } );
    const EsriUSATopo = basemapLayer( "USATopo", {
      detectRetina: true
    } );


    /**const EsriOpenStreetMapVector = basemapLayer( "OpenStreetMap", {
					detectRetina: true
				} );
     const EsriNewspaperVector = L.esri.Vector.basemap( "Newspaper", {
					detectRetina: true
				} );
     const EsriTopographicVector = L.esri.Vector.basemap( "Topographic", {
					detectRetina: true
				} );
     const EsriNavigationVector = L.esri.Vector.basemap( "Navigation", {
					detectRetina: true
				} );
     const EsriStreetsVector = L.esri.Vector.basemap( "Streets", {
					detectRetina: true
				} );
     const EsriStreetsNightVector = L.esri.Vector.basemap( "StreetsNight", {
					detectRetina: true
				} );
     const EsriWorldStreetReliefVector = L.esri.Vector.basemap( "StreetsRelief", {
					detectRetina: true
				} );**/


    const EsriHistoric = tiledMapLayer( {
                                          url: "https://services.arcgisonline.com/ArcGIS/rest/services/USA_Topo_Maps/MapServer"
                                        } );
    /**const EsriNavigationCharts = tiledMapLayer( {
														url: "https://services.arcgisonline.com/ArcGIS/rest/services/Specialty/World_Navigation_Charts/MapServer",
														detectRetina: false,
														minZoom: 3,
														maxZoom: 10
													} );**/

    const mapbox = L.tileLayer( that.getTileUrl( "mapbox" ), {
      attribution: this.attribution
    } );


    const googleRoadMutant = L.gridLayer.googleMutant( {
                                                         maxZoom: 24,
                                                         type: "roadmap"
                                                       } );

    const googleSatMutant = L.gridLayer.googleMutant( {
                                                        maxZoom: 24,
                                                        type: "satellite"
                                                      } );

    const googleTerrainMutant = L.gridLayer.googleMutant( {
                                                            maxZoom: 24,
                                                            type: "terrain"
                                                          } );

    const googleHybridMutant = L.gridLayer.googleMutant( {
                                                           maxZoom: 24,
                                                           type: "hybrid"
                                                         } );

    const googleStyleMutant = L.gridLayer.googleMutant( {
                                                          styles: [
                                                            {elementType: "labels", stylers: [{visibility: "off"}]},
                                                            {featureType: "water", stylers: [{color: "#444444"}]},
                                                            {featureType: "landscape", stylers: [{color: "#eeeeee"}]},
                                                            {featureType: "road", stylers: [{visibility: "off"}]},
                                                            {featureType: "poi", stylers: [{visibility: "off"}]},
                                                            {featureType: "transit", stylers: [{visibility: "off"}]},
                                                            {featureType: "administrative", stylers: [{visibility: "off"}]},
                                                            {featureType: "administrative.locality", stylers: [{visibility: "off"}]}
                                                          ],
                                                          maxZoom: 24,
                                                          type: "roadmap"
                                                        } );

    const googleTrafficMutant = L.gridLayer.googleMutant( {
                                                            maxZoom: 24,
                                                            type: "roadmap"
                                                          } );
    googleTrafficMutant.addGoogleLayer( "TrafficLayer" );


    const googleTransitMutant = L.gridLayer.googleMutant( {
                                                            maxZoom: 24,
                                                            type: "roadmap"
                                                          } );
    googleTransitMutant.addGoogleLayer( "TransitLayer" );


    that.googleControlLayer = L.control.layers( {
                                                  "<i class=\"fab fa-google\"></i>oogle Roadmap": googleRoadMutant,
                                                  "<i class=\"fab fa-google\"></i>oogle Satellite": googleSatMutant,
                                                  "<i class=\"fab fa-google\"></i>oogle Terrain": googleTerrainMutant,
                                                  "<i class=\"fab fa-google\"></i>oogle Hybrid": googleHybridMutant,
                                                  "<i class=\"fab fa-google\"></i>oogle Styles": googleStyleMutant,
                                                  "<i class=\"fab fa-google\"></i>oogle Traffic": googleTrafficMutant,
                                                  "<i class=\"fab fa-google\"></i>oogle Transit": googleTransitMutant,
                                                  "Open Street Map": openStreet,
                                                  "Open Street Map Hot": openStreetHot,
                                                  "Open See Map": OpenSeaMap,
                                                  "Open Pt Map": OpenPtMap,
                                                  "Open Topo Map": OpenTopoMap,
                                                  "Open Railway Map": OpenRailwayMap,
                                                  "Open Fire Map": OpenFireMap,
                                                  "Safe Cast": SafeCast,
                                                  "Open Map Surfer": OpenMapSurfer,
                                                  "Open Map Surfer AdminBounds": OpenMapSurferAdminBounds,
                                                  "Open Map Surfer Grayscale": OpenMapSurferGrayscale,
                                                  "Bing Street": bingStreet,
                                                  "Bing Satellite": bingSat,
                                                  "Bing Hybrid": bingHybrid,
                                                  "Azure Maps": azure,
                                                  "Esri Stra&szlig;en": EsriWorldStreet,
                                                  "Esri Satellite": EsriWorldSatellite,
                                                  "Esri Satellite Labels": EsriSatelliteLabels,
                                                  "Esri Satellite Transportation": EsriImageryTransportation,
                                                  "Esri Satellite Clarity": EsriWorldSatelliteClarity,
                                                  "Esri Satellite Firefly": EsriWorldSatelliteFirefly,
                                                  "Esri DarkGray": EsriDarkGray,
                                                  "Esri DarkGray Labels": EsriGrayLabels,
                                                  "Esri Gray": EsriGray,
                                                  "Esri Gray Labels": EsriDarkGrayLabels,
                                                  "Esri National Geographic": EsriNationalGeographic,
                                                  "Esri Shaded Relief": EsriShadedRelief,
                                                  "Esri Shaded Relief Labels": EsriShadedReliefLabels,
                                                  "Esri Terrain": EsriTerrain,
                                                  "Esri Terrain Labels": EsriTerrainLabels,
                                                  "Esri USA Topo": EsriUSATopo,
                                                  "Esri Physical": EsriPhysical,
                                                  "Esri Meere": EsriPhysical,
                                                  "Esri Topografisch": EsriTopografisch,
                                                  "Esri Oceans": EsriOceans,
                                                  "Esri Oceans Labels": EsriOceansLabels,
                                                  "Esri Historic": EsriHistoric,
                                                  "mapbox": mapbox
                                                }, {}, {
                                                  collapsed: true
                                                } );

    //const user = app.user.getUser();
    const user = {
      bpwAdminBpw: true
    };
    if ( that.showMapSelectorBtn && user !== undefined && user.bpwAdminBpw ) {
      that.googleControlLayer.addTo( that.map );
    }

    const grid = L.gridLayer( {
                                attribution: "Grid Layer"
//      tileSize: L.point(150, 80),
//      tileSize: tileSize
                              } );

    grid.createTile = function( coords ) {
      const tile = L.DomUtil.create( "div", "tile-coords" );
      tile.innerHTML = [coords.x, coords.y, coords.z].join( ", " );

      return tile;
    };

    that.map.addLayer( grid );
  }

  setGoogleTileLayer( mapType, callback ) {
    //const that = this;
    //app.console.log( "setGoogleTileLayer", app.googleScriptIsLoad );
    /**if ( !app.googleScriptIsLoad || app.googleScriptIsLoad === undefined ) {
			//loader.loaderIn( 400, false );
			const url = "https://maps.googleapis.com/maps/api/js?key=" + that.subscriptionKey;
			$.getScript( url, function() {
				$.getScript( "/no/resources/leaflet.googleMutant/Leaflet.GoogleMutant.js", function() {
					//loader.loaderOut( 400, true );
					app.googleScriptIsLoad = true;
					that.setupTileLayerSelect();
					callback();
				} );
			} );
		} else {
			//loader.loaderIn( 400, false );
			$.getScript( "/no/resources/leaflet.googleMutant/Leaflet.GoogleMutant.js", function() {
				//loader.loaderOut( 400, true );
				that.setupTileLayerSelect();
				callback();
			} );
		}**/
  }

  /**
   * Überprüfung ob die Karte Marker besitzt die die Linienfunktion Freischalten
   */
  checkMarkerLineMode() {
    const that = this;
    let isLineMode = false;
    for ( var key in that.markerTemp ) {
      if ( key !== undefined && that.markerTemp.hasOwnProperty( key ) && that.markerTemp[key].lineMode ) {
        isLineMode = true;
      }
    }
    if ( isLineMode ) {
      $( "#" + that.containerId ).find( "#polyMarker" ).parent().removeAttr( "style" );
      $( "#" + that.containerId ).find( "#polyMarker" )
        .removeAttr( "disabled" )
        .find( "span" )
        .removeAttr( "style" );
      $( "#" + that.containerId ).find( "#routeMarker" )
        .removeAttr( "disabled" )
        .find( "span" )
        .removeAttr( "style" );
    } else {
      $( "#" + that.containerId ).find( "#polyMarker" ).parent().css( {"opacity": "0.6"} );
      $( "#" + that.containerId ).find( "#polyMarker" )
        .attr( "disabled", "disabled" )
        .find( "span" )
        .css( {"color": "#808080"} );
      $( "#" + that.containerId ).find( "#routeMarker" )
        .attr( "disabled", "disabled" )
        .find( "span" )
        .css( {"color": "#808080"} );
    }
  }

  roundNumber( num, scale ) {
    if ( !("" + num).includes( "e" ) ) {
      return +(Math.round( num + "e+" + scale ) + "e-" + scale);
    } else {
      const arr = ("" + num).split( "e" );
      let sig = "";
      if ( +arr[1] + scale > 0 ) {
        sig = "+";
      }
      return +(Math.round( +arr[0] + "e" + sig + (+arr[1] + scale) ) + "e-" + scale);
    }
  }

  /**
   * Gibt ein Object mit der Distanz zwischen zwei Punkten zurück
   * @param lat1
   * @param lon1
   * @param lat2
   * @param lon2
   * @returns {{km: *, meter: *, orginal: *}}
   */
  getDistance( lat1, lon1, lat2, lon2 ) {
    const R = 6371; // Radius of the earth in km
    const dLat = (lat2 - lat1) * Math.PI / 180; // deg2rad below
    const dLon = (lon2 - lon1) * Math.PI / 180;
    const a =
      0.5 - Math.cos( dLat ) / 2 +
      Math.cos( lat1 * Math.PI / 180 ) * Math.cos( lat2 * Math.PI / 180 ) *
      (1 - Math.cos( dLon )) / 2;
    const distanz = R * 2 * Math.asin( Math.sqrt( a ) );

    return {
      orginal: distanz,
      km: this.roundNumber( distanz, 4 ),
      meter: this.roundNumber( distanz * 1000, 4 )
    };
  }

  /**
   * Überprüfung ob eine distanz erreicht ist
   * @param lat1
   * @param lon1
   * @param lat2
   * @param lon2
   * @param distance
   * @param key
   * @returns {boolean}
   */
  checkDistance( lat1, lon1, lat2, lon2, distance, key ) {
    if ( key === undefined ) {
      key = "meter";
    }
    const distanceCalc = this.getDistance( lat1, lon1, lat2, lon2 );
    return !!(distanceCalc.hasOwnProperty( key ) && distanceCalc[key] >= distance);
  }

  /**
   * Map Only GPS Locations und Sortirung nach timestamp
   * @param locations
   * @returns {[]}
   */
  sortAndMapLocations( locations ) {
    const sortReverse = false;
    locations = locations.filter( function( result ) {
      return result.hasOwnProperty( "timestamp" ) && result.hasOwnProperty( "source" ) && result.source === "GPS";
    } );

    locations = locations.map( function( marker ) {
      const newMarker = {};
      newMarker.lat = marker.lat;
      newMarker.lng = marker.lng;
      newMarker.timestamp = new Date( marker.timestamp );
      newMarker.timestampSort = moment( new Date( marker.timestamp ) ).format();
      return newMarker;
    } );

    locations = locations.sort( function( a, b ) {
      let aValue;
      let bValue;
      let isString = false;
      let isNummber = false;
      if ( typeof a.timestampSort === "object" ) {
        a.timestampSort = a.timestampSort.toString();
      }
      if ( typeof b.timestampSort === "object" ) {
        b.timestampSort = b.timestampSort.toString();
      }
      if ( typeof a.timestampSort === "number" && typeof b.timestampSort === "number" ) {
        aValue = a.timestampSort;
        bValue = b.timestampSort;
        isNummber = true;
      }
      if ( typeof a.timestampSort === "string" && typeof b.timestampSort === "string" ) {
        aValue = String( a.timestampSort ).toLowerCase();
        bValue = String( b.timestampSort ).toLowerCase();
        isString = true;
      }

      if ( aValue === null || aValue === undefined ) {
        aValue = ((isNummber) ? null : "");
      }
      if ( bValue === null || bValue === undefined ) {
        bValue = ((isNummber) ? null : "");
      }
      if ( sortReverse && isNummber ) {
        return aValue - bValue;
      }
      if ( !sortReverse && isNummber ) {
        return bValue - aValue;
      }
      if ( sortReverse && isString ) {
        return bValue.localeCompare( aValue, "de-DE", {numeric: true, sensitivity: "accent"} );
      }
      if ( !sortReverse && isString ) {
        return aValue.localeCompare( bValue, "de-DE", {numeric: true, sensitivity: "accent"} );
      }
    } );

    const firstLocation = locations[0];
    const lastLocation = locations[locations.length - 1];
    locations = locations.slice( 0, locations.length - 1 );
    locations = locations.slice( 1 );
    const returnLocations = [];
    returnLocations.push( firstLocation );

    for ( let i = 0; i < locations.length; i++ ) {
      const location = locations[i];
      const nextLocation = locations[i + 1];

      if ( nextLocation !== undefined && this.checkDistance( location.lat, location.lng, nextLocation.lat, nextLocation.lng, 1.5, "km" ) ) {

        returnLocations.push( location );
      } else if ( nextLocation === undefined && location !== undefined ) {

        returnLocations.push( location );
      }
    }
    returnLocations.push( lastLocation );

    return returnLocations;
  }

  /**
   * Setzen einer Polyline von Markern
   * @param value Key Value der Marker
   */
  setMarkerPolyLine( value ) {
    const that = this;
    if ( value !== undefined && value !== "" && that.markerTemp.hasOwnProperty( value ) ) {
      const locations = that.markerTemp[value].locations.map( function( marker ) {
        const newMarker = {};
        newMarker.lat = marker.lat;
        newMarker.lng = marker.lng;
        return newMarker;
      } );
      that.removeMarker( "polyLine" );
      that.setPolyline( locations, "polyLine" );
    }
  }

  /**
   * get Map Tile Options
   * @param mapType
   * @returns {string} Map Service URl
   */
  getTileUrl( mapType ) {
    switch ( mapType ) {
      case "openStreet":
        this.minZoom = 1;
        this.maxZoom = 18;
        this.attribution = "<span class=\"own-contributors\">&copy; <a href=\"https://www.openstreetmap.org/copyright\">OpenStreetMap</a> contributors</span>";
        this.changeMapLogger = i18next.t( "bpwLibs.locationLeaFlet.changeMapLoggerOpenStreet" );
        return "/azure/openstreetmap?zoom={z}&x={x}&y={y}";
      case "bingStreet":
        this.minZoom = 2;
        this.maxZoom = 17;
        this.attribution = "<span class=\"own-contributors\">&copy; <a href=\"https://www.microsoft.com/de-de/rechtliche-hinweise/urheberrecht\">Bing Maps</a> contributors</span>";
        this.changeMapLogger = i18next.t( "bpwLibs.locationLeaFlet.changeMapLoggerBingStreet" );
        return "/azure/bing?zoom={z}&x={x}&y={y}&mapType=street"; // type r=standart a=satelite h=Hybrid
      case "bingSat":
        this.minZoom = 2;
        this.maxZoom = 17;
        this.attribution = "<span class=\"own-contributors\">&copy; <a href=\"https://www.microsoft.com/de-de/rechtliche-hinweise/urheberrecht\">Bing Maps</a> contributors</span>";
        this.changeMapLogger = i18next.t( "bpwLibs.locationLeaFlet.changeMapLoggerBingSat" );
        return "/azure/bing?zoom={z}&x={x}&y={y}&mapType=sat"; // type r=standart a=satelite h=Hybrid
      case "bingHybrid":
        this.minZoom = 2;
        this.maxZoom = 17;
        this.attribution = "<span class=\"own-contributors\">&copy; <a href=\"https://www.microsoft.com/de-de/rechtliche-hinweise/urheberrecht\">Bing Maps</a> contributors</span>";
        this.changeMapLogger = i18next.t( "bpwLibs.locationLeaFlet.changeMapLoggerBinHybrid" );
        return "/azure/bing?zoom={z}&x={x}&y={y}&mapType=hybrid"; // type r=standart a=satelite h=Hybrid
      case "azure":
        const layer = "basic"; // basic,hybrid & labels
        const style = "main";
        const language = "de-DE";
        this.minZoom = 4;
        this.maxZoom = 17;
        this.attribution = "<span class=\"own-contributors\">&copy; <a href=\"https://azure.microsoft.com/de-de/support/legal/\">Azure Location Service</a> contributors</span>";
        this.changeMapLogger = i18next.t( "bpwLibs.locationLeaFlet.changeMapLoggerAzure" );
        return "/azure/map/tile/png?&subscription-key=undefined&view=Unified&language=" + language + "&api-version=1.0&layer=" + layer + "&style=" + style + "&zoom={z}&x={x}&y={y}";
      case "google":
        this.minZoom = 2;
        this.maxZoom = 17;
        this.attribution = "<span class=\"own-contributors\">&copy; <a href=\"https://www.microsoft.com/de-de/rechtliche-hinweise/urheberrecht\">Google</a> contributors</span>";
        this.changeMapLogger = i18next.t( "bpwLibs.locationLeaFlet.changeMapLoggerGoogle" );
        return "http://mt1.google.com/vt/lyrs=m@110&hl=pl&x={x}5&y={y}&z={z}";
      case "EsriWorldStreet":
        this.minZoom = 2;
        this.maxZoom = 17;
        this.attribution = "<span class=\"own-contributors\">&copy; <a href=\"https://www.esri.com/de-de/home\">Esri, HERE, Garmin, NGA, USGS</a></span>";
        this.changeMapLogger = i18next.t( "bpwLibs.locationLeaFlet.changeMapLoggerEstriStreet" );
        return "http://services.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}";
      case "EsriWorldSatellite":
        this.minZoom = 2;
        this.maxZoom = 17;
        this.attribution = "<span class=\"own-contributors\">&copy; <a href=\"https://www.esri.com/de-de/home\">Esri, HERE, Garmin, NGA, USGS</a></span>";
        this.changeMapLogger = i18next.t( "bpwLibs.locationLeaFlet.changeMapLoggerEstriSat" );
        return "http://services.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}";
      case "EsriTerrain":
        this.minZoom = 2;
        this.maxZoom = 17;
        this.attribution = "<span class=\"own-contributors\">&copy; <a href=\"https://www.esri.com/de-de/home\">Esri, HERE, Garmin, NGA, USGS</a></span>";
        this.changeMapLogger = i18next.t( "bpwLibs.locationLeaFlet.changeMapLoggerEstriTer" );
        return "http://services.arcgisonline.com/ArcGIS/rest/services/World_Terrain_Base/MapServer/tile/{z}/{y}/{x}";
      case "EsriTopografisch":
        this.minZoom = 2;
        this.maxZoom = 17;
        this.attribution = "<span class=\"own-contributors\">&copy; <a href=\"https://www.esri.com/de-de/home\">Esri, HERE, Garmin, NGA, USGS</a></span>";
        this.changeMapLogger = i18next.t( "bpwLibs.locationLeaFlet.changeMapLoggerEstriTer" );
        return "https://services.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}";
      case "EsriSatWithDesc":
        this.minZoom = 2;
        this.maxZoom = 17;
        this.attribution = "<span class=\"own-contributors\">&copy; <a href=\"https://www.esri.com/de-de/home\">Esri, HERE, Garmin, NGA, USGS</a></span>";
        this.changeMapLogger = i18next.t( "bpwLibs.locationLeaFlet.changeMapLoggerEstriTer" );
        return "https://services.arcgisonline.com/ArcGIS/rest/services/Reference/World_Boundaries_and_Places/MapServer/tile/{z}/{y}/{x}";
      case "EsriWorldDarkGrayReference":
        this.minZoom = 2;
        this.maxZoom = 17;
        this.attribution = "<span class=\"own-contributors\">&copy; <a href=\"https://www.esri.com/de-de/home\">Esri, HERE, Garmin, NGA, USGS</a></span>";
        this.changeMapLogger = i18next.t( "bpwLibs.locationLeaFlet.changeMapLoggerEstriTer" );
        return "https://services.arcgisonline.com/arcgis/rest/services/Canvas/World_Dark_Gray_Reference/MapServer/tile/{z}/{y}/{x}";
      case "EsriWorldLightGrayReference":
        this.minZoom = 2;
        this.maxZoom = 17;
        this.attribution = "<span class=\"own-contributors\">&copy; <a href=\"https://www.esri.com/de-de/home\">Esri, HERE, Garmin, NGA, USGS</a></span>";
        this.changeMapLogger = i18next.t( "bpwLibs.locationLeaFlet.changeMapLoggerEstriTer" );
        return "https://services.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Reference/MapServer/tile/{z}/{y}/{x}";
      case "EsriNatGeoWorldMap":
        this.minZoom = 2;
        this.maxZoom = 17;
        this.attribution = "<span class=\"own-contributors\">&copy; <a href=\"https://www.esri.com/de-de/home\">Esri, HERE, Garmin, NGA, USGS</a></span>";
        this.changeMapLogger = i18next.t( "bpwLibs.locationLeaFlet.changeMapLoggerEstriTer" );
        return "https://services.arcgisonline.com/ArcGIS/rest/services/NatGeo_World_Map/MapServer/tile/{z}/{y}/{x}";
      case "EsriWorldOceanReference":
        this.minZoom = 2;
        this.maxZoom = 17;
        this.attribution = "<span class=\"own-contributors\">&copy; <a href=\"https://www.esri.com/de-de/home\">Esri, HERE, Garmin, NGA, USGS</a></span>";
        this.changeMapLogger = i18next.t( "bpwLibs.locationLeaFlet.changeMapLoggerEstriTer" );
        return "https://services.arcgisonline.com/arcgis/rest/services/Ocean/World_Ocean_Reference/MapServer/tile/{z}/{y}/{x}";
      case "mapbox":
        this.minZoom = 2;
        this.maxZoom = 17;
        this.attribution = "<span class=\"own-contributors\">&copy; <a href=\"https://www.microsoft.com/de-de/rechtliche-hinweise/urheberrecht\">mapbox</a> contributors</span>";
        this.changeMapLogger = i18next.t( "bpwLibs.locationLeaFlet.changeMapLoggerMapBox" );
        return "https://api.tiles.mapbox.com/v4/mapbox.streets-basic/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiZ3JpZmZsYSIsImEiOiJjajRncW10ZXUwNjFnMnFxYWJrcmd5dzY3In0.hWgvv4U33juE9-cZNlw_Ag";
      default:
        return "/azure/openstreetmap?zoom={z}&x={x}&y={y}";
    }
  }

  setScriptSource( callBack ) {
    const that = this;
    if ( !this.scriptIsLoad ) {
      //.loaderIn( 400, false, $( "#" + this.containerId ) );
      $( "<link id=\"leafletStyle\" rel=\"stylesheet\" type=\"text/css\" href=\"/no/resources/leaflet/leaflet.css\" />" ).appendTo( "head" );
      $( "<link id=\"MarkerClusterStyle\" rel=\"stylesheet\" type=\"text/css\" href=\"/no/resources/leaflet.markercluster/MarkerCluster.css\" />" ).appendTo( "head" );
      $( "<link id=\"MarkerClusterDefaultStyle\" rel=\"stylesheet\" type=\"text/css\" href=\"/no/resources/leaflet.markercluster/MarkerCluster.Default.css\" />" ).appendTo( "head" );
      $( "<link id=\"EasyButtonStyle\" rel=\"stylesheet\" type=\"text/css\" href=\"/no/resources/leaflet.easy-button/easy-button.css\" />" ).appendTo( "head" );
      $( "<link id=\"LeafletGestureHandling\" rel=\"stylesheet\" type=\"text/css\" href=\"/no/resources/leaflet.gestureHandling/leaflet-gesture-handling.css\" />" ).appendTo( "head" );
      $( "<link id=\"mapbox-gl\" rel=\"stylesheet\" type=\"text/css\" href=\"/no/resources/leaflet.esri/mapbox-gl.css\" />" ).appendTo( "head" );
      $( "<link id=\"LeafletGestureHandling\" rel=\"stylesheet\" type=\"text/css\" href=\"/no/resources/leaflet.fullscreen/leaflet.fullscreen.css\" />" ).appendTo( "head" );
      $( "<link id=\"LeafletGestureHandling\" rel=\"stylesheet\" type=\"text/css\" href=\"/no/resources/leaflet.measure/dist/leaflet-measure.css\" />" ).appendTo( "head" );

      $.getScript( "/dist/prod/concatSourceLeafletAll.js", function() {
        //loader.loaderOut( 400, true, $( "#" + that.containerId ) );
        that.scriptIsLoad = true;
        callBack();
      } );
    } else {
      callBack();
    }
  }

  buildPoiPopupContent( poiProperties ) {
    const poiTitleBox = document.createElement( "div" );
    poiTitleBox.classList.add( "col" );
    poiTitleBox.innerText = poiProperties.title;


    const poiContentBox = document.createElement( "div" );
    poiContentBox.classList.add( "row", "m-3" );
    poiContentBox.appendChild( poiTitleBox );
    return poiContentBox;
  }

  onLocationFound( e ) {
    const radius = e.accuracy / 2;

    const currentMarker = {
      lat: e.latlng.lat,
      lng: e.latlng.lng,
      latlng: e.latlng,
      title: i18next.t( "bpwLibs.locationLeaFlet.btnOwnPosition" ),
      popup: i18next.t( "bpwLibs.locationLeaFlet.btnOwnPositionPopupText", {radius: radius} ),
      circleRadius: radius,
      zIndex: 1500
    };
    const markerIcon = {
      google: "BpwPalletMarker",
      leaflet: "<i class=\"fas fa-map-marker-alt\" style=\"font-size: 1.5rem;color: #129fd8;position:  relative;top: -12px;left: -2px;\"></i>"
    };
    this.setMarkers( [currentMarker], "current", markerIcon, false, undefined, true );
  }

  onLocationError( e ) {
    //logger.error( e.message );
  }

  setPolyline( locations, key, options ) {
    if ( !this.marker.hasOwnProperty( key ) ) {
      this.marker[key] = [];
    }
    if ( !this.marker.hasOwnProperty( key + "Decorator" ) ) {
      this.marker[key + "Decorator"] = [];
    }
    //app.console.log( "setPolyline", locations );
    const polyline = L.polyline( locations, options ).addTo( this.map );

    const pd = L.polylineDecorator( polyline, {
      patterns: [
        {offset: 25, repeat: 50, symbol: L.Symbol.arrowHead( {pixelSize: 15, pathOptions: {fillOpacity: 1, weight: 0}} )}
      ]
    } ).addTo( this.map );
    // zoom the map to the polyline
    this.marker[key].push( polyline );
    this.marker[key + "Decorator"].push( pd );

    this.map.fitBounds( polyline.getBounds() );
  }

  /**
   * Setzen einer neuen Location Marker Liste
   * @param Array locations Locations Array
   * @param String key Key für das Später Löschen oder der gleichen
   * @param Object icon Default Marker Icon
   * @param Bool cluster Ob es geclustert werden soll
   * @param Bool onMap Ob die Marker direkt nach dem Hinzufügen auch angezeigt werden sollen
   * @param Bool lineMode ob die Marker Polygon oder Routen Funktion Haben sollen.
   * Wichtig für die lineMode, das Location Object muss die Keys timestamp und source aufweisen
   * {timestamp: "2019-11-27T09:43:13.273Z", source: "GPS"}
   * Default Location Object
   * {
        lat: latitude,
        lng: longitude,
        title: "Title",
        circleRadius: undefined,  //undefined oder Integer
        popup: "<div class='row m-3'>" +
               "<div class='col-12'>Typ: " + geoPositionType + "</div>" +
               "<div class='col-12'>lat: " + latitude + "</div>" +
               "<div class='col-12'>lng: " + longitude + "</div>" +
               "<div class='col-12'>SentAt: " + moment( new Date( sentAt ) ).format( 'LLL' ) + "</div>" +
               "</div>",
        zIndex: 1000,
        timestamp: "2019-11-27T09:43:13.273Z",
        source: "GPS",
        icon: {
          google: "BpwPalletMarker",
          leaflet: "<i class=\"fas fa-map-pin\" style='font-size: 1.3rem;color: " + color + ";position:  relative;top: -12px;left: -2px;'></i>"
        }
      }
   */
  setMarkers( locations, key, icon, cluster, onMap, lineMode ) {
    const that = this;
    locations = locations.filter( function( location ) {
      if ( location === undefined || location === null ) {
        return false
      }
      if ( location.lat === undefined || location.lat === null || location.lat === "" ) {
        return false
      }
      if ( location.lng === undefined || location.lng === null || location.lng === "" ) {
        return false
      }
      return true
    } );
    this.removeMarker( "polyLine" );
    if ( !this.marker.hasOwnProperty( key ) ) {
      this.marker[key] = [];
      this.markerTemp[key] = [];
      if ( cluster !== undefined && cluster === true ) {
        this.markerCluster[key] = new L.MarkerClusterGroup( this.clusterOptions );
      }
    }
    this.markerTemp[key] = {
      locations: locations,
      key: key,
      icon: icon,
      cluster: cluster,
      onMap: onMap,
      lineMode: ((lineMode === undefined || lineMode === "") ? false : lineMode)
    };
    for ( let i = 0; i < locations.length; i++ ) {
      let marker;
      if ( icon !== undefined ) {
        let markerIcon;
        if ( icon.leaflet.match( /[&;=<>""]/ ) ) {
          markerIcon = L.divIcon( {className: "", html: icon.leaflet} );
        } else {
          markerIcon = L.divIcon( {className: icon.leaflet} );
        }
        if ( locations[i].hasOwnProperty( "icon" ) && locations[i].icon !== undefined ) {
          if ( locations[i].icon.leaflet.match( /[&;=<>""]/ ) ) {
            markerIcon = L.divIcon( {className: "", html: locations[i].icon.leaflet} );
          } else {
            markerIcon = L.divIcon( {className: locations[i].icon.leaflet} );
          }
        }
        marker = L.marker( [locations[i].lat, locations[i].lng], {icon: markerIcon} );
      } else {
        marker = L.marker( [locations[i].lat, locations[i].lng] );
      }
      if ( locations[i].hasOwnProperty( "popup" ) && locations[i].popup !== undefined ) {
        const container = $( "<div />" );
        container.on( "click", "a", function() {
          const win = window.open( $( this ).attr( "href" ), "_blank" );
          win.focus();
          return false;
        } );
        container.html( locations[i].popup );

        marker.bindPopup( container[0] )
          .openPopup( that.map );
      }
      if ( locations[i].hasOwnProperty( "popupRender" ) && locations[i].popupRender !== undefined ) {
        const container = $( "<div />" );
        container.html( renderToString( locations[i].popupRender, document.createElement( 'div' ) ) );

        marker.bindPopup( container[0] ).on( "popupopen", () => {
          console.log( renderToString( locations[i].popupRender, document.createElement( 'div' ) ) )
        } ).openPopup( that.map );
      }
      if ( locations[i].hasOwnProperty( "tooltip" ) && locations[i].tooltip !== undefined ) {
        marker.bindTooltip( locations[i].tooltip )
          .openTooltip( that.map );
      }
      if ( locations[i].hasOwnProperty( "zIndex" ) && locations[i].zIndex !== undefined ) {
        marker.setZIndexOffset( locations[i].zIndex );
      }
      if ( locations[i].hasOwnProperty( "circleRadius" ) && locations[i].circleRadius !== undefined ) {
        if ( !that.markerPoly.hasOwnProperty( key ) ) {
          that.markerPoly[key] = [];
        }
        that.markerPoly[key].push( L.circle( marker.getLatLng(), locations[i].circleRadius, ((locations[i].circleRadiusColor !== undefined) ? {color: locations[i].circleRadiusColor} : undefined) ) );
      }
      if ( cluster !== undefined && cluster === true ) {
        that.markerCluster[key].addLayer( marker );
      }

      that.marker[key].push( marker );
    }

    if ( cluster !== undefined && cluster === true ) {
      if ( (onMap === undefined && onMap) || (onMap !== undefined && onMap) ) {
        this.map.addLayer( this.markerCluster[key] );
      }
    } else {
      if ( (onMap === undefined && onMap) || (onMap !== undefined && onMap) ) {
        for ( var marker in this.marker[key] ) {
          this.marker[key][marker].addTo( this.map );
        }
        for ( var poly in this.markerPoly[key] ) {
          this.markerPoly[key][poly].addTo( this.map );
        }
      }
    }
    that.checkMarkerLineMode();
  }

  checkGetLatLng( marker ) {
    if ( marker.getLatLng !== undefined &&
         typeof marker.getLatLng === "function" &&
         marker.getLatLng().lng !== undefined &&
         marker.getLatLng().lat !== undefined ) {
      return true;
    } else {
      return false;
    }
  }

  setLatLngMinMax( key ) {
    const that = this;
    this.latMin = 90;
    this.latMax = -90;

    this.lngMin = 180;
    this.lngMax = -180;
    if ( key !== undefined ) {
      $.each( that.marker[key], function( index, marker ) {
        $.each( marker.locations, function( index, location ) {
          if ( that.latMax < location.lat ) {
            that.latMax = location.lat;
          }
          if ( location.lat < that.latMin ) {
            that.latMin = location.lat;
          }
          if ( that.lngMax < location.lng ) {
            that.lngMax = location.lng;
          }
          if ( location.lng < that.lngMin ) {
            that.lngMin = location.lng;
          }
        } );
      } );
    } else {
      let markerArray = []

      for ( key in that.markerTemp ) {
        let marker = that.markerTemp[key]
        $.each( marker.locations, function( index, location ) {
          markerArray.push( location )
        } );
      }
      $.each( markerArray, function( index, location ) {
        if ( that.latMax < location.lat ) {
          that.latMax = location.lat;
        }
        if ( location.lat < that.latMin ) {
          that.latMin = location.lat;
        }
        if ( that.lngMax < location.lng ) {
          that.lngMax = location.lng;
        }
        if ( location.lng < that.lngMin ) {
          that.lngMin = location.lng;
        }
      } );
    }
  }

  /**
   * Zentrieren der Marker auf der Karte mit Berücksichtigung des Circels
   * @param String key zuvor Definierter key unter setMarkers
   * @param Bool withCircle ob der Definierte Circel der Location mit berücksichtigt werden soll
   */
  setNowMarkerCenter( key, withCircle ) {
    this.setLatLngMinMax( key );
    console.log( "setNowMarkerCenter", this.latMax, this.latMin, this.lngMax, this.lngMin, key, withCircle, this.marker );
    this.setMarkerCenter( this.latMax, this.latMin, this.lngMax, this.lngMin, key, withCircle );
  }

  setMarkerCenter( latMax, latMin, lngMax, lngMin, key, withCircle ) {
    const that = this;
    let latLngs = [];
    if ( key !== undefined ) {
      for ( var marker in this.marker[key] ) {
        if ( this.checkGetLatLng( this.marker[key][marker] ) ) {
          latLngs.push( L.latLng( this.marker[key][marker].getLatLng().lat, this.marker[key][marker].getLatLng().lng ) );
        }
      }
    } else {

      for ( var keyMarker in this.marker ) {
        for ( var markerObject in this.marker[keyMarker] ) {
          if ( this.checkGetLatLng( this.marker[keyMarker][markerObject] ) ) {
            latLngs.push( L.latLng( this.marker[keyMarker][markerObject].getLatLng().lat, this.marker[keyMarker][markerObject].getLatLng().lng ) );
          }
        }
      }
    }

    if ( withCircle ) {
      if ( key !== undefined ) {
        for ( var circle in this.markerPoly[key] ) {
          if ( this.markerPoly[key][circle].getBounds !== undefined ) {
            const bounds = this.markerPoly[key][circle].getBounds();
            latLngs = bounds;
          }
        }
      } else {
        let markerPolyArray = []
        latLngs = [];
        for ( key in that.markerPoly ) {
          let markerPoly = that.markerPoly[key]
          $.each( markerPoly, function( index, location ) {
            markerPolyArray.push( location )
          } );
        }
        $.each( markerPolyArray, function( index, circle ) {
          if ( circle.getBounds !== undefined ) {
            const bounds = circle.getBounds();
            latLngs.push( bounds );
          }
        } );
      }
    }


    if ( latLngs.length === undefined ) {
      this.map.fitBounds( latLngs );
    } else if ( latLngs.length > 0 ) {
      if ( latLngs.length === 1 && withCircle === undefined ) {
        this.map.flyTo( latLngs[0], 16, {animate: false} );
      } else {
        this.map.fitBounds( latLngs );
      }
    } else {
      this.map.flyTo( [this.startLatitude, this.startLongitude], this.startZoom, {animate: false} );
    }
  }

  setMapZoom( zoom ) {
    this.map.setZoom( zoom );
  }

  /**
   * Springe in der Karte zu einer Position
   * @param Object latLng
   * @param Integer zoom
   * @param options
   */
  flyTo( latLng, zoom, options ) {
    if ( options === undefined ) {
      options = {};
    }
    this.map.flyTo( latLng, zoom, options );
  }

  /**
   * Löschen von Location Markern von der Karte mit Hilfe des Definierten Key in setMarker
   * @param key
   */
  removeMarker( key ) {
    if ( this.markerCluster.hasOwnProperty( key ) ) {
      this.map.removeLayer( this.markerCluster[key] );
      this.markerCluster[key] = new L.MarkerClusterGroup( this.clusterOptions );
      delete this.markerTemp[key];
    }
    if ( this.marker.hasOwnProperty( key ) ) {
      for ( var markerKey in this.marker[key] ) {
        const marker = this.marker[key][markerKey];
        this.map.removeLayer( marker );
      }
      this.marker[key] = [];
      delete this.markerTemp[key];
    }
    if ( this.markerPoly.hasOwnProperty( key ) ) {
      for ( var polyKey in this.markerPoly[key] ) {
        const poly = this.markerPoly[key][polyKey];
        this.map.removeLayer( poly );
      }
      this.markerPoly[key] = [];
    }
    if ( this.marker.hasOwnProperty( "polyLine" ) ) {
      for ( var polyLineKey in this.marker["polyLine"] ) {
        const marker = this.marker["polyLine"][polyLineKey];
        this.map.removeLayer( marker );
      }
      for ( var polyLineDecoratorKey in this.marker["polyLineDecorator"] ) {
        const marker = this.marker["polyLineDecorator"][polyLineDecoratorKey];
        this.map.removeLayer( marker );
      }
      this.marker["polyLine"] = [];
      this.marker["polyLineDecorator"] = [];
      delete this.markerTemp["polyLine"];
      delete this.markerTemp["polyLineDecorator"];
    }
    if ( this.marker.hasOwnProperty( "polyLineRoute" ) ) {
      for ( var polyLineRouteKey in this.marker["polyLineRoute"] ) {
        const marker = this.marker["polyLineRoute"][polyLineRouteKey];
        this.map.removeLayer( marker );
      }
      this.marker["polyLineRoute"] = [];
      delete this.markerTemp["polyLineRoute"];
      for ( var polyLineRouteDecoratorKey in this.marker["polyLineRouteDecorator"] ) {
        const marker = this.marker["polyLineRouteDecorator"][polyLineRouteDecoratorKey];
        this.map.removeLayer( marker );
      }
      this.marker["polyLineRouteDecorator"] = [];
      delete this.markerTemp["polyLineRouteDecorator"];
    }
    this.lineIsSet = false;
    this.lineRouteIsSet = false;
  }

  cleanMarker( key ) {
    this.map.addPins( [], {
      name: key,
      overwrite: true
    } );
  }

  setMapClickEvent( searchLayerName, event ) {

  }

  centerAndGoToMarkers( positions ) {

  }

  /**
   * Neuladen der Karte
   */
  mapRefresh() {
    this.map._onResize();
  }
}

export default LocationLeaFlet;
