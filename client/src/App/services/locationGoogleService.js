import React, { Component } from 'react';


class LocationGoogleService extends Component {
  subscriptionKey = "AIzaSyBLcL4wSHW_QvLSuzGsm-9uxmB1cr7XAxs";
  map = null;
  currentPositionInfoWindow = null;
  scriptIsLoad = false;
  marker = {};

  initMap( containerId, startLatitude, startLongitude, startZoom, callback, mapType ) {
    this.setScriptSource( function() {
      vm.map = new google.maps.Map( document.getElementById( containerId ), {
        center: {lat: startLatitude, lng: startLongitude},
        zoom: startZoom,
        mapTypeControl: false,
        streetViewControl: false,
        mapTypeId: (mapType !== undefined) ? mapType : google.maps.MapTypeId.ROADMAP
      } );

      if ( mapType !== undefined ) {
        google.maps.visualRefresh = true;
        switch ( mapType ) {
          case "openStreet":
            vm.map.mapTypes.set( "openStreet", new google.maps.ImageMapType(
              {
                getTileUrl: function( coord, zoom ) {
                  const tilesPerGlobe = 1 << zoom;
                  let x = coord.x % tilesPerGlobe;
                  if ( x < 0 ) {
                    x = tilesPerGlobe + x;
                  }
                  return "/azure/openstreetmap?zoom=" + zoom + "&x=" + x + "&y=" + coord.y;
                },
                tileSize: new google.maps.Size( 256, 256 ),
                name: "openStreet",
                maxZoom: 18
              } ) );
            break;
          case "bing":
            vm.map.mapTypes.set( "bing", new google.maps.ImageMapType(
              {
                getTileUrl: function( coord, z ) {
                  const xTile = coord.x;
                  const yTile = coord.y;
                  const quadKey = vm.toQuad( xTile, yTile, z );
                  return "https://t.ssl.ak.dynamic.tiles.virtualearth.net/comp/ch/" + quadKey + "?mkt=de-DE&it=A,G,RL&shading=hill&n=z&og=256&c4w=1";
                },
                tileSize: new google.maps.Size( 256, 256 ),
                name: "bing",
                maxZoom: 17,
                minZoom: 1
              } ) );
            break;
          case "azure":
            vm.map.mapTypes.set( "azure", new google.maps.ImageMapType(
              {
                getTileUrl: function( coord, z ) {
                  const xTile = coord.x;
                  const yTile = coord.y;
                  const layer = "basic"; // basic,hybrid & labels
                  const style = "main";
                  const language = "de-DE";
                  return "/azure/map/tile/png?&subscription-key=undefined&view=Unified&language=" + language + "&api-version=1.0&layer=" + layer + "&style=" + style + "&zoom=" + z + "&x=" + xTile + "&y=" + yTile;
                },
                tileSize: new google.maps.Size( 256, 256 ),
                name: "azure",
                maxZoom: 18,
                minZoom: 0
              } ) );
            break;
        }
      }
      callback();
    }, mapType );
  }

  toQuad( x, y, z ) {
    const quadKey = [];
    for ( let i = z; i > 0; i-- ) {
      let digit = "0";
      const mask = 1 << (i - 1);
      if ( (x & mask) != 0 ) {
        digit++;
      }
      if ( (y & mask) != 0 ) {
        digit++;
        digit++;
      }
      quadKey.push( digit );
    }
    if ( quadKey.join( "" ) === "" ) {
      return null;
    }
    return quadKey.join( "" );
  }

  setScriptSource( callBack, mapType ) {
    if ( !vm.scriptIsLoad ) {
      loader.loaderIn( 400, false );
      const url = "https://maps.googleapis.com/maps/api/js?key=" + vm.subscriptionKey;
      $.getScript( url, function() {
        loader.loaderOut( 400, true );
        $.getScript( "/resources/googleLocation/markerclusterer.js", function() {
          $.getScript( "/resources/googleLocation/locationGoogleServiceCustomMarker.js", function() {
            vm.scriptIsLoad = true;
            loader.loaderOut( 400, true );
            callBack();
          } );
        } );
      } );
    } else {
      callBack();
    }
  }

  setMarkers( locations, key, icon, cluster ) {
    const markers = locations.map( function( location, i ) {
      const coordinaten = {lat: location.lat, lng: location.lng};
      const markerOption = {
        position: coordinaten,
        animation: google.maps.Animation.DROP,
        label: location.title
      };
      if ( !cluster ) {
        markerOption.map = vm.map;
      }
      if ( location.title !== undefined ) {
        markerOption.title = location.title;
      }
      if ( icon !== undefined ) {
        // markerOption.icon = icon;
      }
      let marker;
      const min = 1;
      const max = 10000000000;
      switch ( icon["google"] ) {
        case "BpwRouteTargetMarker":
          marker = new BpwRouteTargetMarker( markerOption.position,
                                             vm.map,
                                             {
                                               marker_id: Math.round( Math.random() * (max - min) ) + min
                                             } );
          break;
        case "BpwTargetMarker":
          marker = new BpwTargetMarker( markerOption.position,
                                        vm.map,
                                        {
                                          marker_id: Math.round( Math.random() * (max - min) ) + min
                                        } );
          break;
        case "BpwTruckMarker":
          marker = new BpwTruckMarker( markerOption.position,
                                       vm.map,
                                       {
                                         marker_id: Math.round( Math.random() * (max - min) ) + min
                                       } );
          break;
        case "BpwPalletMarker":
          marker = new BpwPalletMarker( markerOption, markerOption.position,
                                        vm.map,
                                        {
                                          marker_id: Math.round( Math.random() * (max - min) ) + min
                                        } );
          break;
        default:
          marker = new google.maps.Marker( markerOption );
      }

      if ( location.infoWindow !== undefined ) {
        const infowindow = new google.maps.InfoWindow( {
                                                         content: location.infoWindow
                                                       } );
        marker.addListener( "click", function() {
          infowindow.open( vm.map, marker );
        } );
      }
      if ( !vm.marker.hasOwnProperty( key ) ) {
        vm.marker[key] = [];
      }
      vm.marker[key].push( marker );
      return marker;
    } );
    if ( cluster ) {
      new MarkerClusterer( vm.map, markers,
                           {imagePath: "/tracking-link-beta/resources/googleLocation/m"} );
    }
  }

  removeMarker( key ) {
    if ( vm.marker.hasOwnProperty( key ) ) {
      for ( let i = 0; i < vm.marker[key].length; i++ ) {
        const marker = vm.marker[key][i];
        marker.setMap( null );
        $( "[data-marker_id='" + marker.args.marker_id + "']" ).remove();
      }
      vm.marker[key] = [];
    }
  }

  setCurrentPosition() {
    vm.currentPositionInfoWindow = new google.maps.InfoWindow( {map: map} );
    // Try HTML5 geolocation.
    if ( navigator.geolocation ) {
      navigator.geolocation.getCurrentPosition( function( position ) {
        const pos = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };

        vm.currentPositionInfoWindow.setPosition( pos );
        vm.currentPositionInfoWindow.setContent( "Location found." );
        vm.map.setCenter( pos );
      }, function() {
        vm.handleLocationError( true, vm.currentPositionInfoWindow, vm.map.getCenter() );
      } );
    } else {
      // Browser doesn't support Geolocation
      vm.handleLocationError( false, vm.currentPositionInfoWindow, vm.map.getCenter() );
    }
  }

  setMarkerCenter( latMax, latMin, lngMax, lngMin ) {
    vm.map.setCenter( new google.maps.LatLng(
      ((latMax + latMin) / 2.0),
      ((lngMax + lngMin) / 2.0)
    ) );
    vm.map.fitBounds( new google.maps.LatLngBounds(
      // bottom left
      new google.maps.LatLng( latMin, lngMin ),
      // top right
      new google.maps.LatLng( latMax, lngMax )
    ) );
  }

  cleanMarker( key ) {
    vm.map.clearOverlays();
  }

  handleLocationError( browserHasGeolocation, infoWindow, pos ) {
    vm.currentPositionInfoWindow.setPosition( pos );
    vm.currentPositionInfoWindow.setContent( browserHasGeolocation ?
                                               "Error: The Geolocation service failed." :
                                               "Error: Your browser doesn't support geolocation." );
  }

  setMapClickEvent( searchLayerName, event ) {

  }

  centerAndGoToMarkers( positions ) {

  }
}