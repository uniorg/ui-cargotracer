import React, { Component } from 'react';


class LocationService extends Component {
  pageContent = null;
  marker = {};
  locationService = null;
  containerId = null;
  startLatitude = null;
  startLongitude = null;
  startZoom = null;
  type = null;
  mapType = null;
  latMin = 90;
  latMax = -90;
  lngMin = 180;
  lngMax = -180;


  initMap( containerId, startLatitude, startLongitude, startZoom, type, callback, mapType, options ) {
    this.containerId = containerId;
    this.startLatitude = startLatitude;
    this.startLongitude = startLongitude;
    this.startZoom = startZoom;
    this.type = type;
    this.mapType = mapType;
    this.setMapContainerHeight( containerId );
    this.setMap( this.type, callback, mapType, options );
  }

  setMap( type, callback, mapType, options ) {
    switch ( type ) {
      case "google":
        this.locationService = locationGoogle;
        this.locationService.initMap( this.containerId, this.startLatitude, this.startLongitude, this.startZoom, function() {
          callback( this );
        }, mapType, options );
        break;
      case "leaFlet":
        this.locationService = locationLeaFlet;
        this.locationService.initMap( this.containerId, this.startLatitude, this.startLongitude, this.startZoom, function() {
          callback( this );
        }, mapType, options );
        break;
    }
  }

  setLatLngMinMax( key ) {
    const that = this;
    this.latMin = 90;
    this.latMax = -90;

    this.lngMin = 180;
    this.lngMax = -180;
    if ( key !== undefined ) {
      $.each( that.marker[key]
        , function( index, marker ) {
          $.each( marker.locations
            , function( index, location ) {
              if ( that.latMax < location.lat ) {
                that.latMax = location.lat;
              }
              if ( location.lat < that.latMin ) {
                that.latMin = location.lat;
              }
              if ( that.lngMax < location.lng ) {
                that.lngMax = location.lng;
              }
              if ( location.lng < that.lngMin ) {
                that.lngMin = location.lng;
              }
            } );
        } );
    } else {
      $.each( that.marker, function( index, markers ) {
        $.each( markers
          , function( index, marker ) {
            $.each( marker.locations
              , function( index, location ) {
                if ( that.latMax < location.lat ) {
                  that.latMax = location.lat;
                }
                if ( location.lat < that.latMin ) {
                  that.latMin = location.lat;
                }
                if ( that.lngMax < location.lng ) {
                  that.lngMax = location.lng;
                }
                if ( location.lng < that.lngMin ) {
                  that.lngMin = location.lng;
                }
              } );
          } );
      } );
    }
  }

  setPolyline( locations, key, options ) {
    this.locationService.setPolyline( locations, key, options );
  }

  setMarkers( locations, key, icon, cluster, center, onMap, lineMode ) {

    if ( !this.marker.hasOwnProperty( key ) ) {
      this.marker[key] = [];
    }
    this.marker[key].push( {
                             icon: icon,
                             cluster: cluster,
                             locations: locations,
                             key: key
                           } );
    this.locationService.setMarkers( locations, key, icon, cluster, onMap, lineMode );


    if ( center === undefined || center === true ) {
      setTimeout( function() {
        $( ".bpwPalletMarker" ).addClass( "invisible" );
      }, 100 );
      setTimeout( function() {
        $( ".bpwPalletMarker" ).each( function( index, item ) {
          const position = $( item ).parent().position();
          $( this ).attr( "data-top", position.top );
        } );
      }, 800 );
      setTimeout( function() {
        $( ".bpwPalletMarker" ).parent().each( function( index, item ) {
          $( this )
            .css( {top: -1000} )
            .children().css( {"font-size": "9em"} )
            .animate( {"font-size": "1.5em"}, 1300 )
            .removeClass( "invisible" )
            .parent()
            .animate( {top: $( this ).children().attr( "data-top" )}, 1200 );
        } );
      }, 1000 );
    }
  }

  setMarkerCenter( key, withCircle ) {
    this.setLatLngMinMax( key );
    this.locationService.setMarkerCenter( this.latMax, this.latMin, this.lngMax, this.lngMin, key, withCircle );
  }

  setMapZoom( zoom ) {
    this.locationService.setMapZoom( zoom );
  }

  flyTo( latLng, zoom, options ) {
    this.locationService.flyTo( latLng, zoom, options );
  }

  removeMarker( key ) {
    if ( this.marker.hasOwnProperty( key ) ) {
      this.marker[key] = [];
    }
    this.locationService.removeMarker( key );
  }

  setMapContainerHeight( containerId ) {
    const height = $( "body" ).height() - $( ".navbar-expand-xl" ).outerHeight();
    $( "#" + containerId ).height( height );


    $( window ).resize( function() {
      const height = $( "body" ).height() - $( ".navbar-expand-xl" ).outerHeight();
      $( "#" + containerId ).height( height );
    } );
  }

  setMapClickEvent( searchLayerName, event ) {
    this.locationService.setMapClickEvent( searchLayerName, event );
  }

  centerAndGoToMarkers( positions ) {
    this.locationService.centerAndGoToMarkers( positions );
  }

  mapRefresh() {
    if ( this.locationService !== null ) {
      this.locationService.mapRefresh();
    }
  }

  mapReload( callback ) {
    const parent = $( "#" + vm.containerId ).parent();
    parent.find( "#" + vm.containerId ).remove();
    parent.append( "<div id=\"" + this.containerId + "\"></div>" );
    this.initMap( this.containerId, this.startLatitude, this.startLongitude, this.startZoom, this.type, function() {
      callback();
    }, this.mapType );
  }
}