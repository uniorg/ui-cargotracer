import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import LanguageDetector from 'i18next-browser-languagedetector';

import translationEN from '../../locales/en-EN/translation.json';
import translationDE from '../../locales/de-DE/translation.json';

// the translations
const resources = {
  "en-EN": {
    translation: translationEN
  },
  "de-DE": {
    translation: translationDE
  }
};


i18n
  .use( initReactI18next ) // passes i18n down to react-i18next
  .use( LanguageDetector )
  .init( {
           resources,
           compatibilityAPI: 'v1',
           debug: false,
           fallbackLng: ['en-EN', 'de-DE'],
           ns: ['translation', 'error'],
           lng: 'de-DE',
           backend: {
             // load from i18next-gitbook repo
             addPath: "/i18Next/locales/add/{{lng}}/{{ns}}",
             loadPath: '/i18Next/locales/resources.json?lng={{lng}}&ns={{ns}}&version=1.0.24',
             crossDomain: true,
             allowMultiLoading: true,
           },
           saveMissing: true,
           appendNamespaceToMissingKey: true,
           updateMissing: true,
           whitelist: ['en-EN', 'de-DE'],
           preload: ['en-EN', 'de-DE'],
           resStore: i18n.store,
           interpolation: {
             escapeValue: false // react already safes from xss
           }
         }, function( err, t ) {
  } );

export default i18n;
