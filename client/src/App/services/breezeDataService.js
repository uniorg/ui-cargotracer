import React, { Component } from 'react';
//import "./breezeOdataV4Adapter";
import "odatajs"
import breeze, { config, DataServiceAdapter, EntityManager, EntityQuery, MetadataStore } from "breeze-client";
import { OData4DataService } from 'breeze-odata4';
import { withAITracking } from "@microsoft/applicationinsights-react-js";
import { ai } from "../TelemetryService";


class BreezeDataService extends Component {
  serviceName = "https://services.odata.org/Experimental/OData/OData.svc";
  manager = null;
  metadataStore = null;

  constructor( props ) {
    super( props );
    console.log( "q BreezeDataService", window.q );
    this.init();
  }

  componentDidMount() {

  }

  componentDidUpdate() {

  }

  init() {
    OData4DataService.register();
    config.initializeAdapterInstance( "dataService", "OData4" );
    this.manager = new EntityManager( this.serviceName );
    //this.manager.fetchMetadata();
    this.metadataStore = MetadataStore;
  }

  getUserConfig( callback ) {
    let queryUser = new EntityQuery()
      .from( "Users" ).expand( "UserConfigs" );
    console.log( DataServiceAdapter );
    DataServiceAdapter.executeQuery( queryUser )
      .then( function( dataUser ) {
        if ( dataUser.results !== undefined && dataUser.results.length > 0 && dataUser.results[0].UserConfigs !== undefined ) {
          callback( dataUser.results[0].UserConfigs );
        }
      } ).fail( function( e ) {
      console.log( e );
    } );
  }


  getTest( callback ) {
    let queryUser = new EntityQuery()
      .from( "People" );
    this.manager.executeQuery( queryUser )
      .then( function( dataUser ) {
        callback( dataUser );
      } ).fail( function( e ) {
      console.log( e );
    } );
  }

  getLastAddresses( callback ) {
    let that = this;
    let queryUser = new breeze.EntityQuery()
      .from( "Users" ).expand( "UserConfigs" );
    that.manager.executeQuery( queryUser )
      .then( function( dataUser ) {
        if ( dataUser.results !== undefined && dataUser.results.length > 0 ) {
          let query = new breeze.EntityQuery()
            .from( "UserLastAddresses" ).expand( "Address,Address.Geofence" ).orderBy( 'updatedAt desc' ).take( dataUser.results[0].UserConfigs.lastAddressesItemsCount );
          that.manager.executeQuery( query ).then( function( dataUserLast ) {
            if ( dataUserLast.results !== undefined ) {
              callback( dataUserLast.results );
            }
          } ).fail( function( e ) {
            console.log( e );
          } );
        }

      } ).fail( function( e ) {
      console.log( e );
    } );

  }

  checkLastAddressExist( addressId, callback ) {
    let query = new breeze.EntityQuery()
      .from( "UserLastAddresses" ).where( "addressId", "eq", addressId ).expand( "Address" );
    this.manager.executeQuery( query ).then( function( data ) {
      if ( data.results !== undefined && data.results.length > 0 ) {
        let lastItem = data.results[0];
        lastItem.entityAspect.propertyChanged.subscribe( function( propertyChangedArgs ) {
          //selfDataService.saveChanges(function(){});
        } );
        callback( true, lastItem );
      } else {
        callback( false );
      }

    } ).fail( function( e ) {
      console.log( e );
      callback( false );
    } );
  }

  /**
   *
   * @param {int} addressId
   * @param {object} dataUser
   * @param {function} callback
   */
  setLastAddressGetUserCompletion( addressId, dataUser, callback ) {
    let that = this;
    if ( dataUser.results !== undefined && dataUser.results.length > 0 ) {
      that.checkLastAddressExist( addressId, function( exist, dataUserLast ) {
        if ( exist ) {
          console.log( "setLastAddress data", dataUserLast );
          dataUserLast.setProperty( "updatedAt", new Date().toISOString() );
          callback();
        } else {
          let userLastAddressesEntity = that.metadataStore.getEntityType( 'UserLastAddresses' );

          let newUserLastAddressesObject = {
            //creationUser: app.user.getUser().oid,
            //lastUpdateUser: app.user.getUser().oid,
            creationUser: 1234,
            lastUpdateUser: 1234,
            deleted: false,
            //oid: app.user.getUser().oid,
            oid: 1234,
            createdAt: new Date().toISOString(),
            updatedAt: new Date().toISOString(),
            userId: dataUser.results[0].id,
            addressId: addressId
          };
          console.log( "newUserLastAddressesObject", newUserLastAddressesObject );
          let newUserLastAddresses = userLastAddressesEntity.createEntity( newUserLastAddressesObject );

          that.manager.addEntity( newUserLastAddresses );

          callback();
        }
      } );
    } else {
      callback();
    }
  }

  /**
   *
   * @param {string} lang
   */
  setUserDefaultLanguage( lang, callback ) {
    let that = this;
    let query = new breeze.EntityQuery()
      .from( "Users" ).expand( "UserConfigs" );
    that.manager.executeQuery( query )
      .then( function( dataUser ) {
        if ( dataUser.results !== undefined && dataUser.results.length > 0 ) {
          let user = dataUser.results[0];
          user.UserConfigs.setProperty( "defaultLanguage", lang );
          that.saveChanges( callback );
        }
      } )
      .fail( function( e ) {
        console.log( e );
      } );
  }

  /**
   *
   * @param {int} addressId
   * @param {function} callback
   */
  setLastAddress( addressId, callback ) {
    let that = this;
    let query = new breeze.EntityQuery()
      .from( "Users" ).expand( "UserConfigs,UserLastAddresses,UserLastAddresses.Address" );
    that.manager.executeQuery( query )
      .then( function( dataUser ) {
        that.setLastAddressGetUserCompletion( addressId, dataUser, callback );
      } )
      .fail( function( e ) {
        console.log( e );
      } );
  }

  saveChanges( callback ) {
    let that = this;
    console.log( "selfDataService saveChanges", this.manager.hasChanges() );
    if ( that.manager.hasChanges() ) {
      that.manager.saveChanges()
        .then( function( data ) {
          callback( data );
        } )
        .fail( function( data ) {
          that.manager.getEntities().forEach( function( entity ) {
            var errors = entity.entityAspect.getValidationErrors();
            console.log( errors );
          } );
          console.log( "fail", data );
        } )
        .fin( function( data ) {
          console.log( "fin", data );
        } );
    } else {
      callback( null );
    }
  }
}

export default withAITracking( window.reactPlugin, BreezeDataService, "BreezeDataService" )