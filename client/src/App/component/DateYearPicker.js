// https://www.jqueryscript.net/time-clock/Date-Time-Picker-Bootstrap-4.html
import React, { Component } from 'react'
import $ from "jquery";
import 'pc-bootstrap4-datetimepicker';
import 'pc-bootstrap4-datetimepicker/build/css/bootstrap-datetimepicker.min.css';
import moment from 'moment';

class DateYearPicker extends Component {
  static defaultProps = {
    elementId: "DateRangePicker",
    options: {
      timeZone: '',
      format: 'YYYY',
      dayViewHeaderFormat: 'YYYY',
      extraFormats: false,
      stepping: 1,
      minDate: false,
      maxDate: false,
      useCurrent: false,
      collapse: true,
      locale: moment.locale(),
      defaultDate: false,
      disabledDates: false,
      enabledDates: false,
      icons: {
        time: 'fal fa-clock',
        date: 'fal fa-calendar-alt',
        up: 'fal fa-chevron-up',
        down: 'fal fa-chevron-down',
        previous: 'fal fa-chevron-left',
        next: 'fal fa-chevron-right',
        today: 'fal fa-screenshot',
        clear: 'fal fa-trash'
      },
      tooltips: {
        today: 'Go to today',
        clear: 'Clear selection',
        close: 'Close the picker',
        selectMonth: 'Select Month',
        prevMonth: 'Previous Month',
        nextMonth: 'Next Month',
        selectYear: 'Select Year',
        prevYear: 'Previous Year',
        nextYear: 'Next Year',
        selectDecade: 'Select Decade',
        prevDecade: 'Previous Decade',
        nextDecade: 'Next Decade',
        prevCentury: 'Previous Century',
        nextCentury: 'Next Century',
        pickHour: 'Pick Hour',
        incrementHour: 'Increment Hour',
        decrementHour: 'Decrement Hour',
        pickMinute: 'Pick Minute',
        incrementMinute: 'Increment Minute',
        decrementMinute: 'Decrement Minute',
        pickSecond: 'Pick Second',
        incrementSecond: 'Increment Second',
        decrementSecond: 'Decrement Second',
        togglePeriod: 'Toggle Period',
        selectTime: 'Select Time'
      },
      useStrict: false,
      sideBySide: false,
      daysOfWeekDisabled: false,
      calendarWeeks: false,
      viewMode: 'years',
      toolbarPlacement: 'default',
      showTodayButton: false,
      showClear: true,
      showClose: false,
      widgetPositioning: {
        horizontal: 'auto',
        vertical: 'auto'
      },
      widgetParent: null,
      ignoreReadonly: true,
      keepOpen: false,
      focusOnShow: true,
      inline: false,
      keepInvalid: false,
      datepickerInput: '.datepickerinput',
      keyBinds: {
        up: function( widget ) {
          if ( !widget ) {
            return;
          }
          var d = this.date() || this.getMoment();
          if ( widget.find( '.datepicker' ).is( ':visible' ) ) {
            this.date( d.clone().subtract( 7, 'd' ) );
          } else {
            this.date( d.clone().add( this.stepping(), 'm' ) );
          }
        },
        down: function( widget ) {
          if ( !widget ) {
            this.show();
            return;
          }
          var d = this.date() || this.getMoment();
          if ( widget.find( '.datepicker' ).is( ':visible' ) ) {
            this.date( d.clone().add( 7, 'd' ) );
          } else {
            this.date( d.clone().subtract( this.stepping(), 'm' ) );
          }
        },
        'control up': function( widget ) {
          if ( !widget ) {
            return;
          }
          var d = this.date() || this.getMoment();
          if ( widget.find( '.datepicker' ).is( ':visible' ) ) {
            this.date( d.clone().subtract( 1, 'y' ) );
          } else {
            this.date( d.clone().add( 1, 'h' ) );
          }
        },
        'control down': function( widget ) {
          if ( !widget ) {
            return;
          }
          var d = this.date() || this.getMoment();
          if ( widget.find( '.datepicker' ).is( ':visible' ) ) {
            this.date( d.clone().add( 1, 'y' ) );
          } else {
            this.date( d.clone().subtract( 1, 'h' ) );
          }
        },
        left: function( widget ) {
          if ( !widget ) {
            return;
          }
          var d = this.date() || this.getMoment();
          if ( widget.find( '.datepicker' ).is( ':visible' ) ) {
            this.date( d.clone().subtract( 1, 'd' ) );
          }
        },
        right: function( widget ) {
          if ( !widget ) {
            return;
          }
          var d = this.date() || this.getMoment();
          if ( widget.find( '.datepicker' ).is( ':visible' ) ) {
            this.date( d.clone().add( 1, 'd' ) );
          }
        },
        pageUp: function( widget ) {
          if ( !widget ) {
            return;
          }
          var d = this.date() || this.getMoment();
          if ( widget.find( '.datepicker' ).is( ':visible' ) ) {
            this.date( d.clone().subtract( 1, 'M' ) );
          }
        },
        pageDown: function( widget ) {
          if ( !widget ) {
            return;
          }
          var d = this.date() || this.getMoment();
          if ( widget.find( '.datepicker' ).is( ':visible' ) ) {
            this.date( d.clone().add( 1, 'M' ) );
          }
        },
        enter: function() {
          this.hide();
        },
        escape: function() {
          this.hide();
        },
        //tab: function (widget) { //this break the flow of the form. disabling for now
        //    var toggle = widget.find('.picker-switch a[data-action="togglePicker"]');
        //    if(toggle.length > 0) toggle.click();
        //},
        'control space': function( widget ) {
          if ( !widget ) {
            return;
          }
          if ( widget.find( '.timepicker' ).is( ':visible' ) ) {
            widget.find( '.btn[data-action="togglePeriod"]' ).click();
          }
        },
        t: function() {
          this.date( this.getMoment() );
        },
        'delete': function() {
          this.clear();
        }
      },
      debug: false,
      allowInputToggle: false,
      disabledTimeIntervals: false,
      disabledHours: false,
      enabledHours: false,
      viewDate: false
    }

  }

  constructor( props ) {
    super( props );
  }


  componentDidMount() {
    let {options, onChange, elementId, defaultValue} = this.props

    if (defaultValue !== undefined) {
      options.defaultDate = moment(new Date(defaultValue))
    }
    $( "#" + elementId )
      .datetimepicker( options )
      .on( "dp.change", function() {
        if ( onChange !== undefined ) {
          onChange( $( "#" + elementId ).val() )
        }
      } );
  }

  render() {
    // eslint-disable-next-line react/prop-types
    let {elementId} = this.props
    return (
      <div className={"date-time-picker"}>
        <input type="text" id={elementId} className="form-control form-control-lg cursor-pointer" readOnly={true} placeholder={"Jahr auswählen"}/>
      </div>
    );
  }

}

export default DateYearPicker