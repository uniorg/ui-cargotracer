import React, { Component } from 'react';
import { Col, Form } from "react-bootstrap";
import { withTranslation } from 'react-i18next';
import Select, { components } from 'react-select';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";


class DeliveryTrackerInputCheckDelivery extends Component {

  _isMounted = false;

  constructor( ...args ) {
    super( ...args );

    this.state = {
      isInvalid: undefined,
      isValid: undefined,
      isLoading: false,
      className: "form-control-lg",
      options: []
    };
  }

  componentDidMount() {
    this._isMounted = true;
    this.fetchSelectOptions()
  }



  componentWillUnmount() {
    this._isMounted = false;
  }

  handleChangeInput( e ) {
    const {handleValidate} = this.props;
    if ( e === undefined || e === null ) {
      if ( handleValidate !== undefined )
        this.setState( {isInvalid: undefined, isValid: undefined, isLoading: false} )
        handleValidate( null, false )
      return
    }
    let {value} = e
    if ( value === null || value === undefined || value === "" ) {
      this.setState( {isInvalid: undefined, isValid: undefined, isLoading: false} )
      return
    }
    this.fetchInputValue( value, ( id, data ) => {
      let {hasDelivery} = data
      if ( handleValidate !== undefined )
        handleValidate( id, ((hasDelivery) ? false : true) )
      this.setState( {isInvalid: hasDelivery, isValid: ((hasDelivery) ? false : true), isLoading: false} )
    } )
  }

  handleClearInput() {
    this.setState( {isInvalid: undefined, isValid: undefined, isLoading: false} )
  }

  fetchInputValue( value, callback ) {
    this.setState( {isLoading: true} );
    fetch( '/delivery-ui-service/delivery/assignedDelivery/' + value )
      .then( res => res.json() )
      .then( ( data ) => {
        if ( data.hasOwnProperty( "hasDelivery" ) ) {
          callback( value, data )
        } else {
          callback( null, false )
        }
      } )
      .catch( ( error ) => {
        console.error( error );
        callback( null, false )
      } )
  }

  fetchSelectOptions() {
    this.setState( {options: [], isLoading: true} );
    fetch( '/device-ui-service/devices' )
      .then( res => res.json() )
      .then( ( data ) => {
        if ( data.hasOwnProperty( "value" ) ) {
          return data.value
        } else {
          return []
        }
      } )
      .then( ( values ) => {
        if (this._isMounted) {
          let mapValues = values.map( ( value ) => {
            return {value: value.deviceId, label: value.name}
          } );
          this.setState( {options: mapValues, isLoading: false} );
        }
      } )
      .catch( ( error ) => {
        console.error( error );
      } )
  }

  render() {
    const {t} = this.props;
    const {isLoading, options, isValid} = this.state;
    const ValueContainer = ({ children, ...props }) => {
      return (
        components.ValueContainer && (
          <components.ValueContainer {...props}>
            {!!children && (
              <FontAwesomeIcon icon={['fal', 'search']} style={{ position: 'absolute', left: 6 }}/>
            )}
            {children}
          </components.ValueContainer>
        )
      );
    };

    const ClearIndicator = props => {
      return (
        components.ClearIndicator && (
          <components.ClearIndicator {...props}>
            <FontAwesomeIcon icon={['fal', 'times']}/>
          </components.ClearIndicator>
        )
      );
    };
    const DropdownIndicator = props => {
      return (
        components.DropdownIndicator && (
          <components.DropdownIndicator {...props}>
            <FontAwesomeIcon icon={['fal', 'chevron-down']}/>
          </components.DropdownIndicator>
        )
      );
    };

    const LoadingIndicator = props => {
      return (
        components.LoadingIndicator && (
          <components.DropdownIndicator {...props}>
            <FontAwesomeIcon icon={['fal', 'spinner']} spin/>
          </components.DropdownIndicator>
        )
      );
    };

    const styles = {
      valueContainer: base => ({
        ...base,
        paddingLeft: 24
      })
    }

    return (
      <Form.Group as={Col} md={"6"} controlId="formGridTracker" className={"delivery-tracker-input-check-delivery"}>
        {isLoading ? <FontAwesomeIcon icon={['fal', 'spinner']} size="lg" spin/> : null}<Form.Label>{t( 'sites.Delivery.deliveryCreate.assignTrackers' )}</Form.Label>
        <Select
          className={"form-control-lg " + ((isValid !== undefined) ? (isValid ? "form-control--select-valid" : "form-control--select-invalid") : "") + (isLoading ? " alert alert-warning" : "")}
          classNamePrefix="select"
          defaultValue={options[0]}
          isDisabled={false}
          isLoading={isLoading}
          isClearable={true}
          isRtl={false}
          isSearchable={true}
          name="color"
          options={options}
          components={{ DropdownIndicator, ValueContainer, ClearIndicator, LoadingIndicator }}
          styles={styles}
          placeholder={t( 'sites.Delivery.deliveryCreate.selectTracker' )}
          required="required"
          onChange={this.handleChangeInput.bind( this )}
          clear={this.handleClearInput.bind( this )}
        />
        <div className={"invalid-feedback" + ((isValid !== undefined) ? (isValid ? " hide" : " show") : "")}>{t( 'sites.Delivery.deliveryCreate.errorMessageDeviceExist' )}</div>
      </Form.Group>
    );
  }
}

export default DeliveryTrackerInputCheckDelivery