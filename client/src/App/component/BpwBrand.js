import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'

import logoSvg from '../svg/bpw/logo.svg'
import logoTextSvg from '../svg/bpw/logoFont.svg'

class BpwBrand extends Component {
  render() {
    // eslint-disable-next-line react/prop-types
    const {t} = this.props
    return (
      <NavLink className='navbar-brand' to={'/'}>
        <img className='pl-2 bpw-logo-image' alt={t( 'component.BpwBrand.LogoAlt' )} src={logoSvg}/>
        <img alt={t( 'component.BpwBrand.LogoTextAlt' )} className='d-sm-inline bpw-logo-image' src={logoTextSvg}/>
      </NavLink>
    )
  }
}

export default BpwBrand
