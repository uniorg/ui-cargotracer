import React, { Component } from 'react';
import { Col, Form, FormControl } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

class DeliveryInputCheckExist extends Component {
  timeOut = null

  constructor( ...args ) {
    super( ...args );
    this.state = {
      isLoading: false,
      isInvalid: null,
      isValid: null,
      isEmpty: true
    };
  }

  async handleChangeInput( e ) {
    let value = e.target.value
    const {handleValidate} = this.props;
    if ( value === undefined || value === null || value === "" ) {
      if ( handleValidate !== undefined )
        handleValidate( null, true )
      this.changeValid( undefined, undefined )
      this.setState( {isEmpty: true, isLoading: false} )
      return
    }
    this.changeValid( false, false )
    this.setState( {isEmpty: false} )


    if ( this.timeOut !== null )
      window.clearTimeout( this.timeOut );
    await this.fetchInputValue( value, ( id, isExist ) => {

      let {isEmpty} = this.state
      if ( isEmpty ) {
        if ( handleValidate !== undefined )
          handleValidate( null, true )
        this.changeValid( null, null )
        this.setState( {isLoading: false} )
      } else {
        if ( handleValidate !== undefined )
          handleValidate( id, isExist )
        this.changeValid( isExist, ((!isExist)) )
        this.setState( {isLoading: false} )
      }
    } )
    //handleValidate( e )
  }

  changeValid( isInvalid, isValid ) {
    const {changeValid} = this.props;

    if ( changeValid !== undefined ) {
      changeValid( isInvalid, isValid )
      return
    }

    this.setState( {isInvalid: isInvalid, isValid: isValid} )
  }

  getValid() {
    const {deliveryInputValid} = this.props;
    const {isValid} = this.state;

    if ( deliveryInputValid !== undefined )
      return deliveryInputValid

    return isValid

  }

  getInvalid() {
    const {deliveryInputInvalid} = this.props;
    const {isInvalid} = this.state;

    if ( deliveryInputInvalid !== undefined )
      return deliveryInputInvalid

    return isInvalid
  }

  fetchInputValue( value, callback ) {
    this.timeOut = window.setTimeout( () => {
      this.setState( {isLoading: true} )
      fetch( '/delivery-ui-service/delivery/checkIsExist/' + value )
        .then( res => res.json() )
        .then( ( data ) => {
          if ( data.hasOwnProperty( "value" ) ) {
            return data.value
          } else {
            return false
          }
        } )
        .then( ( isExist ) => {
          callback( value, isExist )
        } )
        .catch( ( error ) => {
          console.error( error );
          //ai.appInsights.trackException(error)
          callback( null, false )
        } )
    }, 2500 )
  }


  render() {
    const {t} = this.props;
    const {isLoading} = this.state;
    return (
      <Form.Group as={Col} md={"6"} controlId="formGridTracker" className={"delivery-tracker-input-check-delivery"}>
        {isLoading ? <FontAwesomeIcon icon={['fal', 'spinner']} size="lg" spin/> : null}<Form.Label>{t( 'sites.Delivery.deliveryCreate.deliverynumber' )}</Form.Label>
        <div className="mb-3">
          <FormControl
            type="text"
            min="1"
            required="required"
            aria-required="true"
            size={"lg"}
            placeholder={t( 'sites.Delivery.deliveryCreate.enterdeliverynumber' )}
            aria-label={t( 'sites.Delivery.deliveryCreate.deliverynumber' )}
            aria-describedby="hashtag"
            onChange={e => this.handleChangeInput( e )}
            isInvalid={this.getInvalid()}
            isValid={this.getValid()}
            disabled={isLoading}
            className={isLoading ? "alert-warning" : null}
          />
          <Form.Control.Feedback type="invalid">
            {t( 'sites.Delivery.deliveryCreate.errorMessage' )}
          </Form.Control.Feedback>
        </div>
      </Form.Group>
    );
  }
}

export default DeliveryInputCheckExist