import React, { Component } from 'react';
import { Accordion, Button, ButtonGroup, Card, Col, Row, ToggleButton } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Form from "react-bootstrap/es/Form";
import Select from 'react-select';

class ModuleSort extends Component {
  static defaultProps = {
    accordionId: "sortAccordion"
  }

  constructor( props, context ) {
    super( props, context );
    this.state = {
      sortKey: undefined,
      sortDirection: "desc"
    };
  }

  handleSortResult() {
    let {sortKey, sortDirection} = this.state
    let {handleSortResult} = this.props

    if ( handleSortResult !== undefined )
      handleSortResult( sortKey, sortDirection )
  }

  handleResetSortResult() {
    let {handleOnClickRemoveSort} = this.props

    if ( handleOnClickRemoveSort !== undefined )
      handleOnClickRemoveSort()
  }

  handleChangeSelect( item ) {
    let {handleChangeSelect} = this.props
    this.setState( {sortKey: item} )
    if ( handleChangeSelect !== undefined )
      handleChangeSelect( item )
  }

  handChangeDirection( item ) {
    let {handleSortDirectionChange} = this.props
    this.setState( {sortDirection: item} )
    if ( handleSortDirectionChange !== undefined )
      handleSortDirectionChange( item )
  }

  render() {
    let {t, accordionId, sortOptions, defaultSortItem} = this.props
    console.log(defaultSortItem)
    return (
      <Accordion defaultActiveKey="0" id={accordionId} className="mt-4 mb-5">
        <Card className={"card--filter card__border-radius"}>
          <Accordion.Toggle as={Card.Header} variant="link"
                            className="card-header--transparent border-bottom-0 pb-2"
                            eventKey="0"
                            data-toggle="collapse"
                            data-target="#sortCollapse"
                            aria-expanded="true"
                            aria-controls="0">
            <div className="d-flex">
              <div className="pt-2 pr-2">
                <FontAwesomeIcon icon={['fal', 'sort-amount-down']} size="lg"/>
              </div>
              <div className="pt-1 pl-1 mb-0">
                <h3>
                  {t( 'sites.Asset.sortResults' )}
                </h3>
              </div>
              <div className="ml-auto pt-2">
                <FontAwesomeIcon icon={['fal', 'chevron-down']} size="lg"/>
              </div>
            </div>
          </Accordion.Toggle>
          <Accordion.Collapse eventKey="0"
                              id="sortCollapse"
                              className="collapse show"
                              aria-labelledby="1"
                              data-parent={"#" + accordionId}>
            <Card.Body className="pt-2">
              <Row>
                <Col md={"12"}>
                  <Form.Row>
                    <Form.Group as={Col} md={"6"} controlId="formGridAssetType">
                      <Form.Label>{t( 'sites.Asset.sortBy' )}</Form.Label>
                      <Select
                        className={"form-control"}
                        value={((defaultSortItem !== null) ? defaultSortItem : sortOptions[0])}
                        defaultValue={((defaultSortItem !== null) ? defaultSortItem : sortOptions[0])}
                        isClearable
                        name="sortKey"
                        onChange={this.handleChangeSelect.bind( this )}
                        options={sortOptions}
                      />
                    </Form.Group>
                    <Form.Group as={Col} md={"6"} controlId="formGridAddress1" className="d-flex align-self-end flex-fill">
                      <ButtonGroup toggle className="flex-grow-1" size={"md"}>
                        <ToggleButton type="radio" name="radio" variant={((this.props.defaultSortDirection === "asc") ? "primary" : "outline-primary")} className="" onClick={() => {
                          this.handChangeDirection( "asc" )
                        }}>
                          {t( 'sites.Asset.ascending' )}
                        </ToggleButton>
                        <ToggleButton type="radio" name="radio" variant={((this.props.defaultSortDirection === "desc") ? "primary" : "outline-primary")} className="" onClick={() => {
                          this.handChangeDirection( "desc" )
                        }}>
                          {t( 'sites.Asset.descending' )}
                        </ToggleButton>
                      </ButtonGroup>
                    </Form.Group>
                  </Form.Row>
                </Col>
              </Row>
              <Row className="mb-1">
                <Col>
                  <div className="d-flex flex-column flex-md-row flex-lg-row">
                    <div className="p-1 flex-fill">
                      <Button size={"md"} variant="light" type="submit" block onClick={this.handleResetSortResult.bind( this )}>{t( 'sites.Asset.resetSorting' )}</Button>
                    </div>
                    <div className="p-1 flex-fill">
                      <Button size={"md"} variant="primary" type="submit" block onClick={this.handleSortResult.bind( this )}>{t( 'sites.Asset.sortResults' )}</Button>
                    </div>
                  </div>
                </Col>
              </Row>
            </Card.Body>
          </Accordion.Collapse>
        </Card>
      </Accordion>
    );
  }
}

export default ModuleSort