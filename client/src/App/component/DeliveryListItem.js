import React, { Component } from 'react';
import { Button, Card, Col, Row } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Link } from "react-router-dom";
import Moment from "react-moment";
import moment from 'moment';
import 'moment/locale/en-gb';
import 'moment/locale/de';

moment().locale( navigator.language )

class DeliveryListItem extends Component {

  render() {
    let {item, t, url} = this.props

    return (
      <div className={"col-12 col-md-6 mb-4"}>
        <Card>
          <Card.Body>
            <div className="d-flex mb-3">
              <div className="pt-2 bd-highlight">
                <FontAwesomeIcon icon={['fal', 'clipboard-list']} size="lg" className="text-primary"/>
              </div>
              <div className="p-2">

                <Link to={`${url}/` + item.id}>
                  <Card.Title>{item.deliveryNumber}</Card.Title>
                </Link>
              </div>

              {/*<div className="ml-auto p-2">
                <EtaPopver item={item} isLate={item.isLate}/>
              </div>*/}
              {item.vibrationCount > 0 ?
                (
                  <div className="ml-auto p-2">
                    <FontAwesomeIcon icon={['fal', 'exclamation-triangle']} size="lg" color={"red"}/>
                  </div>
                ) : ''}
            </div>
            <Row className="mb-2">
              <Col className="col-md-6">
                <Row>
                  <Col>
                    <h6 className="text-muted">
                      {t( 'sites.Delivery.deliveryItem.deliveryPositionsLabel' )}
                    </h6>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <h5 className="display-5">
                      {item.itemCount &&
                       item.itemCount
                      }
                    </h5>
                  </Col>
                </Row>
              </Col>
              <Col className="col-md-6">
                <Row>
                  <Col>
                    <h6 className="text-muted">
                      {t( 'sites.Delivery.deliveryItem.scheduledArrivalItemLabel' )}
                    </h6>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <h5 className="display-6">
                      {item.plannedArrivalTimeTo &&
                       <Moment date={new Date( item.plannedArrivalTimeTo )} format="LT"/>
                      }
                    </h5>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <h6 className="">
                      {item.plannedArrivalTimeTo &&
                       <Moment date={new Date( item.plannedArrivalTimeTo )} format="L"/>
                      }
                    </h6>
                  </Col>
                </Row>
              </Col>
            </Row>
            <Row className="mb-2">
              <Col className="col-md-6">
                <Row>
                  <Col>
                    <h6 className="text-muted">
                      {t( 'sites.Delivery.deliveryItem.deliveryPackagesLabel' )}
                    </h6>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <h5 className="display-5">
                      {item.handlingUnitCount &&
                       item.handlingUnitCount
                      }
                    </h5>
                  </Col>
                </Row>
              </Col>
              <Col className="col-md-6">
                <Row>
                  <Col>
                    <h6 className="text-muted">
                      {t( 'sites.Delivery.deliveryItem.etaItemLabel' )}
                    </h6>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <h5 className="display-6">
                      {item.etaArrival &&
                       <Moment date={new Date( item.etaArrival )} format="LT"/>
                      }
                    </h5>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <h6 className="">
                      {item.etaArrival &&
                       <Moment date={new Date( item.etaArrival )} format="L"/>
                      }
                    </h6>
                  </Col>
                </Row>
              </Col>
            </Row>
            <Link to={`${url}/` + item.id}>
              <Button variant="primary" block>
                {t( 'sites.Delivery.deliveryItem.goToDetail' )}</Button>
            </Link>
          </Card.Body>
        </Card>
      </div>
    );
  }
}

export default DeliveryListItem