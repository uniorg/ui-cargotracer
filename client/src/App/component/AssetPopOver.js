import React, { Component } from 'react';
import { ButtonToolbar, Overlay, OverlayTrigger, Popover, Tooltip } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

class AssetPopOver extends Component {
  constructor( props, context ) {
    super( props, context );

    this.handleClick = ( {target} ) => {
      this.setState( s => ({target, show: !s.show}) );
    };

    this.state = {
      show: false,
    };
  }

  render() {
    let {deviceId, isTracked, t} = this.props
    return (
      <ButtonToolbar className={"asset-popover pt-1"}>
        <OverlayTrigger overlay={<Tooltip id="tooltip-asset">Tracker Identifikationsnummer</Tooltip>}>
          <span className="d-inline-block" style={{marginTop: "-8px"}}>
            <FontAwesomeIcon icon={['fal', 'bookmark']}
                             className={"cursor-pointer" + ((isTracked) ? " text-success" : "")}
                             size="lg"
                             style={{fontSize: '19px'}}
                             onClick={this.handleClick}
            />
          </span>
        </OverlayTrigger>
        <Overlay
          show={this.state.show}
          target={this.state.target}
          placement="auto"
          container={this}
          containerPadding={20}
          rootCloseEvent="mousedown"
        >
          <Popover className="asset-popover-contained" title="Tracker ID">
            {isTracked &&
             <>
               <p>{t("component.AssetPopOver.isTracked")}</p>
               <h2 className={"display-5 text-center"}>{deviceId}</h2>
             </>
            }
            {!isTracked &&
             <>
               <p>{t("component.AssetPopOver.isNotTracked")}</p>
             </>
            }
          </Popover>
        </Overlay>
      </ButtonToolbar>
    );
  }
}

export default AssetPopOver