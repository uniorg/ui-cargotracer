import React, { Component } from 'react';
import { ButtonToolbar, Overlay, OverlayTrigger, Popover, Tooltip } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import moment from 'moment';

class EtaPopOver extends Component {
  constructor( props, context ) {
    super( props, context );

    this.handleClick = ( {target} ) => {
      this.setState( s => ({target, show: !s.show}) );
    };

    this.state = {
      show: false,
    };
  }

  render() {
    let {isLate, t} = this.props
    let end = moment( new Date( this.props.item.etaArrival ) )
    let duration = moment.duration( end.diff( new Date( this.props.item.plannedArrivalTimeTo ) ) );
    let hours = Number( duration.asHours() ).toFixed( 0 );
    let min = Number( duration.asMinutes() ).toFixed( 0 );
    return (
      <ButtonToolbar className={"eta-popover"}>
        <OverlayTrigger overlay={<Tooltip id="tooltip-eta">Abweichungen ETA!</Tooltip>}>
          <span className="d-inline-block">
            <FontAwesomeIcon icon={['fal', 'clock']}
                             className={"cursor-pointer"}
                             size="lg"
                             onClick={this.handleClick}
                             color={isLate ? "red" : "green"}
            />
          </span>
        </OverlayTrigger>
        <Overlay
          show={this.state.show}
          target={this.state.target}
          placement="auto"
          container={this}
          containerPadding={20}
        >
          <Popover className="eta-popover-contained" title="Abweichungen ETA">
            { isLate &&
              <>
                <p>{t('component.EtaPopOver.isLate')}</p>
                <h2 className={"display-5 text-center"}>{hours}:{min}</h2>
              </>
            }
            { !isLate &&
              <>
                <p>{t('component.EtaPopOver.isNotLate')}</p>
                <h2 className={"display-5 text-center"}>{hours}:{min}</h2>
              </>
            }
          </Popover>
        </Overlay>
      </ButtonToolbar>
    );
  }
}

export default EtaPopOver