import React, { Component } from 'react'
import { Button, Col, Row, Table } from 'react-bootstrap'
//import { Button, Col, Row, Table, Accordion, Card, Form, ButtonGroup, ToggleButton, ButtonToolbar } from 'react-bootstrap'
import moment from "moment";
import BpwLoader from "./BpwLoader";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Helper } from "../util/Helper";
import SortIcon from "./SortIcon";
import Loader from "react-loader-spinner";

class TabCycles extends Component {

  _isMounted = false;
  static defaultProps = {
    item: null,
  }

  constructor( props ) {
    super( props );
    this.state = {
      cycles: [],
      lastCycles: [],
      hasMoreItems: true,
      sortDirection: "desc",
      sortKey: "cycleNumber",
      skip: 0,
      top: 10,
      loaded: false
    };

  }

  fetchCycles() {
    const {sortKey, sortDirection, cycles, skip, top} = this.state;
    let {item} = this.props

    if ( item === null )
      return

    if ( this._isMounted )
      this.setState( {loaded: true} )
    fetch( '/asset-ui-service/asset/assetCycleList/' + item.id + '/' + sortKey + '/' + sortDirection + '/' + skip + '/' + top )
      .then( res => res.json() )
      .then( ( data ) => {
        return data.value !== undefined ? data.value : []
      } )
      .then( ( newCycles ) => {

        if ( this._isMounted ) {
          if ( newCycles.length !== 0 ) {
            let returnList = cycles

            returnList = returnList.concat( newCycles )
            this.setState( {
                             cycles: returnList,
                             lastCycles:newCycles,
                             loaded: false,
                             hasMoreItems: true
                           } );
            let that = this
            setTimeout( () => {
              if ( that.state.lastCycles.length < top ) {
                that.setState( {
                                 hasMoreItems: false
                               } );
              }
            }, 500 )
          } else {
            this.setState( {
                             hasMoreItems: false
                           } );
          }
        }
      } )
  }

  componentDidUpdate() {
    console.log( "componentDidUpdate TabCycles" )
  }

  componentDidMount() {
    this._isMounted = true;
    console.log( "componentDidMount TabCycles" )
    this.fetchCycles()
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  async handleChangeSort( sortDirection, newSortKey ) {
    const {sortKey} = this.state;

    if ( newSortKey !== sortKey ) {
      sortDirection = "desc"
    } else {
      sortDirection = (sortDirection === "asc") ? "desc" : "asc"
    }

    await this.setState( {skip:0, hasMoreItems: false, cycles: [], sortKey: newSortKey, sortDirection: sortDirection} )
    this.fetchCycles()
  }

  getTimeFromSec(secounds) {
    let days = secounds / (60 * 60 * 24)
    let daysSec = days * 60 * 60 * 24
    let hours = (secounds - daysSec) / (60 * 60)
    let hoursSec = hours * 60 * 60
    let minutes = (secounds - daysSec - hoursSec) / 60

    return {
      days: days,
      hours: hours,
      minutes: minutes
    }
  }

  render() {
    const {t, selectCycle} = this.props;
    const {cycles, sortKey, sortDirection, hasMoreItems, loaded} = this.state;
    if ( cycles === null )
      return <BpwLoader/>
    return (
      <>
        {/* <Accordion defaultActiveKey="0" id={"accordion"} className="mt-4 mb-5">
                      <Card className={"card--filter card__border-radius"}>
                        <Accordion.Toggle as={Card.Header} variant="link"
                                          className="card-header--transparent border-bottom-0 pb-2"
                                          eventKey="0"
                                          data-toggle="collapse"
                                          data-target="#filterCollapse"
                                          aria-expanded="true"
                                          aria-controls="0">
                          <div className="d-flex">
                            <div className="pt-2 pr-2">
                              <FontAwesomeIcon icon={['fal', 'filter']} size="lg"/>
                            </div>
                            <div className="pt-1 pl-1 mb-0">
                              <h3>
                                {t( 'component.ModuleFilter.filterPanelTitle' )}
                              </h3>
                            </div>
                            <div className="ml-auto pt-2">
                              <FontAwesomeIcon icon={['fal', 'chevron-down']} size="lg"/>
                            </div>
                          </div>
                        </Accordion.Toggle>
                        <Accordion.Collapse eventKey="0"
                                            id="filterCollapse"
                                            className="collapse show"
                                            aria-labelledby="1"
                                            data-parent="#accordion">
                          <Card.Body className="pt-2">
                            <Row>
                              <Col>
                                <Form.Group controlId="formGridAddress1">
                                  <Form.Label>{t( 'sites.Asset.AssetDetail.grouping' )}</Form.Label>
                                  <div className="d-flex flex-column">
                                    <ButtonGroup toggle className="" size={"md"} block>
                                      <ToggleButton type="radio" name="radio" defaultChecked value="1" variant="primary">
                                        {t( 'sites.Asset.AssetDetail.homeAddresses' )}
                                      </ToggleButton>
                                      <ToggleButton type="radio" name="radio" value="2" variant="outline-primary">
                                        {t( 'sites.Asset.AssetDetail.pointOfInterest' )}
                                      </ToggleButton>
                                    </ButtonGroup>
                                  </div>
                                </Form.Group>
                              </Col>
                            </Row>
                            <Row>
                              <ButtonToolbar>
                                <Col md={"4"}>
                                  <Button variant="outline-primary" size="md" block>
                                    BPW-Hungária Kft. <br/><span className="text-muted">Körmendi út 98. 9700 Szombathely</span>
                                  </Button>
                                </Col>
                                <Col md={"4"}>
                                  <Button variant="primary" size="md" block>
                                    BPW Achsen KG <br/><span className="text-muted">Ohlerehmer 20 51674 Köln</span>
                                  </Button>
                                </Col>
                                <Col md={"4"}>
                                  <Button variant="outline-primary" size="md" block>
                                    BPW Kft. <br/><span className="text-muted"> Körmendi út 98. 9700 Szombathely</span>
                                  </Button>
                                </Col>
                              </ButtonToolbar>
                            </Row>
                          </Card.Body>
                        </Accordion.Collapse>
                      </Card>
                    </Accordion>*/}
        <Row>
          <Col>
            <h5 className={""}>{t( 'sites.Asset.AssetDetail.overviewRevolutions' )}:</h5>
          </Col>
        </Row>
        <Row>
          <Col>
            <Table className={"table--asset"} responsive borderless hover>
              <thead>
              <tr>
                <th style={{
                  whiteSpace: "nowrap"}} onClick={() => {
                  this.handleChangeSort( sortDirection, "cycleNumber" )
                }}>{t( 'sites.Asset.AssetDetail.nr' )} <SortIcon show={(sortKey === "cycleNumber")} sortDirection={sortDirection}/></th>
                <th style={{
                  whiteSpace: "nowrap"}}  onClick={() => {
                  this.handleChangeSort( sortDirection, "cycleState" )
                }}>{t( 'sites.Asset.AssetDetail.status' )} <SortIcon show={(sortKey === "cycleState")} sortDirection={sortDirection}/></th>
                <th style={{
                  whiteSpace: "nowrap"}}  onClick={() => {
                  this.handleChangeSort( sortDirection, "startedAt" )
                }}>{t( 'sites.Asset.AssetDetail.start' )} <SortIcon show={(sortKey === "startedAt")} sortDirection={sortDirection}/></th>
                <th style={{
                  whiteSpace: "nowrap"}}  onClick={() => {
                  this.handleChangeSort( sortDirection, "finishedAt" )
                }}>{t( 'sites.Asset.AssetDetail.end' )} <SortIcon show={(sortKey === "finishedAt")} sortDirection={sortDirection}/></th>
                <th style={{
                  whiteSpace: "nowrap"}}  onClick={() => {
                  this.handleChangeSort( sortDirection, "cycleTimeSeconds" )
                }}>{t( 'sites.Asset.AssetDetail.turnaroundTime' )} <SortIcon show={(sortKey === "cycleTimeSeconds")} sortDirection={sortDirection}/></th>
                <th style={{
                  whiteSpace: "nowrap"}}  onClick={() => {
                  this.handleChangeSort( sortDirection, "estimatedDistance" )
                }}>{t( 'sites.Asset.AssetDetail.kmEstimated' )} <SortIcon show={(sortKey === "estimatedDistance")} sortDirection={sortDirection}/></th>
                <th style={{
                  whiteSpace: "nowrap"}}  onClick={() => {
                  this.handleChangeSort( sortDirection, "vibrationCount" )
                }}>{t( 'sites.Asset.AssetDetail.vibration' )} <SortIcon show={(sortKey === "vibrationCount")} sortDirection={sortDirection}/></th>
              </tr>
              </thead>
              <tbody>
              {
                cycles.map( ( item, index ) => {
                  /**
                   * cycleTimeSeconds: 3951428
                   estimatedDistance: null
                   finishedAt: "2018-07-27T06:00:04Z"
                   id: 117
                   startedAt: "2018-06-02T14:07:57Z"
                   vibrationCount: 0
                   */
                  let {startedAt, finishedAt, estimatedDistance, id, vibrationCount, cycleNumber, cycleState, cycleTimeSeconds} = item

                  let cycleTimeSecondsDuration = moment.duration( cycleTimeSeconds, "seconds" )
                  let cycleTimeSecondsDays = Number( cycleTimeSecondsDuration.get( 'days' ) ).toFixed( 0 );
                  let cycleTimeSecondsHours = Number( cycleTimeSecondsDuration.get( 'hours' ) ).toFixed( 0 );
                  let cycleTimeSecondsMinutes = Number( cycleTimeSecondsDuration.get( 'minutes' ) ).toFixed( 0 );


                  return (
                    <tr className={"table--asset-clickable"} data-href={"#"} key={"cycles-" + index} onClick={( event ) => {
                      selectCycle( id, event )
                    }}>
                      <td>
                        <div className="d-flex justify-content-start align-content-center">
                          <Button variant="" size="sm" className="p-0">
                            <FontAwesomeIcon icon={['fas', 'info-square']} size="1x" className={"text-primary pull-left"}/>
                          </Button>
                          <strong className="text-primary pl-2 cursor-pointer">{cycleNumber}</strong>
                        </div>
                      </td>
                      <td>
                        {cycleState &&
                         Helper.getStateAsString( t, cycleState )
                        }
                      </td>
                      <td>
                        {startedAt &&
                         moment( new Date( startedAt ) ).format( 'lll' )
                        }
                      </td>
                      <td>
                        {finishedAt &&
                         moment( new Date( finishedAt ) ).format( 'lll' )
                        }
                      </td>
                      <td>
                        {cycleTimeSeconds &&
                         ((cycleTimeSecondsDays !== "0") ? cycleTimeSecondsDays + " " + t( 'sites.Asset.AssetDetail.days' ) + " " : " ") +
                         ((cycleTimeSecondsHours !== "0") ? cycleTimeSecondsHours + " " + t( 'sites.Asset.AssetDetail.hours' ) : " ") +
                         " " +
                         ((cycleTimeSecondsMinutes !== "0") ? cycleTimeSecondsMinutes + " " + t( 'sites.Asset.AssetDetail.minutes' ) : "")
                        }
                      </td>
                      <td>
                        {estimatedDistance &&
                         estimatedDistance
                        }
                      </td>
                      <td>
                        {vibrationCount &&
                         vibrationCount
                        }
                      </td>
                    </tr>
                  );
                } )
              }

              </tbody>
            </Table>
          </Col>
        </Row>

        {
          loaded &&
          TabCycles.renderLoading()
        }
        {hasMoreItems &&
         this.renderLoadingMore( t )
        }

        <Row>
          <Col>
            <hr/>
          </Col>
        </Row>
      </>
    )
  }

  async handleLoadMore() {
    let {top, skip} = this.state
    await this.setState( {skip: String( parseInt( skip ) + parseInt( top ) )} )
    this.fetchCycles()
  }

  static renderLoading() {
    return <><Row><Col md={"12"} className={"d-flex justify-content-center mt-5"}><Loader type="Oval" color="#CDDC4B" height={70} width={70}/></Col></Row></>;
  }


  renderLoadingMore( t ) {
    return <><Row><Col md={"12"} className={"d-flex flex-column mt-3"}>
      <div className="alert alert-primary" role="alert">
        <Button block onClick={this.handleLoadMore.bind( this )}>{t( "Helper.loadMore" )}</Button>
      </div>
    </Col></Row></>;
  }

  static renderLoadingFinish( t ) {
    return <><Row><Col md={"12"} className={"d-flex flex-column mt-3"}>
      <div className="alert alert-primary" role="alert">
        {t( "sites.Asset.noMoreContentInfo" )}
      </div>
    </Col></Row></>;
  }

  static renderError() {
    return <><Row><Col md={"12"} className={"d-flex flex-column mt-3"}>
      <div className={"alert alert-danger"} role={"alert"}>There was a technical problem. Please contact our <a href="/" className={"alert-link"}>Support Team</a>. Or just reload and try it again.</div>
    </Col></Row></>;
  }
}

export default TabCycles