import React, { Component } from 'react';
import { Button, Card, Col, Row } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Link } from "react-router-dom";
import moment from 'moment';
import 'moment/locale/en-gb';
import 'moment/locale/de';
import { Helper } from '../util/Helper'
import queryString from "query-string";
import CreateUpdateAssetTypeModal from "./CreateUpdateAssetTypeModal";

moment().locale( navigator.language )

class AssetTypeListItem extends Component {

  constructor( props, context ) {
    super( props, context );

    this.state = {
      showAssetTypeModal: false,
    };
  }

  goToDetail() {
    const {item, handleShowAssets} = this.props;

    if (handleShowAssets !== undefined)
      handleShowAssets(item)
  }

  handleEdit() {
    this.setState({showAssetTypeModal: true})
  }

  handleCloseAsseTypeModal() {
    this.setState({showAssetTypeModal: false})
  }

  handleUpdateAssetType(data) {
    let {item} = this.props
    item.name = data.name
    item.description = data.description
    this.setState({showAssetTypeModal: false})
  }

  render() {
    let {item, t} = this.props
    /**
     *  @search.score: 1
     atHomeCount: 1
     atPoiCount: 1
     countAll: 5
     deleted: false
     description: null
     id: "180"
     isActive: true
     name: "Test"
     onTheWayCount: null
     trackedCount: 1
     updatedAt: "2019-10-08T13:10:57.787Z"
     */
    return (
      <div className="col-12 col-md-6 mb-4">
        <Card>
          <Card.Body>
            <div className="d-flex mb-3">
              <div className="pt-2 bd-highlight cursor-pointer"  onClick={this.goToDetail.bind(this)}>
                <FontAwesomeIcon icon={['fal', 'bullseye']} size="lg" className="text-primary"/>
              </div>
              <div className="p-2 cursor-pointer">

                <Card.Title onClick={this.goToDetail.bind(this)} className={"cursor-pointer"}>{item.name}</Card.Title>
              </div>

              {/*<div className="ml-auto p-2">
                <EtaPopver item={item} isLate={item.isLate}/>
              </div>*/}

              <div className="ml-auto p-2 cursor-pointer">
                <div className="pt-2 bd-highlight cursor-pointer"  onClick={this.handleEdit.bind(this)}>
                  <FontAwesomeIcon icon={['fal', 'edit']} size="lg"/>
                </div>
              </div>
            </div>
            <Row className="mb-2">
              <Col className="col-md-6">
                <Row>
                  <Col>
                    <h6 className="text-muted">
                      {t( 'sites.Asset.assetTypeItem.assetCount' )}
                    </h6>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <h5 className="display-5">
                      {item.countAll &&
                       item.countAll
                      }
                    </h5>
                  </Col>
                </Row>
              </Col>
              <Col className="col-md-6">
                <Row>
                  <Col>
                    <h6 className="text-muted">
                      {t( 'sites.Asset.assetTypeItem.assetOnTheWayCount' )}
                    </h6>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <h5 className="display-5">
                      {item.onTheWayCount &&
                       item.onTheWayCount
                      }
                    </h5>
                  </Col>
                </Row>
              </Col>
            </Row>
            <Row className="mb-2">
              <Col className="col-md-6">
                <Row>
                  <Col>
                    <h6 className="text-muted">
                      {t( 'sites.Asset.assetTypeItem.assetAtPoiCount' )}
                    </h6>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <h5 className="display-5">
                      {item.atPoiCount &&
                       item.atPoiCount
                      }
                    </h5>
                  </Col>
                </Row>
              </Col>
              <Col className="col-md-6">
                <Row>
                  <Col>
                    <h6 className="text-muted">
                      {t( 'sites.Asset.assetTypeItem.assetAtHomeCount' )}
                    </h6>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <h5 className="display-5">
                      {item.atHomeCount &&
                       item.atHomeCount
                      }
                    </h5>
                  </Col>
                </Row>
              </Col>
            </Row>
            <Button variant="primary" block onClick={this.goToDetail.bind(this)}>
              {t( 'sites.Asset.assetTypeItem.goToDetail' )}</Button>
          </Card.Body>
        </Card>
        <CreateUpdateAssetTypeModal isUpdate {...this.props} item={item} showModal={this.state.showAssetTypeModal} closeModal={this.handleCloseAsseTypeModal.bind( this )} onSave={this.handleUpdateAssetType.bind( this )}/>
      </div>
    );
  }
}

export default AssetTypeListItem