import React, { Component } from 'react';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

class SortIcon extends Component {

  static defaultProps = {
    show: false,
  }

  constructor( props, context ) {
    super( props, context );

    this.handleClick = ( {target} ) => {
      this.setState( s => ({target, show: !s.show}) );
    };

    this.state = {
      show: false,
    };
  }

  changeSort() {
    let {changeSort, sortDirection, sortKey} = this.props
    if (changeSort !== undefined)
      changeSort(sortDirection, sortKey)
  }

  render() {
    let {show, sortDirection, sortKey} = this.props
    return (
      <>
        {show === true &&
         <FontAwesomeIcon icon={['fal', (sortDirection === "asc") ? 'chevron-up' : 'chevron-down']} onClick={this.changeSort.bind(this)}/>
        }
      </>
    );
  }
}

export default SortIcon