import React from 'react';
import { Button, Col, FormControl, Modal, Row } from 'react-bootstrap';
import Form from "react-bootstrap/es/Form";
import Loader from "react-loader-spinner";

class CreateUpdateAssetTypeModal extends React.Component {
  static defaultProps = {
    show: false,
    isUpdate: false,
    item: null
  }

  constructor( props, context ) {
    super( props, context );

    this.state = {
      name: props.item !== null ? props.item.name : null,
      description: props.item !== null ? props.item.description : null,
      saveMode: false
    };
  }

  static renderLoading() {
    return <><Row id={"modalLoader"}><Col md={"12"} className={"d-flex justify-content-center mt-5"}><Loader type="Oval" color="#CDDC4B" height={70} width={70}/></Col></Row></>;
  }

  onSave() {
    const {isUpdate} = this.props;
    if (!isUpdate) {
      this.handleSaveAssetType()
    } else {
      this.handleUpdateAssetType()
    }

  }

  handleSaveAssetType() {
    let {name, description} = this.state
    if ( name === undefined || name === null || name === "" )
      return false

    this.setState( {saveMode: true} )
    fetch( '/asset-ui-service/asset/CreateAssetType', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify( {
                              name: name,
                              description: description,
                              isActive: true

                            } )
    } )
      .then( ( res ) => {
        if ( res.status !== 201 ) {
          this.setState( {successItem: {}, successSend: false, failSend: true} )
        } else {
          return res.json()
        }
      } )
      .then( ( data ) => {

        this.setState( {saveMode: false} )
        if ( this.props.onSave !== undefined )
          this.props.onSave( data )
        if ( this.props.closeModal !== undefined )
          this.props.closeModal()

      } )
      .catch( ( error ) => {
        console.error( error );
        //ai.appInsights.trackException(error)
        this.setState( {successItem: error, successSend: false, failSend: true} )
      } )
  }



  handleUpdateAssetType() {
    const {item} = this.props;
    let {name, description} = this.state

    if ( name === undefined || name === null || name === "" )
      return false

    this.setState( {saveMode: true} )
    fetch( '/asset-ui-service/asset/updateAssetType', {
      method: 'PATCH',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify( {
                              id: item.id,
                              name: name,
                              description: description,
                              isActive: true

                            } )
    } )
      .then( ( res ) => {
        if ( res.status !== 201 ) {
          this.setState( {successItem: {}, successSend: false, failSend: true} )
        } else {
          return res.json()
        }
      } )
      .then( ( data ) => {

        this.setState( {saveMode: false} )
        if ( this.props.onSave !== undefined )
          this.props.onSave( data )
        if ( this.props.closeModal !== undefined )
          this.props.closeModal()

      } )
      .catch( ( error ) => {
        console.error( error );
        //ai.appInsights.trackException(error)
        this.setState( {successItem: error, successSend: false, failSend: true} )
      } )
  }


  handleChangeName( e ) {
    this.state.name = e.target.value.trim()
  }


  handleChangeDescription( e ) {
    this.state.description = e.target.value.trim()
  }


  render() {
    const {t, item} = this.props;
    let {saveMode} = this.state;
    return (
      <>
        <Modal show={this.props.showModal} onHide={this.props.closeModal}>
          <Modal.Header closeButton>
            <Modal.Title>{t( 'sites.Asset.AssetDetail.editAssetType' )}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {saveMode &&
             CreateUpdateAssetTypeModal.renderLoading()
            }
            {!saveMode &&
             <>
               <Form>
                 <Form.Row>
                   <Form.Group as={Col} md={"6"} controlId="formGridNumber">
                     <Form.Label>{t( 'sites.Asset.AssetDetail.name' )}</Form.Label>
                     <FormControl
                       type="text"
                       min="1"
                       required="required"
                       aria-required="true"
                       className="form-control"
                       placeholder={t( 'sites.Asset.AssetDetail.nameOfTheAssetType' )}
                       aria-label={t( 'sites.Asset.AssetDetail.name' )}
                       aria-describedby="hashtag"
                       defaultValue={item !== null ? item.name : ""}
                       onChange={this.handleChangeName.bind( this )}
                     />
                     <Form.Control.Feedback type="invalid">
                       {t( 'sites.Delivery.deliveryCreate.errorMessage' )}
                     </Form.Control.Feedback>
                   </Form.Group>
                   <Form.Group as={Col} md={"6"} controlId="formGridTracker">
                     <Form.Label>{t( 'sites.Asset.AssetDetail.description' )}</Form.Label>
                     <Form.Control
                       as="textarea"
                       aria-label={t( 'sites.Asset.AssetDetail.description' )}
                       placeholder={t( 'sites.Asset.AssetDetail.describeAssetType' )}
                       defaultValue={item !== null ? item.description : ""}
                       onChange={this.handleChangeDescription.bind( this )}
                       rows="3"/>
                   </Form.Group>
                 </Form.Row>
               </Form>
               <Row className="mt-3">
                 <Col>
                   <div className="d-flex flex-column flex-md-row flex-lg-row">
                     <div className="p-1 flex-fill">
                       <Button variant="light" block onClick={this.props.closeModal}>{t( 'sites.Asset.AssetDetail.cancel' )}</Button>
                     </div>
                     <div className="p-1 flex-fill">
                       <Button variant="primary" block onClick={this.onSave.bind( this )}>{t( 'sites.Asset.AssetDetail.save' )}</Button>
                     </div>
                   </div>
                 </Col>
               </Row>
             </>
            }

          </Modal.Body>
        </Modal>
      </>
    );
  }
}

export default CreateUpdateAssetTypeModal