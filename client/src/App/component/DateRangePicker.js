import React, { Component } from 'react'
import $ from 'jquery';
import "daterangepicker"
//https://github.com/dangrossman/daterangepicker
import moment from 'moment';
import { t } from "i18next";

class DateRangePicker extends Component {
  static defaultProps = {
    elementId: "DateRangePicker",
    opens: t( "component.DateRangePicker.opens" ),
    drops: t( "component.DateRangePicker.drops" ),
    singleDatePicker: false,
    showDropdowns: false,
    showWeekNumbers: true,
    showISOWeekNumbers: false,
    timePicker: true,
    timePicker24Hour: true,
    timePickerSeconds: false,
    autoApply: false,
    //dateLimit: {
    //	days: 7
    //},
    ranges: {
      'Today': [moment().startOf( 'day' ), moment().endOf( 'day' )],
      'Yesterday': [moment().startOf( 'day' ).subtract( 1, 'days' ), moment().endOf( 'day' ).subtract( 1, 'days' )],
      'Last 7 Days': [moment().startOf( 'day' ).subtract( 6, 'days' ), moment().endOf( 'day' )],
      'Last 30 Days': [moment().startOf( 'day' ).subtract( 29, 'days' ), moment().endOf( 'day' )],
      'This Month': [moment().startOf( 'day' ).startOf( 'month' ), moment().endOf( 'month' ).endOf( 'day' )],
      'Last Month': [moment().startOf( 'day' ).subtract( 1, 'month' ).startOf( 'month' ), moment().subtract( 1, 'month' ).endOf( 'month' ).endOf( 'day' )]
    },
    locale: {
      format: 'MM/DD/YYYY HH:mm',
      separator: ' - ',
      applyLabel: 'Apply',
      cancelLabel: 'Cancel',
      fromLabel: 'From',
      toLabel: 'To',
      customRangeLabel: 'Custom',
      daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
      monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
      firstDay: 1
    },
    parentEl: "body",
    buttonClasses: "btn btn-lg btn-primary",
    applyClass: "btn-primary",
    cancelClass: "btn-light",
    startDate: undefined,//"05/23/2019"
    endDate: undefined,//"05/23/2019"
    minDate: undefined,//"05/23/2019"
    maxDate: undefined//"05/23/2019"
  }
  displayName = "DateRangePicker"

  componentWillReceiveProps( props ) {
    console.log(this.props.elementId, "componentWillReceiveProps")
    // eslint-disable-next-line react/prop-types
    let todayKey = t( "component.DateRangePicker.today" )
    let yesterdayKey = t( "component.DateRangePicker.yesterday" )
    let last7DayKey = t( "component.DateRangePicker.last7Day" )
    let last30DayKey = t( "component.DateRangePicker.last30Day" )
    let thisMonthKey = t( "component.DateRangePicker.thisMonth" )
    let lastMonthKey = t( "component.DateRangePicker.lastMonth" )
    //options.ranges = {}

    if (this.props.newRanges === undefined) {
      for ( let range in props.ranges ) {
        delete props.ranges[range]
      }
    } else {
      for ( let range in props.ranges ) {
        delete props.ranges[range]
      }
      for ( let range in this.props.newRanges ) {
        props.ranges[range] = this.props.newRanges[range]
      }
    }
    props.locale.format = t( "component.DateRangePicker.format" )
    props.locale.separator = t( "component.DateRangePicker.separator" )
    props.locale.applyLabel = t( "component.DateRangePicker.applyLabel" )
    props.locale.cancelLabel = t( "component.DateRangePicker.cancelLabel" )
    props.locale.fromLabel = t( "component.DateRangePicker.fromLabel" )
    props.locale.toLabel = t( "component.DateRangePicker.toLabel" )
    props.locale.customRangeLabel = t( "component.DateRangePicker.customRangeLabel" )
    props.locale.daysOfWeek = [
      t( "component.DateRangePicker.daysOfWeek.su" ),
      t( "component.DateRangePicker.daysOfWeek.mo" ),
      t( "component.DateRangePicker.daysOfWeek.tu" ),
      t( "component.DateRangePicker.daysOfWeek.we" ),
      t( "component.DateRangePicker.daysOfWeek.th" ),
      t( "component.DateRangePicker.daysOfWeek.fr" ),
      t( "component.DateRangePicker.daysOfWeek.sa" )
    ]
    props.locale.monthNames = [
      t( "component.DateRangePicker.month.january" ),
      t( "component.DateRangePicker.month.february" ),
      t( "component.DateRangePicker.month.march" ),
      t( "component.DateRangePicker.month.april" ),
      t( "component.DateRangePicker.month.may" ),
      t( "component.DateRangePicker.month.june" ),
      t( "component.DateRangePicker.month.july" ),
      t( "component.DateRangePicker.month.august" ),
      t( "component.DateRangePicker.month.september" ),
      t( "component.DateRangePicker.month.october" ),
      t( "component.DateRangePicker.month.november" ),
      t( "component.DateRangePicker.month.december" ),
    ]

    if (this.props.newRanges === undefined) {
      props.ranges[todayKey] = [moment().startOf( 'day' ), moment().endOf( 'day' )]
      props.ranges[yesterdayKey] = [moment().startOf( 'day' ).subtract( 1, 'days' ), moment().endOf( 'day' ).subtract( 1, 'days' )]
      props.ranges[last7DayKey] = [moment().startOf( 'day' ).subtract( 6, 'days' ), moment().endOf( 'day' )]
      props.ranges[last30DayKey] = [moment().startOf( 'day' ).subtract( 29, 'days' ), moment().endOf( 'day' )]
      props.ranges[thisMonthKey] = [moment().startOf( 'day' ).startOf( 'month' ), moment().endOf( 'month' ).endOf( 'day' )]
      props.ranges[lastMonthKey] = [moment().startOf( 'day' ).subtract( 1, 'month' ).startOf( 'month' ), moment().subtract( 1, 'month' ).endOf( 'month' ).endOf( 'day' )]

    }
    $( "#" + props.elementId ).daterangepicker( props, this.props.onChange );

  }

  componentDidUpdate() {
    console.log(this.props.elementId, "componentDidUpdate")
    $( "#" + this.props.elementId ).daterangepicker( this.props, this.props.onChange );
  }
  componentDidMount() {
    console.log(this.props.elementId, "componentDidMount")
    if (this.props.startDate !== undefined && this.props.endDate !== undefined) {
      if (this.props.newRanges !== undefined) {
        for ( let range in this.props.ranges ) {
          delete this.props.ranges[range]
        }
        for ( let range in this.props.newRanges ) {
          this.props.ranges[range] = this.props.newRanges[range]
        }
      }

      $( "#" + this.props.elementId ).daterangepicker( this.props, this.props.onChange );
    }
  }

  render() {
    // eslint-disable-next-line react/prop-types
    let {elementId} = this.props
    return (
      <div className={"date-range-picker"}>
        <input type="text" id={elementId} className={"form-control"}/>
      </div>
    );
  }

}

export default DateRangePicker