import React, { Component } from 'react'
import { Col, Row } from 'react-bootstrap'
import Loader from "react-loader-spinner";

class BpwLoader extends Component {

  render() {
    return <><Row><Col md={"12"} className={"d-flex justify-content-center mt-5"}><Loader type="Oval" color="#CDDC4B" height={70} width={70}/></Col></Row></>;
  }
}

export default BpwLoader