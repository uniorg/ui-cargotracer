import React, { Component } from 'react';
import { Button, Card, Col, Row } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Link } from "react-router-dom";
import moment from 'moment';
import 'moment/locale/en-gb';
import 'moment/locale/de';
import Moment from "react-moment";

moment().locale( navigator.language )

class DevicesListItem extends Component {

  goToDetail() {
    const {history, url, item} = this.props;

    let pushLocation = {
      pathname: url + "/" + item.deviceId,
      state: {lastLocation: history.location}
    }
    history.push( pushLocation )
  }

  render() {
    let {item, t, url, history} = this.props
    /**
     * @search.score: 1
     "@odata.type": "#com.innolab.tracking.device.ui.service.v2.Device",
     "@odata.id": "Devices('10463C4')",
     "odataId": "10463C4",
     "deviceId": "10463C4",
     "name": "10463C4",
     "lastValidGeoPositionSentAt@odata.type": "#DateTimeOffset",
     "lastValidGeoPositionSentAt": "2019-10-31T04:58:57Z",
     "lastValidGeoPositionLongitude": 6.701849493963279,
     "lastValidGeoPositionLatitude": 51.22452139106608,
     "lastValidGeoPositionRadius@odata.type": "#Int32",
     "lastValidGeoPositionRadius": 25873,
     "lastValidGeoPositionType@odata.type": "#com.innolab.tracking.device.ui.service.v2.GeoPositionType",
     "lastValidGeoPositionType": "SIGFOX",
     "lastValidGeoPositionNumberOfSatellites@odata.type": "#Int32",
     "lastValidGeoPositionNumberOfSatellites": null,
     "lastValidGeoPositionVibrationCount@odata.type": "#Int32",
     "lastValidGeoPositionVibrationCount": null,
     "lastValidGeoPositionVibrationThreshold@odata.type": "#Int32",
     "lastValidGeoPositionVibrationThreshold": null,
     "lastValidGeoPositionSpeed@odata.type": "#Int32",
     "lastValidGeoPositionSpeed": null,
     "lastSeenAt@odata.type": "#DateTimeOffset",
     "lastSeenAt": "2019-10-31T04:58:57Z",
     "lastSeenTemperature": null,
     "lastSeenBatteryVoltage@odata.type": "#Int32",
     "lastSeenBatteryVoltage": null,
     "stillAssigned": true
     */
    return (
      <div className="col-12 col-md-6 mb-4">
        <Card>
          <Card.Body>
            <div className="d-flex mb-3">
              <div className="pt-2 bd-highlight cursor-pointer" onClick={this.goToDetail.bind( this )}>
                <FontAwesomeIcon icon={['fal', 'check-circle']} size="lg" className="text-primary"/>
              </div>
              <div className="p-2">

                <Card.Title onClick={this.goToDetail.bind( this )} className={"cursor-pointer"}>{item.name}</Card.Title>
              </div>

              {/*<div className="ml-auto p-2">
                <EtaPopver item={item} isLate={item.isLate}/>
              </div>

              <div className="ml-auto p-2 cursor-pointer">
                <Link to={`update/device/` + item.id}>
                  <FontAwesomeIcon icon={['fal', 'edit']} size="lg"/>
                </Link>
              </div>*/}
              {item.stillAssigned ?
                (
                  <div className="ml-auto p-2 cursor-pointer">
                    <FontAwesomeIcon icon={['fa', 'lightbulb-exclamation']} size="lg" color={"green"}/>
                  </div>
                ) : (
                  <div className="ml-auto p-2 cursor-pointer">
                    <FontAwesomeIcon icon={['fa', 'lightbulb-exclamation']} size="lg" color={"red"}/>
                  </div>
                )}
            </div>
            <Row className="mb-2">
              <Col className="col-md-6">
                <Row>
                  <Col>
                    <h6 className="text-muted">
                      {t( 'sites.Device.deviceItem.deviceId' )}
                    </h6>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <h6 className="display-6">
                      {item.deviceId &&
                       item.deviceId
                      }
                    </h6>
                  </Col>
                </Row>
              </Col>
              <Col className="col-md-6">
                <Row>
                  <Col>
                    <h6 className="text-muted">
                      {t( 'sites.Device.deviceItem.lastSendAt' )}
                    </h6>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <h6 className="display-6">
                      {item.lastSeenAt &&
                       <Moment date={new Date( item.lastSeenAt )} format="L"/>
                      }
                    </h6>
                    {item.lastSeenAt &&
                     <div>
                       <Moment date={new Date( item.lastSeenAt )} format="LT"/>
                     </div>
                    }
                  </Col>
                </Row>
              </Col>
            </Row>
            <Row className="mb-2">
              <Col className="col-md-6">
                <Row>
                  <Col>
                    <h6 className="text-muted">
                      {t( 'sites.Device.deviceItem.batteryVoltage' )}
                    </h6>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <h5 className="display-6">
                      {item.lastSeenBatteryVoltage &&
                       item.lastSeenBatteryVoltage + " mV"
                      }
                    </h5>
                  </Col>
                </Row>
              </Col>
              <Col className="col-md-6">
                <Row>
                  <Col>
                    <h6 className="text-muted">
                      {t( 'sites.Device.deviceItem.lastPosition' )}
                    </h6>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <h6 className="display-6">
                      {item.lastValidGeoPositionSentAt &&
                       <Moment date={new Date( item.lastValidGeoPositionSentAt )} format="L"/>
                      }
                    </h6>
                    {item.lastValidGeoPositionSentAt &&
                     <div>
                       <Moment date={new Date( item.lastValidGeoPositionSentAt )} format="LT"/>
                     </div>
                    }
                  </Col>
                </Row>
              </Col>
            </Row>
            <Button variant="primary" block onClick={this.goToDetail.bind( this )}>
              {t( 'sites.Asset.assetItem.goToDetail' )}</Button>
          </Card.Body>
        </Card>
      </div>
    );
  }
}

export default DevicesListItem