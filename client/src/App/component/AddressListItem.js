import React, { Component } from 'react';
import { Card, Col, Form, Row } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import moment from 'moment';
import 'moment/locale/en-gb';
import 'moment/locale/de';
import AddressSearchInput from "./AddressSearchInput";

moment().locale( navigator.language )

class AddressListItem extends Component {

  render() {
    let {item, t} = this.props
    return (
      <Card className={"mb-4" + ((!item.isUpdate && !item.isValid) ? " alert alert-danger none-background p-0 text-black" : "")}>
        <Card.Body className={"pt-3"}>
          <Row>
            <Col className={""}>
              <button type="button" className="close d-flex justify-content-end" aria-label="Close" onClick={( e ) => {
                this.props.handleRemoveAddress( item, e )
              }}>
                <FontAwesomeIcon icon={['fal', 'times']} className="text-muted"/>
              </button>
            </Col>
          </Row>
          <Row>
            <AddressSearchInput
              defaultCol={"12"}
              inputId={"from-address-input"}
              title={this.props.label}
              placeholder={t( 'sites.Delivery.deliveryCreate.addStartAddress' )}
              icon={['fal', 'map-marker-alt']}
              handleClickOnAddress={this.props.handleClickOnAddress}
              handleChangeRadius={this.props.handleChangeRadius}
              handeGoToMarker={this.props.handeGoToMarker}
              item={item}
              inputId={item.tempId + "addressInput"}
              isUpdate={item.isUpdate}
              {...this.props}/>

          </Row>
          <Row>
            <Form.Group as={Col} md={"12"} controlId="formGridOptionalDesignation">
              <Form.Label>{t( 'sites.Asset.assetCreate.designationOptional' )}</Form.Label>
              <Form.Control
                type="text"
                className="form-control-lg"
                placeholder={t( 'sites.Asset.assetCreate.addDesignation' )}
                defaultValue={item.name}
                onChange={(e) => {
                  if (this.props.changeName !== undefined)
                    this.props.changeName(e.target.value, this.props.item, e)
                }}>
              </Form.Control>
            </Form.Group>
          </Row>
        </Card.Body>
      </Card>
    );
  }
}

export default AddressListItem