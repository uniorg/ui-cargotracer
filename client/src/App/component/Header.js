import React, { Component } from 'react'
import { Button, ButtonGroup, Nav, Navbar, NavDropdown } from 'react-bootstrap'
import { NavLink } from 'react-router-dom'
import BpwBrand from './BpwBrand'
import i18n from "i18next";
import logoSvg from "../svg/bpw/logo.svg";

class Header extends Component {

  static handleLogoutClick() {
    window.location.href = window.location.origin + '/logout'
  }

  changeLanguage( lng ) {
    i18n.changeLanguage( lng );
  }

  render() {
    let {t, isAdmin, isClient} = this.props
    return (
      <div className="app-header">
        <Navbar collapseOnSelect expand="lg" bg="light" variant="light" className="shadow">
          <div className="container">
            <BpwBrand {...this.props}/>
            <ButtonGroup>
              <Button variant="light" onClick={() => this.changeLanguage( 'de-DE' )}>de</Button>
              <Button variant="light" onClick={() => this.changeLanguage( 'en-EN' )}>en</Button>
            </ButtonGroup>
            <Navbar.Toggle aria-controls="responsive-navbar-nav"/>
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="ml-auto">
                <NavLink exact className="nav-link  ml-3 mr-3" activeClassName="active" to="/delivery">{t( "component.Header.Nav.Delivery" )}</NavLink>
                {isClient &&
                 <>
                   <NavLink exact className="nav-link ml-3 mr-3 cursor-pointer" to="/devices">{t( "component.Header.Nav.Tracker" )}</NavLink>
                   <NavLink exact className="nav-link ml-3 mr-3 cursor-pointer" to="/assets">{t( "component.Header.Nav.Assets" )}</NavLink>
                 </>
                }
                {/*<NavLink exact className="nav-link ml-3 mr-3 cursor-pointer" to="/assetsort">AssetSort</NavLink>
                <NavLink exact className="nav-link ml-3 mr-3 cursor-pointer" to="/createasset">Create Asset</NavLink>*/}
                {isAdmin &&
                 <>
                   <NavLink exact className="nav-link ml-3 mr-3 cursor-pointer" to="/users">{t( "component.Header.Nav.UserAdministration" )}</NavLink>
                 </>
                }
              </Nav>
              <Nav>
                <NavDropdown title={t( "component.Header.Nav.Profile" )} id="basic-nav-dropdown">
                  <NavDropdown.Item href="#action/3.1">

                    <img className='rounded-circle user-image img-fluid' alt={t( 'component.BpwBrand.LogoAlt' )} src={logoSvg}/>
                    {/*<Image className="rounded-circle user-image img-fluid" src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/13/Thyssenkrupp_AG_Logo_2015.svg/1200px-Thyssenkrupp_AG_Logo_2015.svg.png"/>*/}
                  </NavDropdown.Item>
                  <div className="nav-link ml-3 mr-3 cursor-pointer" onClick={Header.handleLogoutClick}>{t( "component.Header.Nav.Logout" )}</div>
                </NavDropdown>
              </Nav>
            </Navbar.Collapse>
          </div>
        </Navbar>
        <div className="header-footer-line"/>
      </div>
    );
  }
}

export default Header
