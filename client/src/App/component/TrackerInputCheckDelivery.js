import React, { Component } from 'react';
import { Col, Form } from "react-bootstrap";
import Select, { components } from 'react-select';
import Async from 'react-select/async';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";


class TrackerInputCheckDelivery extends Component {

  lastEvent: null

  static defaultProps = {
    lifeLoad: true,
    checkValueUrl:'/delivery-ui-service/delivery/assignedDelivery/',
    invalidClass: ' alert alert-warning',
    invalidColorClass: ' select-invalid-warning',
    isValid: null,
    reloadButton: true
  }

  _isMounted = false;

  constructor( ...args ) {
    super( ...args );

    this.state = {
      isInvalid: undefined,
      isValid: undefined,
      isLoading: false,
      className: "form-control-lg",
      options: []
    };
  }

  componentDidMount() {
    this._isMounted = true;
    if ( !this.props.lifeLoad )
      this.fetchSelectOptions()
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  handleChangeInput( e ) {
    this.lastEvent = e
    const {handleValidate} = this.props;

    if ( e === undefined || e === null ) {
      if ( handleValidate !== undefined )
        this.setState( {isInvalid: undefined, isValid: undefined, isLoading: false} )
      handleValidate( null, true )
      if (this.props.handleClearInput !== undefined)
        this.props.handleClearInput()
      return
    }

    let {deviceId} = e
    if ( deviceId === null || deviceId === undefined || deviceId === "" ) {
      this.setState( {isInvalid: undefined, isValid: undefined, isLoading: false} )
      return
    }


    this.fetchInputValue( deviceId, ( id, data ) => {
      let {hasEntry} = data
      console.log(id, data)
      if ( handleValidate !== undefined )
        handleValidate( id, ((hasEntry) ? false : true), data )
      this.setState( {isInvalid: hasEntry, isValid: ((hasEntry) ? false : true), isLoading: false} )
    } )
  }

  handleClearInput() {
    this.setState( {isInvalid: undefined, isValid: undefined, isLoading: false} )


  }

  fetchInputValue( value, callback ) {
    this.setState( {isLoading: true} );
    fetch( this.props.checkValueUrl + value )
      .then( res => res.json() )
      .then( ( data ) => {
        let returnData = {}
        returnData.hasEntry = true
        if ( data.hasDelivery !== undefined ) {
          returnData.hasEntry = data.hasDelivery
          callback( value, returnData )
        } else if ( data.value !== undefined ) {
          returnData.hasEntry = (data.value.length > 0)
          returnData.entries = data.value

          callback( value, returnData )
        } else {
          callback( null, false )
        }
      } )
      .catch( ( error ) => {
        console.error( error );
        //ai.appInsights.trackException(error)
        callback( null, false )
      } )
  }

  fetchSelectOptions() {
    this.setState( {options: [], isLoading: true} );
    fetch( '/device-ui-service/devices' )
      .then( res => res.json() )
      .then( ( data ) => {
        if ( data.hasOwnProperty( "value" ) ) {
          return data.value
        } else {
          return []
        }
      } )
      .then( ( values ) => {
        if ( this._isMounted ) {
          let mapValues = values.map( ( value ) => {
            return {value: value.deviceId, label: value.name}
          } );
          this.setState( {options: mapValues, isLoading: false} );
        }
      } )
      .catch( ( error ) => {
        console.error( error );
        //ai.appInsights.trackException(error)
      } )
  }


  getOptionValue = ( option ) => option.id; // maps the result 'id' as the 'value'

  getOptionLabel = ( option ) => option.deviceId + " - " + option.name;

  async handleSearchDevice( input ) {
    if ( !input ) {
      return [];
    }

    return fetch( `/device-ui-service/devices/searchDevice/` + input )
      .then( ( response ) => response.json() )
      .then( ( json ) => {
        let returnOptions = []
        if ( json.value !== undefined ) {
          returnOptions = json.value;
        }
        console.log( returnOptions )
        return returnOptions
      } );
  }

  getTarckerValidateReloadIcon() {
    const {isValid, reloadButton} = this.props;
    if (!isValid && isValid !== null && reloadButton ) {
      return (
        <div className={"ml-3 float-right"} onClick={() => {this.handleChangeInput( this.lastEvent )}}><FontAwesomeIcon icon={['fal', 'sync-alt']} size="lg" className={"text-primary"}/></div>
      )
    } else {
      return ""
    }
  }

  render() {
    const {t, isPropLoading} = this.props;
    const {isLoading, options} = this.state;
    const {isValid} = this.props;
    const ValueContainer = ( {children, ...props} ) => {
      return (
        components.ValueContainer && (
          <components.ValueContainer {...props}>
            {!!children && (
              <FontAwesomeIcon icon={['fal', 'search']} style={{position: 'absolute', left: 6}}/>
            )}
            {children}
          </components.ValueContainer>
        )
      );
    };

    const ClearIndicator = props => {
      return (
        components.ClearIndicator && (
          <components.ClearIndicator {...props}>
            <FontAwesomeIcon icon={['fal', 'times']}/>
          </components.ClearIndicator>
        )
      );
    };
    const DropdownIndicator = props => {
      return (
        components.DropdownIndicator && (
          <components.DropdownIndicator {...props}>
            <FontAwesomeIcon icon={['fal', 'chevron-down']}/>
          </components.DropdownIndicator>
        )
      );
    };

    const LoadingIndicator = props => {
      return (
        components.LoadingIndicator && (
          <components.DropdownIndicator {...props}>
            <FontAwesomeIcon icon={['fal', 'spinner']} spin/>
          </components.DropdownIndicator>
        )
      );
    };

    const styles = {
      valueContainer: base => ({
        ...base,
        paddingLeft: 24
      })
    }

    const SelectComponent = this.props.lifeLoad
      ? Async
      : Select;

    const defaultOptions = this.props.lifeLoad
      ? undefined
      : options;

    const defaultIsLoading = this.props.lifeLoad
      ? undefined
      : isLoading;

    const defaultDefaultValue = this.props.lifeLoad
      ? undefined
      : options[0];

    let errorMessageDeviceExist = ((this.props.errorMessageDeviceExist === undefined) ? t( 'sites.Delivery.deliveryCreate.errorMessageDeviceExist' ) : this.props.errorMessageDeviceExist )

    return (
      <Form.Group as={Col} md={"6"} controlId="formGridTracker" className={"delivery-tracker-input-check-delivery"}>
        {((isPropLoading) ? isPropLoading : isLoading) ? <FontAwesomeIcon icon={['fal', 'spinner']} size="lg" spin/> : null}<Form.Label>{((this.props.label === undefined) ? t( 'sites.Delivery.deliveryCreate.assignTrackers' ) : this.props.label)}</Form.Label>{this.getTarckerValidateReloadIcon()}

        <SelectComponent
          className={"form-control-lg " + ((isValid !== null && isValid !== undefined) ? (isValid ? "form-control--select-valid" : "form-control--select-invalid" + this.props.invalidColorClass) : "") + (((isPropLoading) ? isPropLoading : isLoading) ? this.props.invalidClass : "")}
          classNamePrefix="select"
          backspaceRemoves
          isClearable
          isSearchable
          isDisabled={false}
          required="required"
          isLoading={defaultIsLoading}
          options={defaultOptions}
          components={{DropdownIndicator, ValueContainer, ClearIndicator, LoadingIndicator}}
          value={((this.props.selectedDevice !== null) ? this.props.selectedDevice : defaultDefaultValue)}
          onChange={this.handleChangeInput.bind( this )}
          handleClearInput={this.handleClearInput.bind(this)}
          placeholder={t( 'sites.Delivery.deliveryCreate.selectTracker' )}
          valueKey="id"
          labelKey="name"
          getOptionValue={this.getOptionValue}
          getOptionLabel={this.getOptionLabel}
          loadOptions={this.handleSearchDevice}
        />


        <div className={"invalid-feedback" + ((isValid !== null && isValid !== undefined) ? (isValid ? " hide" : " show" + this.props.invalidColorClass) : "")}>{errorMessageDeviceExist}</div>
      </Form.Group>
    );
  }
}

export default TrackerInputCheckDelivery