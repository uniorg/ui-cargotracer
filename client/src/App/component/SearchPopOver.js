import React, { Component } from 'react';
import { ButtonToolbar, Overlay, OverlayTrigger, Popover, Tooltip } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default class SearchPopOver extends Component {
  constructor( props, context ) {
    super( props, context );

    this.handleClick = ( {target} ) => {
      this.setState( s => ({target, show: !s.show}) );
    };

    this.state = {
      show: false,
    };
  }

  render() {
    const {t} = this.props;
    return (
      <ButtonToolbar className={"search-popover"}>
        <OverlayTrigger overlay={<Tooltip id="tooltip-searhfield">Suchfeld Info!</Tooltip>}>
          <span className="d-inline-block">
            <FontAwesomeIcon icon={['fal', 'info-circle']}
                             className={"cursor-pointer mr-1 text-primary"}
                             transform="down-1"
                             size="lg"
                             onClick={this.handleClick}/>
          </span>
        </OverlayTrigger>
        <span className="float-left">
          {t( 'component.SearchPopOver.infoToSearch' )}
        </span>
        <Overlay
          show={this.state.show}
          target={this.state.target}
          placement="auto"
          container={this}
          containerPadding={20}
        >
          <Popover className="search-popover-contained" title="Richtig suchen">
            <p dangerouslySetInnerHTML={{__html: t( 'component.SearchPopOver.infoToSearchPopText' )}}/>
          </Popover>
        </Overlay>
      </ButtonToolbar>
    );
  }
}