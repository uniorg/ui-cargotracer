import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import queryString from 'query-string'
//https://jquense.github.io/react-widgets/api/DateTimePicker/
import DateRangePicker from '../component/DateRangePicker'
import "react-widgets/lib/scss/react-widgets.scss"
import { Accordion, Button, ButtonGroup, Card, Col, Form, Row, ToggleButton } from "react-bootstrap";
import Select from 'react-select';
import $ from 'jquery'

class ModuleFilter extends Component {
  static defaultProps = {
    checkTrackerOnlyWhere: "(deliveryState ne 'DONE' and deliveryState ne 'UNKNOWN' and activeDeviceAssignments gt 0)",
    checkTrackerAllWhere: "(activeDeviceAssignments gt 0 or activeDeviceAssignments eq 0)",

    lastStatusQuery: "(deliveryStateTimestamp ge ###START### and deliveryStateTimestamp le ###END###)",
    checkBoxes: [],
    location: {
      search: ""
    },
    withOnlyTrackedToggle: false,
    accordionId: "filterAccordion",
    groupByEnable: false,
    groupByMode: "asset"
  }

  // (stateTimestamp ge 2019-05-28T11:06:16.361Z and stateTimestamp le 2019-05-28T11:06:16.361Z)

  constructor( props ) {
    super( props );
    this.state = {
      width: 0,
      height: 0,
      showResults: false,
      list: [],
      inputValue: "",
      orFilter: {},
      andFilter: [],
      lastStatusStart: undefined,
      lastStatusEnd: undefined,
      onlyTracked: true
    };
  }

  static checkFilterExist( filter, searchQuery ) {
    return (filter.indexOf( searchQuery ) > -1)
  }

  static getDateFromFilterQuery( urlQuery, start, end, callback ) {
    let startSearchPosition = urlQuery.indexOf( "(deliveryStateTimestamp ge" )
    let endSearchPosition = urlQuery.indexOf( ")", startSearchPosition ) + 1
    urlQuery = urlQuery.slice( startSearchPosition, endSearchPosition )
    if ( startSearchPosition === -1 ) {
      return
    }
    let startPosition = urlQuery.indexOf( start ) + 3
    let startString = urlQuery.slice( startPosition )
    let startEndPosition = startString.indexOf( " and" )

    let endPosition = urlQuery.indexOf( end, startEndPosition ) + 3
    let endEndPosition = startString.indexOf( " )", endPosition )
    let endString = urlQuery.slice( endPosition )

    let firstDate = startString.slice( 0, startEndPosition )
    let endDate = endString.slice( 0, endEndPosition )

    callback( firstDate, endDate )

  }

  componentDidMount() {
    this.onFilterInUrl()
  }

  onFilterInUrl() {
    const values = queryString.parse( this.props.location.search )
    const {orFilter, andFilter} = this.state;
    const filterItems = this.props.checkBoxes
    let filter
    if ( values.checkFilter !== undefined ) {
      filter = values.checkFilter
    } else {
      return
    }

    if ( ModuleFilter.checkFilterExist( filter, this.props.checkTrackerOnlyWhere ) ) {
      andFilter["onlyTracked"] = this.props.checkTrackerOnlyWhere
      this.setState( {onlyTracked: true} )
    } else {
      this.setState( {onlyTracked: false} )
    }
    if ( ModuleFilter.checkFilterExist( filter, this.props.checkTrackerAllWhere ) ) {
      andFilter["onlyTracked"] = this.props.checkTrackerAllWhere
      this.setState( {onlyTracked: false} )
    }
    if ( ModuleFilter.checkFilterExist( filter, "(deliveryStateTimestamp ge" ) ) {
      ModuleFilter.getDateFromFilterQuery( filter, "ge ", "le ", ( startDate, endDate ) => {
        this.setState( {lastStatusStart: new Date( startDate ), lastStatusEnd: new Date( endDate )} )
        // this.state.lastStatusStart = new Date( startDate )
        //this.state.lastStatusEnd = new Date( endDate )

        andFilter["lastStatus"] = this.props.lastStatusQuery.replace( "###START###", startDate ).replace( "###END###", endDate )
      } );
    }

    for ( let i in filterItems ) {
      let item = filterItems[i]
      if ( item.operator === "OR" && ModuleFilter.checkFilterExist( filter, item.where ) ) {
        orFilter[item.label] = item.where
      }
      if ( item.operator === "AND" && ModuleFilter.checkFilterExist( filter, item.where ) ) {
        andFilter[item.label] = item.where
      }
    }
    this.createFilterString()
  }

  handleChangeDateRangeStatus( start, end ) {
    const {andFilter} = this.state;

    this.setState( {lastStatusStart: new Date( start ), lastStatusEnd: new Date( end )} )
    andFilter["lastStatus"] = this.props.lastStatusQuery.replace( "###START###", new Date( start ).toISOString() ).replace( "###END###", new Date( end ).toISOString() )
    this.createFilterString()
  }

  handleChangeDateStatus( date, item ) {
    date = new Date( date ).toISOString()
    const {andFilter} = this.state;

    if ( andFilter.hasOwnProperty( item.label ) ) {
      this.setState( {lastStatus: new Date()} )
      delete andFilter[item.label]
    } else {
      this.setState( {lastStatus: new Date( date )} )
      andFilter[item.label] = "(" + this.props.lastStatusQuery + date + ")"
    }
    this.createFilterString()
  }

  handleListTrackedDeliveries( item ) {
    const {andFilter, onlyTracked} = this.state;

    if ( andFilter.hasOwnProperty( item.label ) && onlyTracked ) {
      andFilter[item.label] = this.props.checkTrackerAllWhere
    } else {
      andFilter[item.label] = this.props.checkTrackerOnlyWhere
    }
    this.setState( {onlyTracked: ((!onlyTracked))} )

    this.createFilterString()
  }

  handelClickCheckBoxesFilter( e, item ) {
    const {orFilter, andFilter} = this.state;
    switch ( item.operator ) {
      case "OR":
        if ( orFilter.hasOwnProperty( item.label ) || !e.target.checked ) {
          delete orFilter[item.label]
        } else {
          orFilter[item.label] = item.where
        }
        break
      case "AND":
        if ( andFilter.hasOwnProperty( item.label ) || !e.target.checked ) {
          delete andFilter[item.label]
        } else {
          andFilter[item.label] = item.where
        }
        break
      default:
    }
    this.createFilterString()

  }

  deleteOldFilter() {
    const {orFilter, andFilter} = this.state;
    for ( let key in this.props.checkBoxes ) {
      let item = this.props.checkBoxes[key]
      switch ( item.operator ) {
        case "OR":
          if ( orFilter.hasOwnProperty( item.label ) ) {
            delete orFilter[item.label]
          }
          break
        case "AND":
          if ( andFilter.hasOwnProperty( item.label ) ) {
            delete andFilter[item.label]
          }
          break
        default:
      }
    }
  }

  setFilterSelected() {

  }

  handelClickCheckBoxesFilterMobile( items ) {
    const {orFilter, andFilter} = this.state;

    this.deleteOldFilter()

    for ( let key in items ) {
      let item = items[key]
      switch ( item.operator ) {
        case "OR":
          if ( !orFilter.hasOwnProperty( item.label ) ) {
            orFilter[item.label] = item.where
          }
          break
        case "AND":
          if ( !andFilter.hasOwnProperty( item.label ) ) {
            andFilter[item.label] = item.where
          }
          break
        default:
      }
    }
    this.createFilterString()

  }

  createFilterString() {
    const {orFilter, andFilter} = this.state;
    let orFilterString = "";
    let andFilterString = "";
    let filterString = ""


    let countOr = 0;
    let entryOrLength = Object.keys( orFilter ).length
    // eslint-disable-next-line no-unused-vars
    for ( const [key, value] of Object.entries( orFilter ) ) {
      if ( countOr === 0 ) {
        orFilterString = value
      }
      if ( countOr > 0 ) {
        orFilterString = orFilterString + " or " + value
      }
      countOr++
    }
    let countAnd = 0;
    let entryAndLength = Object.keys( andFilter ).length
    // eslint-disable-next-line no-unused-vars
    for ( const [key, value] of Object.entries( andFilter ) ) {
      if ( countAnd === 0 ) {
        andFilterString = value
      }
      if ( countAnd > 0 ) {
        andFilterString = andFilterString + " and " + value
      }
      countAnd++
    }

    if ( entryAndLength > 0 ) {
      filterString = "(" + andFilterString + ")"
    }
    if ( entryAndLength > 0 && entryOrLength > 0 ) {
      filterString = filterString + " and (" + orFilterString + ")"
    }
    if ( entryAndLength === 0 && entryOrLength > 0 ) {
      filterString = "(" + orFilterString + ")"
    }


    this.setState( {filterString: filterString} )
  }

  handleSetFilterButton() {
    const {filterString, orFilter, andFilter} = this.state
    this.props.handelFilterItems( filterString, orFilter, andFilter )
    $( "#filterCollapse" ).collapse( "toggle" )
  }

  getItemsFromCheckedCharacteristics() {
    const {checkBoxes} = this.props
    let returnIds = []
    for ( let key in checkBoxes ) {
      let item = checkBoxes[key]
      if ( item.hasOwnProperty( "checked" ) && item.checked ) {
        returnIds.push( item )
      }
    }
    return returnIds
  }

  handleChangeGroupByMode( mode ) {
    let {handleChangeGroupByMode} = this.props
    if ( handleChangeGroupByMode !== undefined )
      handleChangeGroupByMode( mode )
  }

  render() {
    let {onlyTracked, selectedItems} = this.state
    const {t, accordionId} = this.props;
    let {lastStatusStart, lastStatusEnd} = this.state
    const values = queryString.parse( this.props.location )
    if ( values.checkFilter !== undefined && values.checkFilter !== undefined && values.checkFilter.indexOf( this.props.checkTrackerOnlyWhere ) > -1 ) {
      //onlyTracked = true
    }
    if ( values.checkFilter !== undefined && values.checkFilter !== undefined && values.checkFilter.indexOf( this.props.checkTrackerAllWhere ) > -1 ) {
      //onlyTracked = false
    }
    return (
      <>
        <Accordion id={accordionId} className="">
          <Card className={"card--filter card__border-radius"}>
            <Accordion.Toggle as={Card.Header} variant="link"
                              className="card-header--transparent border-bottom-0 pb-2"
                              eventKey="0"
                              data-toggle="collapse"
                              data-target="#filterCollapse"
                              aria-expanded="true"
                              aria-controls="0">
              <div className="d-flex">
                <div className="pt-2 pr-2">
                  <FontAwesomeIcon icon={['fal', 'filter']} size="lg"/>
                </div>
                <div className="pt-1 pl-1 mb-0">
                  <h3>
                    {this.props.filterPanelTitle &&
                     this.props.filterPanelTitle
                    }
                    {!this.props.filterPanelTitle &&
                      t( 'component.ModuleFilter.filterPanelTitle' )
                    }
                  </h3>
                </div>
                <div className="ml-auto pt-2">
                  <FontAwesomeIcon icon={['fal', 'chevron-down']} size="lg"/>
                </div>
              </div>
            </Accordion.Toggle>
            <Accordion.Collapse eventKey="0"
                                id="filterCollapse"
                                aria-labelledby="1"
                                data-parent={"#" + accordionId}>
              <Card.Body className="pt-2">
                {this.props.withOnlyTrackedToggle &&
                 <Row className="mb-4">
                   <Col>
                     {onlyTracked ? (
                       <FontAwesomeIcon icon={['fal', 'toggle-on']} size="lg" onClick={(( e ) => this.handleListTrackedDeliveries( {label: "onlyTracked", operator: "AND", where: this.props.checkTrackerOnlyWhere, checked: onlyTracked} ))}/>
                     ) : (
                       <FontAwesomeIcon icon={['fal', 'toggle-on']} size="lg" className="fa-toggle-off--primary" transform="rotate-180"
                                        onClick={(( e ) => this.handleListTrackedDeliveries( {label: "onlyTracked", operator: "AND", where: this.props.checkTrackerOnlyWhere, checked: onlyTracked} ))}/>
                     )}
                     {this.props.onlyTrackedDeliveries &&
                      <span className="ml-1">{this.props.onlyTrackedDeliveries}</span>
                     }
                     {!this.props.onlyTrackedDeliveries &&
                      <span className="ml-1">{t( 'component.ModuleFilter.onlyTrackedDeliveries' )}</span>
                     }
                   </Col>
                 </Row>
                }
                {this.props.groupByEnable &&
                 <Row>
                   <Col>
                     <Form.Label>{t( 'component.ModuleFilter.groupByTitle' )}</Form.Label>
                     <div className="d-flex flex-column mb-4">
                       <ButtonGroup toggle aria-label="Gruppirung">
                         {
                           this.props.groupByButtons.map( ( item, index ) => {
                             return (
                               <ToggleButton type="radio" name="groupByBtn" variant={((this.props.groupByMode === item.groupby) ? "primary" : "outline-primary")} key={index} onClick={() => {
                                 this.handleChangeGroupByMode( item.groupby )
                               }}>{item.label}</ToggleButton>
                             );
                           } )
                         }
                       </ButtonGroup>
                     </div>
                   </Col>
                 </Row>
                }
                {this.props.groupByMode === "asset" &&
                 <>
                   <Row className="mb-4">
                     <Col>

                       {this.props.defineDateTimeTitle &&
                        <Form.Label>{this.props.defineDateTimeTitle}</Form.Label>
                       }
                       {!this.props.defineDateTimeTitle &&
                        <Form.Label>{t( 'component.ModuleFilter.defineDateTimeTitle' )}</Form.Label>
                       }
                       <DateRangePicker onChange={(( start, end, label ) => this.handleChangeDateRangeStatus( start, end, label, {label: "lastStatus", operator: "AND", where: this.props.lastStatusQuery} ))}
                                        startDate={lastStatusStart}
                                        endDate={lastStatusEnd}
                                        elementId={"data-time-range-select"}
                       />

                     </Col>
                   </Row>
                   {this.props.checkBoxes.length > 0 &&
                    <>
                      <Row className="mb-4">
                        <Col>
                          <Form.Label>{t( 'component.ModuleFilter.characteristicsTitle' )}</Form.Label>
                          {this.props.isMobile ? (
                            <>
                              <Select
                                defaultValue={this.getItemsFromCheckedCharacteristics()}
                                onChange={this.handelClickCheckBoxesFilterMobile.bind( this )}
                                options={this.props.checkBoxes}
                                placeholder={'search by tags'}
                                isMulti={true}
                                isSearchable={true}
                              />
                            </>
                          ) : (
                            <>
                              <Form role="form">
                                {
                                  this.props.checkBoxes.map( ( item, index ) => {
                                    const {checked} = item
                                    return (<Form.Check inline label={item.label}
                                                        type="checkbox" key={index}
                                                        onChange={(( e ) => this.handelClickCheckBoxesFilter( e, item ))}
                                                        data-operator={item.operator}
                                                        data-where={item.where}
                                                        defaultChecked={checked}
                                                        data-checked={checked}
                                                        className="bpw-checkbox bpw-checkbox-primary"
                                    />)
                                  } )
                                }
                              </Form>
                            </>
                          )}
                        </Col>
                      </Row>
                    </>
                   }

                 </>
                }
                <Row>
                  <Col>
                    <div className="d-flex flex-column flex-md-row flex-lg-row">
                      <div className="p-1 flex-fill">
                        <Button variant="light" onClick={this.props.handleOnClickRemoveFilter} block>
                          {this.props.btnSetFilterBack &&
                           this.props.btnSetFilterBack
                          }
                          {!this.props.btnSetFilterBack &&
                           t( 'component.ModuleFilter.btnSetFilterBack' )
                          }
                        </Button>

                      </div>
                      <div className="p-1 flex-fill">
                        <Button variant="primary" onClick={this.handleSetFilterButton.bind( this )} block>
                          {this.props.btnViewResult &&
                           this.props.btnViewResult
                          }
                          {!this.props.btnViewResult &&
                           t( 'component.ModuleFilter.btnViewResult' )
                          }
                        </Button>
                      </div>
                    </div>
                  </Col>
                </Row>
              </Card.Body>
            </Accordion.Collapse>
          </Card>
        </Accordion>
      </>
    );
  }
}

export default ModuleFilter