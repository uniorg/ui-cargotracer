import React, { Component } from 'react'
import { Button, Col, Row } from 'react-bootstrap'
import moment from "moment";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Link } from "react-router-dom";
/**
 *
 * @odata.context: "$metadata#Assets"
 assetCycleId: 115
 assetState: "ON_THE_WAY"
 assetStateTimestamp: "2018-07-30T18:19:29Z"
 assetTypeId: 1
 assetTypeName: "Achsgestelle"
 averageAtHomeTimeSeconds: 499314
 averageAtPoiTimeSeconds: 0
 averageCycleTimeSeconds: 1415987
 averageOnTheWayTimeSeconds: 0
 constructionYear: "2018"
 cycleCount: 3
 description: null
 deviceId: "207B85"
 homeAddresses: [{id: 2, keyLabel: "BPW", name: "BPW Bergische Achsen KG", postalCode: "51674", city: "Wiehl",…},…]
 0: {id: 2, keyLabel: "BPW", name: "BPW Bergische Achsen KG", postalCode: "51674", city: "Wiehl",…}
 city: "Wiehl"
 countryCode: "DE"
 geofence: {id: 2, latitude: 50.94652, longitude: 7.565246, radius: 5000, geofenceType: "POINT"}
 id: 2
 keyLabel: "BPW"
 locationSource: "UNKNOWN"
 locationSourceKey: "75b68e47-cad1-4ef6-9e8d-2d6cf2a4f1e0"
 name: "BPW Bergische Achsen KG"
 postalCode: "51674"
 street: "Ohlerhammer 1"
 streetNumber: null
 1: {id: 3, keyLabel: "BPW_UNGARN", name: "BPW-Hungária Kft.", postalCode: "9700", city: "Szombathely",…}
 id: 31
 isTracked: true
 keyLabel: "40"
 lastMaintenance: null
 lastSeenAt: "2018-09-07T19:31:26Z"
 lastSeenBatteryVoltage: null
 lastValidGeoPositionLatitude: 54.75877716666667
 lastValidGeoPositionLongitude: -6.4939816666666665
 lastValidGeoPositionRadius: null
 lastValidGeoPositionSentAt: "2018-09-07T19:31:26Z"
 lastValidGeoPositionSpeed: null
 lastValidGeoPositionTemperature: null
 lastValidGeoPositionType: "GPS"
 lastValidGeoPositionVibrationCount: 0
 lastValidGpsPositionLatitude: 54.75877716666667
 lastValidGpsPositionLongitude: -6.4939816666666665
 lastValidGpsPositionSentAt: "2018-09-07T19:31:26Z"
 name: "BPW 40"
 odataId: "31"
 poiAddresses: []
 version: "v01"
 vibrationCount: 0
 */
class TabSelectedCycle extends Component {

  viewOnMap(address) {
    let {id} = address
    this.props.setAddressCenter( address );
  }

  handleDeleteAsset() {
    const {history, asset} = this.props;
    fetch( '/asset-ui-service/asset/delete/' + asset.id, {
      method: 'DELETE',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    } )
      .then( ( res ) => {
        if ( res.status === 200 ) {
          history.push( {
                          pathname: "/assets"
                        } )
        } else {
          return false
        }
      } )
      .catch( ( error ) => {
        console.error( error );
        //ai.appInsights.trackException(error)
      } )
  }

  render() {
    const {t, asset} = this.props;
    const {vibrationCount, averageAtHomeTimeSeconds, poiAddresses, homeAddresses, averageAtPoiTimeSeconds, averageOnTheWayTimeSeconds, averageCycleTimeSeconds, cycleCount, deviceId, assetTypeName, lastSeenBatteryVoltage, lastSeenAt, lastMaintenance, constructionYear, version, description} = asset;
    return (
      <>
        <Row className="mt-4">
          <Col>
            <h5 className={""}>{t( 'sites.Asset.AssetDetail.header' )}:</h5>
          </Col>
        </Row>
        <Row className={"mt-3"}>
          <Col md={"6"}>
            <small className={"text-muted"}>{t( 'sites.Asset.AssetDetail.assetType' )}:</small>
            <h6 className={"text-primary text-uppercase"}>
              <strong>
                {assetTypeName &&
                 assetTypeName
                }
              </strong>
            </h6>
          </Col>
          <Col md={"6"}>
            <small className={"text-muted"}>
              {t( 'sites.Asset.AssetDetail.description' )}:
            </small>
            <h6 className={"text-primary"}>
              <strong>
                {description &&
                 description
                }
              </strong>
            </h6>
          </Col>
        </Row>
        <Row className={"mt-3"}>
          <Col md={"6"}>
            <small className={"text-muted"}>{t( 'sites.Asset.AssetDetail.constructionYear' )}:</small>
            <h6 className={"text-primary text-uppercase"}><strong>
              {constructionYear &&
               constructionYear
              }
            </strong></h6>
          </Col>
          <Col md={"6"}>
            <small className={"text-muted"}>{t( 'sites.Asset.AssetDetail.version' )}:</small>
            <h6 className={"text-primary text-uppercase"}>
              <strong>
                {version &&
                 version
                }
              </strong>
            </h6>
          </Col>
        </Row>
        {lastMaintenance &&
         <Row className={"mt-3"}>
           <Col md={"4"}>
             <small className={"text-muted"}>{t( 'sites.Asset.AssetDetail.lastMaintenance' )}:</small>
             <h6 className={"text-primary text-uppercase"}><strong>{moment( lastMaintenance ).format( 'LLL' )}</strong></h6>
           </Col>
         </Row>
        }
        <Row className={"mt-3"}>
          <Col>
            <hr/>
          </Col>
        </Row>
        <Row className="mb-2 mt-4 mt-md-3">
          <Col md={"6"}>
            <h5>
              {t( 'sites.Asset.AssetDetail.homeAddresses' )}
            </h5>
            <Row className="dl__info--contact mt-4">
              {
                homeAddresses.map( ( item, index ) => {
                  const {name, street, streetNumber, postalCode, city} = item
                  return (
                    <Col md={"12"} key={"homeAddresses-" + index}>
                      <dl>
                        <dt>
                          <i className="fas fa-circle cursor-pointer mr-1" style={{fontSize: '1rem',color: '#002851'}} onClick={() => {this.viewOnMap(item)}}></i>
                          {name &&
                           name
                          }
                        </dt>
                        <dd>
                          {street &&
                           street
                          }
                          {streetNumber &&
                           " " + streetNumber
                          },&nbsp;
                          {postalCode &&
                           postalCode + " "
                          }
                          {city &&
                           city
                          }
                        </dd>
                      </dl>
                    </Col>
                  );
                } )
              }
            </Row>
          </Col>

          <Col md={"6"} className="mt-4 mt-md-0">
            <h5>
              {t( 'sites.Asset.AssetDetail.pointOfInterests' )}
            </h5>
            <Row className="dl__info--contact mt-4">
              {
                poiAddresses.map( ( item, index ) => {
                  const {name, street, streetNumber, postalCode, city} = item
                  return (
                    <Col md={"12"} key={"poiAddresses-" + index}>
                      <dl>
                        <dt>
                          <i className="fas fa-circle cursor-pointer mr-1" style={{fontSize: '1rem', color: '#CDDC4B'}} onClick={() => {this.viewOnMap(item)}}></i>
                          {name &&
                           name
                          }
                        </dt>
                        <dd>
                          {street &&
                           street
                          }
                          {streetNumber &&
                           " " + streetNumber
                          },&nbsp;
                          {postalCode &&
                           postalCode + " "
                          }
                          {city &&
                           city
                          }
                        </dd>
                      </dl>
                    </Col>
                  );
                } )
              }
            </Row>
          </Col>
        </Row>

        <Row>
          <Col>
            <hr className={""}/>
          </Col>
        </Row>

        <Row className="mt-4">
          <Col>
            <h5 className={""}>
              {t( 'sites.Asset.AssetDetail.statistics' )}:
            </h5>
          </Col>
        </Row>
        <Row className={"mt-3"}>
          <Col md={"4"}>
            <small className={"text-muted"}>
              {t( 'sites.Asset.AssetDetail.cycleCount' )}:
            </small>
            <h6 className={"text-primary text-uppercase"}>
              <strong>
                {cycleCount &&
                 cycleCount
                }
              </strong>
            </h6>
          </Col>
          <Col md={"4"}>
            <small className={"text-muted"}>
              {t( 'sites.Asset.AssetDetail.averageCycleTimeSeconds' )}:
            </small>
            <h6 className={"text-primary text-uppercase"}>
              <strong>
                {averageCycleTimeSeconds &&
                 parseInt( averageCycleTimeSeconds / 86400 ) + " " + t( 'sites.Asset.AssetDetail.days' )
                }
              </strong>
            </h6>
          </Col>
          <Col md={"4"}>
            <small className={"text-muted"}>
              {t( 'sites.Asset.AssetDetail.averageOnTheWayTimeSeconds' )}:
            </small>
            <h6 className={"text-primary text-uppercase"}>
              <strong>
                {averageOnTheWayTimeSeconds &&
                 parseInt( averageOnTheWayTimeSeconds / 86400 ) + " " + t( 'sites.Asset.AssetDetail.days' )
                }
              </strong>
            </h6>
          </Col>
        </Row>
        <Row className={"mt-3"}>
          <Col md={"4"}>
            <small className={"text-muted"}>{t( 'sites.Asset.AssetDetail.vibrationCount' )}:</small>
            <h6 className={"text-primary text-uppercase"}>
              <strong>
                {vibrationCount &&
                 vibrationCount
                }
              </strong>
            </h6>
          </Col>
          <Col md={"4"}>
            <small className={"text-muted"}>
              {t( 'sites.Asset.AssetDetail.averageAtHomeTimeSeconds' )}:
            </small>
            <h6 className={"text-primary text-uppercase"}>
              <strong>
                {averageAtHomeTimeSeconds &&
                 parseInt( averageAtHomeTimeSeconds / 86400 ) + " " + t( 'sites.Asset.AssetDetail.days' )
                }
              </strong>
            </h6>
          </Col>
          <Col md={"4"}>
            <small className={"text-muted"}>
              {t( 'sites.Asset.AssetDetail.averageAtPoiTimeSeconds' )}:
            </small>
            <h6 className={"text-primary text-uppercase"}>
              <strong>
                {averageAtPoiTimeSeconds &&
                 parseInt( averageAtPoiTimeSeconds / 86400 ) + " " + t( 'sites.Asset.AssetDetail.days' )
                }
              </strong>
            </h6>
          </Col>
        </Row>
        <Row className={"mt-3"}>
          <Col>
            <hr/>
          </Col>
        </Row>
        <Row>
          <Col md="12">
            <div className="d-flex">
              <div className="pt-2">
                <FontAwesomeIcon icon={['fal', 'bullseye']} size="sm" className={"text-primary"}/>
              </div>
              <div className="p-2">
                <h5 className={"text-primary"}>146</h5>
              </div>
            </div>
          </Col>
        </Row>
        <Row className={"mb-4"}>
          <Col md="4" className="mb-2">
            <div className="d-flex">
              <div className="pt-2 pr-1">
                <FontAwesomeIcon icon={['fal', 'info-circle']} size="2x" className={"text-primary"}/>
              </div>
              <div className="p-2">
                <h6 className={"text-primary"}>{t( 'sites.Asset.AssetDetail.deviceId' )}</h6>
                <p>
                  {deviceId &&
                   deviceId
                  }
                </p>
              </div>
            </div>
          </Col>
          <Col md="4" className="mb-2">
            <div className="d-flex">
              <div className="pt-2 pr-1">
                <FontAwesomeIcon icon={['far', 'cloud-download-alt']} size="2x" className={"text-primary"}/>
              </div>
              <div className="p-2">
                <h6 className={"text-primary"}>{t( 'sites.Asset.AssetDetail.lastTransmission' )}</h6>
                <p>
                  {lastSeenAt &&
                   moment( new Date( lastSeenAt ) ).format( 'lll' )
                  }
                </p>
              </div>
            </div>
          </Col>
          <Col md="4" className="mb-2">
            <div className="d-flex">
              <div className="pt-2 pr-1">
                <FontAwesomeIcon icon={['fal', 'battery-half']} size="2x" className={"text-primary"}/>
              </div>
              <div className="p-2">
                <h6 className={"text-primary"}>{t( 'sites.Asset.AssetDetail.battery' )}</h6>
                <p>
                  {lastSeenBatteryVoltage &&
                   lastSeenBatteryVoltage + " mV"
                  }
                </p>
              </div>
            </div>
          </Col>
        </Row>
        <Row className="mb-5">
          <Col>
            <div className="d-flex flex-column flex-md-row flex-lg-row">
              <div className="p-1 flex-fill">
                <Button size={"md"} variant="danger" type="submit" block onClick={this.handleDeleteAsset.bind(this)}>{t( 'sites.Asset.AssetDetail.delete' )}</Button>
              </div>
              <div className="p-1 flex-fill">
                <Link to={`/update/asset/` + asset.id}>
                  <Button size={"md"} variant="primary" type="submit" block>{t( 'sites.Asset.AssetDetail.edit' )}</Button>
                </Link>
              </div>
            </div>
          </Col>
        </Row>
      </>
    )
  }
}

export default TabSelectedCycle