import React, { Component } from 'react';
import { Col, Form, FormControl } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

class AssetInputCheckKeyLabel extends Component {
  timeOut = null
  static defaultProps = {
    readOnly: false,
    placeholder: ""
  }

  constructor( ...args ) {
    super( ...args );
    this.state = {
      isLoading: false,
      isInvalid: null,
      isValid: null,
      isEmpty: true
    };
  }

  async handleChangeInput( e ) {
    let value = e.target.value

    if ( this.props.handleChangeInput !== undefined )
      this.props.handleChangeInput( e )

    const {handleValidate} = this.props;
    if ( value === undefined || value === null || value === "" ) {
      if ( handleValidate !== undefined )
        handleValidate( null, true )
      this.changeValid( undefined, undefined )
      this.setState( {isEmpty: true, isLoading: false} )
      return
    }
    this.changeValid( false, false )
    this.setState( {isEmpty: false} )


    if ( this.timeOut !== null )
      window.clearTimeout( this.timeOut );
    await this.fetchInputValue( value, ( id, isExist ) => {

      let {isEmpty} = this.state
      if ( isEmpty ) {
        if ( handleValidate !== undefined )
          handleValidate( null, true )
        this.changeValid( null, null )
        this.setState( {isLoading: false} )
      } else {
        if ( handleValidate !== undefined )
          handleValidate( id, isExist )
        this.changeValid( isExist, ((!isExist)) )
        this.setState( {isLoading: false} )
      }
    } )
    //handleValidate( e )
  }

  changeValid( isInvalid, isValid ) {
    const {changeValid} = this.props;

    if ( changeValid !== undefined ) {
      changeValid( isInvalid, isValid )
      return
    }

    this.setState( {isInvalid: isInvalid, isValid: isValid} )
  }

  getValid() {
    const {keyLabelInputValid} = this.props;
    const {isValid} = this.state;

    if ( keyLabelInputValid !== undefined )
      return keyLabelInputValid

    return isValid

  }

  getInvalid() {
    const {keyLabelInputInvalid} = this.props;
    const {isInvalid} = this.state;

    if ( keyLabelInputInvalid !== undefined )
      return keyLabelInputInvalid

    return isInvalid
  }

  fetchInputValue( value, callback ) {
    this.timeOut = window.setTimeout( () => {
      this.setState( {isLoading: true} )
      fetch( '/asset-ui-service/asset/checkIsExist/' + value )
        .then( res => res.json() )
        .then( ( data ) => {
          if ( data.hasOwnProperty( "value" ) ) {
            return data.value
          } else {
            return false
          }
        } )
        .then( ( isExist ) => {
          callback( value, isExist )
        } )
        .catch( ( error ) => {
          console.error( error );
          //ai.appInsights.trackException(error)
          callback( null, false )
        } )
    }, 2500 )
  }


  render() {
    const {t} = this.props;
    const {isLoading} = this.state;
    return (
      <Form.Group as={Col} md={"6"} controlId="formGridTracker" className={"delivery-tracker-input-check-delivery"}>
        {isLoading ? <FontAwesomeIcon icon={['fal', 'spinner']} size="lg" spin/> : null}<Form.Label>{t( 'sites.Asset.assetCreate.assetId' )}</Form.Label>
        <div className="mb-3">
          <FormControl
            type="text"
            min="1"
            defaultValue={this.props.defaultValue}
            size={"lg"}
            placeholder={this.props.placeholder}
            aria-label={this.props.placeholder}
            aria-describedby="hashtag"
            onChange={e => this.handleChangeInput( e )}
            isInvalid={this.getInvalid()}
            isValid={this.getValid()}
            disabled={isLoading}
            className={isLoading ? "alert-warning" : null}
            readOnly={this.props.readOnly}
          />
          {(this.props.defaultValue !== "" && this.props.defaultValue !== null) &&
           <Form.Control.Feedback type="invalid">
             {t( 'sites.Delivery.deliveryCreate.errorMessage' )}
           </Form.Control.Feedback>
          }
        </div>
      </Form.Group>
    );
  }
}

export default AssetInputCheckKeyLabel