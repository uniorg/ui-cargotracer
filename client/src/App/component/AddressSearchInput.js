import React, { Component } from 'react';
import { Col, Form, FormControl, Row } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import $ from 'jquery';
import InputGroup from "react-bootstrap/es/InputGroup";

class AddressSearchInput extends Component {

  static defaultProps = {
    defaultCol: "6",
    isUpdate: false
  }

  constructor( ...args ) {
    super( ...args );

    this.state = {
      isInvalid: undefined,
      isValid: undefined,
      isSet: false,
      listClassName: "delivery-create--address-hide",
      clearBtnVisible: false,
      addresses: [],
      searchInputValue: "",
      selectedItem: null,
      selectedItemRadius: 5000,

      newAddress: {
        keyLabel: null,
        name: null,
        postalCode: null,
        postalCodeBox: null,
        city: null,
        street: null,
        streetNumber: null,
        countryCode: null,
        locationSource: null,
        locationSourceKey: null,
        geofence: {
          latitude: null,
          longitude: null,
          radius: 10000,
          type: "POINT"
        }
      }
    };
  }

  componentDidMount() {
    if ( this.props.isUpdate === true ) {
      this.setAddress()
    }
  }

  handleChangeInput( e ) {
    // eslint-disable-next-line no-unused-vars
    const {handleValidate} = this.props;
    this.setState( {addresses: [], listClassName: "delivery-create--address-hide", isInvalid: false, isValid: false, clearBtnVisible: ((e.target.value !== null && e.target.value !== undefined && e.target.value !== "") ? true : false)} )
    this.fetchInputValue( e, ( addresses ) => {
      this.setState( {addresses: addresses, listClassName: ((addresses.length > 0) ? "delivery-create--address-show" : "delivery-create--address-hide")} )
    } )
  }

  fetchInputValue( e, callback ) {
    if ( e.target.value !== null && e.target.value !== undefined && e.target.value !== "" ) {


      fetch( '/azure/location/search/address/json?query=' + e.target.value + '&typeahead=true&extendedPostalCodesFor=Addr&countrySet=BE,EL,LT,PT,BG,ES,LU,RO,CZ,FR,HU,SI,DK,HR,MT,SK,DE,IT,NL,FI,EE,CY,AT,SE,IE,LV,PL,UK,IS,NO,LI,CH,RU&api-version=1.0&subscription-key=undefined&minFuzzyLevel=4&language=de-DE&&maxFuzzyLevel=4&limit=20' )
        .then( res => res.json() )
        .then( ( data ) => {
          if ( data.hasOwnProperty( "results" ) ) {
            return data.results
          } else {
            return false
          }
        } )
        .then( ( results ) => {
          callback( results )
        } )
        .catch( ( error ) => {
          console.error( error );
          // ai.appInsights.trackException(error)
          callback( false )
        } )
    }
  }

  handleRemoveSearch() {
    const {inputId} = this.props;
    $( "#" + inputId ).val( "" )
    this.setState( {addresses: [], listClassName: "delivery-create--address-hide", isInvalid: true, isValid: false, clearBtnVisible: false, isSet: false, selectedItemRadius: 5000} )
  }

  handleClickOnAddress( item ) {
    console.log( item )
    console.log( this.props.item )
    const {selectedItemRadius} = this.state;
    const {handleClickOnAddress, inputId} = this.props;
    const {address, position, id} = item;
    const {streetName, streetNumber, postalCode, municipality, countryCode} = address;
    const {lat, lon} = position;

    let streetNumberChecked = ((streetNumber !== undefined) ? " " + streetNumber : "")
    $( "#" + inputId ).val( streetName + streetNumberChecked + ", " + postalCode + " " + municipality )

    let createNewAddress = {
      keyLabel: id,
      name: "",
      postalCode: postalCode,
      city: municipality,
      street: streetName,
      streetNumber: streetNumber,
      countryCode: countryCode,
      locationSource: "azure",
      locationSourceKey: id,
      geofence: {
        latitude: lat,
        longitude: lon,
        radius: selectedItemRadius,
        geofenceType: "POINT"
      }
    };
    if ( this.props.item !== undefined && this.props.item.tempId !== undefined )
      createNewAddress.tempId = this.props.item.tempId
    if ( this.props.item !== undefined && this.props.item.isValid !== undefined )
      createNewAddress.isValid = this.props.item.isValid

    this.setState( {selectedItem: item, addresses: [], listClassName: "delivery-create--address-hide", isInvalid: false, isValid: true, newAddress: createNewAddress, isSet: true} )
    if ( handleClickOnAddress !== undefined ) {
      handleClickOnAddress( item, selectedItemRadius, createNewAddress )
    }
  }

  setAddress() {
    const {inputId, item} = this.props;
    const {street, streetNumber, city, postalCode, geofence} = item;
    const {radius} = geofence;

    let streetNumberChecked = ((streetNumber !== undefined && streetNumber !== null) ? " " + streetNumber : "")
    $( "#" + inputId ).val( street + streetNumberChecked + ", " + postalCode + " " + city )

    this.setState( {selectedItem: item, addresses: [], listClassName: "delivery-create--address-hide", isInvalid: false, isValid: true, newAddress: item, isSet: true, selectedItemRadius: radius} )
  }

  handleChangeRadius( e ) {
    const {handleChangeRadius} = this.props;
    const {selectedItem, newAddress} = this.state;
    newAddress.geofence.radius = parseInt( e.target.value )
    this.setState( {selectedItemRadius: e.target.value, newAddress: newAddress} )
    if ( handleChangeRadius !== undefined && selectedItem !== null ) {
      handleChangeRadius( selectedItem, parseInt( e.target.value ), newAddress )
    }
  }

  handeGoToMarker() {
    const {handeGoToMarker} = this.props;
    const {selectedItem, newAddress, selectedItemRadius} = this.state;
    if ( handeGoToMarker !== undefined && selectedItem !== null ) {
      handeGoToMarker( selectedItem, selectedItemRadius, newAddress )
    }
  }

  render() {
    const {t, inputId, placeholder, icon, title, className, handeGoToMarker} = this.props;
    const {listClassName, addresses, clearBtnVisible, selectedItem, selectedItemRadius, searchInputValue, isSet, isInvalid, isValid} = this.state;
    return (
      <Form.Group as={Col} md={this.props.defaultCol} controlId={inputId}
                  className={"address-search-input " + ((className !== undefined) ? className : "") + ((isValid !== undefined) ? (isValid ? " form-control--select-valid" : " form-control--select-invalid") : "")}>
        <Form.Label>{title}</Form.Label>
        <InputGroup className="mb-3">
          <FormControl
            controlid={inputId}
            min="1"
            required="required"
            aria-required="true"
            autoComplete="off"
            type="text"
            size={"lg"}
            className="border-right-0"
            placeholder={placeholder}
            aria-label={placeholder}
            aria-describedby="map-marker-alt"
            onChange={e => this.handleChangeInput( e )}
            isInvalid={isInvalid}
            isValid={isValid}
            defaultValue={searchInputValue}
            readOnly={this.props.isUpdate}
          />
          <InputGroup.Append>
            <InputGroup.Text id="location-arrow" className={"cursor-pointer input-group-append--transparent border-left-0 border-right-0 " + ((clearBtnVisible) ? "show" : "hide")} onClick={this.handleRemoveSearch.bind( this )}>
              <FontAwesomeIcon icon={['fal', 'times']} className="text-muted" size="lg"/>
            </InputGroup.Text>
            <InputGroup.Text id="map-marker-alt" className={((handeGoToMarker !== undefined && selectedItem !== null) ? "cursor-pointer " : "") + "input-group-append--transparent rounded-right border-left-0"}
                             onClick={this.handeGoToMarker.bind( this )}>
              <FontAwesomeIcon icon={icon} className="text-muted" size="lg"/>
            </InputGroup.Text>
          </InputGroup.Append>
          <Form.Control.Feedback type="invalid">
            {t( 'sites.Delivery.deliveryCreate.errorMessageAddress' )}
          </Form.Control.Feedback>
        </InputGroup>
        <Col md={"12"} className={"delivery-create delivery-create--address " + listClassName + " shadow bg-white rounded"}>
          <Row className="card">
            <div className={""}>
              <ul className="list-group list-group-flush">
                {

                  addresses.map( ( item, index ) => {
                    const {address} = item;
                    const {streetName, postalCode, streetNumber, municipality, countryCodeISO3} = address
                    let streetNumberChecked = ((streetNumber !== undefined) ? " " + streetNumber : "")
                    if ( streetName !== undefined && postalCode !== undefined && municipality !== undefined ) {

                      return (
                        <li className="list-group-item card-body" key={index}>
                          <Row>
                            <Col md={"8"} sm={"12"}>
                              <h5 className="card-title">{municipality} ({countryCodeISO3})</h5>
                              <p className="card-text">{streetName}{streetNumberChecked}, {postalCode} {municipality}</p>
                            </Col>
                            <Col md={"4"} sm={"12"}>
                              <button className="btn btn-outline-primary btn-block mt-3" onClick={( e ) => {
                                this.handleClickOnAddress( item, e )
                              }}>Auswählen
                              </button>
                            </Col>
                          </Row>
                        </li>
                      );
                    }
                    return null;
                  } )
                }
              </ul>
            </div>
          </Row>
        </Col>
        <Form.Group>
          <Form.Label>{t( 'sites.Delivery.deliveryCreate.defineRadius' )}: {selectedItemRadius}m</Form.Label>
          <Form.Control
            type="range"
            className="custom-range border-0 mb-3"
            id="customRange1"
            min="500"
            max="10000"
            step="500"
            value={selectedItemRadius}
            data-slider-enabled="false"
            onChange={this.handleChangeRadius.bind( this )}
            disabled={((!isSet))}
          />
        </Form.Group>
      </Form.Group>
    );
  }
}

export default AddressSearchInput