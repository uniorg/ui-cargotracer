import React, { Component } from 'react';
import { Button, Card, Col, Row } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Link } from "react-router-dom";
import moment from 'moment';
import 'moment/locale/en-gb';
import 'moment/locale/de';
import { Helper } from '../util/Helper'
import queryString from "query-string";

moment().locale( navigator.language )

class AssetListItem extends Component {

  goToDetail() {
    const {history, url, item} = this.props;

    let pushLocation = {
      pathname: url + "/" + item.id,
      state: {lastLocation: history.location}
    }
    console.log(pushLocation)
    history.push( pushLocation)
  }

  render() {
    let {item, t, url, history} = this.props
    console.log("history", history)
    /**
     * @search.score: 1
     assetState: "AT_HOME"
     assetStateTimestamp: "2019-01-31T10:05:37Z"
     assetTypeId: 879
     assetTypeName: "Achsgestelle"
     averageAtHomeTimeSeconds: 998471
     averageAtPoiTimeSeconds: 0
     averageCycleTimeSeconds: 3327359
     averageOnTheWayTimeSeconds: 0
     constructionYear: "2018"
     cycleCount: 4
     deleted: false
     description: null
     deviceId: null
     id: "631"
     keyLabel: "118"
     lastMaintenance: null
     lastSeenAt: "2019-01-31T10:05:37Z"
     lastSeenBatteryVoltage: null
     lastValidGeoPositionLatitude: 50.945153
     lastValidGeoPositionLongitude: 7.569763
     lastValidGeoPositionRadius: null
     lastValidGeoPositionSentAt: "2019-01-31T10:05:37Z"
     lastValidGeoPositionSpeed: null
     lastValidGeoPositionTemperature: null
     lastValidGeoPositionType: "GPS"
     lastValidGeoPositionVibrationCount: 0
     lastValidGpsPositionLatitude: 50.945153
     lastValidGpsPositionLongitude: 7.569763
     lastValidGpsPositionSentAt: "2019-01-31T10:05:37Z"
     name: "BPW 118"
     version: "v01"
     vibrationCount: 16
     */
    return (
      <div className="col-12 col-md-6 mb-4">
        <Card>
          <Card.Body>
            <div className="d-flex mb-3">
              <div className="pt-2 bd-highlight cursor-pointer" onClick={this.goToDetail.bind(this)}>
                <FontAwesomeIcon icon={['fal', 'bullseye']} size="lg" className="text-primary"/>
              </div>
              <div className="p-2">

                <Card.Title onClick={this.goToDetail.bind(this)} className={"cursor-pointer"}>{item.name}</Card.Title>
              </div>

              {/*<div className="ml-auto p-2">
                <EtaPopver item={item} isLate={item.isLate}/>
              </div>*/}

              <div className="ml-auto p-2 cursor-pointer">
                <Link to={`update/asset/` + item.id}>
                  <FontAwesomeIcon icon={['fal', 'edit']} size="lg"/>
                </Link>
              </div>
              {item.deviceId === null ?
                (
                  <div className="p-2">
                    <FontAwesomeIcon icon={['fa', 'bookmark']} size="lg" color={"red"}/>
                  </div>
                ) : (
                  <div className="p-2">
                    <FontAwesomeIcon icon={['fa', 'bookmark']} size="lg" color={"green"}/>
                  </div>
                )}
            </div>
            <Row className="mb-2">
              <Col className="col-md-6">
                <Row>
                  <Col>
                    <h6 className="text-muted">
                      {t( 'sites.Asset.assetItem.cycleCount' )}
                    </h6>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <h5 className="display-5">
                      {item.cycleCount &&
                       item.cycleCount
                      }
                    </h5>
                  </Col>
                </Row>
              </Col>
              <Col className="col-md-6">
                <Row>
                  <Col>
                    <h6 className="text-muted">
                      {t( 'sites.Asset.assetItem.vibrationCount' )}
                    </h6>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <h5 className="display-5">
                      {item.vibrationCount &&
                       item.vibrationCount
                      }
                    </h5>
                  </Col>
                </Row>
              </Col>
            </Row>
            <Row className="mb-2">
              <Col className="col-md-6">
                <Row>
                  <Col>
                    <h6 className="text-muted">
                      {t( 'sites.Asset.assetItem.assetState' )}
                    </h6>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <h5 className="display-6">
                      {item.assetState &&
                       Helper.getStateAsString( t, item.assetState )
                      }
                    </h5>
                  </Col>
                </Row>
              </Col>
              <Col className="col-md-6">
                <Row>
                  <Col>
                    <h6 className="text-muted">
                      {t( 'sites.Asset.assetItem.lastSeenBatteryVoltage' )}
                    </h6>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <h5 className="display-6">
                      {item.lastSeenBatteryVoltage &&
                       item.lastSeenBatteryVoltage + " mV"
                      }
                    </h5>
                  </Col>
                </Row>
              </Col>
            </Row>
            <Button variant="primary" block onClick={this.goToDetail.bind(this)}>
              {t( 'sites.Asset.assetItem.goToDetail' )}</Button>
          </Card.Body>
        </Card>
      </div>
    );
  }
}

export default AssetListItem