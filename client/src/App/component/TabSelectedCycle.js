import React, { Component } from 'react'
import { Col, Row, Table } from 'react-bootstrap'
import moment from "moment";
import { Helper } from '../util/Helper'
import BpwLoader from "./BpwLoader";

class TabSelectedCycle extends Component {

  render() {
    const {t, cycle} = this.props;


    if ( cycle === null )
      return <BpwLoader/>
    let {atHomeTimeSeconds} = cycle;
    let atHomeTimeSecondsDuration = moment.duration( atHomeTimeSeconds, "seconds" );
    let atHomeTimeSecondsDays = Number( atHomeTimeSecondsDuration.get( 'days' ) ).toFixed( 0 );
    let atHomeTimeSecondsHours = Number( atHomeTimeSecondsDuration.get( 'hours' ) ).toFixed( 0 );
    let atHomeTimeSecondsMinutes = Number( atHomeTimeSecondsDuration.get( 'minutes' ) ).toFixed( 0 );

    return (
      /**
       * @odata.context: "$metadata#AssetCycles"
       atHomeTimeSeconds: 303565
       cycleEvents: [{id: 310, eventState: "AT_HOME", eventStateStart: "2018-07-27T06:00:04Z",…},…]
       cycleNumber: 3
       cycleState: "ON_THE_WAY"
       cycleStateTimestamp: "2018-07-30T18:19:29Z"
       cycleTimeSeconds: null
       deviceId: "207B85"
       estimatedDistance: null
       finishedAt: null
       homeAddresses: [{id: 2, keyLabel: "BPW", name: "BPW Bergische Achsen KG", postalCode: "51674", city: "Wiehl",…},…]
       id: 115
       odataId: "115"
       poiAddresses: []
       startedAt: "2018-07-27T06:00:04Z"
       vibrationCount: 0
       */
      <>
        <Row className={"mt-4"}>
          <Col md="6" className="mb-3">
            <h5 className={""}>{t( 'sites.Asset.AssetDetail.circulation' )}: <span className="text-primary">
                              {cycle.cycleNumber &&
                               cycle.cycleNumber
                              }
                            </span></h5>
            <h6 className={""}>{t( 'sites.Asset.AssetDetail.circulationDuration' )}: <span className="text-primary">
                             {cycle.cycleState &&
                              Helper.getStateAsString( t, cycle.cycleState )
                             }
                           </span></h6>
            <h6 className={""}>{t( 'sites.Delivery.deliveryDetail.tour' )}: <span className="text-primary">
                             {cycle.cycleTimeSeconds &&
                              parseInt( cycle.cycleTimeSeconds / 86400 ) + " " + t( 'sites.Asset.AssetDetail.days' )
                             }
                           </span></h6>
          </Col>
          <Col md="6" className="mb-3 border-left-0 border-md-left border-primary">
            <h5 className={""}>{t( 'sites.Asset.AssetDetail.circulationData' )}:</h5>
            <h6>{t( 'sites.Asset.AssetDetail.averageAtHomeTimeSeconds' )}:
              <span className="text-primary">
                             {cycle.atHomeTimeSeconds &&
                              ((atHomeTimeSecondsDays !== "0") ? atHomeTimeSecondsDays + " " + t( 'sites.Asset.AssetDetail.days' ) + " " : " ") +
                              ((atHomeTimeSecondsHours !== "0") ? atHomeTimeSecondsHours + " " + t( 'sites.Asset.AssetDetail.hours' ) : " ") +
                              " " +
                              ((atHomeTimeSecondsMinutes !== "0") ? atHomeTimeSecondsMinutes + " " + t( 'sites.Asset.AssetDetail.minutes' ) : "")
                             }
                             </span>
            </h6>
            <h6>{t( 'sites.Asset.AssetDetail.estimatedDistance' )}:
              <span className="text-primary">
                             {cycle.estimatedDistance &&
                              " " + cycle.estimatedDistance + " " + t( 'sites.Asset.AssetDetail.kmEstimated' )
                             }
                             </span>
            </h6>
            <h6 className={"mb-0"}>{t( 'sites.Asset.AssetDetail.vibrationCount' )}:
              <span className="text-primary">
                             {cycle.vibrationCount !== 0 &&
                              " " + cycle.vibrationCount
                             }
                {cycle.vibrationCount === 0 &&
                 " " + 0
                }
                             </span>
            </h6>
          </Col>
        </Row>
        <Row>
          <Col>
            <hr className={""}/>
          </Col>
        </Row>
        <Row>
          <Col>
            <h5 className={""}>{t( 'sites.Asset.AssetDetail.circulationDetail' )}:</h5>
          </Col>
        </Row>
        <Row>
          <Col>
            <Table responsive borderless hover>
              <thead>
              <tr>
                <th>{t( 'sites.Asset.AssetDetail.status' )}</th>
                <th>{t( 'sites.Asset.AssetDetail.start' )}</th>
                <th>{t( 'sites.Asset.AssetDetail.end' )}</th>
                <th>{t( 'sites.Asset.AssetDetail.duration' )}</th>
                <th>{t( 'sites.Asset.AssetDetail.vibration' )}</th>
              </tr>
              </thead>
              <tbody>
              {
                cycle.cycleEvents.map( ( item, index ) => {
                  let {eventState, address, eventStateStart, eventStateEnd, eventDurationSeconds, vibrationCount} = item
                  return (
                    <tr key={"cycleEvents-" + index}>
                      <td>
                        <strong className="text-primary">{Helper.getStateAsString( t, eventState )}</strong><br/>
                        {address &&
                         <address>
                           <strong>{address.name}</strong><br/>
                           {address.street} {address.streetNumber}<br/>
                           {address.postalCode} {address.city}<br/>
                         </address>
                        }
                      </td>
                      <td>
                        {eventStateStart &&
                         moment( new Date( eventStateStart ) ).format( 'lll' )
                        }
                      </td>
                      <td>
                        {eventStateEnd &&
                         moment( new Date( eventStateEnd ) ).format( 'lll' )
                        }
                      </td>
                      <td>
                        {eventDurationSeconds &&
                         parseInt( eventDurationSeconds / 86400 ) + " " + t( 'sites.Asset.AssetDetail.days' )
                        }
                      </td>
                      <td>
                        {vibrationCount &&
                         vibrationCount
                        }
                      </td>
                    </tr>
                  );
                } )
              }
              </tbody>
            </Table>
          </Col>
        </Row>
        <Row>
          <Col>
            <hr/>
          </Col>
        </Row>
      </>
    )
  }
}

export default TabSelectedCycle