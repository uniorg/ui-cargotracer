import React, { Component } from 'react';
import { ButtonToolbar, Overlay, OverlayTrigger, Popover, Tooltip } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default class UserManagementPopOver extends Component {
  constructor( props, context ) {
    super( props, context );

    this.handleClick = ( {target} ) => {
      this.setState( s => ({target, show: !s.show}) );
    };

    this.state = {
      show: false,
    };
  }

  render() {
    return (
      <ButtonToolbar className={"search-popover "}>
        <OverlayTrigger overlay={<Tooltip id="tooltip-searhfield">Informationen</Tooltip>}>
          <span className="d-inline-block">
            <FontAwesomeIcon icon={['fal', 'info-circle']}
                             className={"cursor-pointer mr-1 text-muted"}
                             transform="down-1"
                             size="md"
                             onClick={this.handleClick}/>
          </span>
        </OverlayTrigger>
        <Overlay
          show={this.state.show}
          target={this.state.target}
          placement="auto"
          container={this}
          containerPadding={20}
        >
          <Popover className="search-popover-contained" title="User Management">
            <p>Lorem ipsum dolor sit consectetur adipiscing elit, sed do eiusmod tempor incididunt </p>
          </Popover>
        </Overlay>
      </ButtonToolbar>
    );
  }
}