import React, { Component } from 'react';
import moment from "moment";

class DeviceMarker extends Component {

  render() {
    let {lastValidGeoPositionLatitude, lastValidGeoPositionLongitude, name, deviceId, assetTypeName, lastValidGeoPositionType, id, lastValidGeoPositionSentAt} = this.props.item
    return (
      <div className='row m-3'>
        <div className='col-12 cursor-pointer'><a href={'/devices/' + deviceId}><h5>{name}</h5></a></div>
        <div className='col-12'>Device Id: {deviceId}</div>
        <div className='col-12'>Typ: {lastValidGeoPositionType}</div>
        <div className='col-12'>SentAt: {moment( new Date( lastValidGeoPositionSentAt ) ).format( 'LLL' )}</div>
      </div>
    )
  }
}

export default DeviceMarker
