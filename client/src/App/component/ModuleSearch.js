import React, { Component } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Button, Col, Form, InputGroup, ListGroup, Row } from "react-bootstrap";

import SearchPopver from '../component/SearchPopOver'

class ModuleSearchResult extends Component {
  constructor( props ) {
    super( props );
    this.state = {list: [], isSearchFinish: false};
  }

  static renderLoading() {
    return <>Loading...</>;
  }

  componentWillUnmount() {
  }

  componentDidUpdate() {
  }

  componentDidMount() {
    this.getSearch()
  }

  getSearch() {
    const {search} = this.props;
    //this.setState( {isSearchFinish: false} )
    fetch( '/azureSearch/services.svc?search=' + search )
      .then( res => res.json() )
      .then( this.onLoad )
  }

  onLoad = ( data ) => {
    this.setState( {
                     list: data.value !== undefined ? data.value : data,
                     isSearchFinish: true
                   } );
  }

  render() {
    const {list, isSearchFinish} = this.state;
    return isSearchFinish ?
      this.renderData( list ) :
      ModuleSearchResult.renderLoading()
  }

  renderData( data ) {
    const {search} = this.props;
    return (
      <>
        <Row>
          <Col>
            <ListGroup>
              {/* Render the list of items */}
              {data.map( ( item, index ) => {
                return (
                  <ListGroup.Item key={index} onClick={( e ) => this.props.handleClickSearchItem( e, item, search )}>
                    {item.deliveryNumber}
                  </ListGroup.Item>
                );
              } )}
            </ListGroup>
          </Col>
        </Row>
      </>
    )
  }
}

class ModuleSearch extends Component {
  static defaultProps = {
    searchInputValue: "",
  }
  _isMounted = false;

  constructor( props ) {
    super( props );
    this.state = {showResults: false, list: [], inputValue: "", searchInputValue: ""};
  }

  componentDidMount() {
    this._isMounted = false;
    this.setState( {
                     searchInputValue: this.props.searchInputValue
                   } );
  }
  componentDidUpdate() {
    if (this._isMounted) {
      this.setState( {
                       searchInputValue: this.props.searchInputValue
                     } );
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  searchInputHandleChange( e ) {
    this.setState( {
                     showResults: false,
                     searchInputValue: e.target.value
                   } );

  }

  onPressEnter( e ) {
    if ( e.target.type === 'text' && e.which === 13 /* Enter */ ) {
      this.setState( {
                       searchInputValue: e.target.value
                     } );
      this.props.handleClickSearchItem( e, undefined, e.target.value )

      e.preventDefault();
    }
  }

  render() {
    const {list, searchInputValue} = this.state;
    let id = "module-search-input"
    if ( this.props.id !== undefined ) {
      id = this.props.id
    }
    return (
      <>
        <Row>
          <Col lg="12">
            <Form>
              <Form.Group as={Row}>
                <Col md="8">
                  <Form.Label>{this.props.searchTitle}</Form.Label>
                  <InputGroup>
                    <InputGroup.Prepend>
                      <InputGroup.Text className={"cursor-pointer input-group-append--transparent rounded-left border-right-0"} id="basic-addon3">
                        <FontAwesomeIcon icon={['fal', 'search']}/>
                      </InputGroup.Text>
                    </InputGroup.Prepend>
                    <Form.Control
                      size="lg"
                      placeholder={this.props.placeholder}
                      id={id}
                      aria-describedby="basic-addon1"
                      value={searchInputValue}
                      className={"border-right-0 border-left-0"}
                      onKeyDown={this.onPressEnter.bind( this )}
                      onChange={this.searchInputHandleChange.bind( this )}
                    />
                    <InputGroup.Append>
                      <InputGroup.Text className={"cursor-pointer input-group-append--transparent rounded-right border-left-0"} id="basic-addon3" onClick={this.onRemoveSearch.bind( this )}>
                        <FontAwesomeIcon icon={['fal', 'times']}/>
                      </InputGroup.Text>
                    </InputGroup.Append>
                    <Form.Control.Feedback type="invalid">
                      Fehlermeldung kommt hier ...
                    </Form.Control.Feedback>
                  </InputGroup>
                  <Row>
                    <Col>
                      <Form.Text className="text-muted">
                        <SearchPopver {...this.props}/>
                      </Form.Text>
                    </Col>
                  </Row>
                </Col>
                <Col md="4" className="align-self-center mt-2 col-md-4">
                  <Button variant="primary" size="lg" block onClick={( e ) => {
                    this.props.handleClickSearchItem( e, undefined, searchInputValue )
                  }}>{this.props.buttonText}</Button>
                </Col>
              </Form.Group>
            </Form>
          </Col>
        </Row>
        {this.state.showResults ? <ModuleSearchResult list={list} search={this.state.searchInputValue} handleClickSearchItem={this.props.handleClickSearchItem}/> : null}
      </>
    );
  }

  onRemoveSearch( e ) {
    this.props.handleOnClickRemoveSearch( e )
    this.setState( {
                     searchInputValue: "",
                     showResults: false,
                     list: []
                   } );
  }

  onSearchClick() {
    this.setState( {
                     showResults: true,
                     list: []
                   } );
  }
}

export default ModuleSearch
