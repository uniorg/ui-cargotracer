import React from 'react';
import { Route, Router as Router, Switch } from 'react-router-dom'
import $ from 'jquery'

import './App.scss';
import 'react-device-detect';


import Header from './component/Header';
import Dashboard from './sites/Dashboard';
import Delivery from './sites/Delivery';
import DeliveryDetail from './sites/DeliveryDetail';
import DeliveryCreate from './sites/DeliveryCreate';
import Devices from './sites/Devices';
import DevicesDetail from './sites/DevicesDetail';
import Asset from './sites/Asset';
import AssetDetail from './sites/AssetDetail';
import AssetSort from './sites/AssetSort';
import AssetCreateUpdate from './sites/AssetCreateUpdate';
import UserManagement from './sites/UserManagement';
import User from './sites/User';
import UserCreateUpdate from './sites/UserCreateUpdate'
import NotFound from './sites/NotFound';
import { Col, Row } from "react-bootstrap";
import Loader from "react-loader-spinner";
import { withTranslation } from "react-i18next";
import AbstractSiteComponent from "./sites/AbstractSiteComponent";
import withSizes from "react-sizes";

import history from "./history";
import { ai } from './TelemetryService';


class App extends AbstractSiteComponent {
  static mapSizesToProps = super.mapSizesToProps()

  constructor( props ) {
    super( props );
    this.state = {
      user: null,
      subClients: null,
      isClient: false,
      adminBpw: false,
      adminSender: false,
      adminRecipient: false,
      isAdmin: false,
      bpwCompany: null,
      bpwMandant: null,
      bpwMandantSub: null,
    };
    this.fetchUser()
    this.fetchSubMandanten()
  }

  static renderLoading() {
    return <div><Row><Col md={"12"} className={"d-flex justify-content-center mt-5"}><Loader type="Oval" color="#CDDC4B" height={70} width={70}/></Col></Row></div>;
  }

  renderContent() {
    let {isClient, containerHeight, isAdmin} = this.state
    let props = this.props;
    const App = () => (
      <React.Fragment>
        <Router history={history}>
          <Switch>
            <React.Fragment>
              <Header isClient={isClient} {...props} isAdmin={isAdmin}/>
              <div className="application-main-container" style={{"height": containerHeight}}>
                <Route exact path='/' render={() => <Dashboard {...this.props} {...this.state} isClient={isClient} isAdmin={isAdmin}/>}/>

                <Route exact path='/delivery' render={() => <Delivery {...this.props} {...this.state} isClient={isClient} isAdmin={isAdmin}/>}/>
                <Route exact path='/delivery/:id' render={() => <DeliveryDetail {...this.props} {...this.state} isClient={isClient} isAdmin={isAdmin}/>}/>

                <Route exact path='/create/delivery' render={() => <DeliveryCreate {...this.props} {...this.state} isClient={isClient} isAdmin={isAdmin}/>}/>

                {isClient &&
                 <>
                   <Route exact path='/devices' render={() => <Devices {...this.props} {...this.state} isClient={isClient} isAdmin={isAdmin}/>}/>
                   <Route exact path='/devices/:id' render={() => <DevicesDetail {...this.props} {...this.state} isClient={isClient} isAdmin={isAdmin}/>}/>
                   <Route exact path='/devices/:id/:startDate/:endDate' render={() => <DevicesDetail {...this.props} {...this.state} isClient={isClient} isAdmin={isAdmin}/>}/>

                   <Route exact path='/assets' render={() => <Asset {...this.props} {...this.state} isClient={isClient} isAdmin={isAdmin}/>}/>
                   <Route exact path='/assets/:id' render={() => <AssetDetail {...this.props} {...this.state} isClient={isClient} isAdmin={isAdmin}/>}/>
                   <Route exact path='/assets/:id/:cycle' render={() => <AssetDetail {...this.props} {...this.state} isClient={isClient} isAdmin={isAdmin}/>}/>

                   <Route exact path='/create/asset' render={() => <AssetCreateUpdate {...this.props} {...this.state} isClient={isClient} isAdmin={isAdmin}/>}/>
                   <Route exact path='/update/asset/:id' render={() => <AssetCreateUpdate {...this.props} {...this.state} isClient={isClient} isAdmin={isAdmin}/>}/>
                 </>
                }

                <Route exact path='/assetsort' render={() => <AssetSort {...this.props} {...this.state} isClient={isClient} isAdmin={isAdmin}/>}/>

                <Route path='/usermanagement' render={() => <UserManagement {...this.props} {...this.state} isClient={isClient} isAdmin={isAdmin}/>}/>
                <Route path='/users' render={() => <User {...this.props} {...this.state} isClient={isClient} isAdmin={isAdmin}/>}/>
                <Route path='/create/user' render={() => <UserCreateUpdate {...this.props} {...this.state} isClient={isClient} isAdmin={isAdmin}/>}/>
                <Route path='/update/user/:id' render={() => <UserCreateUpdate {...this.props} {...this.state} isClient={isClient} isAdmin={isAdmin}/>}/>

                <Route exact path='/not-found' render={() => <NotFound {...this.props} {...this.state} isClient={isClient} isAdmin={isAdmin}/>}/>
              </div>
            </React.Fragment>
          </Switch>
        </Router>
      </React.Fragment>
    )
    return (
      <App/>
    );
  }

  componentDidMount() {
    super.componentDidMount()
    this.setContainerHeight()
  }

  componentWillUnmount() {
    super.componentWillUnmount()
  }

  render() {
    let {user} = this.state
    return super.render( ((user === null) ? App.renderLoading() : this.renderContent()) );
  }

  fetchUser() {
    fetch( '/user-ui-service/me' )
      .then( res => res.json() )
      .then( ( data ) => {
        console.log("fetchUser", data, ((data['isAdminBpw'] || data['isAdminClient'] || data['isAdminSubclient'])))
        this.setState( {
                         user: data,
                         adminBpw: data['isAdminBpw'],
                         adminSender: data['isAdminClient'],
                         adminRecipient: data['isAdminSubclient'],
                         isAdmin: (data['isAdminBpw'] || data['isAdminClient'] || data['isAdminSubclient']),
                         bpwCompany: ((data['extension_BpwCompany'] !== undefined) ? data['extension_BpwCompany'] : null),
                         bpwMandant: data['clientId'],
                         bpwMandantSub: data['subclientId'],
                       } )
      } ).catch( ( error ) => {
      console.error( error );
      //ai.appInsights.trackException(error)
    } )
  }

  fetchSubMandanten() {
    fetch( '/user-ui-service/Subclients' )
      .then( res => res.json() )
      .then( ( data ) => {
        if ( data.hasOwnProperty( "value" ) ) {
          return data.value
        } else {
          return []
        }
      } )
      .then( ( data ) => {
        this.setState( {subClients: data} )
        return data
      } )
      .then( ( data ) => {
        let that = this
        data.map( ( item, index ) => {
          if ( item.isClient !== undefined && item.isClient ) {
            that.setState( {isClient: true} )
          }
          return null
        } )
      } ).catch( ( error ) => {
      console.error( error );
    } )
  }

  setContainerHeight() {
    let that = this;
    setTimeout( () => {
      const height = $( "body" ).outerHeight() - $( ".app-header" ).outerHeight() - (($( ".dev-breadcrumb" ).length) > 0 ? $( ".dev-breadcrumb" ).outerHeight() : 0);
      $( ".application-main-container" ).height( height );
      that.setState( {containerHeight: height} )
    }, 1000 );
    $( window ).resize( function() {
      const height = $( "body" ).outerHeight() - $( ".app-header" ).outerHeight() - (($( ".dev-breadcrumb" ).length) > 0 ? $( ".dev-breadcrumb" ).outerHeight() : 0);
      $( ".application-main-container" ).height( height );
    } );
  }
}

ai.initialize( {history: history}, () => {

} );

export default withTranslation()( withSizes( App.mapSizesToProps )( App ) );
