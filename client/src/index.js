import 'react-app-polyfill/ie9';
import 'react-app-polyfill/ie11';
import 'react-app-polyfill/stable';

import React from 'react';
import { render } from 'react-dom';
import globalizeLocalizer from 'react-widgets-globalize';

import { library } from '@fortawesome/fontawesome-svg-core'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { fal } from '@fortawesome/pro-light-svg-icons'
import { far } from '@fortawesome/pro-regular-svg-icons'
import { fas } from '@fortawesome/pro-solid-svg-icons'
import { fad } from '@fortawesome/pro-duotone-svg-icons'

//import "./App/mockServer/server"
import './App/services/i18n';

import './index.scss';

import 'leaflet/dist/leaflet.css';
import 'leaflet-easybutton/src/easy-button.css'

import './sass/style.scss'

import 'bootstrap'
import 'holderjs'

import App from './App/App';



globalizeLocalizer()
library.add( fab, fal, far, fas, fad )


class AppContainer extends React.Component {

	render() {
		return (
						<App/>
		);
	}
}

render( <AppContainer/>, document.getElementById( 'root' ) )
