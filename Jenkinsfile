def branch
def scmUrl
def scmCredentialsId
def azureAppName
def azureCredentialId
def azureDeployFilePath
def azureResourceGroup
def azureSlotName
def azureSourceDirectory
def azureTargetDirectory
def withRestart
def buildVersion
def version
def parameterBindingHandler
def azurePublishType

branch = 'development'
scmUrl = 'git@bitbucket.org:bpwinnolab/ui-cargotracer.git'
scmCredentialsId = '09ae7d48-14b8-44f9-8728-78ff9075c632'

azureAppName = 'ui-cargotracer-dev'
azureCredentialId = 'mySp'
azureDeployFilePath = '**/*'
azureResourceGroup = 'RG-LINUX'
azureSlotName = env.azureSlotName
azurePublishType = 'file'
azureSourceDirectory = ''
azureTargetDirectory = ''
withRestart = env.withRestart
buildVersion = '${BUILD_NUMBER}'
version = ''


class ParameterBinding implements Serializable {
    public params
    private String azureAppName
    private String azureResourceGroup
    private String branch
    private String scmUrl
    private String scmCredentialsId

    public String azureAppName() {
        try {
            azureAppName = params.azureAppName
            return azureAppName
        }
        catch (MissingPropertyException e) {
            azureAppName = 'ui-cargotracer-dev'
            return azureAppName
        }
    }

    public String azureResourceGroup() {
        try {
            azureResourceGroup = params.azureResourceGroup
            return azureResourceGroup
        }
        catch (MissingPropertyException e) {
            azureResourceGroup = 'RG-LINUX'
            return azureResourceGroup
        }
    }

    public String branch() {
        try {
            branch = params.branch
            return branch
        }
        catch (MissingPropertyException e) {
            branch = 'development'
            return branch
        }
    }

    public String scmUrl() {
        try {
            scmUrl = params.scmUrl
            return scmUrl
        }
        catch (MissingPropertyException e) {
            scmUrl = 'git@bitbucket.org:bpwinnolab/ui-cargotracer.git'
            return scmUrl
        }
    }

    public String scmCredentialsId() {
        try {
            scmCredentialsId = params.scmCredentialsId
            return scmCredentialsId
        }
        catch (MissingPropertyException e) {
            scmCredentialsId = '09ae7d48-14b8-44f9-8728-78ff9075c632'
            return scmCredentialsId
        }
    }
}

node {
    //stage('Start SSH Tunnel') {
    //    build 'Azure Remote Tunnel SSH Start'
    //}
    stage('delete old Deployment') {
        sh 'sudo journalctl -u azureSshRemote.service'
        chuckNorris()
        deleteDir()

        parameterBindingHandler = new ParameterBinding()
        parameterBindingHandler.params = env
        azureAppName = parameterBindingHandler.azureAppName()
        azureResourceGroup = parameterBindingHandler.azureResourceGroup()
        branch = parameterBindingHandler.branch()
        scmCredentialsId = parameterBindingHandler.scmCredentialsId()
        scmUrl = parameterBindingHandler.scmUrl()

        echo azureAppName
        echo azureResourceGroup
        echo branch
        echo scmCredentialsId
        echo scmUrl
        echo withRestart
        echo "projectName " + currentBuild.projectName
        echo "number" + currentBuild.number
        echo "result" + currentBuild.result
        echo "fullDisplayName" + currentBuild.fullDisplayName
        echo "displayName" + currentBuild.displayName
        echo "fullProjectName" + currentBuild.fullProjectName
        echo "id" + currentBuild.id
        echo "timeInMillis" + currentBuild.timeInMillis
        echo "buildVariables" + currentBuild.buildVariables
    }
    stage('checkout git') {
        checkout([$class: 'GitSCM', branches: [[name: branch]], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'SubmoduleOption', disableSubmodules: false, parentCredentials: true, recursiveSubmodules: true, reference: '', trackingSubmodules: false]], submoduleCfg: [], userRemoteConfigs: [[credentialsId: scmCredentialsId, url: scmUrl]]])

        def tag = sh(returnStdout: true, script: "git describe --tags \$(git rev-list --tags --max-count=1)").trim()
        if (tag) {
            version = VersionNumber(
                    projectStartDate: '1970-01-01',
                    versionNumberString: tag + '.' + buildVersion,
                    worstResultForIncrement: 'NOT_BUILT',
                    versionPrefix: "v.",
                    )
        }
        script {
            currentBuild.displayName = version
            currentBuild.description = azureAppName + ' - ' + version + ' - ' + branch
        }
        echo tag
        echo version
    }

    stage('Install') {
        sh label: 'fortawesome npm config set', script: 'npm config set "@fortawesome:registry" https://npm.fontawesome.com/ && npm config set "//npm.fontawesome.com/:_authToken" 54662E71-FE69-4185-A7EE-3B1D4C41E2DA'
        dir("client") {
            sh label: 'yarn Client Install', script: 'yarn install'
        }
        sh label: 'npm Server Install', script: 'npm install'
    }

    stage('Build') {
        dir("client") {
            sh label: 'yarn Client build', script: 'yarn build'
        }
    }

    stage('create Artifact') {
        sh 'mkdir deploy'
        sh "rsync -r -m --exclude '.git' --exclude '.idea' --exclude 'node_modules' --exclude 'deploy' --exclude 'client/public' --exclude 'client/src' --exclude 'server/mockDbServerSql' --exclude 'server/src' ./ ./deploy"
        dir('deploy') {
            sh 'zip -r ../deploy-' + version + '.zip .'
        }
        archiveArtifacts artifacts: 'deploy-' + version + '.zip', fingerprint: true
        azureUpload blobProperties: [cacheControl: '', contentEncoding: '', contentLanguage: '', contentType: '', detectContentType: true], cleanUpContainerOrShare: false, containerName: azureAppName, fileShareName: '', filesPath: '*.zip', storageCredentialId: 'azure-store', storageType: 'blobstorage', uploadArtifactsOnlyIfSuccessful: true
    }

    stage('deploy to azure') {
        dir('deploy') {
            if (azureSlotName != null) {
                azureWebAppPublish azureCredentialsId: azureCredentialId, publishType: azurePublishType,
                                   resourceGroup: azureResourceGroup, appName: azureAppName, slotName: azureSlotName,
                                   filePath: azureDeployFilePath, sourceDirectory: azureSourceDirectory, targetDirectory: azureTargetDirectory
            } else {
                azureWebAppPublish azureCredentialsId: azureCredentialId, publishType: azurePublishType,
                                   resourceGroup: azureResourceGroup, appName: azureAppName,
                                   filePath: azureDeployFilePath, sourceDirectory: azureSourceDirectory, targetDirectory: azureTargetDirectory
            }
        }
    }

    stage('Check Site Availability') {
        waitUntil {
            try {
                sh "curl -s --head  --request GET https://ui-cargotracer-dev.azurewebsites.net | grep '200'"
                return true
            } catch (Exception e) {
                return false
            }
        }
    }

    stage('Azure App Stop ') {
        echo withRestart
        if (withRestart == true) {
            echo '' + azureAppName + ' --- ' + azureResourceGroup + ''
            azureCLI commands: [
                    [exportVariablesString: '', script: 'az webapp stop  --name ' + azureAppName + ' --resource-group ' + azureResourceGroup]
            ], principalCredentialId: azureCredentialId
        } else {
            return true
        }
    }
    stage('Azure App Start ') {
        echo withRestart
        if (withRestart == true) {
            echo '' + azureAppName + ' --- ' + azureResourceGroup + ''
            azureCLI commands: [
                    [exportVariablesString: '', script: 'az webapp start  --name ' + azureAppName + ' --resource-group ' + azureResourceGroup]
            ], principalCredentialId: azureCredentialId
        } else {
            return true
        }
    }
}
