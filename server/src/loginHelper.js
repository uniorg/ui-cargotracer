import bunyan from 'bunyan'
import path from 'path'
import es6Request from 'es6-request'
import config from './config'


const log = bunyan.createLogger( {
                                 name: 'Tracking App'
                               } );
const users = []
const host = config.host.UserApiHost

let getUserObject = (idToken, sendUserData, callback) => {
	try {
		let header = {
			"Content-Type": "application/json",
      authorization: "Bearer " + idToken
		};
		let searchUrl = "https://" + host + "/tracking-user-ui-service/v2/SaveOrUpdateUser"
    es6Request.post(searchUrl)
			.headers(header)
      .send(JSON.stringify(sendUserData))
			.then(([body, res]) => {
				callback(body)
				// should output this README file!
			});
	} catch (e) {
		callback(e.toString());
	}
}

let findByOid = ( oid, fn ) => {
  for ( let i = 0, len = users.length; i < len; i++ ) {
    let user = users[i];
    log.info( 'we are using user: ', user );
    if ( user.oid === oid ) {
      return fn( null, user );
    }
  }
  return fn( null, null );
}

let findAndUpdateByOid = ( oid, fn, newUser ) => {
  for ( let i = 0, len = users.length; i < len; i++ ) {
    let user = users[i];
    if ( user.oid === oid ) {
      users[i] = newUser;
      return fn( null, newUser );
    }
  }
}

let findByEmail = ( email, fn ) => {
  for ( let i = 0, len = this.users.length; i < len; i++ ) {
    let user = this.users[i];
    if ( user.email === email ) {
      return fn( null, user );
    }
  }
  return fn( null, null );
}

let ensureAuthenticated = ( req, res, next ) => {
  if ( req.isAuthenticated() ) {
    return next();
  } else {
    res.sendFile( path.join( __dirname + '/../../client/build/loginStatic/index.html' ) );
  }
}

let ensureIsBpwAdmin = ( req, res, next ) => {
  let returnBool = false;
  if ( req.hasOwnProperty( "user" ) && req.user.hasOwnProperty( "_json" ) && req.user["_json"].hasOwnProperty( "extension_BpwAdminBpw" ) && req.user["_json"]["extension_BpwAdminBpw"] === 1 )
    returnBool = true;

  if ( returnBool ) {
    return next();
  }
  res.redirect( '/login' );
}

let ensureIsAdmin = ( req, res, next ) => {
  let returnBool = false;
  if ( req.hasOwnProperty( "user" ) && req.user.hasOwnProperty( "_json" ) && req.user["_json"].hasOwnProperty( "extension_BpwAdminBpw" ) && req.user["_json"]["extension_BpwAdminBpw"] === 1 )
    returnBool = true;
  if ( req.hasOwnProperty( "user" ) && req.user.hasOwnProperty( "_json" ) && req.user["_json"].hasOwnProperty( "extension_BpwAdminSender" ) && req.user["_json"]["extension_BpwAdminSender"] === 1 )
    returnBool = true;
  if ( req.hasOwnProperty( "user" ) && req.user.hasOwnProperty( "_json" ) && req.user["_json"].hasOwnProperty( "extension_BpwAdminRecipient" ) && req.user["_json"]["extension_BpwAdminRecipient"] === 1 )
    returnBool = true;

  if ( returnBool ) {
    return next();
  }
  res.redirect( '/login' );
}

let checkIsAdmin = ( req ) => {
  let returnBool = false;
  if ( req.hasOwnProperty( "user" ) && req.user.hasOwnProperty( "_json" ) && req.user["_json"].hasOwnProperty( "extension_BpwAdminBpw" ) && req.user["_json"]["extension_BpwAdminBpw"] === 1 )
    returnBool = true;
  if ( req.hasOwnProperty( "user" ) && req.user.hasOwnProperty( "_json" ) && req.user["_json"].hasOwnProperty( "extension_BpwAdminSender" ) && req.user["_json"]["extension_BpwAdminSender"] === 1 )
    returnBool = true;
  if ( req.hasOwnProperty( "user" ) && req.user.hasOwnProperty( "_json" ) && req.user["_json"].hasOwnProperty( "extension_BpwAdminRecipient" ) && req.user["_json"]["extension_BpwAdminRecipient"] === 1 )
    returnBool = true;

  return returnBool;
}

let checkIsAdminSenderBpw = ( req ) => {
  let returnBool = false;
  if ( req.hasOwnProperty( "user" ) && req.user.hasOwnProperty( "_json" ) && req.user["_json"].hasOwnProperty( "extension_BpwAdminBpw" ) && req.user["_json"]["extension_BpwAdminBpw"] === 1 )
    returnBool = true;
  if ( req.hasOwnProperty( "user" ) && req.user.hasOwnProperty( "_json" ) && req.user["_json"].hasOwnProperty( "extension_BpwAdminSender" ) && req.user["_json"]["extension_BpwAdminSender"] === 1 )
    returnBool = true;

  return returnBool;
}

let checkMandantEqSubMandant = ( req ) => {
  let returnBool = false;
  if ( req.hasOwnProperty( "user" ) &&
       req.user.hasOwnProperty( "_json" ) &&
       req.user["_json"].hasOwnProperty( "extension_isMandant" ) &&
       req.user["_json"].hasOwnProperty( "extension_BpwMandant" ) &&
       req.user["_json"]["extension_BpwMandantSub"] === req.user["_json"]["extension_BpwMandant"] )
    returnBool = true;

  return returnBool;
}

let checkIsBpwAdmin = ( req ) => {
  let returnBool = false;
  if ( req.hasOwnProperty( "user" ) && req.user.hasOwnProperty( "_json" ) && req.user["_json"].hasOwnProperty( "extension_BpwAdminBpw" ) && req.user["_json"]["extension_BpwAdminBpw"] === 1 )
    returnBool = true;

  return returnBool;
}

export { users, getUserObject, findByOid, findAndUpdateByOid, findByEmail, ensureAuthenticated, ensureIsBpwAdmin, ensureIsAdmin, checkIsAdmin, checkIsAdminSenderBpw, checkMandantEqSubMandant, checkIsBpwAdmin }