import express from 'express'
import compression from "compression"; // compresses requests
import expressValidator from "express-validator";
import bodyParser from 'body-parser'
import cookieParser from 'cookie-parser'
import path from 'path'
import EnvironmentProxy from './libs/EnvironmentProxy';
import trackingDeliveryUiServiceProxy from './libs/proxyServices/TrackingDeliveryUiServiceProxy';
import trackingDeviceUiServiceProxy from './libs/proxyServices/TrackingDeviceUiServiceProxy';
import TrackingAssetUiServiceProxy from './libs/proxyServices/TrackingAssetUiServiceProxy';
import AzureSearchProxy from './libs/proxyServices/AzureSearchProxy';
import AzureLocationServiceProxy from './libs/AzureLocationServiceProxy';
import ResourcesProxy from './libs/ResourcesProxy'
import { initLoginService } from './libs/LoginServiceProxy';
import UserProxy from './libs/proxyServices/UserProxy';
import UserAzureProxy from './libs/proxyServices/UserAzureProxy';
import morgan from "morgan";
import { ensureAuthenticated, ensureIsBpwAdmin } from './loginHelper'
import config from './config'

let appInsights = require( "applicationinsights" );
appInsights.setup( config.applicationinsights.AppInsightsInstrumentationKey )
  .setAutoDependencyCorrelation( true )
  .setAutoCollectRequests( true )
  .setAutoCollectPerformance( true, true )
  .setAutoCollectExceptions( true )
  .setAutoCollectDependencies( true )
  .setAutoCollectConsole( true )
  .setUseDiskRetryCaching( true )
  .setSendLiveMetrics( true )
  .start();

const app = express();
const Sequelize = require( 'sequelize' );

app.use( compression() );
app.use( bodyParser.json() );
app.use( bodyParser.urlencoded( {extended: true} ) );
app.use( cookieParser() );
app.use( morgan( 'combined' ) );
app.use( expressValidator() );


initLoginService( app )

// Override timezone formatting for MSSQL
Sequelize.DATE.prototype._stringify = function _stringify( date, options ) {
  return this._applyTimezone( date, options ).format( 'YYYY-MM-DD HH:mm:ss.SSS' );
};

app.use( "/static-files", express.static( path.join( __dirname + '../../../client/build' ) ) );
app.use( "/azure", AzureLocationServiceProxy );

app.use( "/delivery-ui-service", trackingDeliveryUiServiceProxy )
app.use( "/device-ui-service", trackingDeviceUiServiceProxy )
app.use( "/asset-ui-service", TrackingAssetUiServiceProxy )
app.use( "/user-ui-service", UserProxy )
app.use( "/user-azure-ui-service", UserAzureProxy )
app.use( AzureSearchProxy )
app.use( EnvironmentProxy )
app.use( ResourcesProxy )

// Serve the static files from the React app


app.get( '/account2', ensureAuthenticated, ensureIsBpwAdmin, function( req, res ) {
  res.type( "application/json" );
  let returnValue = {user: req.user};
  res.send( returnValue );
} );

// Handles any requests that don't match the ones above
app.get( '/', ensureAuthenticated, ( req, res ) => {
  console.log( "start site" )
  res.sendFile( path.join( __dirname + '../../../client/build/index.html' ) );
} );

app.get( '/*', ensureAuthenticated, ( req, res ) => {
  console.log( "start site index" )
  res.sendFile( path.join( __dirname + '../../../client/build/index.html' ) );
} );


const port = process.env.PORT || 1337;
app.listen( port );
console.log( 'App is listening on port ' + port + ' in ' + process.env.NODE_ENV );


