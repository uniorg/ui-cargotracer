import express from "express";
import es6Request from 'es6-request'
import config from '../../config'

const router = express.Router();

const host = config.host.DeliveryUIDeviceHost
const env = config.environment

// POST /user/signin
router.get('/delivery/detail/:id', (request, response) => {
  let {params, user} = request
  let {id} = params
  try {
    let header = {
      "Content-Type": ((request.headers.hasOwnProperty( "content-type" )) ? request.headers["content-type"] : "application/json"),
      "maxdataserviceversion": ((request.headers.hasOwnProperty( "maxdataserviceversion" )) ? request.headers["maxdataserviceversion"] : "3.0"),
      "dataserviceversion": ((request.headers.hasOwnProperty( "dataserviceversion" )) ? request.headers["dataserviceversion"] : "2.0")
    };
      header.authorization = "Bearer " + user.id_token
    es6Request.get("https://" + host + "/tracking-delivery-ui-service/v2/Deliveries('" + id + "')")
      .headers(header)
      .then( ( [body, res] ) => {
        response.type( ((request.headers.hasOwnProperty( "content-type" )) ? request.headers["content-type"] : "application/json") );
        if ( res.statusCode === 200 || res.statusCode === 201) {
          response.status( res.statusCode );
          response.send( body );
        } else {
          response.status( res.statusCode );
          response.send( {
                           url: "https://" + host + "/tracking-delivery-ui-service/v2/Deliveries('" + id + "')",
                           authorization: "Bearer " + user.id_token,
                           body: body,
                         } );
        }
        // should output this README file!
      } );
  } catch (e) {
    response.status(500).send(e.toString());
  }
});

// /tracking-delivery-ui-service/v2/DeliveryGeoMessages(deliveryId=25,handlingUnitId=111,smoothingType=DeliveryV2.SmoothingType'NONE',fromTimestamp=null,toTimestamp=null,skip=0,top=100)

router.get('/delivery/geoMessages/:deliveryId/:handlingUnitId/:fromTimestamp/:toTimestamp/:skip/:top', (request, response) => {
  let {params, user} = request
  let {deliveryId, handlingUnitId, fromTimestamp, toTimestamp, skip, top} = params
  try {
    let header = {
      "Content-Type": ((request.headers.hasOwnProperty( "content-type" )) ? request.headers["content-type"] : "application/json"),
      "maxdataserviceversion": ((request.headers.hasOwnProperty( "maxdataserviceversion" )) ? request.headers["maxdataserviceversion"] : "3.0"),
      "dataserviceversion": ((request.headers.hasOwnProperty( "dataserviceversion" )) ? request.headers["dataserviceversion"] : "2.0")
    };
      header.authorization = "Bearer " + user.id_token
    es6Request.get("https://" + host + "/tracking-delivery-ui-service/v2/DeliveryGeoMessages(deliveryId=" + deliveryId + ",handlingUnitId=" + handlingUnitId + ",smoothingType=DeliveryV2.SmoothingType'NONE',fromTimestamp=" + fromTimestamp + ",toTimestamp=" + toTimestamp + ",skip=" + skip + ",top=" + top + ")")
      .headers(header)
      .then(([body, res]) => {
        response.type( ((request.headers.hasOwnProperty( "content-type" )) ? request.headers["content-type"] : "application/json") );
        response.status( res.statusCode );
        response.send( body );
        // should output this README file!
      });
  } catch (e) {
    response.status(500).send(e.toString());
  }
});


router.get('/delivery/checkIsExist/:deliveryId', (request, response) => {
  let {params, user} = request
  let {deliveryId} = params
  try {
    let header = {
      "Content-Type": ((request.headers.hasOwnProperty( "content-type" )) ? request.headers["content-type"] : "application/json"),
      "maxdataserviceversion": ((request.headers.hasOwnProperty( "maxdataserviceversion" )) ? request.headers["maxdataserviceversion"] : "3.0"),
      "dataserviceversion": ((request.headers.hasOwnProperty( "dataserviceversion" )) ? request.headers["dataserviceversion"] : "2.0")
    };
      header.authorization = "Bearer " + user.id_token
    es6Request.get("https://" + host + "/tracking-delivery-ui-service/v2/ExistsDelivery(deliveryNumber='" + deliveryId + "')")
      .headers(header)
      .then(([body, res]) => {
        response.type( ((request.headers.hasOwnProperty( "content-type" )) ? request.headers["content-type"] : "application/json") );
        response.status( res.statusCode );
        response.send( body );
        // should output this README file!
      });
  } catch (e) {
    response.status(500).send(e.toString());
  }
});


router.get('/delivery/assignedDelivery/:deviceId', (request, response) => {
  let {params, user} = request
  let {deviceId} = params
  try {
    let header = {
      "Content-Type": ((request.headers.hasOwnProperty( "content-type" )) ? request.headers["content-type"] : "application/json"),
      "maxdataserviceversion": ((request.headers.hasOwnProperty( "maxdataserviceversion" )) ? request.headers["maxdataserviceversion"] : "3.0"),
      "dataserviceversion": ((request.headers.hasOwnProperty( "dataserviceversion" )) ? request.headers["dataserviceversion"] : "2.0")
    };
    if (env !== "dev")
      header.authorization = "Bearer " + user.id_token
    es6Request.get("https://" + host + "/tracking-delivery-ui-service/v2/AssignedDelivery(deviceId='" + deviceId + "')")
      .headers(header)
      .then(([body, res]) => {
        response.type( ((request.headers.hasOwnProperty( "content-type" )) ? request.headers["content-type"] : "application/json") );
        response.status( res.statusCode );
        response.send( body );
        // should output this README file!
      });
  } catch (e) {
    response.status(500).send(e.toString());
  }
});

router.post('/delivery/CreateDelivery', (request, response) => {
  let {body, user} = request
  try {
    let header = {
      "Content-Type": "application/json",
      "maxdataserviceversion": ((request.headers.hasOwnProperty( "maxdataserviceversion" )) ? request.headers["maxdataserviceversion"] : "3.0"),
      "dataserviceversion": ((request.headers.hasOwnProperty( "dataserviceversion" )) ? request.headers["dataserviceversion"] : "2.0")
    };
      header.authorization = "Bearer " + user.id_token
    es6Request.post("https://" + host + "/tracking-delivery-ui-service/v2/CreateSimpleDelivery")
      .headers(header)
      .send(JSON.stringify(body))
      .then(([body, res]) => {
        response.type( ((request.headers.hasOwnProperty( "content-type" )) ? request.headers["content-type"] : "application/json") );
        response.status( res.statusCode );
        response.send( body );
        // should output this README file!
      });
  } catch (e) {
    response.status(500).send(e.toString());
  }
});

export default router;