import express from "express";
import es6Request from 'es6-request'
import config from '../../config'
import {ensureAuthenticated, users} from '../../loginHelper'

const router = express.Router();

const host = config.host.UserApiHost


router.get('/Subclients', (request, response) => {
  let {params, user} = request
  try {
    let header = {
      "Content-Type": ((request.headers.hasOwnProperty( "content-type" )) ? request.headers["content-type"] : "application/json"),
      "maxdataserviceversion": ((request.headers.hasOwnProperty( "maxdataserviceversion" )) ? request.headers["maxdataserviceversion"] : "3.0"),
      "dataserviceversion": ((request.headers.hasOwnProperty( "dataserviceversion" )) ? request.headers["dataserviceversion"] : "2.0")
    };
    header.authorization = "Bearer " + user.id_token

    let searchUrl = "https://" + host + "/tracking-user-ui-service/v2/Subclients"
    es6Request.get(searchUrl)
      .headers(header)
      .then(([body, res]) => {
        response.type( ((request.headers.hasOwnProperty( "content-type" )) ? request.headers["content-type"] : "application/json") );
        response.status( res.statusCode );
        response.send( body );
        // should output this README file!
      });
  } catch (e) {
    response.status(500).send(e.toString());
  }
});

router.get('/clients', (request, response) => {
  let {params, user} = request
  try {
    let header = {
      "Content-Type": ((request.headers.hasOwnProperty( "content-type" )) ? request.headers["content-type"] : "application/json"),
      "maxdataserviceversion": ((request.headers.hasOwnProperty( "maxdataserviceversion" )) ? request.headers["maxdataserviceversion"] : "3.0"),
      "dataserviceversion": ((request.headers.hasOwnProperty( "dataserviceversion" )) ? request.headers["dataserviceversion"] : "2.0")
    };
    header.authorization = "Bearer " + user.id_token

    let searchUrl = "https://" + host + "/tracking-user-ui-service/v2/Clients"
    es6Request.get(searchUrl)
      .headers(header)
      .then(([body, res]) => {
        response.type( ((request.headers.hasOwnProperty( "content-type" )) ? request.headers["content-type"] : "application/json") );
        response.status( res.statusCode );
        response.send( body );
        // should output this README file!
      });
  } catch (e) {
    response.status(500).send(e.toString());
  }
});

router.get( '/me', ensureAuthenticated, function( req, res ) {
  res.type( "application/json" );
  let returnValue = {...req.user["_json"], ...req.user["backendUser"] };
  res.send( returnValue );
} );

router.get( '/usersLogedin', ensureAuthenticated, function( req, res ) {
  if (req.user.backendUser.isAdminBpw) {
    let returnUsers = users.map((user)=> {
      return user.backendUser
    })
    res.send( returnUsers );
  } else {
    res.sendStatus(403)
  }
} );

export default router
