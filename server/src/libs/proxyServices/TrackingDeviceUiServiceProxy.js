import express from "express";
import es6Request from 'es6-request'
import requestCaller from "request";
import config from '../../config'

const router = express.Router();

const host = config.host.TrackingUIDeviceHost
const env = config.environment

// POST /user/signin https://cargotracer-backend-dev.azurewebsites.net/tracking-device-ui-service/v2/Devices
router.get( ['/devices'], ( request, response ) => {
  let {params, user} = request
  try {
    let header = {
      "Content-Type": ((request.headers.hasOwnProperty( "content-type" )) ? request.headers["content-type"] : "application/json"),
      "maxdataserviceversion": ((request.headers.hasOwnProperty( "maxdataserviceversion" )) ? request.headers["maxdataserviceversion"] : "3.0"),
      "dataserviceversion": ((request.headers.hasOwnProperty( "dataserviceversion" )) ? request.headers["dataserviceversion"] : "2.0")
    };
    if ( env !== "dev" )
      header.authorization = "Bearer " + user.id_token
    es6Request.get( "https://" + host + "/tracking-device-ui-service/v2/Devices" )
      .headers( header )
      .then( ( [body, res] ) => {
        response.type( ((request.headers.hasOwnProperty( "content-type" )) ? request.headers["content-type"] : "application/json") );
        if ( res.statusCode === 200 || res.statusCode === 201 ) {
          response.status( res.statusCode );
          response.send( body );
        } else {
          response.status( res.statusCode );
          response.send( {
                           url: "https://" + host + "/tracking-device-ui-service/v2/Devices",
                           authorization: "Bearer " + user.id_token,
                           body: body,
                         } );
        }
        // should output this README file!
      } );
  } catch ( e ) {
    response.status( 500 ).send( e.toString() );
  }
} );

function urlStringReplace( string ) {

  string = string.replaceAll( "https://cargotracer-backend-dev.azurewebsites.net/tracking-device-ui-service/v2/Devices?", "" );
  string = string.replaceAll( "https://cargotracer-backend-qa.azurewebsites.net/tracking-device-ui-service/v2/Devices?", "" );
  string = string.replaceAll( "https://cargotracer-devices-production.azurewebsites.net/tracking-device-ui-service/v2/Devices?", "" );
  string = string.replaceAll( "https://" + host + "/tracking-device-ui-service/v2/Devices?", "" );
  string = string.replaceAll( "/docs?", "" );
  string = string.replaceAll( "&api-version=2019-05-06&$count=true", "" );
  string = string.replace( "* ", "*" );
  string = string.replace( "$skip", "skip" );
  string = string.replace( "$top", "top" );
  string = string.replace( "$filter", "filter" );

  return string
}

router.get( '/devices/list/*', ( Request, Response ) => {
  let {params, user} = Request
  if ( Request.user === undefined ) {
    Response.status( 401 );
  }
  let pars = (Object.keys( Request.body ).length > 0) ? Request.body : Request.query;

  let query = ""
  let search = ""
  if ( pars.search !== undefined && pars.search !== "undefined" && pars.search !== "*" ) {
    search = "(contains(name,'" + pars.search.trim() + "') or contains(deviceId,'" + pars.search.trim() + "')) and "
  }
  if ( pars.filter !== undefined ) {
    query = query + "$filter=" + ((search !== "") ? search : "") + pars.filter
  }

  if ( pars.top !== undefined ) {
    query = query + ((pars.filter !== undefined) ? "&" : "") + "$top=" + pars.top
  }
  if ( pars.skip !== undefined ) {
    query = query + ((pars.filter !== undefined || pars.top !== undefined) ? "&" : "") + "$skip=" + pars.skip
  }
  if ( pars.orderby !== undefined ) {
    query = query + ((pars.filter !== undefined || pars.top !== undefined || pars.skip !== undefined) ? "&" : "") + "$orderby=" + pars.orderby
  }

  let url = "https://" + host + "/tracking-device-ui-service/v2/Devices?" + query + "";
  let countUrl = "https://" + host + "/tracking-device-ui-service/v2/Devices/$count?" + ((pars.filter !== undefined) ? "$filter=" + ((search !== "") ? search : "") + pars.filter : "");

  url = url.replace( "* ", "*" );
  let header = {
    "Content-Type": ((Request.headers.hasOwnProperty( "content-type" )) ? Request.headers["content-type"] : "application/json"),
    "Cache-Control": ((Request.headers.hasOwnProperty( "Cache-Control" )) ? Request.headers["Cache-Control"] : "no-cache")
  };
  header.authorization = "Bearer " + user.id_token
  requestCaller.get(
    {
      url: countUrl,
      headers: header,
      time: true
    },
    function( countError, callerCountResponse, countBody ) {

      requestCaller.get(
        {
          url: url,
          headers: header,
          time: true
        },
        function( error, callerResponse, body ) {
          Response.type( ((callerResponse.headers.hasOwnProperty( "content-type" )) ? callerResponse.headers["content-type"] : "application/json") );
          Response.status( ((callerResponse.hasOwnProperty( "statusCode" )) ? callerResponse["statusCode"] : 200) );
          let returnBody = JSON.stringify( body );
          let returnBodyObject = JSON.parse( returnBody )

          if (typeof returnBodyObject === "string")
            returnBodyObject = JSON.parse( returnBodyObject )

          returnBodyObject.lastUrl = url
          returnBodyObject.lastCountUrl = countUrl
          returnBodyObject.lastSkip = parseInt( pars.top )
          returnBodyObject.lastTop = parseInt( pars.skip )
          if ( pars.top !== undefined && pars.skip !== undefined ) {
            returnBodyObject.nextTop = parseInt( pars.top )
            returnBodyObject.nextSkip = parseInt( pars.skip ) + parseInt( pars.top )
          }

          if ( returnBodyObject["@odata.nextLink"] !== undefined ) {
            returnBodyObject.nextUrlQuery = decodeURIComponent( returnBodyObject["@odata.nextLink"] );
            returnBodyObject.nextUrlQuery = urlStringReplace( returnBodyObject.nextUrlQuery );
            returnBodyObject["@odata.nextLink"] = returnBodyObject.nextUrlQuery;

            if ( pars.top !== undefined && pars.skip !== undefined ) {
              returnBodyObject.nextUrlQuery = returnBodyObject.nextUrlQuery.replaceAll( "skip=" + pars.skip, "skip=" + returnBodyObject.nextSkip )
              returnBodyObject.nextUrlQuery = returnBodyObject.nextUrlQuery.replaceAll( "top=" + pars.top, "top=" + returnBodyObject.nextTop )
              returnBodyObject.nextUrlQuery = returnBodyObject.nextUrlQuery.replaceAll( "$filter", "filter" )
            }

          } else {
            returnBodyObject.nextUrlQuery = null

            if ( pars.top !== undefined && pars.skip !== undefined ) {
              returnBodyObject.nextUrlQuery = decodeURIComponent( url );
              returnBodyObject.nextUrlQuery = urlStringReplace( returnBodyObject.nextUrlQuery );
              returnBodyObject.nextUrlQuery = returnBodyObject.nextUrlQuery.replaceAll( "skip=" + pars.skip, "skip=" + returnBodyObject.nextSkip )
              returnBodyObject.nextUrlQuery = returnBodyObject.nextUrlQuery.replaceAll( "top=" + pars.top, "top=" + returnBodyObject.nextTop )
              returnBodyObject.nextUrlQuery = returnBodyObject.nextUrlQuery.replaceAll( "$filter", "filter" )
            }
          }
          if ( returnBodyObject["@odata.context"] !== undefined ) {
            returnBodyObject["@odata.context"] = decodeURIComponent( returnBodyObject["@odata.context"] );
            returnBodyObject["@odata.context"] = urlStringReplace( returnBodyObject["@odata.context"] );
          }

          if ( returnBodyObject["@odata.count"] !== undefined ) {
            returnBodyObject.count = returnBodyObject["@odata.count"];
          } else {
            returnBodyObject.count = parseInt( countBody )
          }

          Response.send( returnBodyObject );
        }
      );
    } )
} );

router.get( '/devices/searchDevice/:search', ( request, response ) => {
  let {params, user} = request
  let {search} = params
  try {
    let header = {
      "Content-Type": ((request.headers.hasOwnProperty( "content-type" )) ? request.headers["content-type"] : "application/json"),
      "maxdataserviceversion": ((request.headers.hasOwnProperty( "maxdataserviceversion" )) ? request.headers["maxdataserviceversion"] : "3.0"),
      "dataserviceversion": ((request.headers.hasOwnProperty( "dataserviceversion" )) ? request.headers["dataserviceversion"] : "2.0")
    };
    header.authorization = "Bearer " + user.id_token

    let searchUrl = "https://" + host + "/tracking-device-ui-service/v2/Devices?$filter=contains(deviceId,'" + search + "') or contains(name,'" + search + "')"
    if ( search === "*" )
      searchUrl = "https://" + host + "/tracking-asset-ui-service/v2/Devices"
    es6Request.get( searchUrl )
      .headers( header )
      .then( ( [body, res] ) => {
        response.type( ((request.headers.hasOwnProperty( "content-type" )) ? request.headers["content-type"] : "application/json") );
        response.status( res.statusCode );
        response.send( body );
        // should output this README file!
      } );
  } catch ( e ) {
    response.status( 500 ).send( e.toString() );
  }
} );

router.get( '/devices/searchDeviceById/:id', ( request, response ) => {
  let {params, user} = request
  let {id} = params
  try {
    let header = {
      "Content-Type": ((request.headers.hasOwnProperty( "content-type" )) ? request.headers["content-type"] : "application/json"),
      "maxdataserviceversion": ((request.headers.hasOwnProperty( "maxdataserviceversion" )) ? request.headers["maxdataserviceversion"] : "3.0"),
      "dataserviceversion": ((request.headers.hasOwnProperty( "dataserviceversion" )) ? request.headers["dataserviceversion"] : "2.0")
    };
    header.authorization = "Bearer " + user.id_token

    let searchUrl = "https://" + host + "/tracking-device-ui-service/v2/Devices('" + id + "')"
    es6Request.get( searchUrl )
      .headers( header )
      .then( ( [body, res] ) => {
        response.type( ((request.headers.hasOwnProperty( "content-type" )) ? request.headers["content-type"] : "application/json") );
        response.status( res.statusCode );
        response.send( body );
        // should output this README file!
      } );
  } catch ( e ) {
    response.status( 500 ).send( e.toString() );
  }
} );

router.patch( '/devices/updateDevice', ( request, response ) => {
  let {body, user} = request
  try {
    let header = {
      "Content-Type": ((request.headers.hasOwnProperty( "content-type" )) ? request.headers["content-type"] : "application/json"),
      "maxdataserviceversion": ((request.headers.hasOwnProperty( "maxdataserviceversion" )) ? request.headers["maxdataserviceversion"] : "3.0"),
      "dataserviceversion": ((request.headers.hasOwnProperty( "dataserviceversion" )) ? request.headers["dataserviceversion"] : "2.0")
    };
    header.authorization = "Bearer " + user.id_token

    let searchUrl = "https://" + host + "/tracking-device-ui-service/v2/Devices('" + body.deviceId + "')"
    es6Request.patch( searchUrl )
      .headers( header )
      .send( JSON.stringify( {
                               name: body.name

                             } ) )
      .then( ( [returnBody, res] ) => {
        if ( res.statusCode === 200 || res.statusCode === 201 ) {
          response.type( ((request.headers.hasOwnProperty( "content-type" )) ? request.headers["content-type"] : "application/json") );
          response.status( res.statusCode );
          response.send( returnBody );
        } else {
          response.status( res.statusCode );
          response.send( {
                           url: "https://" + host + "/tracking-asset-ui-service/v2/AssetTypes('" + body.id + "')",
                           authorization: "Bearer " + user.id_token,
                           body: body,
                           returnBody: returnBody
                         } );
        }
      } );
  } catch ( e ) {
    response.status( res.statusCode ).send( e.toString() );
  }
} );

router.get( '/devices/deviceGeoMessages/:id/:from/:to', ( request, response ) => {
  let {params, user} = request
  let {id, from, to} = params
  try {
    let header = {
      "Content-Type": ((request.headers.hasOwnProperty( "content-type" )) ? request.headers["content-type"] : "application/json"),
      "maxdataserviceversion": ((request.headers.hasOwnProperty( "maxdataserviceversion" )) ? request.headers["maxdataserviceversion"] : "3.0"),
      "dataserviceversion": ((request.headers.hasOwnProperty( "dataserviceversion" )) ? request.headers["dataserviceversion"] : "2.0")
    };
    header.authorization = "Bearer " + user.id_token

    let searchUrl = "https://" + host + "/tracking-device-ui-service/v2/DeviceGeoMessages(deviceId='" + id + "',fromTimestamp=" + from + ",toTimestamp=" + to + ",skip=0,top=1000)"
    es6Request.get( searchUrl )
      .headers( header )
      .then( ( [body, res] ) => {
        response.type( ((request.headers.hasOwnProperty( "content-type" )) ? request.headers["content-type"] : "application/json") );
        response.status( res.statusCode );
        response.send( body );
        // should output this README file!
      } );
  } catch ( e ) {
    response.status( 500 ).send( e.toString() );
  }
} );

export default router
