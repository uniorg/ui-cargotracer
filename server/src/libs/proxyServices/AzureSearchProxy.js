import requestCaller from 'request'
import config from '../../config'
import express from "express";


const router = express.Router();

let urlStringReplace = ( string, subClientsArray, index ) => {

  string = string.replaceAll( "https://cargo-search.search.windows.net/indexes('" + index + "')", "" );
  string = string.replaceAll( "https://cargo-search.search.windows.net/indexes/" + index, "" );
  string = string.replaceAll( "/docs?", "" );
  string = string.replaceAll( "&api-version=2019-05-06&$count=true", "" );
  let clientFilter = "clientId eq 2" + getSubClientEq( subClientsArray )
  string = string.replaceAll( " and " + clientFilter, "" );
  string = string.replace( " and " + clientFilter, "" );
  string = string.replaceAll( clientFilter, "" );
  string = string.replace( "* ", "*" );
  string = string.replace( "$skip", "skip" );
  string = string.replace( "$top", "top" );
  string = string.replace( "$filter", "filter" );

  return string
}

let getSubClientEq = ( subClientsArray ) => {
  let query = "";
  if ( subClientsArray === undefined ) {
    return
  }
  for ( var i = 0; i < subClientsArray.length; i++ ) {
    if ( i == 0 ) {
      query = query + " and ("
    }
    query = query + "subclientId eq " + subClientsArray[i];
    if ( i >= 0 && i < (subClientsArray.length - 1) ) {
      query = query + " or "
    }
    if ( i == (subClientsArray.length - 1) ) {
      query = query + ")"
    }
  }
  return query;
}


String.prototype.replaceAll = function( target, replacement ) {
  return this.split( target ).join( replacement );
};


router.get( ["/azureSearch/:index/services.svc", "/azureSearch/:index/services.svc/*"], function( Request, Response ) {
  let {params} = Request
  let {index} = params
  if ( Request.user === undefined ) {
    Response.status( 401 );
  }
  let clinetId = String( Request.user["_json"].extension_BpwMandant )
  let subClinetId = String( Request.user["_json"].extension_BpwMandantSub )
  let pars = (Object.keys( Request.body ).length > 0) ? Request.body : Request.query;
  let subClients = subClinetId;
  let subClientsArray = subClients.split( "," );

  let env = config.environment
  let query = ""
  if ( pars.search !== undefined ) {
    query = "search=" + pars.search.trim()
  }

  let subClientsString = getSubClientEq( subClientsArray )
  if ( pars.filter !== undefined ) {
    query = query + ((pars.search !== undefined) ? "&" : "") + "$filter=" + pars.filter + " and (clientId eq " + clinetId + " and deleted ne true" + subClientsString + ")"
  } else {
    query = query + ((pars.search !== undefined) ? "&" : "") + "$filter=(clientId eq " + clinetId + " and deleted ne true" + subClientsString + ")"
  }
  if ( pars.top !== undefined ) {
    query = query + ((pars.search !== undefined || pars.filter !== undefined) ? "&" : "") + "$top=" + pars.top
  }
  if ( pars.skip !== undefined ) {
    query = query + ((pars.search !== undefined || pars.filter !== undefined || pars.top !== undefined) ? "&" : "") + "$skip=" + pars.skip
  }
  if ( pars.orderby !== undefined ) {
    query = query + ((pars.search !== undefined || pars.filter !== undefined || pars.top !== undefined || pars.skip !== undefined) ? "&" : "") + "$orderby=" + pars.orderby
  }
  let indexKey;
  switch ( index ) {
    case "delivery":
      indexKey = "deliveries-search-" + env;
      break;
    case "asset":
      indexKey = "assets-search-" + env;
      break;
    case "assetTypes":
      indexKey = "assettypes-search-" + env;
      break;
    default:
      indexKey = "deliveries-search-" + env;
      break;
  }


  let url = "https://cargo-search.search.windows.net/indexes/" + indexKey + "/docs?" + query + "&api-version=2019-05-06&$count=true";

  url = url.replace( "* ", "*" );
  let header = {
    "Content-Type": ((Request.headers.hasOwnProperty( "content-type" )) ? Request.headers["content-type"] : "application/json"),
    "Cache-Control": ((Request.headers.hasOwnProperty( "Cache-Control" )) ? Request.headers["Cache-Control"] : "no-cache"),
    "api-key": "1BDD610C06568D19780B9A783ED59E1A"
  };
  requestCaller.get(
    {
      url: url,
      headers: header,
      time: true
    },
    function( error, callerResponse, body ) {
      Response.type( ((callerResponse.headers.hasOwnProperty( "content-type" )) ? callerResponse.headers["content-type"] : "application/json") );
      Response.status( ((callerResponse.hasOwnProperty( "statusCode" )) ? callerResponse["statusCode"] : 200) );
      let returnBody = JSON.stringify( body );


      let returnBodyObject = JSON.parse( JSON.parse( returnBody ) )

      returnBodyObject.lastUrl = url
      returnBodyObject.lastSkip = parseInt( pars.top )
      returnBodyObject.lastTop = parseInt( pars.skip )
      if ( pars.top !== undefined && pars.skip !== undefined ) {
        returnBodyObject.nextTop = parseInt( pars.top )
        returnBodyObject.nextSkip = parseInt( pars.skip ) + parseInt( pars.top )
      }

      if ( returnBodyObject["@odata.nextLink"] !== undefined ) {
        returnBodyObject.nextUrlQuery = decodeURIComponent( returnBodyObject["@odata.nextLink"] );
        returnBodyObject.nextUrlQuery = urlStringReplace( returnBodyObject.nextUrlQuery, subClientsArray, indexKey );
        returnBodyObject["@odata.nextLink"] = returnBodyObject.nextUrlQuery;

        if ( pars.top !== undefined && pars.skip !== undefined ) {
          returnBodyObject.nextUrlQuery = returnBodyObject.nextUrlQuery.replaceAll( "skip=" + pars.skip, "skip=" + returnBodyObject.nextSkip )
          returnBodyObject.nextUrlQuery = returnBodyObject.nextUrlQuery.replaceAll( "top=" + pars.top, "top=" + returnBodyObject.nextTop )
          returnBodyObject.nextUrlQuery = returnBodyObject.nextUrlQuery.replaceAll( "$filter", "filter" )
        }

      } else {
        returnBodyObject.nextUrlQuery = null

        if ( pars.top !== undefined && pars.skip !== undefined ) {
          returnBodyObject.nextUrlQuery = decodeURIComponent( url );
          returnBodyObject.nextUrlQuery = urlStringReplace( returnBodyObject.nextUrlQuery, subClientsArray, indexKey );
          returnBodyObject.nextUrlQuery = returnBodyObject.nextUrlQuery.replaceAll( "skip=" + pars.skip, "skip=" + returnBodyObject.nextSkip )
          returnBodyObject.nextUrlQuery = returnBodyObject.nextUrlQuery.replaceAll( "top=" + pars.top, "top=" + returnBodyObject.nextTop )
          returnBodyObject.nextUrlQuery = returnBodyObject.nextUrlQuery.replaceAll( "$filter", "filter" )
        }
      }
      if ( returnBodyObject["@odata.context"] !== undefined ) {
        returnBodyObject["@odata.context"] = decodeURIComponent( returnBodyObject["@odata.context"] );
        returnBodyObject["@odata.context"] = urlStringReplace( returnBodyObject["@odata.context"], subClientsArray, indexKey );
      }

      if ( returnBodyObject["@odata.count"] !== undefined ) {
        returnBodyObject.count = returnBodyObject["@odata.count"];
      } else {
        returnBodyObject.count = null
      }

      Response.send( returnBodyObject );
    }
  );
} );

export default router