import express from "express";
import es6Request from 'es6-request'
import config from '../../config'

const router = express.Router();

const UserAzureApiHost = config.host.UserAzureApiHost
const UserApiHost = config.host.UserApiHost
const userApiClientKey = config.creds.userApiClientKey

/**{
                    odata.type: "Microsoft.DirectoryServices.User"
                  objectType: "User"
                  objectId: "059e02ba-fd7c-4f1b-ad95-f8430809f1e1"
                  deletionTimestamp: null
                  accountEnabled: true
                  ageGroup: null
                  assignedLicenses: []
                  assignedPlans: []
                  city: null
                  companyName: null
                  consentProvidedForMinor: null
                  country: null
                  createdDateTime: "2018-01-23T10:03:36Z"
                  creationType: "LocalAccount"
                  department: null
                  dirSyncEnabled: null
                  displayName: "ZZZ ZZZ"
                  employeeId: null
                  facsimileTelephoneNumber: null
                  givenName: "ZZZ"
                  immutableId: null
                  isCompromised: null
                  jobTitle: null
                  lastDirSyncTime: null
                  legalAgeGroupClassification: null
                  mail: null
                  mailNickname: "ZZZ"
                  mobile: null
                  onPremisesDistinguishedName: null
                  onPremisesSecurityIdentifier: null
                  otherMails: []
                  passwordPolicies: "DisablePasswordExpiration"
                  passwordProfile: {password: null, forceChangePasswordNextLogin: true, enforceChangePasswordPolicy: false}
                  password: null
                  forceChangePasswordNextLogin: true
                  enforceChangePasswordPolicy: false
                  physicalDeliveryOfficeName: null
                  postalCode: null
                  preferredLanguage: null
                  provisionedPlans: []
                  provisioningErrors: []
                  proxyAddresses: []
                  refreshTokensValidFromDateTime: "2018-01-23T10:03:36Z"
                  showInAddressList: null
                  signInNames: [{type: "emailAddress", value: "ZZZ@bpw.de"}]
                  0: {type: "emailAddress", value: "ZZZ@bpw.de"}
                  type: "emailAddress"
                  value: "ZZZ@bpw.de"
                  sipProxyAddress: null
                  state: null
                  streetAddress: null
                  surname: "ZZZ"
                  telephoneNumber: null
                  thumbnailPhoto@odata.mediaEditLink: "directoryObjects/059e02ba-fd7c-4f1b-ad95-f8430809f1e1/Microsoft.DirectoryServices.User/thumbnailPhoto"
                  usageLocation: null
                  userIdentities: []
                  userPrincipalName: "11647a78-237d-4dac-824d-a430c29f7643@bpwinnolabbpwdev.onmicrosoft.com"
                  userState: null
                  userStateChangedOn: null
                  userType: "Member"
                  extension_6eae7dd58bae41f6b4b3f2471ca79944_BpwMandantSub: 1
                  extension_6eae7dd58bae41f6b4b3f2471ca79944_BpwMandant: 1
                  }*/

let searchAndGetClient = ( clientId, subClients ) => {
  let client = subClients.filter( ( client ) => {
    return (client.id === clientId)
  } )

  return (client.length > 0) ? client[0] : null
}

let searchAndGetSubClient = ( clientId, subClients ) => {
  let client = subClients.filter( ( client ) => {
    return (client.id === clientId)
  } )

  return (client.length > 0) ? client[0] : null
}

let getClientId = (item) => {
  for ( let index in item ) {
    if ( item.hasOwnProperty( index ) ) {

      if ( index.indexOf( "_BpwMandant" ) > -1) {
        return item[index]
      }
    }
  }
}

let mapUserItems = ( request, items, subclients, clients ) => {
  let newItems = items.map( ( item ) => {
    let {givenName, surname, objectId, accountEnabled, displayName} = item
    let newReturnItem = {}

    newReturnItem = {givenName, surname, objectId, accountEnabled, displayName}
    newReturnItem.bpwMandant = null
    newReturnItem.bpwMandantSub = null
    newReturnItem.bpwAdminBpw = null
    newReturnItem.bpwAdminSender = null
    newReturnItem.bpwAdminRecipient = null
    newReturnItem.bpwCompany = null
    newReturnItem.mail = null
    for ( let index in item ) {
      if ( item.hasOwnProperty( index ) ) {

        if ( index.indexOf( "_BpwMandant" ) > -1 && request.user["backendUser"].isAdminBpw ) {
          newReturnItem.bpwMandant = item[index]
          newReturnItem.bpwMandantClient = searchAndGetClient( item[index], clients )
        }
        if ( index.indexOf( "_BpwMandantSub" ) > -1 ) {
          newReturnItem.bpwMandantSub = item[index]
          newReturnItem.bpwMandantSubClient = searchAndGetSubClient( item[index], subclients )
        }
        if ( index.indexOf( "_BpwAdminBpw" ) > -1 ) {
          newReturnItem.bpwAdminBpw = item[index]
        }
        if ( index.indexOf( "_BpwAdminSender" ) > -1 ) {
          newReturnItem.bpwAdminSender = item[index]
        }
        if ( index.indexOf( "_BpwAdminRecipient" ) > -1 ) {
          newReturnItem.bpwAdminRecipient = item[index]
        }
        if ( index.indexOf( "_BpwCompany" ) > -1 ) {
          newReturnItem.bpwCompany = item[index]
        }
        if ( index.indexOf( "signInNames" ) > -1 && item["signInNames"].length > 0 && item["signInNames"][0].type === "emailAddress" ) {

          newReturnItem.mail = item[index][0].value
        }
      }
    }
    return newReturnItem
  } )

  return newItems
}

let getSubClients = ( response, request, user, callback ) => {
  let header = {
    "Content-Type": ((request.headers.hasOwnProperty( "content-type" )) ? request.headers["content-type"] : "application/json"),
    "maxdataserviceversion": ((request.headers.hasOwnProperty( "maxdataserviceversion" )) ? request.headers["maxdataserviceversion"] : "3.0"),
    "dataserviceversion": ((request.headers.hasOwnProperty( "dataserviceversion" )) ? request.headers["dataserviceversion"] : "2.0")
  };
  header.authorization = "Bearer " + user.id_token

  let searchUrl = "https://" + UserApiHost + "/tracking-user-ui-service/v2/Subclients"
  es6Request.get( searchUrl )
    .headers( header )
    .then( ( [body, res] ) => {
      let newBody = JSON.parse( body )
      if ( callback !== undefined && newBody.value !== undefined )
        callback( newBody.value )
    } );
}
let getSubClientsForClient = ( response, request, user, clientId, callback ) => {
  let header = {
    "Content-Type": ((request.headers.hasOwnProperty( "content-type" )) ? request.headers["content-type"] : "application/json"),
    "maxdataserviceversion": ((request.headers.hasOwnProperty( "maxdataserviceversion" )) ? request.headers["maxdataserviceversion"] : "3.0"),
    "dataserviceversion": ((request.headers.hasOwnProperty( "dataserviceversion" )) ? request.headers["dataserviceversion"] : "2.0")
  };
  header.authorization = "Bearer " + user.id_token

  let searchUrl = "https://" + UserApiHost + "/tracking-user-ui-service/v2/GetSubclientsForClient(clientId=" + clientId + ")"
  es6Request.get( searchUrl )
    .headers( header )
    .then( ( [body, res] ) => {
      let newBody = JSON.parse( body )
      if ( callback !== undefined && newBody.value !== undefined )
        callback( newBody.value )
    } );
}

let getClients = ( response, request, user, callback ) => {
  let header = {
    "Content-Type": ((request.headers.hasOwnProperty( "content-type" )) ? request.headers["content-type"] : "application/json"),
    "maxdataserviceversion": ((request.headers.hasOwnProperty( "maxdataserviceversion" )) ? request.headers["maxdataserviceversion"] : "3.0"),
    "dataserviceversion": ((request.headers.hasOwnProperty( "dataserviceversion" )) ? request.headers["dataserviceversion"] : "2.0")
  };
  header.authorization = "Bearer " + user.id_token

  let searchUrl = "https://" + UserApiHost + "/tracking-user-ui-service/v2/Clients"
  es6Request.get( searchUrl )
    .headers( header )
    .then( ( [body, res] ) => {
      let newBody = JSON.parse( body )
      if ( callback !== undefined && newBody.value !== undefined )
        callback( newBody.value )
    } );
}


let createNewBody = ( pars, request ) => {
  let {user} = request
  let {backendUser} = user
  let {isAdminBpw, clientId, isAdminClient} = backendUser
  let newBody = {};
  
  for ( let index in pars ) {
    if ( pars.hasOwnProperty( index ) ) {
      if ( index.indexOf( "_BpwMandant" ) > -1 && index === "extension_<<clientId>>_BpwMandant" ) {
        newBody["extension_" + userApiClientKey + "_BpwMandant"] = ((isAdminBpw) ? pars[index] : clientId);
      }

      if ( index.indexOf( "_BpwMandantSub" ) > -1 && index === "extension_<<clientId>>_BpwMandantSub" ) {
        newBody["extension_" + userApiClientKey + "_BpwMandantSub"] = pars[index];
      }

      if ( index.indexOf( "_BpwAdminBpw" ) > -1 && isAdminBpw ) {
        newBody["extension_" + userApiClientKey + "_BpwAdminBpw"] = pars[index];
      }
      if ( index.indexOf( "_BpwAdminSender" ) > -1 && isAdminBpw ) {
        newBody["extension_" + userApiClientKey + "_BpwAdminSender"] = pars[index];
      }
      if ( index.indexOf( "_BpwAdminRecipient" ) > -1 && (isAdminBpw || isAdminClient) ) {
        newBody["extension_" + userApiClientKey + "_BpwAdminRecipient"] = pars[index];
      }

      if ( index.indexOf( "_BpwCompany" ) > -1 ) {
        newBody["extension_" + userApiClientKey + "_BpwCompany"] = pars[index];
      }
      if ( index.indexOf( "_BpwMandantSub" ) === -1 &&
           index.indexOf( "_BpwAdminBpw" ) === -1 &&
           index.indexOf( "_BpwAdminSender" ) === -1 &&
           index.indexOf( "_BpwAdminRecipient" ) === -1 &&
           index.indexOf( "_BpwCompany" ) === -1 &&
           index.indexOf( "_BpwMandant" ) === -1 ) {
        newBody[index] = pars[index];
      }
    }
  }
  if (newBody["extension_" + userApiClientKey + "_BpwMandant"] === undefined)
    newBody["extension_" + userApiClientKey + "_BpwMandant"] = clientId;

  return newBody;
}

router.get( '/Users', ( request, response ) => {
  let {params, user} = request

  getSubClients( response, request, user, ( subclients ) => {
    try {
      let header = {
        "Content-Type": ((request.headers.hasOwnProperty( "content-type" )) ? request.headers["content-type"] : "application/json"),
        "maxdataserviceversion": ((request.headers.hasOwnProperty( "maxdataserviceversion" )) ? request.headers["maxdataserviceversion"] : "3.0"),
        "dataserviceversion": ((request.headers.hasOwnProperty( "dataserviceversion" )) ? request.headers["dataserviceversion"] : "2.0")
      };
      header.authorization = "Bearer " + user.id_token

      let searchUrl = "https://" + UserAzureApiHost + "/api/Users"
      es6Request.get( searchUrl )
        .headers( header )
        .then( ( [body, res] ) => {
          response.type( ((request.headers.hasOwnProperty( "content-type" )) ? request.headers["content-type"] : "application/json") );
          response.status( res.statusCode );
          let newBody = JSON.parse( body )
          if ( request.user["backendUser"].isAdminBpw ) {
            getClients( response, request, user, ( clients ) => {
              newBody.value = mapUserItems( request, newBody.value, subclients, clients )
              response.send( JSON.stringify( newBody ) );
            } )
          } else {
            newBody.value = mapUserItems( request, newBody.value, subclients )
            response.send( JSON.stringify( newBody ) );
          }
          // should output this README file!
        } );
    } catch ( e ) {
      console.log( e.toString() );
      response.status( 500 ).send( e.toString() );
    }
  } )

} );

router.get( '/Subclients', ( request, response ) => {
  let {params, user} = request
  let header = {
    "Content-Type": ((request.headers.hasOwnProperty( "content-type" )) ? request.headers["content-type"] : "application/json"),
    "maxdataserviceversion": ((request.headers.hasOwnProperty( "maxdataserviceversion" )) ? request.headers["maxdataserviceversion"] : "3.0"),
    "dataserviceversion": ((request.headers.hasOwnProperty( "dataserviceversion" )) ? request.headers["dataserviceversion"] : "2.0")
  };
  header.authorization = "Bearer " + user.id_token

  let searchUrl = "https://" + UserApiHost + "/tracking-user-ui-service/v2/Subclients"
  es6Request.get( searchUrl )
    .headers( header )
    .then( ( [body, res] ) => {
      let newBody = JSON.parse( body )
      response.send( JSON.stringify( newBody.value ) )
    } );
} )

router.get( '/Clients', ( request, response ) => {
  let {params, user} = request
  let header = {
    "Content-Type": ((request.headers.hasOwnProperty( "content-type" )) ? request.headers["content-type"] : "application/json"),
    "maxdataserviceversion": ((request.headers.hasOwnProperty( "maxdataserviceversion" )) ? request.headers["maxdataserviceversion"] : "3.0"),
    "dataserviceversion": ((request.headers.hasOwnProperty( "dataserviceversion" )) ? request.headers["dataserviceversion"] : "2.0")
  };
  header.authorization = "Bearer " + user.id_token

  let searchUrl = "https://" + UserApiHost + "/tracking-user-ui-service/v2/Clients"
  es6Request.get( searchUrl )
    .headers( header )
    .then( ( [body, res] ) => {
      let newBody = JSON.parse( body )
      response.send( JSON.stringify( newBody.value ) )
    } );
} )

router.get( '/Subclients/:clientId', ( request, response ) => {
  let {params, user} = request
  let {clientId} = params

  if ( !user.backendUser.isAdminBpw ) {
    response.status( 401 );
    response.send( false )
  }
  let header = {
    "Content-Type": ((request.headers.hasOwnProperty( "content-type" )) ? request.headers["content-type"] : "application/json"),
    "maxdataserviceversion": ((request.headers.hasOwnProperty( "maxdataserviceversion" )) ? request.headers["maxdataserviceversion"] : "3.0"),
    "dataserviceversion": ((request.headers.hasOwnProperty( "dataserviceversion" )) ? request.headers["dataserviceversion"] : "2.0")
  };
  header.authorization = "Bearer " + user.id_token

  let searchUrl = "https://" + UserApiHost + "/tracking-user-ui-service/v2/GetSubclientsForClient(clientId=" + clientId + ")"
  es6Request.get( searchUrl )
    .headers( header )
    .then( ( [body, res] ) => {
      let newBody = JSON.parse( body )
      response.send( JSON.stringify( newBody.value ) )
    } );
} )


router.post( "/Users", ( request, response ) => {
  let {body, user} = request
  let url = "https://" + UserAzureApiHost + "/api/Users";
  let header = {
    "Content-Type": "application/json",
    "authorization": "Bearer " + user.id_token
  };
  let pars = (Object.keys( body ).length > 0) ? body : body;
  console.log(pars)
  let newBody = createNewBody( pars, request );
  console.log(newBody)
  es6Request.post( url )
    .headers( header )
    .send( JSON.stringify( newBody ) )
    .then( ( [body, res] ) => {
      response.status( res.statusCode ).send( body )
    } );
} );

router.get( "/Users/:userId", ( request, response ) => {
  let {params, user} = request
  let {userId} = params

  getSubClients( response, request, user, ( subclients ) => {
    try {
      let header = {
        "Content-Type": ((request.headers.hasOwnProperty( "content-type" )) ? request.headers["content-type"] : "application/json"),
        "maxdataserviceversion": ((request.headers.hasOwnProperty( "maxdataserviceversion" )) ? request.headers["maxdataserviceversion"] : "3.0"),
        "dataserviceversion": ((request.headers.hasOwnProperty( "dataserviceversion" )) ? request.headers["dataserviceversion"] : "2.0")
      };
      header.authorization = "Bearer " + user.id_token

      let searchUrl = "https://" + UserAzureApiHost + "/api/Users/" + userId;
      es6Request.get( searchUrl )
        .headers( header )
        .then( ( [body, res] ) => {
          response.type( ((request.headers.hasOwnProperty( "content-type" )) ? request.headers["content-type"] : "application/json") );
          response.status( res.statusCode );
          let newBody = JSON.parse( body )
          if ( request.user["backendUser"].isAdminBpw ) {
            getClients( response, request, user, ( clients ) => {
              getSubClientsForClient( response, request, user, getClientId(newBody), ( subClientsForClient ) => {
                newBody = mapUserItems( request, [newBody], subClientsForClient, clients )[0]
                newBody.clients = clients
                newBody.subclients = subclients
                newBody.subClientsForClient = subClientsForClient
                response.send( JSON.stringify( newBody ) );
              } )
            } )
          } else {
            newBody = mapUserItems( request, [newBody], subclients )[0]
            newBody.subclients = subclients
            response.send( JSON.stringify( newBody ) );
          }
          // should output this README file!
        } );
    } catch ( e ) {
      console.log( e.toString() );
      response.status( 500 ).send( e.toString() );
    }
  } )
} );

router.patch( "/Users/:userId", ( request, response ) => {
  let {body, params, user} = request
  let {userId} = params
  let url = "https://" + UserAzureApiHost + "/api/Users/" + userId;
  let header = {
    "Content-Type": "application/json",
    "authorization": "Bearer " + user.id_token
  };

  let pars = (Object.keys( body ).length > 0) ? body : body;
  let newBody = createNewBody( pars, request );
  es6Request.patch( url )
    .headers( header )
    .send( JSON.stringify( newBody ) )
    .then( ( [body, res] ) => {
      response.status( res.statusCode ).send( body )
    } );
} );


router.delete( "/Users/:userId", ( request, response ) => {
  let {params, user} = request
  let {userId} = params
  let url = "https://" + UserAzureApiHost + "/api/Users/" + userId;
  let header = {
    "Content-Type": "application/json",
    "authorization": "Bearer " + user.id_token
  };

  es6Request.delete( url )
    .headers( header )
    .then( ( [body, res] ) => {
      response.status( res.statusCode ).send( body )
    } );
} );


export default router
