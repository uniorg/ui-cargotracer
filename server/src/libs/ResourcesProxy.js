import express from "express";
import path from "path";
import url from 'url'

const router = express.Router();

router.get( "/fontAwesomeFonts/*", function( req, res ) {

  let pathUrl = path.resolve( __dirname + "/../../../" + "client/build/" + url.parse( req.url ).pathname );
  res.sendFile( pathUrl );
} );

export default router;