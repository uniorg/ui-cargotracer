import bunyan from 'bunyan'
import config from '../config'
import passport from "passport";
import passportAzureAd from "passport-azure-ad";
import { findAndUpdateByOid, findByOid, getUserObject, users } from "../loginHelper";
import expressSession from "express-session";
import methodOverride from "method-override";

const log = bunyan.createLogger( {
                                   name: 'Tracking App'
                                 } )
const destroySessionUrl = config.destroySessionUrl;

let setuserLogin = ( user, callback ) => {
  let newDate = new Date()
  let sendUserData = {
    lastLogin: newDate.toISOString(),
    lastAction: newDate.toISOString(),
    userConfigs: []
  }
  getUserObject( user.id_token, sendUserData, ( userBack ) => {
    return callback( userBack );
  } )

}

let initLoginService = ( app ) => {
  const OIDCStrategy = passportAzureAd.OIDCStrategy
  const OIDConfig = {
    identityMetadata: config.creds.identityMetadata,
    clientID: config.creds.clientID,
    responseType: config.creds.responseType,
    responseMode: config.creds.responseMode,
    redirectUrl: config.creds.redirectUrlLocal,
    allowHttpForRedirectUrl: config.creds.allowHttpForRedirectUrl,
    clientSecret: config.creds.clientSecret,
    validateIssuer: config.creds.validateIssuer,
    isB2C: config.creds.isB2C,
    issuer: config.creds.issuer,
    passReqToCallback: config.creds.passReqToCallback,
    scope: config.creds.scope,
    loggingLevel: config.creds.loggingLevel,
    nonceLifetime: config.creds.nonceLifetime,
    nonceMaxAmount: config.creds.nonceMaxAmount,
    useCookieInsteadOfSession: config.creds.useCookieInsteadOfSession,
    cookieEncryptionKeys: config.creds.cookieEncryptionKeys,
    clockSkew: config.creds.clockSkew
  }

  passport.serializeUser( function( user, done ) {
    done( null, user.oid );
  } );

  passport.deserializeUser( function( oid, done ) {
    findByOid( oid, function( err, user ) {
      done( err, user );
    } );
  } );

  passport.use( new OIDCStrategy( OIDConfig,
                                  function( iss, sub, profile, accessToken, refreshToken, done ) {
                                    if ( !profile.oid ) {
                                      return done( new Error( "No oid found" ), null );
                                    }
                                    // asynchronous verification, for effect...
                                    process.nextTick( function() {
                                      profile.accessToken = accessToken;
                                      profile.refreshToken = refreshToken;
                                      setuserLogin( profile, function( userBack ) {
                                        findByOid( profile.oid, function( err, instanceUser ) {
                                          if ( err ) {
                                            return done( err );
                                          }

                                          if ( !instanceUser ) {
                                            // "Auto-registration"
                                            profile.backendUser = JSON.parse( userBack )
                                            users.push( profile );
                                            return done( null, profile );
                                          } else {
                                            findAndUpdateByOid( profile.oid, function( err, user ) {
                                              profile.backendUser = JSON.parse( userBack )
                                              return done( null, profile );
                                            }, profile );
                                          }
                                        } );
                                      } );
                                    } );
                                  }
  ) );

  app.use( expressSession(
    {
      secret: 'keyboard cat',
      cookie: {
        maxAge: config.sessionMaxAge * 1000,
        secure: false
      },
      resave: false,
      saveUninitialized: false
    } ) );
  app.use( methodOverride() );

  app.use( passport.initialize( {} ) );
  app.use( passport.session( {
                               secret: 'keyboard cat',
                               cookie: {
                                 maxAge: config.sessionMaxAge * 1000,
                                 secure: false
                               },
                               resave: false,
                               saveUninitialized: false
                             } ) );

  app.get( '/login',
           function( req, res, next ) {
             passport.authenticate( 'azuread-openidconnect',
                                    {
                                      response: res,                       // optional. Provide a value if you want to provide custom state value.
                                      failureRedirect: '/',
                                      session: false
                                    }
             )( req, res, next );
           },
           function( req, res ) {
             log.info( 'Login was called in the Sample' );
             res.redirect( '/' );
           } );

  app.post( '/auth/openid/return',
            function( req, res, next ) {
              passport.authenticate( 'azuread-openidconnect',
                                     {
                                       response: res,                      // required
                                       failureRedirect: '/'
                                     }
              )( req, res, next );
            },
            function( req, res ) {
              res.redirect( "/" );
            } );

  app.get( '/logout', function( req, res ) {
    //setuserLogin( req.user, "inactive", function() {
      req.session.destroy( function( err ) {
        req.logOut();
        res.redirect( destroySessionUrl );
      } );
   // } );
  } );
}


export { initLoginService }
