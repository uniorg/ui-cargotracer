import express from "express";
import request from 'request'
import path from "path";
import { ensureAuthenticated } from '../loginHelper';
import http from "https";
import util from "util";
import fs from "fs";

const router = express.Router();
let mapHost = "atlas.microsoft.com";

router.get( "/location/*", ensureAuthenticated, function( req1, res1 ) {
  console.log( "test" );

  let url = "https://" + mapHost + req1.url.split( "/location" )[1].replace( "subscription-key=undefined", "" );
  request.get(
    {
      url: url + "&api-version=1.0&subscription-key=FFBS_8Ze1lxaR0K6TBV2CImFUbtCm4eqX-4eqf1l71M",
      headers: {
        "Content-Type": ((req1.headers.hasOwnProperty( "content-type" )) ? req1.headers["content-type"] : "application/json")
      }
    },
    function( error, response, body ) {
      res1.type( response.headers["content-type"] || "application/json" );
      res1.send( body );
    }
  );
} );

let getLocationParts = ( locations ) => {
  let partLength = 2500;
  let actualPartLength = 0;
  let parts = [];
  let actualPart = [];
  for ( let i = 0; i < locations.length; i++ ) {
    let location = locations[i];
    let partString = location.lat + "," + location.lng + ":";
    let newPartLength = actualPartLength + partString.length;

    if ( newPartLength <= partLength ) {
      actualPartLength = newPartLength;
      actualPart.push( partString );
    } else {
      parts.push( actualPart );
      actualPartLength = 0;
      actualPart = [];

      actualPartLength = actualPartLength + partString.length;
      actualPart.push( partString );
    }
  }
  if ( parts.length === 0 && actualPart.length > 0 ) {
    parts.push( actualPart );
  }
  return parts;
}

let setMainRoute = ( routes ) => {

  let summary = {
    arrivalTime: [],
    departureTime: [],
    historicTrafficTravelTimeInSeconds: [],
    lengthInMeters: [],
    liveTrafficIncidentsTravelTimeInSeconds: [],
    noTrafficTravelTimeInSeconds: [],
    trafficDelayInSeconds: [],
    travelTimeInSeconds: [],
  };
  let back = {
    legs: [],
    points: [],
    sections: [],
    summary: []
  };

  for ( let i = 0; i < routes.length; i++ ) {

    let route = routes[i];

    for ( let j = 0; j < route.length; j++ ) {

      let routePart = route[j];
      if ( back.summary[j] === undefined ) {
        back.summary[j] = summary;
      }

      for ( let l = 0; l < routePart.legs.length; l++ ) {
        if ( back.legs[j] === undefined ) {
          back.legs[j] = [];
        }
        back.legs[j].push( routePart.legs[l] );
        for ( let lp = 0; lp < routePart.legs[l].points.length; lp++ ) {
          if ( back.points[j] === undefined ) {
            back.points[j] = [];
          }
          back.points[j].push( routePart.legs[l].points[lp] );
        }
      }
      for ( let s = 0; s < routePart.sections.length; s++ ) {
        if ( back.sections[j] === undefined ) {
          back.sections[j] = [];
        }
        back.sections[j].push( routePart.sections[s] );
      }

      if ( routePart.hasOwnProperty( "summary" ) ) {
        if ( routePart.summary.hasOwnProperty( "arrivalTime" ) )
          back.summary[j].arrivalTime.push( routePart.summary.arrivalTime );
        if ( routePart.summary.hasOwnProperty( "departureTime" ) )
          back.summary[j].departureTime.push( routePart.summary.departureTime );
        if ( routePart.summary.hasOwnProperty( "historicTrafficTravelTimeInSeconds" ) )
          back.summary[j].historicTrafficTravelTimeInSeconds.push( routePart.summary.historicTrafficTravelTimeInSeconds );
        if ( routePart.summary.hasOwnProperty( "lengthInMeters" ) )
          back.summary[j].lengthInMeters.push( routePart.summary.lengthInMeters );
        if ( routePart.summary.hasOwnProperty( "liveTrafficIncidentsTravelTimeInSeconds" ) )
          back.summary[j].liveTrafficIncidentsTravelTimeInSeconds.push( routePart.summary.liveTrafficIncidentsTravelTimeInSeconds );
        if ( routePart.summary.hasOwnProperty( "trafficDelayInSeconds" ) )
          back.summary[j].trafficDelayInSeconds.push( routePart.summary.trafficDelayInSeconds );
        if ( routePart.summary.hasOwnProperty( "travelTimeInSeconds" ) )
          back.summary[j].travelTimeInSeconds.push( routePart.summary.travelTimeInSeconds );
      }
    }
  }
  return back;
}

let callDestinationParts = ( parts, urls, bodyRouts, pars, callback ) => {
  if ( parts[0] === undefined ) {
    callback( {routes: bodyRouts, urls: urls, mainRout: setMainRoute( bodyRouts )} );
  } else {

    let query = parts[0].join( "" );
    if ( query.slice( -1 ) === ":" )
      query = query.substring( 0, query.length - 1 );
    let url = "https://" + mapHost + "/route/directions/json?subscription-key=FFBS_8Ze1lxaR0K6TBV2CImFUbtCm4eqX-4eqf1l71M&api-version=1.0&query=" + query + "&travelMode=truck&vehicleWidth=2.60&vehicleHeight=4&vehicleLength=20&computeTravelTimeFor=all";
    urls.push( url );
    urls.push( url );
    request.get(
      {
        url: url,
        headers: {
          "Content-Type": "application/json"
        }
      },
      function( error, response, body ) {
        let routes = JSON.parse( body );
        routes = ((routes.hasOwnProperty( "routes" )) ? routes.routes : null);
        bodyRouts.push( routes );
        parts.splice( 0, 1 );
        if ( parts.length === 0 ) {
          callback( {routes: bodyRouts, urls: urls, mainRout: setMainRoute( bodyRouts )} );
        } else {
          callDestinationParts( parts, urls, bodyRouts, pars, callback );
        }
      }
    );
  }
}

router.post( "/direction", function( req1, res1 ) {

  let pars = (Object.keys( req1.body ).length > 0) ? req1.body : req1.query;
  let parts = getLocationParts( pars.locations );
  callDestinationParts( parts, [], [], pars, function( back ) {
    res1.type( "application/json" );
    res1.send( back );
  } );
} );

router.get( "/locationSdk/map/tile/pbf*", ensureAuthenticated, function( req, res ) {
  let url = "https://" + mapHost + req.url
    .replace( "/locationSdk", "" )
    .replace( "subscription-key=undefined", "subscription-key=FFBS_8Ze1lxaR0K6TBV2CImFUbtCm4eqX-4eqf1l71M" );

  res.redirect( url );
} );
router.get( "*.ttf", ensureAuthenticated, function( req, res ) {
  let url = "https://" + mapHost + req.url
    .replace( "/locationSdk", "" );

  res.redirect( url );
} );
router.get( "/sdk/js/atlas.min.js", ensureAuthenticated, function( req1, res1 ) {

  request.get(
    {
      url: "https://" + mapHost + "/sdk/js/atlas.min.js?api-version=1.0",
      headers: {
        "Content-Type": ((req1.headers.hasOwnProperty( "content-type" )) ? req1.headers["content-type"] : "application/javascript")
      }
    },
    function( error, response, body ) {
      res1.type( response.headers["content-type"] || "application/javascript" );
      res1.send( body.replace( '{subdomain:"atlas",domain:"microsoft.com",path:"traffic/flow/tile/png",queryParams:{"subscription-key":r.map.getServiceOptions()["subscription-key"],"api-version":"1.0",style:r.map.getTraffic().flow,zoom:"{z}",x:"{x}",y:"{y}"}}',
                               '{domain:"localhost:1337/locationSdk",path:"traffic/flow/tile/png",queryParams:{"subscription-key":r.map.getServiceOptions()["subscription-key"],"api-version":"1.0",style:r.map.getTraffic().flow,zoom:"{z}",x:"{x}",y:"{y}"}}' )

                   .replace( '{subdomain:"atlas",domain:"microsoft.com",path:"map/tile/pbf",queryParams:{"subscription-key":l["subscription-key"],view:l.view,language:l.language,"api-version":"1.0",layer:"basic",style:"main",zoom:"{z}",x:"{x}",y:"{y}"}}',
                             '{domain:"localhost:1337/locationSdk",path:"map/tile/pbf",queryParams:{"subscription-key":l["subscription-key"],view:l.view,language:l.language,"api-version":"1.0",layer:"basic",style:"main",zoom:"{z}",x:"{x}",y:"{y}"}}' )

                   .replace( '{subdomain:"atlas",domain:"microsoft.com",path:"map/tile/pbf",queryParams:{"subscription-key":t["subscription-key"],view:t.view,language:t.language,"api-version":"1.0",layer:"basic",style:"main",zoom:"{z}",x:"{x}",y:"{y}"}}',
                             '{domain:"localhost:1337/locationSdk",path:"map/tile/pbf",queryParams:{"subscription-key":t["subscription-key"],view:t.view,language:t.language,"api-version":"1.0",layer:"basic",style:"main",zoom:"{z}",x:"{x}",y:"{y}"}}' )

                   .replace( '{subdomain:"atlas",domain:"microsoft.com",path:"map/tile/pbf",queryParams:{"subscription-key":o["subscription-key"],view:o.view,language:o.language,"api-version":"1.0",layer:"basic",style:"main",zoom:"{z}",x:"{x}",y:"{y}"}}',
                             '{domain:"localhost:1337/locationSdk",path:"map/tile/pbf",queryParams:{"subscription-key":o["subscription-key"],view:o.view,language:o.language,"api-version":"1.0",layer:"basic",style:"main",zoom:"{z}",x:"{x}",y:"{y}"}}' )

                   .replace( '{subdomain:"atlas",domain:"microsoft.com",path:"map/copyright/caption/json",queryParams:{"subscription-key":this.serviceOptions["subscription-key"],"api-version":i.version},headers:{"Accept-Language":this.serviceOptions["session-id"]}}',
                             '{domain:"localhost:1337/locationSdk",path:"map/copyright/caption/json",queryParams:{"subscription-key":this.serviceOptions["subscription-key"],"api-version":i.version},headers:{"Accept-Language":this.serviceOptions["session-id"]}}' )
                   .replace( '{subdomain:"atlas",domain:"microsoft.com",path:"traffic/incident/detail/json",queryParams:__assign({"subscription-key":this.serviceOptions["subscription-key"],"api-version":i.version,projection:"EPSG4326",style:"s3",boundingbox:n,boundingzoom:i.zoom,trafficmodelid:-1},i.params),headers:{"Accept-Language":this.serviceOptions["session-id"]}}',
                             '{domain:"localhost:1337/locationSdk",path:"traffic/incident/detail/json",queryParams:__assign({"subscription-key":this.serviceOptions["subscription-key"],"api-version":i.version,projection:"EPSG4326",style:"s3",boundingbox:n,boundingzoom:i.zoom,trafficmodelid:-1},i.params),headers:{"Accept-Language":this.serviceOptions["session-id"]}}' )
                   .replace( '{subdomain:"atlas",domain:"microsoft.com",path:"traffic/flow/tile/png",queryParams:{"subscription-key":r.map.getServiceOptions()["subscription-key"],"api-version":"1.0",style:r.map.getTraffic().flow,zoom:"{z}",x:"{x}",y:"{y}"}}',
                             '{domain:"localhost:1337/locationSdk",path:"traffic/flow/tile/png",queryParams:{"subscription-key":r.map.getServiceOptions()["subscription-key"],"api-version":"1.0",style:r.map.getTraffic().flow,zoom:"{z}",x:"{x}",y:"{y}"}}' )
                   .replace( '"https"', '"http"' ) );
    }
  );
} );
router.get( "/locationSdk/*", ensureAuthenticated, function( req1, res1 ) {
  let url = "https://" + mapHost + req1.url
    .replace( "/locationSdk", "" )
    .replace( "subscription-key=undefined", "subscription-key=FFBS_8Ze1lxaR0K6TBV2CImFUbtCm4eqX-4eqf1l71M" );
  request.get(
    {
      url: url,
      headers: {
        "Content-Type": ((req1.headers.hasOwnProperty( "content-type" )) ? req1.headers["content-type"] : "application/javascript")
      }
    },
    function( error, response, body ) {
      res1.type( response.headers["content-type"] || "application/javascript" );
      res1.send( body.replace( /https:\/\/atlas\.microsoft\.com/g, "http://localhost:1337/locationSdk" )
                   .replace( /microsoft\.com/g, "localhost:1337" ) );
    }
  );
} );

let deleteFile = ( file, callback ) => {
  if ( fs.existsSync( file ) ) {
    fs.unlink( file, function( err ) {
      ;
      callback()
      if ( err ) return console.log( err )
    } );
  } else {
    callback()
  }
}

let pDownload = ( url, dest ) => {
  var file = fs.createWriteStream( dest );
  return new Promise( ( resolve, reject ) => {
    var responseSent = false; // flag to make sure that response is sent only once.
    http.get( url, response => {
      response.pipe( file );
      file.on( 'finish', () => {
        file.close( () => {
          if ( responseSent ) return;
          responseSent = true;
          resolve();
        } );
      } );
    } ).on( 'error', err => {
      if ( responseSent ) return;
      responseSent = true;
      reject( err );
    } );
  } );
}

router.get( "/map/tile/png*", function( req, res ) {
  res.set( 'Cache-Control', 'public, max-age=259200' );
  let url = "https://" + mapHost + req.url
    .replace( "/azure", "" )
    .replace( "subscription-key=undefined", "subscription-key=FFBS_8Ze1lxaR0K6TBV2CImFUbtCm4eqX-4eqf1l71M" );
  let splitUrl = req.url.split( "&" );
  let zoom = null;
  let x = null;
  let y = null;
  let layer = null;
  for ( let pathUrl in splitUrl ) {
    let item = splitUrl[pathUrl];
    if ( item.search( "zoom" ) > -1 ) {
      zoom = item.split( "=" )[1]
    }
    if ( item.search( "x" ) > -1 ) {
      x = item.split( "=" )[1]
    }
    if ( item.search( "y" ) > -1 ) {
      y = item.split( "=" )[1]
    }
    if ( item.search( "layer" ) > -1 ) {
      layer = item.split( "=" )[1]
    }
  }
  if ( zoom !== null && x !== null && y !== null && layer !== null ) {
    let pathUrl = path.resolve( __dirname + "/../../../mapFilesAzure/" + zoom + x + y + layer + ".png" );

    if ( fs.existsSync( pathUrl ) ) {
      fs.stat( pathUrl, function( err, stats ) {
        if ( stats.size !== 317 && stats.size !== 0 ) {

          let mtime = new Date( util.inspect( stats.mtime ) );
          let options = {
            dotfiles: "deny",
            headers: {
              "x-timestamp": Date.parse( util.inspect( stats.mtime ) ),
              "Last-Modified": mtime,
              "x-sent": true
            }
          };
          res.sendFile( pathUrl, options );
        } else {
          pDownload( url, pathUrl )
            .then( function() {
              fs.stat( pathUrl, function( err, stats ) {
                let mtime = new Date( util.inspect( stats.mtime ) );
                let options = {
                  dotfiles: "deny",
                  etag: true,
                  headers: {
                    "x-timestamp": Date.parse( util.inspect( stats.mtime ) ),
                    "Last-Modified": mtime,
                    "x-sent": true
                  }
                };
                res.sendFile( pathUrl, options );
              } );
            } )
            .catch( e => console.error( 'error while downloading', e ) );
        }

      } );
    } else {
      pDownload( url, pathUrl )
        .then( function() {
          fs.stat( pathUrl, function( err, stats ) {
            let mtime = new Date( util.inspect( stats.mtime ) );
            let options = {
              dotfiles: "deny",
              etag: true,
              headers: {
                "x-timestamp": Date.parse( util.inspect( stats.mtime ) ),
                "Last-Modified": mtime,
                "x-sent": true
              }
            };
            res.sendFile( pathUrl, options );
          } );
        } )
        .catch( e => console.error( 'error while downloading', e ) );
    }
  }
} );

router.get( "/openstreetmap*", function( req, res ) {
  res.set( 'Cache-Control', 'public, max-age=259200' );
  let splitUrl = req.url.split( "&" );
  let zoom = null;
  let x = null;
  let y = null;
  for ( let pathUrl in splitUrl ) {
    let item = splitUrl[pathUrl];
    if ( item.search( "zoom" ) > -1 ) {
      zoom = item.split( "=" )[1]
    }
    if ( item.search( "x" ) > -1 ) {
      x = item.split( "=" )[1]
    }
    if ( item.search( "y" ) > -1 ) {
      y = item.split( "=" )[1]
    }
  }
  if ( zoom !== null && x !== null && y !== null ) {
    let url = "tile.openstreetmap.org/" + zoom + "/" + x + "/" + y + ".png";
    let pathUrl = path.resolve( __dirname + "/../../../mapFilesOpenStreetMap/" + zoom + x + y + ".png" );

    if ( fs.existsSync( pathUrl ) ) {
      fs.stat( pathUrl, function( err, stats ) {
        console.log( "size", stats.size )
        if ( stats.size !== 305 && stats.size !== 304 && stats.size !== 0 && stats.size !== 162 ) {

          let mtime = new Date( util.inspect( stats.mtime ) );
          let options = {
            dotfiles: "deny",
            etag: true,
            headers: {
              "x-timestamp": Date.parse( util.inspect( stats.mtime ) ),
              "Last-Modified": mtime,
              "x-sent": true
            }
          };
          res.sendFile( pathUrl, options );
        } else {
          console.log( "download", url )
          pDownload( url, pathUrl )
            .then( function() {
              fs.stat( pathUrl, function( err, stats ) {
                let mtime = new Date( util.inspect( stats.mtime ) );
                let options = {
                  dotfiles: "deny",
                  etag: true,
                  headers: {
                    "x-timestamp": Date.parse( util.inspect( stats.mtime ) ),
                    "Last-Modified": mtime,
                    "x-sent": true
                  }
                };
                res.sendFile( pathUrl, options );
              } );
            } )
            .catch( e => console.error( 'error while downloading', e ) );
        }

      } );
    } else {
      pDownload( url, pathUrl )
        .then( function() {
          fs.stat( pathUrl, function( err, stats ) {
            let mtime = new Date( util.inspect( stats.mtime ) );
            let options = {
              dotfiles: "deny",
              etag: true,
              headers: {
                "x-timestamp": Date.parse( util.inspect( stats.mtime ) ),
                "Last-Modified": mtime,
                "x-sent": true
              }
            };
            res.sendFile( pathUrl, options );
          } );
        } )
        .catch( e => console.error( 'error while downloading', e ) );
    }
  }
} );

let toQuad = ( x, y, z ) => {
  let quadKey = [];
  for ( let i = z; i > 0; i-- ) {
    let digit = '0';
    let mask = 1 << (i - 1);
    if ( (x & mask) != 0 ) {
      digit++;
    }
    if ( (y & mask) != 0 ) {
      digit++;
      digit++;
    }
    quadKey.push( digit );
  }
  if ( quadKey.join( '' ) === "" ) {
    return null;
  }
  return quadKey.join( '' );
}

router.get( "/bing*", function( req, res ) {
  res.set( 'Cache-Control', 'public, max-age=259200' );
  let splitUrl = req.url.split( "&" );
  let zoom = null;
  let x = null;
  let y = null;
  let mapType = null;

  for ( let pathUrl in splitUrl ) {
    let item = splitUrl[pathUrl];
    if ( item.indexOf( "zoom" ) > -1 ) {
      zoom = item.split( "=" )[1];
    }
    if ( item.indexOf( "x" ) === 0 ) {
      x = item.split( "=" )[1];
    }
    if ( item.indexOf( "y" ) === 0 ) {
      y = item.split( "=" )[1];
    }
    if ( item.indexOf( "mapType" ) === 0 ) {
      mapType = item.split( "=" )[1];
    }
  }
  if ( zoom !== null && x !== null && y !== null ) {
    let quadKey = toQuad( x, y, zoom );
    //let url = "https://ecn.t3.tiles.virtualearth.net/tiles/" + type + quadKey + ".png?g=471&mkt=de-DE&it=A,G,RL&shading=hill&n=z&og=256&c4w=1&key=AiYKfPDBW2EtBlLK0Dkapiigfgy4wt7cwPoaC72GhBtCKbODUb5bhiEPlmxxgkjO";
    //let url = "https://t.ssl.ak.dynamic.tiles.virtualearth.net/comp/ch/" + quadKey + "?mkt=de-DE&it=G,VE,BX,L,LA&shading=hill&n=z&og=256&c4w=1&key=AiYKfPDBW2EtBlLK0Dkapiigfgy4wt7cwPoaC72GhBtCKbODUb5bhiEPlmxxgkjO";
    let url;
    switch ( mapType ) {
      case "street":
        url = "https://t.ssl.ak.dynamic.tiles.virtualearth.net/comp/ch/" + quadKey + "?mkt=de-DE&it=G,BX,RL&shading=hill&n=z&og=256&c4w=1&key=AiYKfPDBW2EtBlLK0Dkapiigfgy4wt7cwPoaC72GhBtCKbODUb5bhiEPlmxxgkjO"; // Street
        break;
      case "sat":
        url = "https://t.ssl.ak.tiles.virtualearth.net/tiles/a" + quadKey + ".png?g=6422&n=z&c4w=1&key=AiYKfPDBW2EtBlLK0Dkapiigfgy4wt7cwPoaC72GhBtCKbODUb5bhiEPlmxxgkjO"; // Satellite
        break;
      case "hybrid":
        url = "https://t.ssl.ak.dynamic.tiles.virtualearth.net/comp/ch/" + quadKey + "?mkt=de-DE&it=A,G,RL&shading=hill&n=z&og=256&c4w=1&key=AiYKfPDBW2EtBlLK0Dkapiigfgy4wt7cwPoaC72GhBtCKbODUb5bhiEPlmxxgkjO"; // Hybrid
        break;
      default:
        url = "https://t.ssl.ak.dynamic.tiles.virtualearth.net/comp/ch/" + quadKey + "?mkt=de-DE&it=A,G,RL&shading=hill&n=z&og=256&c4w=1&key=AiYKfPDBW2EtBlLK0Dkapiigfgy4wt7cwPoaC72GhBtCKbODUb5bhiEPlmxxgkjO";
        break;
    }

    let pathUrl = path.resolve( __dirname + "/../../../mapFilesBing/" + quadKey + "-" + mapType + ".png" );
    if ( fs.existsSync( pathUrl ) ) {
      fs.stat( pathUrl, function( err, stats ) {
        if ( stats.size !== 305 && stats.size !== 304 && stats.size !== 0 ) {

          let mtime = new Date( util.inspect( stats.mtime ) );
          let options = {
            dotfiles: "deny",
            etag: true,
            headers: {
              "x-timestamp": Date.parse( util.inspect( stats.mtime ) ),
              "Last-Modified": mtime,
              "x-sent": true
            }
          };
          res.sendFile( pathUrl, options );
        } else {
          pDownload( url, pathUrl )
            .then( function() {
              fs.stat( pathUrl, function( err, stats ) {
                let mtime = new Date( util.inspect( stats.mtime ) );
                let options = {
                  dotfiles: "deny",
                  etag: true,
                  headers: {
                    "x-timestamp": Date.parse( util.inspect( stats.mtime ) ),
                    "Last-Modified": mtime,
                    "x-sent": true
                  }
                };
                res.sendFile( pathUrl, options );
              } );
            } )
            .catch( e => console.error( 'error while downloading', e ) );
        }

      } );
    } else {
      pDownload( url, pathUrl )
        .then( function() {
          fs.stat( pathUrl, function( err, stats ) {
            let mtime = new Date( util.inspect( stats.mtime ) );
            let options = {
              dotfiles: "deny",
              etag: true,
              headers: {
                "x-timestamp": Date.parse( util.inspect( stats.mtime ) ),
                "Last-Modified": mtime,
                "x-sent": true
              }
            };
            res.sendFile( pathUrl, options );
          } );
        } )
        .catch( e => console.error( 'error while downloading', e ) );
    }
  }
} );


export default router