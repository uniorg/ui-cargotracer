import express from "express";
import config from '../config'
import { users } from "../loginHelper";

const router = express.Router();

let checkKey = (key) => {
  switch(key) {
    case "APPSETTING_AppInsightsInstrumentationKey":
      return true
    case "APPSETTING_environment":
      return true
    default:
      return false
  }
}

// POST /user/signin
router.get( '/environment/:key', ( request, response ) => {
  let {params, user} = request
  let {key} = params
  if ( checkKey(key) && user !== undefined && user.id_token !== undefined ) {
    response.status( 200 ).send( {
                                   key: key,
                                   value: ((process.env[key] !== undefined) ? process.env[key] : ((config.creds[key] !== undefined) ? config.creds[key] : null))
                                 } );
  } else {
    response.status( 403 ).send( {
                                   key: key,
                                   error: "401 Unauthorized"
                                 } );
  }
} );


router.post( '/environment', ( request, response ) => {
  let {body, user} = request
  let {keys} = body
  if ( Array.isArray(keys) && user !== undefined && user.id_token !== undefined ) {
    let keysReturn = [];
    for (let i=0; i<keys.length; i++) {
      let key = keys[i]
      if (checkKey(key)) {
          keysReturn.push({
                            key: key,
                            value: ((process.env[key] !== undefined) ? process.env[key] : ((config.creds[key] !== undefined) ? config.creds[key] : null))
                          })
      }
    }
    response.status( 200 ).send( keysReturn );
  } else {
    response.status( 403 ).send( {
                                   body: body,
                                   keys: keys,
                                   error: "401 Unauthorized"
                                 } );
  }
} );


router.get( '/serverConfig', ( request, response ) => {
  if (request.user.backendUser.isAdminBpw) {
    response.send( config );
  } else {
    response.sendStatus(403)
  }
} );

export default router;