const config = {}
config.host = {
	UserApiHost: process.env["APPSETTING_UserApiHost"] || "cargotracer-backend-dev.azurewebsites.net",
	UserAzureApiHost: process.env["APPSETTING_UserAzureApiHost"] || "trackingbpwuserapi-dev.azurewebsites.net",
	AssetUIBackendHost: process.env["APPSETTING_AssetUIBackendHost"] || "cargotracer-backend-dev.azurewebsites.net",
	DeliveryUIDeviceHost: process.env["APPSETTING_DeliveryUIDeviceHost"] || "cargotracer-backend-dev.azurewebsites.net",
	TrackingUIDeviceHost: process.env["APPSETTING_TrackingUIDeviceHost"] || "cargotracer-backend-dev.azurewebsites.net"
}
config.applicationinsights = {
	AppInsightsInstrumentationKey: process.env["APPSETTING_AppInsightsInstrumentationKey"] || '2c07150a-e0f3-4a34-a36c-9c4648ac3fb2'
}

config.environment = process.env.environment || "dev"
config.creds = {
	userApiClientKey: process.env["APPSETTING_UserApiClientKey"] || "6eae7dd58bae41f6b4b3f2471ca79944", //dev
	//userApiClientKey: "23b4d61746844a2c9331289ebf7f77da", //qa
	//userApiClientKey: "836d2de46ad84914bc0f08eff7ccfc86", //prod

	identityMetadata: process.env["APPSETTING_IdentityMetadata"] || 'https://login.microsoftonline.com/bpwinnolabbpwdev.onmicrosoft.com/v2.0/.well-known/openid-configuration', //dev
	//identityMetadata: 'https://login.microsoftonline.com/bpwinnolabbpwqa.onmicrosoft.com/v2.0/.well-known/openid-configuration', //qa
	//identityMetadata: 'https://login.microsoftonline.com/bpwinnolabbpw.onmicrosoft.com/v2.0/.well-known/openid-configuration', //qa

	// Required, the client ID of your app in AAD
	clientID: process.env["APPSETTING_AADclientID"] || '53574537-53d0-4f21-97bd-2687a49f7bba', //dev
	//clientID: '3fbee5b0-f790-412e-b322-3cba3f9ab83c', //qa
	//clientID: 'e5dd271d-6b60-4415-a001-5af59f7e7c8a', //prod

	// Required, must be 'code', 'code id_token', 'id_token code' or 'id_token'
	// If you want to get access_token, you must be 'code', 'code id_token' or 'id_token code'
	responseType: 'code id_token',

	// Required
	responseMode: 'form_post',

	// Required, the reply URL registered in AAD for your app
	redirectUrlLocal: process.env["APPSETTING_OIDConfigRedirectUrl"] || 'http://localhost:1337/auth/openid/return',

	// Required if we use http for redirectUrl
	allowHttpForRedirectUrl: true,

	// Required if `responseType` is 'code', 'id_token code' or 'code id_token'.
	// If app key contains '\', replace it with '\\'.
	clientSecret: process.env["APPSETTING_AADclientSecret"] || '8g~#Y0YJhrCXTA$kV~%|LXJ8', //dev
	//clientSecret: '>U9xW"o2p)tj8I5#6(Sfn&)6', //qa
	//clientSecret: 'Z%%:HP$9l0Axs9pOEe,L^ahi', // prod

	// Required, must be true for B2C
	isB2C: true,

	// Required to set to false if you don't want to validate issuer
	validateIssuer: true,

	// Required if you want to provide the issuer(s) you want to validate instead of using the issuer from metadata
	issuer: null,

	// Required to set to true if the `verify` function has 'req' as the first parameter
	passReqToCallback: false,

	// Recommended to set to true. By default we save state in express session, if this option is set to true, then
	// we encrypt state and save it in cookie instead. This option together with { session: false } allows your app
	// to be completely express session free.
	useCookieInsteadOfSession: true,

	// Required if `useCookieInsteadOfSession` is set to true. You can provide multiple set of key/iv pairs for key
	// rollover purpose. We always use the first set of key/iv pair to encrypt cookie, but we will try every set of
	// key/iv pair to decrypt cookie. Key can be any string of length 32, and iv can be any string of length 12.
	cookieEncryptionKeys: [
		{'key': '12345678901234567890123456789012', 'iv': '123456789012'},
		{'key': 'abcdefghijklmnopqrstuvwxyzabcdef', 'iv': 'abcdefghijkl'}
	],

	// Optional. The additional scope you want besides 'openid'
	// (1) if you want refresh_token, use 'offline_access'
	// (2) if you want access_token, use the clientID
	scope: ['offline_access', "https://bpwinnolabbpwdev.onmicrosoft.com/Usermanagementapi/demo.read", "https://bpwinnolabbpwdev.onmicrosoft.com/Usermanagementapi/demo.write"],

	// Optional, 'error', 'warn' or 'info'
	loggingLevel: 'info',

	// Optional. The lifetime of nonce in session or cookie, the default value is 3600 (seconds).
	nonceLifetime: null,

	// Optional. The max amount of nonce saved in session or cookie, the default value is 10.
	nonceMaxAmount: 5,

	// Optional. The clock skew allowed in token validation, the default value is 300 seconds.
	clockSkew: null,
};

// The url you need to go to destroy the session with AAD,
// replace <tenant_name> with your tenant name, and
// replace <signin_policy_name> with your signin policy name.
config.destroySessionUrl = process.env["APPSETTING_destroySessionUrl"] || 'https://login.microsoftonline.com/bpwinnolabbpwdev.onmicrosoft.com/oauth2/v2.0/logout?p=b2c_1_signin&post_logout_redirect_uri=http://localhost:1337';
//exports.destroySessionUrlLiveDevdev01 = 'https://login.microsoftonline.com/bpwinnolabbpwqa.onmicrosoft.com/oauth2/v2.0/logout?p=b2c_1_signin&post_logout_redirect_uri=https://ui5-tracking-qa.azurewebsites.net';
//exports.destroySessionUrlLiveDev = 'https://login.microsoftonline.com/bpwinnolabbpwdev.onmicrosoft.com/oauth2/v2.0/logout?p=b2c_1_signin&post_logout_redirect_uri=https://tracker.internet-of-transport.de';

// How long you want to keep session in mongoDB.
//exports.sessionMaxAge = 14400; //0.1 * 60 * 60;  // 1 day (unit is second)
//exports.sessionMaxAge = 900; //15 min
config.sessionMaxAge = process.env["APPSETTING_sessionMaxAge"] || 3600; //1h

export default config
