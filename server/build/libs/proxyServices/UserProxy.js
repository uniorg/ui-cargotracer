"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _express = _interopRequireDefault(require("express"));

var _es6Request = _interopRequireDefault(require("es6-request"));

var _config = _interopRequireDefault(require("../../config"));

var _loginHelper = require("../../loginHelper");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const router = _express.default.Router();

const host = _config.default.host.UserApiHost;
router.get('/Subclients', (request, response) => {
  let {
    params,
    user
  } = request;

  try {
    let header = {
      "Content-Type": request.headers.hasOwnProperty("content-type") ? request.headers["content-type"] : "application/json",
      "maxdataserviceversion": request.headers.hasOwnProperty("maxdataserviceversion") ? request.headers["maxdataserviceversion"] : "3.0",
      "dataserviceversion": request.headers.hasOwnProperty("dataserviceversion") ? request.headers["dataserviceversion"] : "2.0"
    };
    header.authorization = "Bearer " + user.id_token;
    let searchUrl = "https://" + host + "/tracking-user-ui-service/v2/Subclients";

    _es6Request.default.get(searchUrl).headers(header).then(([body, res]) => {
      response.type(request.headers.hasOwnProperty("content-type") ? request.headers["content-type"] : "application/json");
      response.status(res.statusCode);
      response.send(body); // should output this README file!
    });
  } catch (e) {
    response.status(500).send(e.toString());
  }
});
router.get('/clients', (request, response) => {
  let {
    params,
    user
  } = request;

  try {
    let header = {
      "Content-Type": request.headers.hasOwnProperty("content-type") ? request.headers["content-type"] : "application/json",
      "maxdataserviceversion": request.headers.hasOwnProperty("maxdataserviceversion") ? request.headers["maxdataserviceversion"] : "3.0",
      "dataserviceversion": request.headers.hasOwnProperty("dataserviceversion") ? request.headers["dataserviceversion"] : "2.0"
    };
    header.authorization = "Bearer " + user.id_token;
    let searchUrl = "https://" + host + "/tracking-user-ui-service/v2/Clients";

    _es6Request.default.get(searchUrl).headers(header).then(([body, res]) => {
      response.type(request.headers.hasOwnProperty("content-type") ? request.headers["content-type"] : "application/json");
      response.status(res.statusCode);
      response.send(body); // should output this README file!
    });
  } catch (e) {
    response.status(500).send(e.toString());
  }
});
router.get('/me', _loginHelper.ensureAuthenticated, function (req, res) {
  res.type("application/json");
  let returnValue = { ...req.user["_json"],
    ...req.user["backendUser"]
  };
  res.send(returnValue);
});
router.get('/usersLogedin', _loginHelper.ensureAuthenticated, function (req, res) {
  if (req.user.backendUser.isAdminBpw) {
    let returnUsers = _loginHelper.users.map(user => {
      return user.backendUser;
    });

    res.send(returnUsers);
  } else {
    res.sendStatus(403);
  }
});
var _default = router;
exports.default = _default;