"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _express = _interopRequireDefault(require("express"));

var _es6Request = _interopRequireDefault(require("es6-request"));

var _config = _interopRequireDefault(require("../../config"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const router = _express.default.Router();

const host = _config.default.host.AssetUIBackendHost; // POST /user/signin

router.get('/asset/detail/:id', (request, response) => {
  let {
    params,
    user
  } = request;
  let {
    id
  } = params;

  try {
    let header = {
      "Content-Type": request.headers.hasOwnProperty("content-type") ? request.headers["content-type"] : "application/json",
      "maxdataserviceversion": request.headers.hasOwnProperty("maxdataserviceversion") ? request.headers["maxdataserviceversion"] : "3.0",
      "dataserviceversion": request.headers.hasOwnProperty("dataserviceversion") ? request.headers["dataserviceversion"] : "2.0"
    };
    header.authorization = "Bearer " + user.id_token;

    _es6Request.default.get("https://" + host + "/tracking-asset-ui-service/v2/Assets('" + id + "')").headers(header).then(([body, res]) => {
      response.type(request.headers.hasOwnProperty("content-type") ? request.headers["content-type"] : "application/json");

      if (res.statusCode === 200 || res.statusCode === 201) {
        response.status(res.statusCode);
        response.send(body);
      } else {
        response.status(res.statusCode);
        response.send({
          url: "https://" + host + "/tracking-asset-ui-service/v2/Assets('" + id + "')",
          authorization: "Bearer " + user.id_token,
          body: body
        });
      } // should output this README file!

    });
  } catch (e) {
    response.status(res.statusCode).send(e.toString());
  }
}); // POST /user/signin

router.delete('/asset/delete/:id', (request, response) => {
  let {
    params,
    user
  } = request;
  let {
    id
  } = params;

  try {
    let header = {
      "Content-Type": request.headers.hasOwnProperty("content-type") ? request.headers["content-type"] : "application/json",
      "maxdataserviceversion": request.headers.hasOwnProperty("maxdataserviceversion") ? request.headers["maxdataserviceversion"] : "3.0",
      "dataserviceversion": request.headers.hasOwnProperty("dataserviceversion") ? request.headers["dataserviceversion"] : "2.0"
    };
    header.authorization = "Bearer " + user.id_token;

    _es6Request.default.delete("https://" + host + "/tracking-asset-ui-service/v2/Assets('" + id + "')").headers(header).then(([body, res]) => {
      response.type(request.headers.hasOwnProperty("content-type") ? request.headers["content-type"] : "application/json");

      if (res.statusCode === 200 || res.statusCode === 201) {
        response.status(res.statusCode);
        response.send(body);
      } else {
        response.status(res.statusCode);
        response.send({
          url: "https://" + host + "/tracking-asset-ui-service/v2/Assets('" + id + "')",
          authorization: "Bearer " + user.id_token,
          body: body
        });
      } // should output this README file!

    });
  } catch (e) {
    response.status(res.statusCode).send(e.toString());
  }
});
router.get('/asset/cycle/:id', (request, response) => {
  let {
    params,
    user
  } = request;
  let {
    id
  } = params;

  try {
    let header = {
      "Content-Type": request.headers.hasOwnProperty("content-type") ? request.headers["content-type"] : "application/json",
      "maxdataserviceversion": request.headers.hasOwnProperty("maxdataserviceversion") ? request.headers["maxdataserviceversion"] : "3.0",
      "dataserviceversion": request.headers.hasOwnProperty("dataserviceversion") ? request.headers["dataserviceversion"] : "2.0"
    };
    header.authorization = "Bearer " + user.id_token;

    _es6Request.default.get("https://" + host + "/tracking-asset-ui-service/v2/AssetCycles('" + id + "')").headers(header).then(([body, res]) => {
      response.type(request.headers.hasOwnProperty("content-type") ? request.headers["content-type"] : "application/json");

      if (res.statusCode === 200 || res.statusCode === 201) {
        response.status(res.statusCode);
        response.send(body);
      } else {
        response.status(res.statusCode);
        response.send({
          url: "https://" + host + "/tracking-asset-ui-service/v2/AssetCycles('" + id + "')",
          authorization: "Bearer " + user.id_token,
          body: body
        });
      } // should output this README file!

    });
  } catch (e) {
    response.status(res.statusCode).send(e.toString());
  }
});
router.get('/asset/assetCycleList/:id/:sort/:order/:skip/:top', (request, response) => {
  let {
    params,
    user
  } = request;
  let {
    id,
    sort,
    order,
    skip,
    top
  } = params;

  try {
    let header = {
      "Content-Type": request.headers.hasOwnProperty("content-type") ? request.headers["content-type"] : "application/json",
      "maxdataserviceversion": request.headers.hasOwnProperty("maxdataserviceversion") ? request.headers["maxdataserviceversion"] : "3.0",
      "dataserviceversion": request.headers.hasOwnProperty("dataserviceversion") ? request.headers["dataserviceversion"] : "2.0"
    };
    header.authorization = "Bearer " + user.id_token;

    _es6Request.default.get("https://" + host + "/tracking-asset-ui-service/v2/AssetCycleList(assetId=" + id + ",sortField='" + sort + "',sortOrder='" + order + "',skip=" + skip + ",top=" + top + ")").headers(header).then(([body, res]) => {
      response.type(request.headers.hasOwnProperty("content-type") ? request.headers["content-type"] : "application/json");

      if (res.statusCode === 200 || res.statusCode === 201) {
        response.status(res.statusCode);
        response.send(body);
      } else {
        response.status(res.statusCode);
        response.send({
          url: "https://" + host + "/tracking-asset-ui-service/v2/AssetCycleList(assetId=" + id + ",sortField='" + sort + "',sortOrder='" + order + "',skip=" + skip + ",top=" + top + ")",
          authorization: "Bearer " + user.id_token,
          body: body
        });
      } // should output this README file!

    });
  } catch (e) {
    response.status(res.statusCode).send(e.toString());
  }
});
router.get('/asset/checkIsExist/:keyLabel', (request, response) => {
  let {
    params,
    user
  } = request;
  let {
    keyLabel
  } = params;

  try {
    let header = {
      "Content-Type": request.headers.hasOwnProperty("content-type") ? request.headers["content-type"] : "application/json",
      "maxdataserviceversion": request.headers.hasOwnProperty("maxdataserviceversion") ? request.headers["maxdataserviceversion"] : "3.0",
      "dataserviceversion": request.headers.hasOwnProperty("dataserviceversion") ? request.headers["dataserviceversion"] : "2.0"
    };
    header.authorization = "Bearer " + user.id_token;

    _es6Request.default.get("https://" + host + "/tracking-asset-ui-service/v2/CheckAssetAlreadyAssigned(keyLabel='" + keyLabel + "')").headers(header).then(([body, res]) => {
      response.type(request.headers.hasOwnProperty("content-type") ? request.headers["content-type"] : "application/json");
      response.status(res.statusCode);
      response.send(body); // should output this README file!
    });
  } catch (e) {
    response.status(res.statusCode).send(e.toString());
  }
});
router.get('/asset/getCheckIsExist/:keyLabel', (request, response) => {
  let {
    params,
    user
  } = request;
  let {
    keyLabel
  } = params;

  try {
    let header = {
      "Content-Type": request.headers.hasOwnProperty("content-type") ? request.headers["content-type"] : "application/json",
      "maxdataserviceversion": request.headers.hasOwnProperty("maxdataserviceversion") ? request.headers["maxdataserviceversion"] : "3.0",
      "dataserviceversion": request.headers.hasOwnProperty("dataserviceversion") ? request.headers["dataserviceversion"] : "2.0"
    };
    header.authorization = "Bearer " + user.id_token;

    _es6Request.default.get("https://" + host + "/tracking-asset-ui-service/v2/GetAssetAlreadyAssigned(keyLabel='" + keyLabel + "')").headers(header).then(([body, res]) => {
      response.type(request.headers.hasOwnProperty("content-type") ? request.headers["content-type"] : "application/json");
      response.status(res.statusCode);
      response.send(body); // should output this README file!
    });
  } catch (e) {
    response.status(res.statusCode).send(e.toString());
  }
});
router.get('/asset/searchAssetType/:search', (request, response) => {
  let {
    params,
    user
  } = request;
  let {
    search
  } = params;

  try {
    let header = {
      "Content-Type": request.headers.hasOwnProperty("content-type") ? request.headers["content-type"] : "application/json",
      "maxdataserviceversion": request.headers.hasOwnProperty("maxdataserviceversion") ? request.headers["maxdataserviceversion"] : "3.0",
      "dataserviceversion": request.headers.hasOwnProperty("dataserviceversion") ? request.headers["dataserviceversion"] : "2.0"
    };
    header.authorization = "Bearer " + user.id_token;
    let searchUrl = "https://" + host + "/tracking-asset-ui-service/v2/AssetTypes?$filter=contains(name,'" + search + "') or contains(description,'" + search + "')";
    if (search === "*") searchUrl = "https://" + host + "/tracking-asset-ui-service/v2/AssetTypes";

    _es6Request.default.get(searchUrl).headers(header).then(([body, res]) => {
      response.type(request.headers.hasOwnProperty("content-type") ? request.headers["content-type"] : "application/json");
      response.status(res.statusCode);
      response.send(body); // should output this README file!
    });
  } catch (e) {
    response.status(res.statusCode).send(e.toString());
  }
});
router.get('/asset/searchAssetTypeById/:id', (request, response) => {
  let {
    params,
    user
  } = request;
  let {
    id
  } = params;

  try {
    let header = {
      "Content-Type": request.headers.hasOwnProperty("content-type") ? request.headers["content-type"] : "application/json",
      "maxdataserviceversion": request.headers.hasOwnProperty("maxdataserviceversion") ? request.headers["maxdataserviceversion"] : "3.0",
      "dataserviceversion": request.headers.hasOwnProperty("dataserviceversion") ? request.headers["dataserviceversion"] : "2.0"
    };
    header.authorization = "Bearer " + user.id_token;
    let searchUrl = "https://" + host + "/tracking-asset-ui-service/v2/AssetTypes('" + id + "')";

    _es6Request.default.get(searchUrl).headers(header).then(([body, res]) => {
      response.type(request.headers.hasOwnProperty("content-type") ? request.headers["content-type"] : "application/json");
      response.status(res.statusCode);
      response.send(body); // should output this README file!
    });
  } catch (e) {
    response.status(res.statusCode).send(e.toString());
  }
});
router.get('/asset/assetGeoMessages/:assetId/:assetCyleId/:skip/:top', (request, response) => {
  let {
    params,
    user
  } = request;
  let {
    assetId,
    assetCyleId,
    skip,
    top
  } = params;

  try {
    let header = {
      "Content-Type": request.headers.hasOwnProperty("content-type") ? request.headers["content-type"] : "application/json",
      "maxdataserviceversion": request.headers.hasOwnProperty("maxdataserviceversion") ? request.headers["maxdataserviceversion"] : "3.0",
      "dataserviceversion": request.headers.hasOwnProperty("dataserviceversion") ? request.headers["dataserviceversion"] : "2.0"
    };
    header.authorization = "Bearer " + user.id_token;
    let searchUrl = "https://" + host + "/tracking-asset-ui-service/v2/AssetGeoMessages(assetId=" + assetId + ",assetCycleId=" + assetCyleId + ",smoothingType=AssetV2.SmoothingType'NONE',skip=" + skip + ",top=" + top + ")";

    _es6Request.default.get(searchUrl).headers(header).then(([body, res]) => {
      response.type(request.headers.hasOwnProperty("content-type") ? request.headers["content-type"] : "application/json");
      response.status(res.statusCode);
      response.send(body); // should output this README file!
    });
  } catch (e) {
    response.status(res.statusCode).send(e.toString());
  }
});
router.post('/asset/createAssetType', (request, response) => {
  let {
    body,
    user
  } = request;

  try {
    let header = {
      "Content-Type": request.headers.hasOwnProperty("content-type") ? request.headers["content-type"] : "application/json",
      "maxdataserviceversion": request.headers.hasOwnProperty("maxdataserviceversion") ? request.headers["maxdataserviceversion"] : "3.0",
      "dataserviceversion": request.headers.hasOwnProperty("dataserviceversion") ? request.headers["dataserviceversion"] : "2.0"
    };
    header.authorization = "Bearer " + user.id_token;
    let searchUrl = "https://" + host + "/tracking-asset-ui-service/v2/CreateAssetType";

    _es6Request.default.post(searchUrl).headers(header).send(JSON.stringify(body)).then(([body, res]) => {
      response.type(request.headers.hasOwnProperty("content-type") ? request.headers["content-type"] : "application/json");
      response.status(res.statusCode);
      response.send(body);
    });
  } catch (e) {
    response.status(res.statusCode).send(e.toString());
  }
});
router.patch('/asset/updateAssetType', (request, response) => {
  let {
    body,
    user
  } = request;

  try {
    let header = {
      "Content-Type": request.headers.hasOwnProperty("content-type") ? request.headers["content-type"] : "application/json",
      "maxdataserviceversion": request.headers.hasOwnProperty("maxdataserviceversion") ? request.headers["maxdataserviceversion"] : "3.0",
      "dataserviceversion": request.headers.hasOwnProperty("dataserviceversion") ? request.headers["dataserviceversion"] : "2.0"
    };
    header.authorization = "Bearer " + user.id_token;
    let searchUrl = "https://" + host + "/tracking-asset-ui-service/v2/AssetTypes('" + body.id + "')";

    _es6Request.default.patch(searchUrl).headers(header).send(JSON.stringify({
      name: body.name,
      description: body.description,
      isActive: body.isActive
    })).then(([returnBody, res]) => {
      if (res.statusCode === 200 || res.statusCode === 201) {
        response.type(request.headers.hasOwnProperty("content-type") ? request.headers["content-type"] : "application/json");
        response.status(res.statusCode);
        response.send(returnBody);
      } else {
        response.status(res.statusCode);
        response.send({
          url: "https://" + host + "/tracking-asset-ui-service/v2/AssetTypes('" + body.id + "')",
          authorization: "Bearer " + user.id_token,
          body: body,
          returnBody: returnBody
        });
      }
    });
  } catch (e) {
    response.status(res.statusCode).send(e.toString());
  }
});
router.post('/asset/createAsset', (request, response) => {
  let {
    body,
    user
  } = request;

  try {
    let header = {
      "Content-Type": request.headers.hasOwnProperty("content-type") ? request.headers["content-type"] : "application/json",
      "maxdataserviceversion": request.headers.hasOwnProperty("maxdataserviceversion") ? request.headers["maxdataserviceversion"] : "3.0",
      "dataserviceversion": request.headers.hasOwnProperty("dataserviceversion") ? request.headers["dataserviceversion"] : "2.0"
    };
    header.authorization = "Bearer " + user.id_token;
    let searchUrl = "https://" + host + "/tracking-asset-ui-service/v2/CreateAsset";

    _es6Request.default.post(searchUrl).headers(header).send(JSON.stringify(body)).then(([body, res]) => {
      response.type(request.headers.hasOwnProperty("content-type") ? request.headers["content-type"] : "application/json");
      response.status(res.statusCode);
      response.send(body);
    });
  } catch (e) {
    response.status(res.statusCode).send(e.toString());
  }
});
router.patch('/asset/updateAsset', (request, response) => {
  let {
    body,
    user
  } = request;

  try {
    let header = {
      "Content-Type": request.headers.hasOwnProperty("content-type") ? request.headers["content-type"] : "application/json",
      "maxdataserviceversion": request.headers.hasOwnProperty("maxdataserviceversion") ? request.headers["maxdataserviceversion"] : "3.0",
      "dataserviceversion": request.headers.hasOwnProperty("dataserviceversion") ? request.headers["dataserviceversion"] : "2.0"
    };
    header.authorization = "Bearer " + user.id_token;
    let searchUrl = "https://" + host + "/tracking-asset-ui-service/v2/Assets('" + body.id + "')";

    _es6Request.default.patch(searchUrl).headers(header).send(JSON.stringify(body)).then(([body, res]) => {
      response.type(request.headers.hasOwnProperty("content-type") ? request.headers["content-type"] : "application/json");
      response.status(res.statusCode);
      response.send(body);
    });
  } catch (e) {
    response.status(res.statusCode).send(e.toString());
  }
});
router.get('/asset/assignedAsset/:deviceId', (request, response) => {
  let {
    params,
    user
  } = request;
  let {
    deviceId
  } = params;

  try {
    let header = {
      "Content-Type": request.headers.hasOwnProperty("content-type") ? request.headers["content-type"] : "application/json",
      "maxdataserviceversion": request.headers.hasOwnProperty("maxdataserviceversion") ? request.headers["maxdataserviceversion"] : "3.0",
      "dataserviceversion": request.headers.hasOwnProperty("dataserviceversion") ? request.headers["dataserviceversion"] : "2.0"
    };
    header.authorization = "Bearer " + user.id_token;

    _es6Request.default.get("https://" + host + "/tracking-asset-ui-service/v2/CheckDeviceAlreadyAssigned(deviceId='" + deviceId + "')").headers(header).then(([body, res]) => {
      response.type(request.headers.hasOwnProperty("content-type") ? request.headers["content-type"] : "application/json");
      response.status(res.statusCode);
      response.send(body); // should output this README file!
    });
  } catch (e) {
    response.status(res.statusCode).send(e.toString());
  }
});
var _default = router;
exports.default = _default;