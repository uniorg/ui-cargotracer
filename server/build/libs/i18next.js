"use strict";

let i18next = require('i18next'),
    i18nextMiddleware = require('i18next-express-middleware'),
    //backendMongoDb = require( 'i18next-node-mongodb-backend' ),
backendMongoDb = require('i18next-node-fs-backend'),
    path = require('path'),
    configMongo;

module.exports = function (app) {
  let backendMongoDbUse = new backendMongoDb(null, configMongo);
  i18next.use(i18nextMiddleware.LanguageDetector).use(backendMongoDbUse).init({
    backend: configMongo,
    fallbackLng: ['de-DE', 'en-EN'],
    load: ['en-EN', 'de-DE'],
    whitelist: ['en-EN', 'de-DE'],
    saveMissing: true,
    fallbackNS: ["translation"],
    cache: false,
    allowMultiLoading: true,
    detection: {
      caches: false
    }
  }, function (err, t) {
    // initialized and ready to go!
    app.i18nService = i18next;
  }); // missing keys

  app.use(i18nextMiddleware.handle(i18next, {
    ignoreRoutes: ["/translation"],
    removeLngFromUrl: true,
    caches: false,
    allowMultiLoading: true,
    cache: false
  }));
  app.get("/*", function (req, res, next) {
    i18next.reloadResources(['en-EN', 'de-DE'], "translation");
    setTimeout(function () {
      next();
    }, 300);
  });
  app.post('/i18Next/locales/add/:lng/:ns', i18nextMiddleware.missingKeyHandler(i18next)); // multiload backend route

  app.get('/i18Next/locales/resources.json*', i18nextMiddleware.getResourcesHandler(i18next));
};