"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.initLoginService = void 0;

var _bunyan = _interopRequireDefault(require("bunyan"));

var _config = _interopRequireDefault(require("../config"));

var _passport = _interopRequireDefault(require("passport"));

var _passportAzureAd = _interopRequireDefault(require("passport-azure-ad"));

var _loginHelper = require("../loginHelper");

var _expressSession = _interopRequireDefault(require("express-session"));

var _methodOverride = _interopRequireDefault(require("method-override"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const log = _bunyan.default.createLogger({
  name: 'Tracking App'
});

const destroySessionUrl = _config.default.destroySessionUrl;

let setuserLogin = (user, callback) => {
  let newDate = new Date();
  let sendUserData = {
    lastLogin: newDate.toISOString(),
    lastAction: newDate.toISOString(),
    userConfigs: []
  };
  (0, _loginHelper.getUserObject)(user.id_token, sendUserData, userBack => {
    return callback(userBack);
  });
};

let initLoginService = app => {
  const OIDCStrategy = _passportAzureAd.default.OIDCStrategy;
  const OIDConfig = {
    identityMetadata: _config.default.creds.identityMetadata,
    clientID: _config.default.creds.clientID,
    responseType: _config.default.creds.responseType,
    responseMode: _config.default.creds.responseMode,
    redirectUrl: _config.default.creds.redirectUrlLocal,
    allowHttpForRedirectUrl: _config.default.creds.allowHttpForRedirectUrl,
    clientSecret: _config.default.creds.clientSecret,
    validateIssuer: _config.default.creds.validateIssuer,
    isB2C: _config.default.creds.isB2C,
    issuer: _config.default.creds.issuer,
    passReqToCallback: _config.default.creds.passReqToCallback,
    scope: _config.default.creds.scope,
    loggingLevel: _config.default.creds.loggingLevel,
    nonceLifetime: _config.default.creds.nonceLifetime,
    nonceMaxAmount: _config.default.creds.nonceMaxAmount,
    useCookieInsteadOfSession: _config.default.creds.useCookieInsteadOfSession,
    cookieEncryptionKeys: _config.default.creds.cookieEncryptionKeys,
    clockSkew: _config.default.creds.clockSkew
  };

  _passport.default.serializeUser(function (user, done) {
    done(null, user.oid);
  });

  _passport.default.deserializeUser(function (oid, done) {
    (0, _loginHelper.findByOid)(oid, function (err, user) {
      done(err, user);
    });
  });

  _passport.default.use(new OIDCStrategy(OIDConfig, function (iss, sub, profile, accessToken, refreshToken, done) {
    if (!profile.oid) {
      return done(new Error("No oid found"), null);
    } // asynchronous verification, for effect...


    process.nextTick(function () {
      profile.accessToken = accessToken;
      profile.refreshToken = refreshToken;
      setuserLogin(profile, function (userBack) {
        (0, _loginHelper.findByOid)(profile.oid, function (err, instanceUser) {
          if (err) {
            return done(err);
          }

          if (!instanceUser) {
            // "Auto-registration"
            profile.backendUser = JSON.parse(userBack);

            _loginHelper.users.push(profile);

            return done(null, profile);
          } else {
            (0, _loginHelper.findAndUpdateByOid)(profile.oid, function (err, user) {
              profile.backendUser = JSON.parse(userBack);
              return done(null, profile);
            }, profile);
          }
        });
      });
    });
  }));

  app.use((0, _expressSession.default)({
    secret: 'keyboard cat',
    cookie: {
      maxAge: _config.default.mongoDBSessionMaxAge * 1000,
      secure: false
    },
    resave: false,
    saveUninitialized: false
  }));
  app.use((0, _methodOverride.default)());
  app.use(_passport.default.initialize({}));
  app.use(_passport.default.session({
    secret: 'keyboard cat',
    cookie: {
      maxAge: _config.default.mongoDBSessionMaxAge * 1000,
      secure: false
    },
    resave: false,
    saveUninitialized: false
  }));
  app.get('/login', function (req, res, next) {
    _passport.default.authenticate('azuread-openidconnect', {
      response: res,
      // optional. Provide a value if you want to provide custom state value.
      failureRedirect: '/',
      session: false
    })(req, res, next);
  }, function (req, res) {
    log.info('Login was called in the Sample');
    res.redirect('/');
  });
  app.post('/auth/openid/return', function (req, res, next) {
    _passport.default.authenticate('azuread-openidconnect', {
      response: res,
      // required
      failureRedirect: '/'
    })(req, res, next);
  }, function (req, res) {
    res.redirect("/");
  });
  app.get('/logout', function (req, res) {
    //setuserLogin( req.user, "inactive", function() {
    req.session.destroy(function (err) {
      req.logOut();
      res.redirect(destroySessionUrl);
    }); // } );
  });
};

exports.initLoginService = initLoginService;