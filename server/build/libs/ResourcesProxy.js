"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _express = _interopRequireDefault(require("express"));

var _path = _interopRequireDefault(require("path"));

var _url = _interopRequireDefault(require("url"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const router = _express.default.Router();

router.get("/fontAwesomeFonts/*", function (req, res) {
  let pathUrl = _path.default.resolve(__dirname + "/../../../" + "client/build/" + _url.default.parse(req.url).pathname);

  res.sendFile(pathUrl);
});
var _default = router;
exports.default = _default;