"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.checkIsBpwAdmin = exports.checkMandantEqSubMandant = exports.checkIsAdminSenderBpw = exports.checkIsAdmin = exports.ensureIsAdmin = exports.ensureIsBpwAdmin = exports.ensureAuthenticated = exports.findByEmail = exports.findAndUpdateByOid = exports.findByOid = exports.getUserObject = exports.users = void 0;

var _bunyan = _interopRequireDefault(require("bunyan"));

var _path = _interopRequireDefault(require("path"));

var _es6Request = _interopRequireDefault(require("es6-request"));

var _config = _interopRequireDefault(require("./config"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const log = _bunyan.default.createLogger({
  name: 'Tracking App'
});

const users = [];
exports.users = users;
const host = _config.default.host.UserApiHost;

let getUserObject = (idToken, sendUserData, callback) => {
  try {
    let header = {
      "Content-Type": "application/json",
      authorization: "Bearer " + idToken
    };
    let searchUrl = "https://" + host + "/tracking-user-ui-service/v2/SaveOrUpdateUser";

    _es6Request.default.post(searchUrl).headers(header).send(JSON.stringify(sendUserData)).then(([body, res]) => {
      callback(body); // should output this README file!
    });
  } catch (e) {
    callback(e.toString());
  }
};

exports.getUserObject = getUserObject;

let findByOid = (oid, fn) => {
  for (let i = 0, len = users.length; i < len; i++) {
    let user = users[i];
    log.info('we are using user: ', user);

    if (user.oid === oid) {
      return fn(null, user);
    }
  }

  return fn(null, null);
};

exports.findByOid = findByOid;

let findAndUpdateByOid = (oid, fn, newUser) => {
  for (let i = 0, len = users.length; i < len; i++) {
    let user = users[i];

    if (user.oid === oid) {
      users[i] = newUser;
      return fn(null, newUser);
    }
  }
};

exports.findAndUpdateByOid = findAndUpdateByOid;

let findByEmail = (email, fn) => {
  for (let i = 0, len = (void 0).users.length; i < len; i++) {
    let user = (void 0).users[i];

    if (user.email === email) {
      return fn(null, user);
    }
  }

  return fn(null, null);
};

exports.findByEmail = findByEmail;

let ensureAuthenticated = (req, res, next) => {
  if (req.isAuthenticated()) {
    return next();
  } else {
    res.sendFile(_path.default.join(__dirname + '/../../client/build/loginStatic/index.html'));
  }
};

exports.ensureAuthenticated = ensureAuthenticated;

let ensureIsBpwAdmin = (req, res, next) => {
  let returnBool = false;
  if (req.hasOwnProperty("user") && req.user.hasOwnProperty("_json") && req.user["_json"].hasOwnProperty("extension_BpwAdminBpw") && req.user["_json"]["extension_BpwAdminBpw"] === 1) returnBool = true;

  if (returnBool) {
    return next();
  }

  res.redirect('/login');
};

exports.ensureIsBpwAdmin = ensureIsBpwAdmin;

let ensureIsAdmin = (req, res, next) => {
  let returnBool = false;
  if (req.hasOwnProperty("user") && req.user.hasOwnProperty("_json") && req.user["_json"].hasOwnProperty("extension_BpwAdminBpw") && req.user["_json"]["extension_BpwAdminBpw"] === 1) returnBool = true;
  if (req.hasOwnProperty("user") && req.user.hasOwnProperty("_json") && req.user["_json"].hasOwnProperty("extension_BpwAdminSender") && req.user["_json"]["extension_BpwAdminSender"] === 1) returnBool = true;
  if (req.hasOwnProperty("user") && req.user.hasOwnProperty("_json") && req.user["_json"].hasOwnProperty("extension_BpwAdminRecipient") && req.user["_json"]["extension_BpwAdminRecipient"] === 1) returnBool = true;

  if (returnBool) {
    return next();
  }

  res.redirect('/login');
};

exports.ensureIsAdmin = ensureIsAdmin;

let checkIsAdmin = req => {
  let returnBool = false;
  if (req.hasOwnProperty("user") && req.user.hasOwnProperty("_json") && req.user["_json"].hasOwnProperty("extension_BpwAdminBpw") && req.user["_json"]["extension_BpwAdminBpw"] === 1) returnBool = true;
  if (req.hasOwnProperty("user") && req.user.hasOwnProperty("_json") && req.user["_json"].hasOwnProperty("extension_BpwAdminSender") && req.user["_json"]["extension_BpwAdminSender"] === 1) returnBool = true;
  if (req.hasOwnProperty("user") && req.user.hasOwnProperty("_json") && req.user["_json"].hasOwnProperty("extension_BpwAdminRecipient") && req.user["_json"]["extension_BpwAdminRecipient"] === 1) returnBool = true;
  return returnBool;
};

exports.checkIsAdmin = checkIsAdmin;

let checkIsAdminSenderBpw = req => {
  let returnBool = false;
  if (req.hasOwnProperty("user") && req.user.hasOwnProperty("_json") && req.user["_json"].hasOwnProperty("extension_BpwAdminBpw") && req.user["_json"]["extension_BpwAdminBpw"] === 1) returnBool = true;
  if (req.hasOwnProperty("user") && req.user.hasOwnProperty("_json") && req.user["_json"].hasOwnProperty("extension_BpwAdminSender") && req.user["_json"]["extension_BpwAdminSender"] === 1) returnBool = true;
  return returnBool;
};

exports.checkIsAdminSenderBpw = checkIsAdminSenderBpw;

let checkMandantEqSubMandant = req => {
  let returnBool = false;
  if (req.hasOwnProperty("user") && req.user.hasOwnProperty("_json") && req.user["_json"].hasOwnProperty("extension_isMandant") && req.user["_json"].hasOwnProperty("extension_BpwMandant") && req.user["_json"]["extension_BpwMandantSub"] === req.user["_json"]["extension_BpwMandant"]) returnBool = true;
  return returnBool;
};

exports.checkMandantEqSubMandant = checkMandantEqSubMandant;

let checkIsBpwAdmin = req => {
  let returnBool = false;
  if (req.hasOwnProperty("user") && req.user.hasOwnProperty("_json") && req.user["_json"].hasOwnProperty("extension_BpwAdminBpw") && req.user["_json"]["extension_BpwAdminBpw"] === 1) returnBool = true;
  return returnBool;
};

exports.checkIsBpwAdmin = checkIsBpwAdmin;