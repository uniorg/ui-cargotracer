"use strict";

let bunyan = require('bunyan');

const path = require("path");

let log = bunyan.createLogger({
  name: 'Tracking App'
});
module.exports = {
  // array to hold logged in users
  users: [],
  findByOid: function (oid, fn) {
    for (let i = 0, len = this.users.length; i < len; i++) {
      let user = this.users[i];
      log.info('we are using user: ', user);

      if (user.oid === oid) {
        return fn(null, user);
      }
    }

    return fn(null, null);
  },
  findAndUpdateByOid: function (oid, fn, newUser) {
    for (let i = 0, len = this.users.length; i < len; i++) {
      let user = this.users[i];

      if (user.oid === oid) {
        this.users[i] = newUser;
        return fn(null, newUser);
      }
    }
  },
  findByEmail: function (email, fn) {
    for (let i = 0, len = this.users.length; i < len; i++) {
      let user = this.users[i];

      if (user.email === email) {
        return fn(null, user);
      }
    }

    return fn(null, null);
  },
  ensureAuthenticated: function (req, res, next) {
    if (req.isAuthenticated()) {
      return next();
    } else {
      res.sendFile(path.join(__dirname + '/../../client/build/loginStatic/index.html'));
    }
  },
  ensureIsBpwAdmin: function (req, res, next) {
    let returnBool = false;
    if (req.hasOwnProperty("user") && req.user.hasOwnProperty("_json") && req.user["_json"].hasOwnProperty("extension_BpwAdminBpw") && req.user["_json"]["extension_BpwAdminBpw"] === 1) returnBool = true;

    if (returnBool) {
      return next();
    }

    res.redirect('/login');
  },
  ensureIsAdmin: function (req, res, next) {
    let returnBool = false;
    if (req.hasOwnProperty("user") && req.user.hasOwnProperty("_json") && req.user["_json"].hasOwnProperty("extension_BpwAdminBpw") && req.user["_json"]["extension_BpwAdminBpw"] === 1) returnBool = true;
    if (req.hasOwnProperty("user") && req.user.hasOwnProperty("_json") && req.user["_json"].hasOwnProperty("extension_BpwAdminSender") && req.user["_json"]["extension_BpwAdminSender"] === 1) returnBool = true;
    if (req.hasOwnProperty("user") && req.user.hasOwnProperty("_json") && req.user["_json"].hasOwnProperty("extension_BpwAdminRecipient") && req.user["_json"]["extension_BpwAdminRecipient"] === 1) returnBool = true;

    if (returnBool) {
      return next();
    }

    res.redirect('/login');
  },
  checkIsAdmin: function (req) {
    let returnBool = false;
    if (req.hasOwnProperty("user") && req.user.hasOwnProperty("_json") && req.user["_json"].hasOwnProperty("extension_BpwAdminBpw") && req.user["_json"]["extension_BpwAdminBpw"] === 1) returnBool = true;
    if (req.hasOwnProperty("user") && req.user.hasOwnProperty("_json") && req.user["_json"].hasOwnProperty("extension_BpwAdminSender") && req.user["_json"]["extension_BpwAdminSender"] === 1) returnBool = true;
    if (req.hasOwnProperty("user") && req.user.hasOwnProperty("_json") && req.user["_json"].hasOwnProperty("extension_BpwAdminRecipient") && req.user["_json"]["extension_BpwAdminRecipient"] === 1) returnBool = true;
    return returnBool;
  },
  checkIsAdminSenderBpw: function (req) {
    let returnBool = false;
    if (req.hasOwnProperty("user") && req.user.hasOwnProperty("_json") && req.user["_json"].hasOwnProperty("extension_BpwAdminBpw") && req.user["_json"]["extension_BpwAdminBpw"] === 1) returnBool = true;
    if (req.hasOwnProperty("user") && req.user.hasOwnProperty("_json") && req.user["_json"].hasOwnProperty("extension_BpwAdminSender") && req.user["_json"]["extension_BpwAdminSender"] === 1) returnBool = true;
    return returnBool;
  },
  checkMandantEqSubMandant: function (req) {
    let returnBool = false;
    if (req.hasOwnProperty("user") && req.user.hasOwnProperty("_json") && req.user["_json"].hasOwnProperty("extension_isMandant") && req.user["_json"].hasOwnProperty("extension_BpwMandant") && req.user["_json"]["extension_BpwMandantSub"] === req.user["_json"]["extension_BpwMandant"]) returnBool = true;
    return returnBool;
  },
  checkIsBpwAdmin: function (req) {
    let returnBool = false;
    if (req.hasOwnProperty("user") && req.user.hasOwnProperty("_json") && req.user["_json"].hasOwnProperty("extension_BpwAdminBpw") && req.user["_json"]["extension_BpwAdminBpw"] === 1) returnBool = true;
    return returnBool;
  }
};