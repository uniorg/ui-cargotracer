"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _express = _interopRequireDefault(require("express"));

var _config = _interopRequireDefault(require("../config"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const router = _express.default.Router();

let checkKey = key => {
  switch (key) {
    case "APPSETTING_AppInsightsInstrumentationKey":
      return true;

    case "APPSETTING_environment":
      return true;

    default:
      return false;
  }
}; // POST /user/signin


router.get('/environment/:key', (request, response) => {
  let {
    params,
    user
  } = request;
  let {
    key
  } = params;

  if (checkKey(key) && user !== undefined && user.id_token !== undefined) {
    response.status(200).send({
      key: key,
      value: process.env[key] !== undefined ? process.env[key] : _config.default.creds[key] !== undefined ? _config.default.creds[key] : null
    });
  } else {
    response.status(401).send({
      key: key,
      error: "401 Unauthorized"
    });
  }
});
router.post('/environment', (request, response) => {
  let {
    body,
    user
  } = request;
  let {
    keys
  } = body;

  if (Array.isArray(keys) && user !== undefined && user.id_token !== undefined) {
    let keysReturn = [];

    for (let i = 0; i < keys.length; i++) {
      let key = keys[i];

      if (checkKey(key)) {
        keysReturn.push({
          key: key,
          value: process.env[key] !== undefined ? process.env[key] : _config.default.creds[key] !== undefined ? _config.default.creds[key] : null
        });
      }
    }

    response.status(200).send(keysReturn);
  } else {
    response.status(401).send({
      body: body,
      keys: keys,
      error: "401 Unauthorized"
    });
  }
});
var _default = router;
exports.default = _default;