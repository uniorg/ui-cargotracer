"use strict";

var _passport = _interopRequireDefault(require("passport"));

var _expressSession = _interopRequireDefault(require("express-session"));

var _bunyan = _interopRequireDefault(require("bunyan"));

var _methodOverride = _interopRequireDefault(require("method-override"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

let azure = {
  sub: 'b0282feb-f733-4b02-b09d-8c1cfd27d828',
  oid: 'b0282feb-f733-4b02-b09d-8c1cfd27d828',
  upn: undefined,
  id_token: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ilg1ZVhrNHh5b2pORnVtMWtsMll0djhkbE5QNC1jNTdkTzZRR1RWQndhTmsifQ.eyJleHAiOjE1NDI3MjU0NTYsIm5iZiI6MTU0MjcyMTg1NiwidmVyIjoiMS4wIiwiaXNzIjoiaHR0cHM6Ly9sb2dpbi5taWNyb3NvZnRvbmxpbmUuY29tL2YxMGUxMzcxLTRiODEtNGQ3ZC1hNGZhLWI5ZGQwNWU0ZTE0Zi92Mi4wLyIsInN1YiI6ImIwMjgyZmViLWY3MzMtNGIwMi1iMDlkLThjMWNmZDI3ZDgyOCIsImF1ZCI6IjUzNTc0NTM3LTUzZDAtNGYyMS05N2JkLTI2ODdhNDlmN2JiYSIsIm5vbmNlIjoiY0JmRkZJUlR2LVVoMkFncGRWRU81WHRhODFLYmJOR04iLCJpYXQiOjE1NDI3MjE4NTYsImF1dGhfdGltZSI6MTU0MjcyMTg1Niwib2lkIjoiYjAyODJmZWItZjczMy00YjAyLWIwOWQtOGMxY2ZkMjdkODI4IiwiZmFtaWx5X25hbWUiOiJNw7xsbGVyIiwiZ2l2ZW5fbmFtZSI6Ikd1aWRvIiwibmFtZSI6Ik3DvGxsZXIgR3VpZG8iLCJleHRlbnNpb25fQnB3TWFuZGFudFN1YiI6MiwiZXh0ZW5zaW9uX0Jwd01hbmRhbnQiOjIsImV4dGVuc2lvbl9CcHdDb21wYW55IjoiVW5pb3JnIiwiZXh0ZW5zaW9uX0Jwd0FkbWluQnB3IjoxLCJlbWFpbHMiOlsiZy5tdWVsbGVyQHVuaW9yZy5kZSIsImd1aWRvLm11ZWxsZXJAdW5pb3JnLmRlIl0sInRmcCI6IkIyQ18xX3NpZ25pbiJ9.JE_K_RiJ4oXjFbw14VGCvlbkKw3cHLD1CcuGMYTr09wF1bARi6Kjxf9EdE1DGlwhPSYWCFcXp9xoWyLBNkDIf8hIJKUuIVRBvhIcyXCFiWa-oXGUuYr7oDbjY8hJfVXGfwjDBDJGdfv7O-00_r9LAolVNmRxaMotd5mjS7RU6KyUYhsaaQju9yZdXF_JF5Hxopp-ibsOKdDMwh46vwOkqwUgu_vnWj63yK7761mA6uYaaJ01TB0ZkD8Mid1XPlJEfVfjzqHXwPvJQD6TCsy6A59Ixt4jP8_aEZIeQ0kAq1SzDfSY8Rckn6onHI4a1VcTlWUV0CtxJU45xb-V9TsW6w',
  displayName: 'Müller Guido',
  name: {
    familyName: 'Müller',
    givenName: 'Guido',
    middleName: undefined
  },
  emails: ['g.mueller@uniorg.de', 'guido.mueller@uniorg.de'],
  _raw: '{"exp":1542725456,"nbf":1542721856,"ver":"1.0","iss":"https://login.microsoftonline.com/f10e1371-4b81-4d7d-a4fa-b9dd05e4e14f/v2.0/","sub":"b0282feb-f733-4b02-b09d-8c1cfd27d828","aud":"53574537-53d0-4f21-97bd-2687a49f7bba","nonce":"cBfFFIRTv-Uh2AgpdVEO5Xta81KbbNGN","iat":1542721856,"auth_time":1542721856,"oid":"b0282feb-f733-4b02-b09d-8c1cfd27d828","family_name":"Müller","given_name":"Guido","name":"Müller Guido","extension_BpwMandantSub":2,"extension_BpwMandant":2,"extension_BpwCompany":"Uniorg","extension_BpwAdminBpw":1,"emails":["g.mueller@uniorg.de","guido.mueller@uniorg.de"],"tfp":"B2C_1_signin"}',
  _json: {
    exp: 1542725456,
    nbf: 1542721856,
    ver: '1.0',
    iss: 'https://login.microsoftonline.com/f10e1371-4b81-4d7d-a4fa-b9dd05e4e14f/v2.0/',
    sub: 'b0282feb-f733-4b02-b09d-8c1cfd27d828',
    aud: '53574537-53d0-4f21-97bd-2687a49f7bba',
    nonce: 'cBfFFIRTv-Uh2AgpdVEO5Xta81KbbNGN',
    iat: 1542721856,
    auth_time: 1542721856,
    oid: 'b0282feb-f733-4b02-b09d-8c1cfd27d828',
    family_name: 'Müller',
    given_name: 'Guido',
    name: 'Müller Guido',
    extension_BpwMandantSub: 2,
    extension_BpwMandant: 2,
    extension_BpwCompany: 'Uniorg',
    extension_BpwAdminBpw: 1,
    emails: ['g.mueller@uniorg.de', 'guido.mueller@uniorg.de'],
    tfp: 'B2C_1_signin'
  },
  accessToken: undefined,
  refreshToken: 'eyJraWQiOiJjcGltY29yZV8wOTI1MjAxNSIsInZlciI6IjEuMCIsInppcCI6IkRlZmxhdGUiLCJzZXIiOiIxLjAifQ..ko0SC1x6bw58MwPJ.KKmvTU0rTaWO0W2b32_VAvw4nv8PMeMFd8RGEj4pPWs_ItChevytQa5dJ31_Tawylfk-7OY6PuVc1c1mmfmY2Daq1owird4rXj7MCFikGya9VwonLURjjMKROd-hOpKUkN56kQ_Dk4n944M9Lq6TEsI8rlyxVc1SIj1yyB2ZJ3YmOH7ssb8x0RCAGfY3jrThUQ-yPsbEcgNRIZjb2yFCVaby523jHXt-YXr_hkwFOeNgCRaWZ8wtv6N8Awmfhi7bkma03VztUUOeVdaFz6tDF9Hacp946gJVaWOOEcmNA3__W52BNyDxXxhh2dAwr8f6gyxLu51mUfFB-j_phfMs7sUrVSyEDtuWKEHOUF0fNQURDc5sdJjy3r6GxiurR1i4rqYDQGDFNX70Ey_g2_wPdooTjG44LVxIJbQGc4kwhiNJ09Bsco3L4_045t3piqt1G0d_OBbJ1z7Gv89B1TxhoEixaS42sxk-E-H6wz-NZ5rLWXhdGIa1jQhg4YpOSQFnIOMG9NDdEX8RIINTp7-iJ3naqUJ9n3scLc2JRJPzoHpuBdFJeQ2bB3D2dnwH71W-Nn1f0pZPQDkVIvlu4jDq6RTl0ipCtwkcJ52HwWSDIktNLVDVnNREJjYKpqWf5hg5Kii-qb6ihNBWzahxPy2FSMm7-q2coMPaP3UarvERZ9wQPswW-NNpgbFarri166uXhR6GHntVD1lwhUPC2iidE47fN-6KMZMpz5mFnqNhoEGakxphzmiPl96lR-TusRhfLVIJ-GmKwBrel2tyx4u0.0uxY5XRLEbouxgsn_gijCw'
};

let OIDCStrategy = require('passport-azure-ad').OIDCStrategy,
    log = _bunyan.default.createLogger({
  name: 'Tracking App'
}),
    login = require(__dirname + '/../login.js'),
    config = require(__dirname + '/../config'),
    userHistory = process.env["APPSETTING_userHistory"] || false,
    OIDConfig = {
  identityMetadata: process.env["APPSETTING_IdentityMetadata"] || config.creds.identityMetadata,
  clientID: process.env["APPSETTING_AADclientID"] || config.creds.clientID,
  responseType: config.creds.responseType,
  responseMode: config.creds.responseMode,
  redirectUrl: process.env["APPSETTING_OIDConfigRedirectUrl"] || config.creds.redirectUrlLocal,
  allowHttpForRedirectUrl: config.creds.allowHttpForRedirectUrl,
  clientSecret: process.env["APPSETTING_AADclientSecret"] || config.creds.clientSecret,
  validateIssuer: config.creds.validateIssuer,
  isB2C: config.creds.isB2C,
  issuer: config.creds.issuer,
  passReqToCallback: config.creds.passReqToCallback,
  scope: config.creds.scope,
  loggingLevel: config.creds.loggingLevel,
  nonceLifetime: config.creds.nonceLifetime,
  nonceMaxAmount: config.creds.nonceMaxAmount,
  useCookieInsteadOfSession: config.creds.useCookieInsteadOfSession,
  cookieEncryptionKeys: config.creds.cookieEncryptionKeys,
  clockSkew: config.creds.clockSkew
};

module.exports = function (app, models, sequelize) {
  _passport.default.serializeUser(function (user, done) {
    done(null, user.oid);
  });

  _passport.default.deserializeUser(function (oid, done) {
    login.findByOid(oid, function (err, user) {
      done(err, user);
    });
  });

  function setuserLogin(user, status, callback) {
    models.User.findOne({
      where: {
        creationUser: user.oid
      }
    }).then(function (dbUser) {
      let data = {
        sub: user.sub !== undefined ? user.sub : null,
        oid: user.oid !== undefined ? user.oid : null,
        creationUser: user.oid !== undefined ? user.oid : null,
        lastUpdateUser: user.oid !== undefined ? user.oid : null,
        email: user.emails[0] !== undefined ? user.emails[0] : null,
        idToken: user.id_token !== undefined ? user.id_token : null,
        displayName: user.displayName !== undefined ? user.displayName : null,
        accessToken: user.accessToken !== undefined ? user.accessToken : null,
        refreshToken: user.refreshToken !== undefined ? user.refreshToken : null,
        createdAt: models.literal('CURRENT_TIMESTAMP'),
        createdFrom: user.oid,
        updatedAt: models.literal('CURRENT_TIMESTAMP'),
        updatedFrom: user.oid
      };

      if (status === "inactive") {
        data.lastLogout = new Date();
      } else {
        data.lastLogin = new Date();
      }

      data.lastAction = new Date();
      data.status = status;

      if (user.hasOwnProperty("_json") && user["_json"] !== undefined) {
        let userJson = user["_json"];
        data.name = userJson.name !== undefined ? userJson.name : null;
        data.familyName = userJson.family_name !== undefined ? userJson.family_name : null;
        data.givenName = userJson.given_name !== undefined ? userJson.given_name : null;
        data.authTime = userJson.auth_time !== undefined ? new Date(userJson.auth_time) : null;
        data.extensionBpwMandantSub = userJson.extension_BpwMandantSub !== undefined ? userJson.extension_BpwMandantSub : null;
        data.extensionBpwMandant = userJson.extension_BpwMandant !== undefined ? userJson.extension_BpwMandant : null;
        data.extensionBpwAdminBpw = userJson.extension_BpwAdminBpw !== undefined ? userJson.extension_BpwAdminBpw : null;
        data.extensionBpwCompany = userJson.extension_BpwCompany !== undefined ? userJson.extension_BpwCompany : null;
      }

      if (dbUser) {
        dbUser.update(data).then(function (newUser, created) {
          if (!newUser) {
            return callback(false, user, false);
          }

          if (newUser) {
            return callback(true, user, false);
          }
        });
      } else {
        models.User.create(data).then(function (newUser, created) {
          if (!newUser) {
            return callback(false, false, true);
          }

          if (newUser) {
            let newUserConfig = {
              oid: data.oid,
              creationUser: data.oid,
              lastUpdateUser: data.oid,
              deleted: false,
              createdAt: models.literal('CURRENT_TIMESTAMP'),
              createdFrom: user.oid,
              updatedAt: models.literal('CURRENT_TIMESTAMP'),
              updatedFrom: user.oid
            };
            models.UserConfig.create(newUserConfig).then(function (newUserConfig, created) {
              if (!newUserConfig) {
                return callback(false, false, true);
              }

              if (newUserConfig) {
                let setUserConfigId = {
                  userConfigId: newUserConfig.id
                };
                newUser.update(setUserConfigId).then(function (updateUser, created) {
                  if (!updateUser) {
                    return callback(false, user, false);
                  }

                  if (updateUser) {
                    return callback(true, user, false);
                  }
                });
              }
            });
          }
        });
      }
    });
  }

  _passport.default.use(new OIDCStrategy(OIDConfig, function (iss, sub, profile, accessToken, refreshToken, done) {
    if (!profile.oid) {
      return done(new Error("No oid found"), null);
    } // asynchronous verification, for effect...


    process.nextTick(function () {
      profile.accessToken = accessToken;
      profile.refreshToken = refreshToken;
      setuserLogin(profile, "active", function (isCreate, user, isNew) {
        login.findByOid(profile.oid, function (err, instanceUser) {
          if (err) {
            return done(err);
          }

          if (!instanceUser) {
            // "Auto-registration"
            login.users.push(profile);
            return done(null, profile);
          } else {
            login.findAndUpdateByOid(profile.oid, function (err, user) {
              return done(null, profile);
            }, profile);
          }
        });
      });
    });
  }));

  app.use((0, _expressSession.default)({
    secret: 'keyboard cat',
    cookie: {
      maxAge: config.mongoDBSessionMaxAge * 1000,
      secure: false
    },
    resave: false,
    saveUninitialized: false
  }));
  app.use((0, _methodOverride.default)());
  app.use(_passport.default.initialize({}));
  app.use(_passport.default.session({
    secret: 'keyboard cat',
    cookie: {
      maxAge: config.mongoDBSessionMaxAge * 1000,
      secure: false
    },
    resave: false,
    saveUninitialized: false
  }));
  app.get('/login', function (req, res, next) {
    _passport.default.authenticate('azuread-openidconnect', {
      response: res,
      // optional. Provide a value if you want to provide custom state value.
      failureRedirect: '/',
      session: false
    })(req, res, next);
  }, function (req, res) {
    log.info('Login was called in the Sample');
    res.redirect('/');
  });
  app.post('/auth/openid/return', function (req, res, next) {
    _passport.default.authenticate('azuread-openidconnect', {
      response: res,
      // required
      failureRedirect: '/'
    })(req, res, next);
  }, function (req, res) {
    console.log('/auth/openid/return', "req.user", req.user);
    models.UserCourse.findOne({
      where: {
        creationUser: req.user.oid
      } //order: [['updatedAt', 'DESC']]

    }).then(function (userCourse) {
      console.log('/auth/openid/return', "userCourse", userCourse);

      if (userCourse && userHistory) {
        res.redirect(userCourse.url);
      } else {
        res.redirect("/");
      }
    });
  });
  let destroySessionUrl = process.env["APPSETTING_destroySessionUrl"] || config.destroySessionUrl;
  app.get('/logout', function (req, res) {
    setuserLogin(req.user, "inactive", function () {
      req.session.destroy(function (err) {
        req.logOut();
        res.redirect(destroySessionUrl);
      });
    });
  });
};