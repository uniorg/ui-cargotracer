"use strict";

var _express = _interopRequireDefault(require("express"));

var _compression = _interopRequireDefault(require("compression"));

var _expressValidator = _interopRequireDefault(require("express-validator"));

var _bodyParser = _interopRequireDefault(require("body-parser"));

var _cookieParser = _interopRequireDefault(require("cookie-parser"));

var _path = _interopRequireDefault(require("path"));

var _EnvironmentProxy = _interopRequireDefault(require("./libs/EnvironmentProxy"));

var _TrackingDeliveryUiServiceProxy = _interopRequireDefault(require("./libs/proxyServices/TrackingDeliveryUiServiceProxy"));

var _TrackingDeviceUiServiceProxy = _interopRequireDefault(require("./libs/proxyServices/TrackingDeviceUiServiceProxy"));

var _TrackingAssetUiServiceProxy = _interopRequireDefault(require("./libs/proxyServices/TrackingAssetUiServiceProxy"));

var _AzureSearchProxy = _interopRequireDefault(require("./libs/proxyServices/AzureSearchProxy"));

var _AzureLocationServiceProxy = _interopRequireDefault(require("./libs/AzureLocationServiceProxy"));

var _ResourcesProxy = _interopRequireDefault(require("./libs/ResourcesProxy"));

var _LoginServiceProxy = require("./libs/LoginServiceProxy");

var _UserProxy = _interopRequireDefault(require("./libs/proxyServices/UserProxy"));

var _UserAzureProxy = _interopRequireDefault(require("./libs/proxyServices/UserAzureProxy"));

var _morgan = _interopRequireDefault(require("morgan"));

var _loginHelper = require("./loginHelper");

var _config = _interopRequireDefault(require("./config"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// compresses requests
let appInsights = require("applicationinsights");

appInsights.setup(_config.default.applicationinsights.AppInsightsInstrumentationKey).setAutoDependencyCorrelation(true).setAutoCollectRequests(true).setAutoCollectPerformance(true, true).setAutoCollectExceptions(true).setAutoCollectDependencies(true).setAutoCollectConsole(true).setUseDiskRetryCaching(true).setSendLiveMetrics(true).start();
const app = (0, _express.default)();

const Sequelize = require('sequelize');

app.use((0, _compression.default)());
app.use(_bodyParser.default.json());
app.use(_bodyParser.default.urlencoded({
  extended: true
}));
app.use((0, _cookieParser.default)());
app.use((0, _morgan.default)('combined'));
app.use((0, _expressValidator.default)());
(0, _LoginServiceProxy.initLoginService)(app); // Override timezone formatting for MSSQL

Sequelize.DATE.prototype._stringify = function _stringify(date, options) {
  return this._applyTimezone(date, options).format('YYYY-MM-DD HH:mm:ss.SSS');
};

app.use("/static-files", _express.default.static(_path.default.join(__dirname + '../../../client/build')));
app.use("/azure", _AzureLocationServiceProxy.default);
app.use("/delivery-ui-service", _TrackingDeliveryUiServiceProxy.default);
app.use("/device-ui-service", _TrackingDeviceUiServiceProxy.default);
app.use("/asset-ui-service", _TrackingAssetUiServiceProxy.default);
app.use("/user-ui-service", _UserProxy.default);
app.use("/user-azure-ui-service", _UserAzureProxy.default);
app.use(_AzureSearchProxy.default);
app.use(_EnvironmentProxy.default);
app.use(_ResourcesProxy.default); // Serve the static files from the React app

app.get('/account2', _loginHelper.ensureAuthenticated, _loginHelper.ensureIsBpwAdmin, function (req, res) {
  res.type("application/json");
  let returnValue = {
    user: req.user
  };
  res.send(returnValue);
}); // Handles any requests that don't match the ones above

app.get('/', _loginHelper.ensureAuthenticated, (req, res) => {
  console.log("start site");
  res.sendFile(_path.default.join(__dirname + '../../../client/build/index.html'));
});
app.get('/*', _loginHelper.ensureAuthenticated, (req, res) => {
  console.log("start site index");
  res.sendFile(_path.default.join(__dirname + '../../../client/build/index.html'));
});
const port = process.env.PORT || 1337;
app.listen(port);
console.log('App is listening on port ' + port + ' in ' + process.env.NODE_ENV);