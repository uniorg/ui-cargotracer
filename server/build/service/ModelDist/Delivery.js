"use strict";

/* jshint indent: 1 */
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('Delivery', {
    ID: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    Creation_Date: {
      type: DataTypes.DATE,
      allowNull: true
    },
    Creation_User: {
      type: DataTypes.STRING,
      allowNull: true
    },
    Last_Update: {
      type: DataTypes.DATE,
      allowNull: true
    },
    Last_Update_User: {
      type: DataTypes.STRING,
      allowNull: true
    },
    Mandant: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    MandantSub: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    ShippingFromID: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'ShippingFrom',
        key: 'ID'
      }
    },
    ShippingToID: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'ShippingTo',
        key: 'ID'
      }
    },
    Delivery_nr: {
      type: DataTypes.STRING,
      allowNull: true
    },
    Delivery_date: {
      type: DataTypes.DATE,
      allowNull: true
    },
    Order_nr: {
      type: DataTypes.STRING,
      allowNull: true
    },
    Material_aggregate: {
      type: DataTypes.STRING,
      allowNull: true
    },
    Purchase_order_recipient: {
      type: DataTypes.STRING,
      allowNull: true
    },
    Shipping_conditions: {
      type: DataTypes.STRING,
      allowNull: true
    },
    Arrivaltime_text: {
      type: DataTypes.STRING,
      allowNull: true
    },
    Arrivaltime_from: {
      type: DataTypes.DATE,
      allowNull: true
    },
    Arrivaltime_to: {
      type: DataTypes.DATE,
      allowNull: true
    },
    Shipment_mode: {
      type: DataTypes.STRING,
      allowNull: true
    },
    Sold_to_party_code: {
      type: DataTypes.STRING,
      allowNull: true
    },
    Weight_total: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    Weight_total_unit: {
      type: DataTypes.STRING,
      allowNull: true
    },
    Tour_nr: {
      type: DataTypes.STRING,
      allowNull: true
    },
    Carrier: {
      type: DataTypes.STRING,
      allowNull: true
    },
    Handing_Over_Date: {
      type: DataTypes.DATE,
      allowNull: true
    },
    Active: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      defaultValue: '1'
    },
    Deleted: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      defaultValue: '0'
    },
    Status: {
      type: DataTypes.STRING,
      allowNull: true
    }
  }, {
    tableName: 'Delivery'
  });
};