"use strict";

/* jshint indent: 1 */
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('UserConfigs', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    oid: {
      type: DataTypes.STRING,
      allowNull: true
    },
    creationUser: {
      type: DataTypes.STRING,
      allowNull: true
    },
    lastUpdateUser: {
      type: DataTypes.STRING,
      allowNull: true
    },
    deleted: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    defaultLanguage: {
      type: DataTypes.STRING,
      allowNull: true
    },
    lastAddressesItemsCount: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false
    }
  });
};