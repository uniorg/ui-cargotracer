"use strict";

/* jshint indent: 1 */
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('ShippingFrom', {
    ID: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    Creation_Date: {
      type: DataTypes.DATE,
      allowNull: true
    },
    Creation_User: {
      type: DataTypes.STRING,
      allowNull: true
    },
    Last_Update: {
      type: DataTypes.DATE,
      allowNull: true
    },
    Last_Update_User: {
      type: DataTypes.STRING,
      allowNull: true
    },
    Mandant: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    MandantSub: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    SenderID: {
      type: DataTypes.BIGINT,
      allowNull: true,
      primaryKey: true
    },
    ContactID: {
      type: DataTypes.BIGINT,
      allowNull: true,
      primaryKey: true
    },
    AddressID: {
      type: DataTypes.BIGINT,
      allowNull: true,
      primaryKey: true
    },
    Active: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      defaultValue: '1'
    },
    Deleted: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      defaultValue: '0'
    }
  }, {
    tableName: 'ShippingFrom'
  });
};