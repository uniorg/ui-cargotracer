"use strict";

/* jshint indent: 1 */
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('Geofence', {
    ID: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    Creation_Date: {
      type: DataTypes.DATE,
      allowNull: true
    },
    Creation_User: {
      type: DataTypes.STRING,
      allowNull: true
    },
    Last_Update: {
      type: DataTypes.DATE,
      allowNull: true
    },
    Last_Update_User: {
      type: DataTypes.STRING,
      allowNull: true
    },
    Mandant: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    MandantSub: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    Name: {
      type: DataTypes.STRING,
      allowNull: true
    },
    Type: {
      type: DataTypes.STRING,
      allowNull: true
    },
    Radius: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: '((0))'
    },
    Geolocation: {
      type: "GEOGRAPHY",
      allowNull: true
    },
    Active: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      defaultValue: '1'
    },
    Deleted: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      defaultValue: '0'
    },
    Longitude: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    Latitude: {
      type: DataTypes.FLOAT,
      allowNull: true
    }
  }, {
    tableName: 'Geofence'
  });
};