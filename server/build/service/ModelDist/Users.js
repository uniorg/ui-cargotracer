"use strict";

/* jshint indent: 1 */
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('Users', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    sub: {
      type: DataTypes.STRING,
      allowNull: true
    },
    oid: {
      type: DataTypes.STRING,
      allowNull: true
    },
    userConfigId: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    creationUser: {
      type: DataTypes.STRING,
      allowNull: true
    },
    lastUpdateUser: {
      type: DataTypes.STRING,
      allowNull: true
    },
    deleted: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: true
    },
    familyName: {
      type: DataTypes.STRING,
      allowNull: true
    },
    givenName: {
      type: DataTypes.STRING,
      allowNull: true
    },
    about: {
      type: DataTypes.STRING,
      allowNull: true
    },
    email: {
      type: DataTypes.STRING,
      allowNull: true
    },
    lastLogin: {
      type: DataTypes.DATE,
      allowNull: true
    },
    lastLogout: {
      type: DataTypes.DATE,
      allowNull: true
    },
    lastAction: {
      type: DataTypes.DATE,
      allowNull: true
    },
    status: {
      type: DataTypes.STRING,
      allowNull: true
    },
    idToken: {
      type: DataTypes.STRING,
      allowNull: true
    },
    accessToken: {
      type: DataTypes.STRING,
      allowNull: true
    },
    refreshToken: {
      type: DataTypes.STRING,
      allowNull: true
    },
    displayName: {
      type: DataTypes.STRING,
      allowNull: true
    },
    authTime: {
      type: DataTypes.DATE,
      allowNull: true
    },
    extensionBpwMandantSub: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    extensionBpwMandant: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    extensionBpwAdminBpw: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    extensionBpwCompany: {
      type: DataTypes.STRING,
      allowNull: true
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false
    }
  }, {
    tableName: 'Users'
  });
};