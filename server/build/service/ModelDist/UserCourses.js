"use strict";

/* jshint indent: 1 */
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('UserCourses', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    creationUser: {
      type: DataTypes.STRING,
      allowNull: true
    },
    lastUpdateUser: {
      type: DataTypes.STRING,
      allowNull: true
    },
    deleted: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    url: {
      type: DataTypes.STRING,
      allowNull: true
    },
    clicks: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    browserName: {
      type: DataTypes.STRING,
      allowNull: true
    },
    browserOs: {
      type: DataTypes.STRING,
      allowNull: true
    },
    browserVersion: {
      type: DataTypes.STRING,
      allowNull: true
    },
    windowHeight: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    windowWidth: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    documentHeight: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    documentWidth: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false
    }
  }, {
    tableName: 'UserCourses'
  });
};