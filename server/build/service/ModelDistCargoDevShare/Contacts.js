"use strict";

/* jshint indent: 1 */
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('Contacts', {
    id: {
      type: DataTypes.BIGINT,
      autoIncrement: true,
      allowNull: false,
      primaryKey: true
    },
    keyLabel: {
      type: DataTypes.STRING,
      allowNull: false
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    emailAddress: {
      type: DataTypes.STRING,
      allowNull: true
    },
    phoneNumber: {
      type: DataTypes.STRING,
      allowNull: true
    },
    faxNumber: {
      type: DataTypes.STRING,
      allowNull: true
    },
    guid: {
      type: DataTypes.STRING,
      allowNull: false
    },
    clientId: {
      type: DataTypes.BIGINT,
      allowNull: false
    },
    subclientId: {
      type: DataTypes.BIGINT,
      allowNull: false
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    createdFrom: {
      type: DataTypes.STRING,
      allowNull: false
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updatedFrom: {
      type: DataTypes.STRING,
      allowNull: false
    },
    deleted: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    }
  }, {
    tableName: 'Contacts'
  });
};