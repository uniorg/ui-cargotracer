"use strict";

/* jshint indent: 1 */
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('SubclientsToAssignedSubclients', {
    clientId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    subclientId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    assignedSubClientId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    deleted: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    createdFrom: {
      type: DataTypes.STRING,
      allowNull: false
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updatedFrom: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
    tableName: 'SubclientsToAssignedSubclients'
  });
};