"use strict";

/* jshint indent: 1 */
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('DeliveriesActualData', {
    deliveryId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    deliveryState: {
      type: DataTypes.STRING,
      allowNull: false
    },
    deliveryStateTimestamp: {
      type: DataTypes.DATE,
      allowNull: false
    },
    deliveryStateDeviceId: {
      type: DataTypes.STRING,
      allowNull: true
    },
    lastValidGeoPositionSentAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    lastValidGeoPositionLongitude: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    lastValidGeoPositionLatitude: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    lastValidGeoPositionRadius: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    lastValidGeoPositionType: {
      type: DataTypes.STRING,
      allowNull: true
    },
    lastValidGeoPositionSpeed: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    lastValidGeoPositionTemperature: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    lastValidGpsPositionSentAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    lastValidGpsPositionLongitude: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    lastValidGpsPositionLatitude: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    etaArrival: {
      type: DataTypes.DATE,
      allowNull: true
    },
    etaArrivalGeoPositionType: {
      type: DataTypes.STRING,
      allowNull: true
    },
    etaArrivalLastValidGpsPosition: {
      type: DataTypes.DATE,
      allowNull: true
    },
    isLate: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    vibrationCount: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    shippingFromGeofenceId: {
      type: DataTypes.BIGINT,
      allowNull: true,
      primaryKey: true
    },
    shippingToGeofenceId: {
      type: DataTypes.BIGINT,
      allowNull: true,
      primaryKey: true
    },
    guid: {
      type: DataTypes.STRING,
      allowNull: false
    },
    clientId: {
      type: DataTypes.BIGINT,
      allowNull: false
    },
    subclientId: {
      type: DataTypes.BIGINT,
      allowNull: false
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    createdFrom: {
      type: DataTypes.STRING,
      allowNull: false
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updatedFrom: {
      type: DataTypes.STRING,
      allowNull: false
    },
    deleted: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    }
  }, {
    tableName: 'DeliveriesActualData'
  });
};