"use strict";

/* jshint indent: 1 */
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('DeliveriesToAssignedDevices', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true
    },
    deliveryId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    deviceId: {
      type: DataTypes.STRING,
      allowNull: false
    },
    active: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    assignmentBegin: {
      type: DataTypes.DATE,
      allowNull: false
    },
    assignmentEnd: {
      type: DataTypes.DATE,
      allowNull: true
    },
    guid: {
      type: DataTypes.STRING,
      allowNull: false
    },
    clientId: {
      type: DataTypes.BIGINT,
      allowNull: false
    },
    subclientId: {
      type: DataTypes.BIGINT,
      allowNull: false
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    createdFrom: {
      type: DataTypes.STRING,
      allowNull: false
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updatedFrom: {
      type: DataTypes.STRING,
      allowNull: false
    },
    deleted: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    handlingUnitId: {
      type: DataTypes.BIGINT,
      allowNull: true,
      primaryKey: true
    }
  }, {
    tableName: 'DeliveriesToAssignedDevices'
  });
};