"use strict";

/* jshint indent: 1 */
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('Deliveries', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true
    },
    deliveryNumber: {
      type: DataTypes.STRING,
      allowNull: true
    },
    shippingFromAddressId: {
      type: DataTypes.BIGINT,
      allowNull: true,
      primaryKey: true
    },
    shippingFromContactId: {
      type: DataTypes.BIGINT,
      allowNull: true,
      primaryKey: true
    },
    shippingToAddressId: {
      type: DataTypes.BIGINT,
      allowNull: true,
      primaryKey: true
    },
    plannedArrivalTimeFrom: {
      type: DataTypes.DATE,
      allowNull: true
    },
    plannedArrivalTimeTo: {
      type: DataTypes.DATE,
      allowNull: true
    },
    materialAggregate: {
      type: DataTypes.STRING,
      allowNull: true
    },
    shippingConditions: {
      type: DataTypes.STRING,
      allowNull: true
    },
    shipmentMode: {
      type: DataTypes.STRING,
      allowNull: true
    },
    weightTotal: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    weightTotalUnit: {
      type: DataTypes.STRING,
      allowNull: true
    },
    carrier: {
      type: DataTypes.STRING,
      allowNull: true
    },
    handingOverDate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    orderId: {
      type: DataTypes.BIGINT,
      allowNull: true,
      primaryKey: true
    },
    transportId: {
      type: DataTypes.BIGINT,
      allowNull: true,
      primaryKey: true
    },
    soldToPartyCode: {
      type: DataTypes.STRING,
      allowNull: true
    },
    purchaseOrderRecipient: {
      type: DataTypes.STRING,
      allowNull: true
    },
    guid: {
      type: DataTypes.STRING,
      allowNull: false
    },
    clientId: {
      type: DataTypes.BIGINT,
      allowNull: false
    },
    subclientId: {
      type: DataTypes.BIGINT,
      allowNull: false
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    createdFrom: {
      type: DataTypes.STRING,
      allowNull: false
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updatedFrom: {
      type: DataTypes.STRING,
      allowNull: false
    },
    deleted: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    }
  }, {
    tableName: 'Deliveries'
  });
};