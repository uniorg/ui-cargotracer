"use strict";

/* jshint indent: 1 */
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('DeviceScans', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true
    },
    scannedAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    deviceId: {
      type: DataTypes.STRING,
      allowNull: false
    },
    scanType: {
      type: DataTypes.STRING,
      allowNull: false
    },
    scannedCode: {
      type: DataTypes.STRING,
      allowNull: false
    },
    weight: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    weightUnit: {
      type: DataTypes.STRING,
      allowNull: true
    },
    comment: {
      type: DataTypes.STRING,
      allowNull: true
    },
    guid: {
      type: DataTypes.STRING,
      allowNull: false
    },
    clientId: {
      type: DataTypes.BIGINT,
      allowNull: false
    },
    subclientId: {
      type: DataTypes.BIGINT,
      allowNull: false
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    createdFrom: {
      type: DataTypes.STRING,
      allowNull: false
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updatedFrom: {
      type: DataTypes.STRING,
      allowNull: false
    },
    deleted: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    }
  }, {
    tableName: 'DeviceScans'
  });
};