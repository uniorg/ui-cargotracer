"use strict";

/* jshint indent: 1 */
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('DeliveriesStateHistory', {
    deliveryId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    savedAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    scanAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    scanVibrationCount: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    onTheWayAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    onTheWayVibrationCount: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    onTheWayMessageIdentity: {
      type: DataTypes.STRING,
      allowNull: true
    },
    arrivedAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    arrivedVibrationCount: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    arrivedMessageIdentity: {
      type: DataTypes.STRING,
      allowNull: true
    },
    doneAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    unknownAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    transitTimeSeconds: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    guid: {
      type: DataTypes.STRING,
      allowNull: false
    },
    clientId: {
      type: DataTypes.BIGINT,
      allowNull: false
    },
    subclientId: {
      type: DataTypes.BIGINT,
      allowNull: false
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    createdFrom: {
      type: DataTypes.STRING,
      allowNull: false
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updatedFrom: {
      type: DataTypes.STRING,
      allowNull: false
    },
    deleted: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    }
  }, {
    tableName: 'DeliveriesStateHistory'
  });
};