"use strict";

/* jshint indent: 1 */
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('HandlingUnits', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true
    },
    deliveryId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    handlingUnitCode: {
      type: DataTypes.STRING,
      allowNull: false
    },
    descriptionShort: {
      type: DataTypes.STRING,
      allowNull: true
    },
    dimensions: {
      type: DataTypes.STRING,
      allowNull: true
    },
    quantity: {
      type: DataTypes.DECIMAL,
      allowNull: false
    },
    quantityUnit: {
      type: DataTypes.STRING,
      allowNull: true
    },
    weightGross: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    weightNet: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    weightUnit: {
      type: DataTypes.STRING,
      allowNull: true
    },
    batchCode: {
      type: DataTypes.STRING,
      allowNull: true
    },
    materialOrigin: {
      type: DataTypes.STRING,
      allowNull: true
    },
    meltingBatch: {
      type: DataTypes.STRING,
      allowNull: true
    },
    guid: {
      type: DataTypes.STRING,
      allowNull: false
    },
    clientId: {
      type: DataTypes.BIGINT,
      allowNull: false
    },
    subclientId: {
      type: DataTypes.BIGINT,
      allowNull: false
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    createdFrom: {
      type: DataTypes.STRING,
      allowNull: false
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updatedFrom: {
      type: DataTypes.STRING,
      allowNull: false
    },
    deleted: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    }
  }, {
    tableName: 'HandlingUnits'
  });
};