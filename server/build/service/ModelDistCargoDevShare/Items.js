"use strict";

/* jshint indent: 1 */
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('Items', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true
    },
    deliveryId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    itemNumber: {
      type: DataTypes.STRING,
      allowNull: false
    },
    materialSender: {
      type: DataTypes.STRING,
      allowNull: false
    },
    materialRecipient: {
      type: DataTypes.STRING,
      allowNull: true
    },
    descriptionShort: {
      type: DataTypes.STRING,
      allowNull: false
    },
    descriptionLong: {
      type: DataTypes.STRING,
      allowNull: true
    },
    dimensions: {
      type: DataTypes.STRING,
      allowNull: true
    },
    quantity: {
      type: DataTypes.DECIMAL,
      allowNull: false
    },
    quantityUnit: {
      type: DataTypes.STRING,
      allowNull: true
    },
    weightGross: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    weightNet: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    weightUnit: {
      type: DataTypes.STRING,
      allowNull: true
    },
    packagingUnit: {
      type: DataTypes.STRING,
      allowNull: true
    },
    packagingUnitDescription: {
      type: DataTypes.STRING,
      allowNull: true
    },
    workProcedure: {
      type: DataTypes.STRING,
      allowNull: true
    },
    commodityCode: {
      type: DataTypes.STRING,
      allowNull: true
    },
    guid: {
      type: DataTypes.STRING,
      allowNull: false
    },
    clientId: {
      type: DataTypes.BIGINT,
      allowNull: false
    },
    subclientId: {
      type: DataTypes.BIGINT,
      allowNull: false
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    createdFrom: {
      type: DataTypes.STRING,
      allowNull: false
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updatedFrom: {
      type: DataTypes.STRING,
      allowNull: false
    },
    deleted: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    }
  }, {
    tableName: 'Items'
  });
};