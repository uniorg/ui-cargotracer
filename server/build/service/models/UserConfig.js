"use strict";

module.exports = function (sequelize, Sequelize) {
  let UserConfig = sequelize.define('UserConfig', {
    id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      allowNull: false,
      primaryKey: true
    },
    oid: {
      type: Sequelize.STRING,
      allowNull: true
    },
    creationUser: {
      type: Sequelize.STRING,
      allowNull: true
    },
    lastUpdateUser: {
      type: Sequelize.STRING,
      allowNull: true
    },
    deleted: {
      type: Sequelize.BOOLEAN,
      allowNull: true
    },
    defaultLanguage: {
      type: Sequelize.STRING,
      allowNull: true
    },
    lastAddressesItemsCount: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    createdAt: {
      type: Sequelize.DATE,
      allowNull: false
    },
    createdFrom: {
      type: Sequelize.STRING,
      allowNull: false
    },
    updatedAt: {
      type: Sequelize.DATE,
      allowNull: false
    },
    updatedFrom: {
      type: Sequelize.STRING,
      allowNull: false
    }
  }, {
    tableName: 'UserConfigs',
    indexes: []
  });
  return UserConfig;
};