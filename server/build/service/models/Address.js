"use strict";

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('Address', {
    ID: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    Creation_Date: {
      type: DataTypes.DATE,
      allowNull: true
    },
    Creation_User: {
      type: DataTypes.STRING,
      allowNull: true
    },
    Last_Update: {
      type: DataTypes.DATE,
      allowNull: true
    },
    Last_Update_User: {
      type: DataTypes.STRING,
      allowNull: true
    },
    Mandant: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    MandantSub: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    Name: {
      type: DataTypes.STRING,
      allowNull: true
    },
    District: {
      type: DataTypes.STRING,
      allowNull: true
    },
    Street_house_nr: {
      type: DataTypes.STRING,
      allowNull: true
    },
    Postal_code: {
      type: DataTypes.STRING,
      allowNull: true
    },
    Po_box: {
      type: DataTypes.STRING,
      allowNull: true
    },
    City: {
      type: DataTypes.STRING,
      allowNull: true
    },
    Country: {
      type: DataTypes.STRING,
      allowNull: true
    },
    GeofenceID: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'Geofence',
        key: 'ID'
      }
    },
    Key_label: {
      type: DataTypes.STRING,
      allowNull: true
    },
    Active: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      defaultValue: '1'
    },
    Deleted: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      defaultValue: '0'
    },
    Location_Type: {
      type: DataTypes.STRING,
      allowNull: true
    }
  }, {
    tableName: 'Address'
  });
};