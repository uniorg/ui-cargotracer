"use strict";

module.exports = function (sequelize, Sequelize) {
  let UserCourse = sequelize.define('UserCourse', {
    id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    creationUser: {
      type: Sequelize.STRING,
      allowNull: true
    },
    lastUpdateUser: {
      type: Sequelize.STRING,
      allowNull: true
    },
    deleted: {
      type: Sequelize.BOOLEAN,
      allowNull: true
    },
    url: {
      type: Sequelize.STRING,
      allowNull: true
    },
    clicks: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    browserName: {
      type: Sequelize.STRING,
      allowNull: true
    },
    browserOs: {
      type: Sequelize.STRING,
      allowNull: true
    },
    browserVersion: {
      type: Sequelize.STRING,
      allowNull: true
    },
    windowHeight: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    windowWidth: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    documentHeight: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    documentWidth: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    createdAt: {
      type: Sequelize.DATE,
      allowNull: false
    },
    createdFrom: {
      type: Sequelize.STRING,
      allowNull: false
    },
    updatedAt: {
      type: Sequelize.DATE,
      allowNull: false
    },
    updatedFrom: {
      type: Sequelize.STRING,
      allowNull: false
    }
  }, {
    indexes: [{
      name: 'creationUserIndex',
      fields: ['creationUser']
    }, {
      name: 'creationUserUrlIndex',
      fields: ['creationUser', "url"]
    }],
    tableName: 'UserCourses'
  });
  return UserCourse;
};