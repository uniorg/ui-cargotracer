"use strict";

module.exports = function (sequelize) {
  const fs = require("fs");

  const path = require("path");

  const pluralize = require("pluralize");

  const db = sequelize;
  db.modelsName = [];
  fs.readdirSync(__dirname).filter(function (file) {
    return file.indexOf(".") !== 0 && file !== "index.js";
  }).forEach(function (file) {
    const model = sequelize.import(path.join(__dirname, file));

    try {
      if (fs.existsSync(__dirname + "/../models/" + model.name + ".js")) {
        db.modelsName.push(model.name);
      } else if (fs.existsSync(__dirname + "/../models/" + pluralize(model.name) + ".js")) {
        db.modelsName.push(pluralize(model.name));
      }
    } catch (err) {
      console.error(err);
    }

    db[model.name] = model;
  });
  Object.keys(db).forEach(function (modelName) {
    if ("associate" in db[modelName]) {
      db[modelName].associate(db);
    }
  });

  if (db.hasOwnProperty("UserLastAddress") && db.hasOwnProperty("Address")) {//db["UserLastAddress"].hasOne( db["Address"], {as: "LastAddress", foreignKey: "addressId"} );
  }

  return db;
};