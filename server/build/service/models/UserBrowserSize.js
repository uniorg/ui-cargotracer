"use strict";

module.exports = function (sequelize, Sequelize) {
  let UserBrowserSizes = sequelize.define('UserBrowserSize', {
    id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      allowNull: false,
      primaryKey: true
    },
    deleted: {
      type: Sequelize.BOOLEAN,
      allowNull: true
    },
    count: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    browserName: {
      type: Sequelize.STRING,
      allowNull: true
    },
    browserOs: {
      type: Sequelize.STRING,
      allowNull: true
    },
    browserVersion: {
      type: Sequelize.STRING,
      allowNull: true
    },
    windowHeight: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    windowWidth: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    documentHeight: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    documentWidth: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    createdAt: {
      type: Sequelize.DATE,
      allowNull: false
    },
    createdFrom: {
      type: Sequelize.STRING,
      allowNull: false
    },
    updatedAt: {
      type: Sequelize.DATE,
      allowNull: false
    },
    updatedFrom: {
      type: Sequelize.STRING,
      allowNull: false
    }
  }, {
    indexes: [{
      name: 'creationUserValueIndex',
      fields: ['browserName', "browserOs", "browserVersion", "windowHeight", "windowWidth", "documentHeight", "documentWidth"]
    }],
    tableName: 'UserBrowserSizes'
  });
  return UserBrowserSizes;
};