"use strict";

module.exports = function (sequelize, Sequelize) {
  let User = sequelize.define('User', {
    id: {
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER
    },
    sub: {
      type: Sequelize.STRING,
      notEmpty: true
    },
    oid: {
      type: Sequelize.STRING,
      notEmpty: true
    },
    userConfigId: {
      type: Sequelize.INTEGER,
      notEmpty: true
    },
    creationUser: {
      type: Sequelize.STRING,
      notEmpty: true
    },
    lastUpdateUser: {
      type: Sequelize.STRING,
      notEmpty: true
    },
    deleted: {
      type: Sequelize.BOOLEAN,
      notEmpty: false,
      defaultValue: false
    },
    name: {
      type: Sequelize.STRING,
      notEmpty: true
    },
    familyName: {
      type: Sequelize.STRING,
      notEmpty: true
    },
    givenName: {
      type: Sequelize.STRING
    },
    about: {
      type: Sequelize.TEXT
    },
    email: {
      type: Sequelize.STRING,
      validate: {
        isEmail: true
      }
    },
    lastLogin: {
      type: Sequelize.DATE
    },
    lastLogout: {
      type: Sequelize.DATE
    },
    lastAction: {
      type: Sequelize.DATE
    },
    status: {
      type: Sequelize.ENUM('active', 'inactive'),
      defaultValue: 'active'
    },
    idToken: {
      type: Sequelize.TEXT
    },
    accessToken: {
      type: Sequelize.TEXT
    },
    refreshToken: {
      type: Sequelize.TEXT
    },
    displayName: {
      type: Sequelize.TEXT
    },
    authTime: {
      type: Sequelize.DATE
    },
    extensionBpwMandantSub: {
      type: Sequelize.INTEGER
    },
    extensionBpwMandant: {
      type: Sequelize.INTEGER
    },
    extensionBpwAdminBpw: {
      type: Sequelize.INTEGER
    },
    extensionBpwCompany: {
      type: Sequelize.TEXT
    },
    createdAt: {
      type: Sequelize.DATE,
      allowNull: false
    },
    createdFrom: {
      type: Sequelize.STRING,
      allowNull: false
    },
    updatedAt: {
      type: Sequelize.DATE,
      allowNull: false
    },
    updatedFrom: {
      type: Sequelize.STRING,
      allowNull: false
    }
  }, {
    indexes: [{
      name: 'creationUserIndex',
      fields: ["creationUser"]
    }],
    tableName: 'Users'
  });
  return User;
};