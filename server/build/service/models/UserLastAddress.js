"use strict";

module.exports = function (sequelize, Sequelize) {
  let UserLastAddress = sequelize.define('UserLastAddress', {
    id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    creationUser: {
      type: Sequelize.STRING,
      allowNull: true
    },
    lastUpdateUser: {
      type: Sequelize.STRING,
      allowNull: true
    },
    deleted: {
      type: Sequelize.BOOLEAN,
      allowNull: true
    },
    oid: {
      type: Sequelize.STRING,
      allowNull: true
    },
    addressId: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    userId: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    createdAt: {
      type: Sequelize.DATE,
      allowNull: false
    },
    createdFrom: {
      type: Sequelize.STRING,
      allowNull: false
    },
    updatedAt: {
      type: Sequelize.DATE,
      allowNull: false
    },
    updatedFrom: {
      type: Sequelize.STRING,
      allowNull: false
    }
  }, {
    tableName: 'UserLastAddresses'
  });
  return UserLastAddress;
};