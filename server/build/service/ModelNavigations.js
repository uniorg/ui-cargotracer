"use strict";

module.exports = function () {
  return {
    "Users": [{
      "name": "UserConfigs",
      "entityTypeName": "UserConfigs:#Models",
      "isScalar": true,
      "associationName": "Users_UserConfigs",
      "foreignKeyNames": ["userConfigId"]
    }, {
      "name": "UserLastAddresses",
      "entityTypeName": "UserLastAddresses:#Models",
      "isScalar": false,
      "associationName": "Users_UserLastAddresses"
    }],
    "UserLastAddresses": [{
      "name": "Address",
      "entityTypeName": "Address:#Models",
      "isScalar": true,
      "associationName": "UserLastAddresses_Address",
      "foreignKeyNames": ["addressId"]
    }, {
      "name": "Users",
      "entityTypeName": "Users:#Models",
      "isScalar": true,
      "associationName": "Users_UserLastAddresses",
      "foreignKeyNames": ["userId"]
    }],
    "Address": [{
      "name": "UserLastAddresses",
      "entityTypeName": "UserLastAddresses:#Models",
      "isScalar": true,
      "associationName": "UserLastAddresses_Address",
      "invForeignKeyNames": ["addressId"]
    }, {
      "name": "Geofence",
      "entityTypeName": "Geofence:#Models",
      "isScalar": true,
      "associationName": "Geofence_Address",
      "foreignKeyNames": ["GeofenceID"]
    }],
    "UserConfigs": [{
      "name": "Users",
      "entityTypeName": "Users:#Models",
      "isScalar": true,
      "associationName": "Users_UserConfigs",
      "invForeignKeyNames": ["userConfigId"]
    }],
    "Geofence": [{
      "name": "Address",
      "entityTypeName": "Address:#Models",
      "isScalar": false,
      "associationName": "Geofence_Address"
    }]
  };
};