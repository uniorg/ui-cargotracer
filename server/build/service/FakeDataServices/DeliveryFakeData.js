"use strict";

module.exports = function (app) {
  let sequelizeReturn = false;

  function createItemsToHandlingUnits(dbModels, mandant, item, handlingUnit, callback) {
    let itemsToHandlingUnit = function () {
      const faker = require('faker');

      faker.locale = "de";
      return {
        itemId: item.id,
        handlingUnitId: handlingUnit.id,
        guid: faker.random.uuid(),
        clientId: mandant,
        subclientId: mandant,
        createdAt: dbModels.literal('CURRENT_TIMESTAMP'),
        createdFrom: "fake",
        updatedAt: dbModels.literal('CURRENT_TIMESTAMP'),
        updatedFrom: "fake",
        deleted: 0
      };
    };

    dbModels.ItemsToHandlingUnits.create(new itemsToHandlingUnit()).then((newItemsToHandlingUnits, created) => {
      if (!newItemsToHandlingUnits) {
        console.log("newItemsToHandlingUnits fail");
      }

      if (newItemsToHandlingUnits) {
        callback(newItemsToHandlingUnits);
      }
    }).catch(error => {
      console.log("ItemsToHandlingUnits", error);
      callback(false);
    });
  }

  function createContact(dbModels, mandant, callback) {
    let contact = function () {
      const faker = require('faker');

      faker.locale = "de";
      let name = faker.name.firstName();
      let last = faker.name.lastName();
      return {
        keyLabel: name + "-" + last,
        name: name + " " + last,
        emailAddress: faker.internet.exampleEmail(name, last),
        phoneNumber: faker.phone.phoneNumber(),
        faxNumber: faker.phone.phoneNumber(),
        guid: faker.random.uuid(),
        clientId: mandant,
        subclientId: mandant,
        createdAt: dbModels.literal('CURRENT_TIMESTAMP'),
        createdFrom: "fake",
        updatedAt: dbModels.literal('CURRENT_TIMESTAMP'),
        updatedFrom: "fake",
        deleted: 0
      };
    };

    dbModels.Contacts.create(new contact()).then((newContacts, created) => {
      if (!newContacts) {
        console.log("newContacts fail");
      }

      if (newContacts) {
        callback(newContacts);
      }
    }).catch(error => {
      console.log("Contacts", error);
      callback(false);
    });
  }

  function createDeliveryTransport(dbModels, mandant, callback) {
    let deliveryTransport = function () {
      const faker = require('faker');

      faker.locale = "de";
      return {
        transportNumber: String(faker.random.number()),
        carrier: String(faker.random.number()),
        guid: faker.random.uuid(),
        clientId: mandant,
        subclientId: mandant,
        createdAt: dbModels.literal('CURRENT_TIMESTAMP'),
        createdFrom: "fake",
        updatedAt: dbModels.literal('CURRENT_TIMESTAMP'),
        updatedFrom: "fake",
        deleted: 0
      };
    };

    dbModels.Transports.create(new deliveryTransport()).then((newTransports, created) => {
      if (!newTransports) {
        console.log("newTransports fail");
      }

      if (newTransports) {
        callback(newTransports);
      }
    }).catch(error => {
      console.log("Transports", error);
      callback(false);
    });
  }

  function createDeliveriesToAssignedDevices(dbModels, mandant, delivery, handlingUnit, callback) {
    let deliveriesToAssignedDevices = function () {
      const faker = require('faker');

      faker.locale = "de";
      return {
        deliveryId: delivery.id,
        deviceId: String(faker.random.number()),
        active: 1,
        assignmentBegin: dbModels.literal('CURRENT_TIMESTAMP'),
        assignmentEnd: dbModels.literal('CURRENT_TIMESTAMP'),
        guid: faker.random.uuid(),
        clientId: mandant,
        subclientId: mandant,
        createdAt: dbModels.literal('CURRENT_TIMESTAMP'),
        createdFrom: "fake",
        updatedAt: dbModels.literal('CURRENT_TIMESTAMP'),
        updatedFrom: "fake",
        deleted: 0,
        handlingUnitId: handlingUnit.id
      };
    };

    console.log(new deliveriesToAssignedDevices());
    dbModels.DeliveriesToAssignedDevices.create(new deliveriesToAssignedDevices()).then((newDeliveriesToAssignedDevices, created) => {
      if (!newDeliveriesToAssignedDevices) {
        console.log("newDeliveriesToAssignedDevices fail");
      }

      if (newDeliveriesToAssignedDevices) {
        callback(newDeliveriesToAssignedDevices);
      }
    }).catch(error => {
      console.log("DeliveriesToAssignedDevices", error);
      callback(false);
    });
  }

  function createDeliveryItem(dbModels, mandant, delivery, callback) {
    let deliveryItem = function () {
      const faker = require('faker');

      faker.locale = "de";
      return {
        deliveryId: delivery.id,
        itemNumber: String(faker.random.number()),
        materialSender: String(faker.random.number()),
        materialRecipient: String(faker.random.number()),
        descriptionShort: faker.lorem.words(5),
        descriptionLong: faker.lorem.words(5),
        dimensions: faker.random.arrayElement([100, 50, 30, 70, 120, 40, 55]) + "x" + faker.random.arrayElement([100, 50, 30, 70, 120, 40, 55]) + "x" + faker.random.arrayElement([100, 50, 30, 70, 120, 40, 55]),
        quantity: faker.random.number(),
        quantityUnit: faker.random.arrayElement(["KG", "ST"]),
        weightGross: faker.random.number(),
        weightNet: faker.random.number(),
        weightUnit: faker.random.arrayElement(["TO", "KG"]),
        packagingUnit: null,
        packagingUnitDescription: null,
        workProcedure: null,
        commodityCode: null,
        guid: faker.random.uuid(),
        clientId: mandant,
        subclientId: mandant,
        createdAt: dbModels.literal('CURRENT_TIMESTAMP'),
        createdFrom: "fake",
        updatedAt: dbModels.literal('CURRENT_TIMESTAMP'),
        updatedFrom: "fake",
        deleted: 0
      };
    };

    dbModels.Items.create(new deliveryItem()).then(function (newItems, created) {
      if (!newItems) {
        console.log("newItems fail");
      }

      if (newItems) {
        callback(newItems);
      }
    }).catch(error => {
      console.log("Items", error);
      callback(false);
    });
  }

  function createDeliveryActualData(dbModels, mandant, delivery, geofenceFrom, geofenceTo, callback) {
    let deliveryActualData = function () {
      const faker = require('faker');

      faker.locale = "de";
      return {
        deliveryId: delivery.id,
        deliveryState: faker.random.arrayElement(["SAVED", "SCANNED", "ON_THE_WAY", "ARRIVED", "DONE", "ABORTED", "UNKNOWN"]),
        deliveryStateTimestamp: dbModels.literal('CURRENT_TIMESTAMP'),
        deliveryStateDeviceId: String(faker.random.number()),
        lastValidGeoPositionSentAt: dbModels.literal('CURRENT_TIMESTAMP'),
        lastValidGeoPositionLongitude: faker.address.longitude(),
        lastValidGeoPositionLatitude: faker.address.latitude(),
        lastValidGeoPositionRadius: 10000,
        lastValidGeoPositionType: faker.random.arrayElement(["SIGFOX", "GPS"]),
        lastValidGeoPositionSpeed: faker.random.arrayElement([100, 50, 30, 70, 120, 40, 55]),
        lastValidGeoPositionTemperature: faker.random.arrayElement([20.2, 33, 26.5, 10, -20, 2, -10]),
        lastValidGpsPositionSentAt: dbModels.literal('CURRENT_TIMESTAMP'),
        lastValidGpsPositionLongitude: faker.address.longitude(),
        lastValidGpsPositionLatitude: faker.address.latitude(),
        etaArrival: dbModels.literal('CURRENT_TIMESTAMP'),
        etaArrivalGeoPositionType: "",
        etaArrivalLastValidGpsPosition: dbModels.literal('CURRENT_TIMESTAMP'),
        isLate: faker.random.arrayElement([0, 1]),
        vibrationCount: faker.random.arrayElement([0, 100, 50, 30, 70, 120, 40, 55]),
        shippingFromGeofenceId: geofenceFrom.id,
        shippingToGeofenceId: geofenceTo.id,
        guid: faker.random.uuid(),
        clientId: mandant,
        subclientId: mandant,
        createdAt: dbModels.literal('CURRENT_TIMESTAMP'),
        createdFrom: "fake",
        updatedAt: dbModels.literal('CURRENT_TIMESTAMP'),
        updatedFrom: "fake",
        deleted: 0
      };
    };

    dbModels.DeliveriesActualData.create(new deliveryActualData()).then(function (newDeliveriesActualData, created) {
      if (!newDeliveriesActualData) {
        console.log("newDeliveriesActualData fail");
      }

      if (newDeliveriesActualData) {
        callback(newDeliveriesActualData);
      }
    }).catch(error => {
      console.log("DeliveriesActualData", error);
      callback(false);
    });
  }

  function createGeofences(dbModels, mandant, address, callback) {
    let geofenceFrom = function () {
      const faker = require('faker');

      faker.locale = "de";
      return {
        longitude: faker.address.longitude(),
        latitude: faker.address.latitude(),
        radius: 10000,
        geofenceType: "POINT",
        addressId: address.id,
        guid: faker.random.uuid(),
        clientId: mandant,
        subclientId: mandant,
        createdAt: dbModels.literal('CURRENT_TIMESTAMP'),
        createdFrom: "fake",
        updatedAt: dbModels.literal('CURRENT_TIMESTAMP'),
        updatedFrom: "fake",
        deleted: 0
      };
    };

    dbModels.Geofences.create(new geofenceFrom()).then(function (newGeofences, created) {
      if (!newGeofences) {
        console.log("newGeofences fail");
      }

      if (newGeofences) {
        callback(newGeofences);
      }
    }).catch(error => {
      console.log("Geofences", error);
      callback(false);
    });
  }

  function createAddress(dbModels, mandant, callback) {
    let address = function () {
      const faker = require('faker');

      faker.locale = "de";
      let addressFromName = faker.name.findName();
      return {
        keyLabel: addressFromName,
        name: addressFromName,
        postalCode: faker.address.zipCode(),
        city: faker.address.city(),
        street: faker.address.streetName(true),
        streetNumber: faker.random.number(),
        countryCode: faker.address.countryCode(),
        postalCodeBox: null,
        locationSource: "",
        locationSourceKey: "",
        guid: faker.random.uuid(),
        clientId: mandant,
        subclientId: mandant,
        createdAt: dbModels.literal('CURRENT_TIMESTAMP'),
        createdFrom: "fake",
        updatedAt: dbModels.literal('CURRENT_TIMESTAMP'),
        updatedFrom: "fake",
        deleted: 0
      };
    };

    dbModels.Addresses.create(new address()).then(function (newAddresses, created) {
      if (!newAddresses) {
        console.log("newAddresses fail");
      }

      if (newAddresses) {
        callback(newAddresses);
      }
    }).catch(error => {
      console.log("Addresses", error);
      callback(false);
    });
  }

  function createDelivers(dbModels, mandant, addressTo, addressFrom, contactFrom, transport, weightTotal, order, callback) {
    let delivery = function () {
      const faker = require('faker');

      faker.locale = "de";
      return {
        deliveryNumber: faker.random.number(),
        shippingFromAddressId: addressFrom.id,
        shippingFromContactId: contactFrom.id,
        shippingToAddressId: addressTo.id,
        plannedArrivalTimeFrom: dbModels.literal('CURRENT_TIMESTAMP'),
        plannedArrivalTimeTo: dbModels.literal('CURRENT_TIMESTAMP'),
        materialAggregate: null,
        shippingConditions: null,
        shipmentMode: null,
        weightTotal: weightTotal,
        weightTotalUnit: "KG",
        carrier: null,
        handingOverDate: null,
        orderId: order.id,
        transportId: transport.id,
        soldToPartyCode: null,
        purchaseOrderRecipient: "",
        guid: faker.random.uuid(),
        clientId: mandant,
        subclientId: mandant,
        createdAt: dbModels.literal('CURRENT_TIMESTAMP'),
        createdFrom: "fake",
        updatedAt: dbModels.literal('CURRENT_TIMESTAMP'),
        updatedFrom: "fake",
        deleted: 0
      };
    };

    dbModels.Deliveries.create(new delivery()).then(function (newDeliveries, created) {
      if (!newDeliveries) {
        console.log("newDeliveries fail");
      }

      if (newDeliveries) {
        callback(newDeliveries);
      }
    }).catch(error => {
      console.log("Deliveries", error);
      callback(false);
    });
  }

  function createOrder(dbModels, mandant, callback) {
    let order = function () {
      const faker = require('faker');

      faker.locale = "de";
      return {
        orderNumber: String(faker.random.number()),
        soldToPartyCode: null,
        purchaseOrderRecipient: null,
        guid: faker.random.uuid(),
        clientId: mandant,
        subclientId: mandant,
        createdAt: dbModels.literal('CURRENT_TIMESTAMP'),
        createdFrom: "fake",
        updatedAt: dbModels.literal('CURRENT_TIMESTAMP'),
        updatedFrom: "fake",
        deleted: 0
      };
    };

    dbModels.Orders.create(new order()).then(function (newOrders, created) {
      if (!newOrders) {
        console.log("newOrders fail");
      }

      if (newOrders) {
        callback(newOrders);
      }
    }).catch(error => {
      console.log("Orders", error);
      callback(false);
    });
  }

  function createDeliveriesStateHistory(dbModels, mandant, delivery, callback) {
    let deliveriesStateHistory = function () {
      const faker = require('faker');

      faker.locale = "de";
      return {
        deliveryId: delivery.id,
        savedAt: dbModels.literal('CURRENT_TIMESTAMP'),
        scanAt: dbModels.literal('CURRENT_TIMESTAMP'),
        scanVibrationCount: 5,
        onTheWayAt: dbModels.literal('CURRENT_TIMESTAMP'),
        onTheWayVibrationCount: 6,
        onTheWayMessageIdentity: null,
        arrivedAt: dbModels.literal('CURRENT_TIMESTAMP'),
        arrivedVibrationCount: 7,
        arrivedMessageIdentity: null,
        doneAt: dbModels.literal('CURRENT_TIMESTAMP'),
        unknownAt: dbModels.literal('CURRENT_TIMESTAMP'),
        transitTimeSeconds: null,
        guid: faker.random.uuid(),
        clientId: mandant,
        subclientId: mandant,
        createdAt: dbModels.literal('CURRENT_TIMESTAMP'),
        createdFrom: "fake",
        updatedAt: dbModels.literal('CURRENT_TIMESTAMP'),
        updatedFrom: "fake",
        deleted: 0
      };
    };

    dbModels.DeliveriesStateHistory.create(new deliveriesStateHistory()).then(function (newDeliveriesStateHistory, created) {
      if (!newDeliveriesStateHistory) {
        console.log("newDeliveriesStateHistory fail");
      }

      if (newDeliveriesStateHistory) {
        callback(newDeliveriesStateHistory);
      }
    }).catch(error => {
      console.log("DeliveriesStateHistory", error);
      callback(false);
    });
  }

  function createHandlingUnit(dbModels, mandant, delivery, callback) {
    let handlingUnits = function () {
      const faker = require('faker');

      faker.locale = "de";
      return {
        deliveryId: delivery.id,
        handlingUnitCode: String(faker.random.number()),
        descriptionShort: faker.lorem.words(5),
        dimensions: null,
        quantity: 10,
        quantityUnit: faker.random.arrayElement(["TO", "KG"]),
        weightGross: faker.random.number(),
        weightNet: faker.random.number(),
        weightUnit: faker.random.arrayElement(["cm", "mm"]),
        batchCode: String(faker.random.number()),
        materialOrigin: String(faker.random.number()),
        meltingBatch: String(faker.random.number()),
        guid: faker.random.uuid(),
        clientId: mandant,
        subclientId: mandant,
        createdAt: dbModels.literal('CURRENT_TIMESTAMP'),
        createdFrom: "fake",
        updatedAt: dbModels.literal('CURRENT_TIMESTAMP'),
        updatedFrom: "fake",
        deleted: 0
      };
    };

    dbModels.HandlingUnits.create(new handlingUnits()).then(function (newHandlingUnits, created) {
      if (!newHandlingUnits) {
        console.log("newHandlingUnits fail");
      }

      if (newHandlingUnits) {
        callback(newHandlingUnits);
      }
    }).catch(error => {
      console.log("HandlingUnits", error);
      callback(false);
    });
  }

  function createHandlingUnitsActualData(dbModels, mandant, delivery, handlingUnit, geofenceFrom, geofenceTo, callback) {
    let handlingUnitsActualData = function () {
      const faker = require('faker');

      faker.locale = "de";
      return {
        handlingUnitId: handlingUnit.id,
        handlingUnitState: faker.random.arrayElement(["SAVED", "SCANNED", "ON_THE_WAY", "ARRIVED", "DONE", "ABORTED", "UNKNOWN"]),
        handlingUnitStateTimestamp: dbModels.literal('CURRENT_TIMESTAMP'),
        handlingUnitStateDeviceId: null,
        lastValidGeoPositionSentAt: dbModels.literal('CURRENT_TIMESTAMP'),
        lastValidGeoPositionLongitude: faker.address.longitude(),
        lastValidGeoPositionLatitude: faker.address.latitude(),
        lastValidGeoPositionRadius: faker.random.arrayElement([100, 600, 1000, 10000]),
        lastValidGeoPositionType: faker.random.arrayElement(["SIGFOX", "GPS"]),
        lastValidGeoPositionSpeed: faker.random.arrayElement([80, 60, 100, 40]),
        lastValidGeoPositionTemperature: faker.random.arrayElement([10, 15, 20, 30, 8, -5, -6, -20]),
        lastValidGpsPositionSentAt: dbModels.literal('CURRENT_TIMESTAMP'),
        lastValidGpsPositionLongitude: faker.address.longitude(),
        lastValidGpsPositionLatitude: faker.address.latitude(),
        etaArrival: dbModels.literal('CURRENT_TIMESTAMP'),
        etaArrivalGeoPositionType: faker.random.arrayElement(["SIGFOX", "GPS"]),
        etaArrivalLastValidGpsPosition: dbModels.literal('CURRENT_TIMESTAMP'),
        isLate: faker.random.arrayElement([0, 1]),
        vibrationCount: faker.random.arrayElement([0, 10, 15, 20, 30, 8]),
        shippingFromGeofenceId: geofenceFrom.id,
        shippingToGeofenceId: geofenceTo.id,
        guid: faker.random.uuid(),
        clientId: mandant,
        subclientId: mandant,
        createdAt: dbModels.literal('CURRENT_TIMESTAMP'),
        createdFrom: "fake",
        updatedAt: dbModels.literal('CURRENT_TIMESTAMP'),
        updatedFrom: "fake",
        deleted: 0
      };
    };

    dbModels.HandlingUnitsActualData.create(new handlingUnitsActualData()).then(function (newHandlingUnitsActualData, created) {
      if (!newHandlingUnitsActualData) {
        console.log("newHandlingUnitsActualData fail");
      }

      if (newHandlingUnitsActualData) {
        callback(newHandlingUnitsActualData);
      }
    }).catch(error => {
      console.log("newHandlingUnitsActualData", error);
      callback(false);
    });
  }

  function createHandlingUnitsStateHistory(dbModels, mandant, delivery, handlingUnit, callback) {
    let handlingUnitsStateHistory = function () {
      const faker = require('faker');

      faker.locale = "de";
      return {
        handlingUnitId: handlingUnit.id,
        savedAt: dbModels.literal('CURRENT_TIMESTAMP'),
        scanAt: dbModels.literal('CURRENT_TIMESTAMP'),
        scanVibrationCount: faker.random.arrayElement([0, 10, 15, 20, 30, 8]),
        onTheWayAt: dbModels.literal('CURRENT_TIMESTAMP'),
        onTheWayVibrationCount: faker.random.arrayElement([0, 10, 15, 20, 30, 8]),
        onTheWayMessageIdentity: null,
        arrivedAt: dbModels.literal('CURRENT_TIMESTAMP'),
        arrivedVibrationCount: faker.random.arrayElement([0, 10, 15, 20, 30, 8]),
        arrivedMessageIdentity: null,
        doneAt: dbModels.literal('CURRENT_TIMESTAMP'),
        unknownAt: dbModels.literal('CURRENT_TIMESTAMP'),
        transitTimeSeconds: faker.random.arrayElement([0, 10, 15, 20, 30, 8]),
        guid: faker.random.uuid(),
        clientId: mandant,
        subclientId: mandant,
        createdAt: dbModels.literal('CURRENT_TIMESTAMP'),
        createdFrom: "fake",
        updatedAt: dbModels.literal('CURRENT_TIMESTAMP'),
        updatedFrom: "fake",
        deleted: 0
      };
    };

    dbModels.HandlingUnitsStateHistory.create(new handlingUnitsStateHistory()).then(function (newHandlingUnitsStateHistory, created) {
      if (!newHandlingUnitsStateHistory) {
        console.log("newHandlingUnitsStateHistory fail");
      }

      if (newHandlingUnitsStateHistory) {
        callback(newHandlingUnitsStateHistory);
      }
    }).catch(error => {
      console.log("newHandlingUnitsStateHistory", error);
      callback(false);
    });
  }

  function setItemsToHandlingUnit(dbModels, mandant, handlingUnit, items, callback) {
    if (items.length !== 0) {
      createItemsToHandlingUnits(dbModels, mandant, items[0], handlingUnit, function () {
        items.shift();

        if (items.length === 0) {
          callback();
        } else {
          setItemsToHandlingUnit(dbModels, mandant, handlingUnit, items, callback);
        }
      });
    } else {
      callback();
    }
  }

  function createItemHandlingUnits(count, dbModels, mandant, delivery, item, geofenceFrom, geofenceTo, callback) {
    count = count - 1;
    createHandlingUnit(dbModels, mandant, delivery, function (newHandlingUnit) {
      setItemsToHandlingUnit(dbModels, mandant, newHandlingUnit, [item], function () {
        createDeliveriesToAssignedDevices(dbModels, mandant, delivery, newHandlingUnit, function () {
          createHandlingUnitsActualData(dbModels, mandant, delivery, newHandlingUnit, geofenceFrom, geofenceTo, function () {
            createHandlingUnitsStateHistory(dbModels, mandant, delivery, newHandlingUnit, function () {
              if (count > 0) {
                createItemHandlingUnits(count, dbModels, mandant, delivery, geofenceFrom, geofenceTo, callback);
              } else {
                callback();
              }
            });
          });
        });
      });
    });
  }

  function createItemsWithHandlingUnits(count, dbModels, mandant, delivery, geofenceFrom, geofenceTo, callback) {
    count = count - 1;
    createDeliveryItem(dbModels, mandant, delivery, function (newItem) {
      const faker = require('faker');

      let handlingUnitsCount = faker.random.arrayElement([2, 3, 4, 5]);
      createItemHandlingUnits(handlingUnitsCount, dbModels, mandant, delivery, newItem, geofenceFrom, geofenceTo, function () {
        if (count > 0) {
          createItemsWithHandlingUnits(count, dbModels, mandant, delivery, geofenceFrom, geofenceTo, callback);
        } else {
          callback();
        }
      });
    });
  }

  function createAddresses(modelsDb, mandant, callback) {
    createAddress(modelsDb, mandant, function (addressFromReturn) {
      if (!addressFromReturn) {
        return false;
      }

      let addressFrom = addressFromReturn;
      createGeofences(modelsDb, mandant, addressFrom, function (geofenceFromReturn) {
        if (!geofenceFromReturn) {
          return false;
        }

        let geofenceFrom = geofenceFromReturn;
        createAddress(modelsDb, mandant, function (addressToReturn) {
          if (!addressToReturn) {
            return false;
          }

          let addressTo = addressToReturn;
          createGeofences(modelsDb, mandant, addressTo, function (geofenceToReturn) {
            if (!geofenceToReturn) {
              return false;
            }

            let geofenceTo = geofenceToReturn;
            callback({
              addressFrom: addressFrom,
              geofenceFrom: geofenceFrom,
              addressTo: addressTo,
              geofenceTo: geofenceTo
            });
          });
        });
      });
    });
  }

  function createDeliveryItems(count, models, mandant, delivery, items, callback) {
    count = count - 1;
    createDeliveryItem(models, mandant, delivery, function (newItem) {
      items.push(newItem);

      if (count > 0) {
        createDeliveryItems(count, models, mandant, delivery, items, callback);
      } else {
        callback(items);
      }
    });
  }

  function destroyFakeDate(cleanTables, callback) {
    cleanTables[0].destroy({
      where: {
        createdFrom: "fake"
      }
    }).then(function () {
      cleanTables.shift();

      if (cleanTables.length === 0) {
        callback();
      } else {
        destroyFakeDate(cleanTables, callback);
      }
    }).catch(error => {
      console.log("destroyFakeDate", error);
      return false;
    });
  }

  function updateFakeDate(cleanTables, callback) {
    cleanTables[0].update({
      deleted: 1
    }, {
      where: {
        createdFrom: "fake"
      }
    }).then(function () {
      cleanTables.shift();

      if (cleanTables.length === 0) {
        callback();
      } else {
        updateFakeDate(cleanTables, callback);
      }
    }).catch(error => {
      console.log("destroyFakeDate", error);
      return false;
    });
  }

  function cleanAzureSearch(modelsDb, callback) {
    let cleanTables = [modelsDb.ItemsToHandlingUnits, modelsDb.Items, modelsDb.HandlingUnitsActualData, modelsDb.HandlingUnitsStateHistory, modelsDb.DeliveriesToAssignedDevices, modelsDb.HandlingUnits, modelsDb.DeliveriesStateHistory, modelsDb.DeliveriesActualData, modelsDb.Deliveries, modelsDb.Orders, modelsDb.Transports, modelsDb.Contacts, modelsDb.Geofences, modelsDb.Addresses];
    destroyFakeDate(cleanTables, callback);
  }

  function createNewAzureSearch(modelsDb, count, callback) {
    console.log("createNewAzureSearch", count);
    count = count - 1;

    const faker = require('faker');

    faker.locale = "de"; //let mandant = faker.random.arrayElement( [1, 2, 3, 4, 5, 6, 7, 8, 9] );

    let mandant = 2;
    let geofenceTo, geofenceFrom, addressFrom, addressTo, transport, contact, delivery, deliveryActualData, deliveriesToAssignedDevices, order, deliveriesStateHistory;
    createAddresses(modelsDb, mandant, function (response) {
      if (response) {
        geofenceTo = response.geofenceTo;
        geofenceFrom = response.geofenceFrom;
        addressFrom = response.addressFrom;
        addressTo = response.addressTo;
        createDeliveryTransport(modelsDb, mandant, function (newTransport) {
          if (!newTransport) {
            return false;
          }

          transport = newTransport;
          createContact(modelsDb, mandant, function (newContact) {
            if (!newContact) {
              return false;
            }

            contact = newContact;
            createOrder(modelsDb, mandant, function (newOrder) {
              if (!newOrder) {
                return false;
              }

              order = newOrder;
              createDelivers(modelsDb, mandant, addressTo, addressFrom, contact, transport, faker.random.number(), order, function (newDelivery) {
                if (!newDelivery) {
                  return false;
                }

                delivery = newDelivery;
                createDeliveryActualData(modelsDb, mandant, delivery, geofenceFrom, geofenceTo, function (newDeliveryActualData) {
                  if (!newDeliveryActualData) {
                    return false;
                  }

                  deliveryActualData = newDeliveryActualData;
                  createDeliveriesStateHistory(modelsDb, mandant, delivery, function (newDeliveriesStateHistory) {
                    if (!newDeliveriesStateHistory) {
                      return false;
                    }

                    deliveriesStateHistory = newDeliveriesStateHistory;
                    console.log("new geofenceFrom", geofenceFrom);
                    console.log("new geofenceTo", geofenceTo);
                    createItemsWithHandlingUnits(faker.random.arrayElement([2, 3, 4]), modelsDb, mandant, delivery, geofenceFrom, geofenceTo, function () {
                      if (count > 0) {
                        createNewAzureSearch(modelsDb, count, callback);
                      } else {
                        callback();
                      }
                    });
                  });
                });
              });
            });
          });
        });
      } else {
        return false;
      }
    });
  }

  app.get("/bpwService/admin/CreateTestDataCargoDevShare/:count", function (req, res, next) {
    const count = req.params.count;

    try {
      const breezeSequelize = require("breeze-sequelize");

      const sequelizeManager = breezeSequelize.SequelizeManager;
      /*let connectionObjectDb = {
        host: "innodev01.database.windows.net",
        user: "muellerg",
        password: "DGFxKlCDXyNmcDG1Lf0a",
        dbName: "cargodevshared"
      };*/

      let connectionObjectDb = {
        host: "innodev01.database.windows.net",
        user: "delivery-user",
        password: "kX88F83NOUkfvo0z6lEu",
        dbName: "cargoqashared"
      };
      const sm = new sequelizeManager(connectionObjectDb, {
        host: "innodev01.database.windows.net",
        dialect: "mssql",
        schema: 'delivery',
        operatorsAliases: false,
        port: 1433,
        // or 5432 (for postgres)
        pool: {
          max: 5,
          min: 0,
          acquire: 30000,
          idle: 10000
        },
        dialectOptions: {
          encrypt: true
        }
      });
      sm.authenticate().then(() => {
        console.log("db init successful 2");
        sequelizeReturn = sm.sequelize;

        let modelsDb = require("./../ModelDistCargoDevShare")(sequelizeReturn);

        cleanAzureSearch(modelsDb, function () {
          try {
            createNewAzureSearch(modelsDb, count, function () {
              res.send(count + " Items generated successfully!");
            });
          } catch (e) {
            console.log(e);
          }
        });
      }).catch(err => {
        console.error("Unable to connect to the database: err ", err);
      });
    } catch (e) {
      console.log(e);
    }
  });
  app.get("/bpwService/admin/DeleteTestDataCargoDevShare", function (req, res, next) {
    try {
      const breezeSequelize = require("breeze-sequelize");

      const sequelizeManager = breezeSequelize.SequelizeManager;
      let connectionObjectDb = {
        host: "innodev01.database.windows.net",
        user: "delivery-user",
        password: "kX88F83NOUkfvo0z6lEu",
        dbName: "cargoqashared"
      };
      const sm = new sequelizeManager(connectionObjectDb, {
        host: "innodev01.database.windows.net",
        dialect: "mssql",
        schema: 'delivery',
        operatorsAliases: false,
        port: 1433,
        // or 5432 (for postgres)
        pool: {
          max: 5,
          min: 0,
          acquire: 30000,
          idle: 10000
        },
        dialectOptions: {
          encrypt: true
        }
      });
      sm.authenticate().then(() => {
        console.log("db init successful 2");
        sequelizeReturn = sm.sequelize;

        let modelsDb = require("./../ModelDistCargoDevShare")(sequelizeReturn);

        cleanAzureSearch(modelsDb, function () {
          res.send("Delete successfully!");
        });
      }).catch(err => {
        console.error("111 Unable to connect to the database: err ", err);
      });
    } catch (e) {
      console.log(e);
    }
  });
};