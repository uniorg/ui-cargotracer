"use strict";

// Models
module.exports = function (models, sequelize, callback) {
  // Sync Database
  sequelize.sync().then(function () {
    console.log("Nice! Database looks fine", models.user);
    callback();
  }).catch(function (err) {
    console.log(err, "Something went wrong with the Database Update!");
    callback();
  });
};